Grid = require '../core/Grid.coffee'

fk = require 'fieldkit'
Vec2 = fk.math.Vec2

class Renderer
	constructor: ( container ) ->
		@width = Math.max window.innerWidth, 512
		
		@canvas = el 'canvas'
		@canvas.width = @width
		@canvas.height = @width

		@ctx = @canvas.getContext '2d'

		container.appendChild @canvas

		@grid = new Grid 16, new Vec2( @width, @width ), true

		@grid.drawBuffer "#999", 1.5

	render: ->
		@ctx.clearRect 0, 0, @width, @width
		
		# draw big grid
		@ctx.drawImage @grid.buffer, 0, 0

		# draw active cells
		@ctx.fillStyle = "#AAA"
		@ctx.beginPath()
		@grid.drawMarkedCellPaths @ctx
		@ctx.fill()

	resize: () ->
		@width = Math.max window.innerWidth, 512
		#@height = Math.max window.innerHeight, 540
		@canvas.width = @width
		@canvas.height = @width
		@grid.setSize @width, @width
		@grid.drawBuffer "#999", 1.5

module.exports = Renderer;