fk = require 'fieldkit'
Vec2 = fk.math.Vec2

Rect2 = require './Rect2.coffee'

# 100x100 Grid class for character generator
class Grid
	constructor: ( @res, @size, createBuffer=false ) ->
		@createCells()
		@step = new Vec2 @size.x / @res, @size.y / @res
		@buffer = null

		if createBuffer
			@buffer = el 'canvas'
			@bctx = @buffer.getContext '2d'
			@buffer.width = @size.x
			@buffer.height = @size.y

	setSize: ( w, h ) ->
		@size.set2 w, h
		@step = new Vec2 @size.x / @res, @size.y / @res
		if @buffer != null
			@buffer.width = @size.x
			@buffer.height = @size.y

	createCells: () ->
		if @cells
			@cells = null

		@cells = for x in [1..@res]
			for y in [1..@res]
				false

	drawBuffer: ( col="#666", lineWidth=.5 ) ->
		if @buffer == null
			return

		@bctx.clearRect 0, 0, @buffer.width, @buffer.height
		@bctx.strokeStyle = col
		@bctx.lineWidth = lineWidth
		@bctx.beginPath()
		@drawPaths @bctx
		@bctx.stroke()

	drawPaths: ( ctx, offset={ x: 0, y: 0 }, scale=1 ) ->
		x1 = offset.x
		x2 = offset.x + @size.x * scale
		y1 = offset.y
		y2 = offset.y + @size.y * scale

		for i in [0..@res]
			ctx.moveTo x1, y1 + @step.y * i * scale
			ctx.lineTo x2, y1 + @step.y * i * scale
			ctx.moveTo x1 + @step.x * i * scale, y1
			ctx.lineTo x1 + @step.x * i * scale, y2

	# recives a coord in 100x100 [-50..50] and returns intersection cell rect if any, or null
	getIntersectionCell: ( x=0, y=0, mark=false ) ->
	
		if x < 0 or x > @size.x
			return null
		if y < 0 or y > @size.y
			return null

		i = Math.floor ( x / @size.x ) * @res
		j = Math.floor ( y / @size.y ) * @res

		if mark
			@cells[j][i] = true

		return @getCellRect j, i

	getCellRect: ( i=0, j=0 ) ->
		new Rect2 @step.x * j, @step.y * i, @step.x, @step.y

	getNewDensityGrid: ( factor=2 ) ->
		return new Grid Math.floor( @res * factor ), @size.clone()

	clone: () ->
		g = @getNewDensityGrid 1

		for x in [0...@res]
			for y in [0...@res]
				g.cells[x][y] = @cells[x][y]

		return g

	clearCells: () ->
		for x in [0...@res]
			for y in [0...@res]
				@cells[x][y] = false

	markCell : ( i, j, val=true ) ->
		@cells[i][j] = val

	markRandomCells: ( seed=Date.now(), threshold=.5 ) ->
		Random = new fk.math.Random seed
		for x in [0...@res]
			for y in [0...@res]
				@cells[x][y] = Random.random() < threshold

	drawMarkedCellPaths: ( ctx ) ->
		for i in [0...@res]
			for j in [0...@res]
				if @cells[i][j]
					r = @getCellRect j, i
					ctx.rect r.x, r.y, r.width, r.height

	getNeighbours: ( i, j ) ->
		n = [ null, null, null, null, null, null, null, null ]
		
		if i > 0
			n[0] = @cells[i-1][j]
			if j < @res-1
				n[1] = @cells[i-1][j+1]

		if j < @res-1
			n[2] = @cells[i][j+1]
			if i < @res-1
				n[3] = @cells[i+1][j+1]

		if i < @res-1
			n[4] = @cells[i+1][j] 
			if j > 0
				n[5] = @cells[i+1][j-1] 

		if j > 0
			n[6] = @cells[i][j-1] 
			if i > 0
				n[7] = @cells[i-1][j-1]

		return n

	getDifferentCellCount: ( g ) ->
		if g.res != @res
			return -1

		d = 0
		for i in [0...@res]
			for j in [0...@res]
				if @cells[i][j] != g.cells[i][j]
					d++

		return d

module.exports = Grid;

