Renderer = require '../gfx/Renderer.coffee'

fk = require 'fieldkit'
Vec2 = fk.math.Vec2

noisejs = require 'noisejs'

GameOfLife = require '../game/GameOfLife.coffee'

class App
	constructor: ( @container ) ->
		@renderer = new Renderer @container
		@time = 0
		@oTime = Date.now()

		window.addEventListener 'resize', ( e ) =>
			@renderer.resize()
		, false

		@noise = new noisejs.Noise Math.random()

		@settings = 
			res: @renderer.grid.res
			speed: 1
			noise: "Game Of Life"

		changeRes = =>
			grid = @renderer.grid
			grid.res = @settings.res

			grid.setSize grid.size.x, grid.size.y
			grid.createCells()
			grid.drawBuffer "#999", 1.5

			@game = new GameOfLife @renderer.grid

		@gui = new dat.GUI()
		@gui.add( @settings, "res", 8, 128 ).step(1).onChange( changeRes )
		@gui.add( @settings, "speed", 0.1, 1 )
		@gui.add( @settings, "noise", [ "simplex", "perlin", "Game Of Life" ] )

		@game = new GameOfLife @renderer.grid

	update: () ->
		t = Date.now()
		time = ( t - @oTime ) * .001
		dt = time - @time
		@time = time

		res = @renderer.grid.res

		if @settings.noise == "Game Of Life"
			@game.update()
			return

		for x in [0...res]
			for y in [0...res]
				if @settings.noise == "simplex"
					value = @noise.simplex3 x, y, t * .001 * @settings.speed
				else
					value = @noise.perlin3 x, y, t * .001 * @settings.speed
				@renderer.grid.markCell x, y, value > 0
	
	render: () ->
		@renderer.render()

module.exports = App;