App = require './apps/App.coffee'
#TWEEN = require 'tween.js'

#navigator.getUserMedia  = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia

Main = new ->
	initialized = false
	app = null

	init = () ->
		if !initialized
			initialized = true
			
			app = new App $( 'container' )
			animate()
	
	animate = () ->
		requestAnimationFrame animate
		#TWEEN.update()
		app.update()
		app.render()
		

	# --- HELPERS ---------

	isMobile = () ->
		if( navigator.userAgent.match(/Android/i) or navigator.userAgent.match(/webOS/i) or navigator.userAgent.match(/iPhone/i) or navigator.userAgent.match(/iPad/i) or navigator.userAgent.match(/iPod/i) or navigator.userAgent.match(/BlackBerry/i) or navigator.userAgent.match(/Windows Phone/i) )
			return true

		else
			return false

	css3D = () ->
		e = el 'p' 
		has3d = false
		transforms = {
			'webkitTransform':'-webkit-transform',
			'OTransform':'-o-transform',
			'msTransform':'-ms-transform',
			'MozTransform':'-moz-transform',
			'transform':'transform'
		}

		# Add it to the body to get the computed style.
		document.body.insertBefore e, null

		for t,v of transforms
		    if ( e.style[t] != undefined )
		        e.style[t] = "translate3d(1px,1px,1px)"
		        has3d = window.getComputedStyle( e ).getPropertyValue( transforms[t] )

		document.body.removeChild e

		return ( has3d != undefined && has3d.length > 0 && has3d != "none" )

	setTransform = ( e, t, p = [ 'webkit', 'Moz', 'ms' ] ) ->
		for k,v of p
			e.style[ p[k] + 'Transform' ] = t
		e.style.transform = t

	return {
		init: init
		getContents: () ->
			return contents
		isMobile: isMobile
		css3D: css3D
		setTransform: setTransform
	}

# globals
globals =

	$ : ( id ) ->
		return document.getElementById id

	el : ( type, cls ) ->
		e = document.createElement type
		if ( cls ) 
			e.className = cls;
		return e

	Main: Main

for k,v of globals
	window[k] = v

#launch
if window.addEventListener
	window.addEventListener 'load', ( e ) ->
		Main.init()
	, false
else if window.attachEvent
	window.attachEvent 'load', ( e ) ->
		Main.init()
	, false