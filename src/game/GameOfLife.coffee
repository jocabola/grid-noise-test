# Conway's game of Life
# requires Grid class

class GameOfLife
	constructor: ( @grid ) ->
		@updateStep = 8
		@step = 0
		@buffer = [ 0, 1, 2, 0 ]
		@startTime = Date.now() * .001
	update: () ->
		@step = ( @step + 1 ) % ( @updateStep )

		if @step > 0
			return

		# create a copy
		g = @grid.clone()

		for i in [0...g.res]
			for j in [0...g.res]
				n = g.getNeighbours i, j
				alive = 0
				for k in [0..7]
					if n[k] == true
						alive++
				
				if g.cells[i][j]
					# is currently alive
					if alive < 2 or alive > 3
						# must die
						@grid.cells[i][j] = false

				else
					# is currently dead
					if alive == 3
						# new cell is born
						@grid.cells[i][j] = true

		d = @grid.getDifferentCellCount( g )
		@buffer[0] = @buffer[1]
		@buffer[1] = @buffer[2]
		@buffer[2] = d

		if @buffer[0] == @buffer[1] or @buffer[0] == @buffer[2]
			@buffer[3]++
		else
			@buffer[3] = 0

		t = Date.now() * .001
		
		if d < 2 or @buffer[3] > 5 or t - @startTime > 16
			@restart()

	restart: () ->
		@startTime = Date.now() * .001
		@grid.markRandomCells()

	render: ( ctx, debug=false ) ->
		if !debug
			return

		ctx.fillStyle = "rgba( 255, 255, 255, .2 )"
		ctx.beginPath()
		@grid.drawMarkedCellPaths ctx
		ctx.fill()

module.exports = GameOfLife;