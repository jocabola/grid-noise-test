(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
/*!
 * The buffer module from node.js, for the browser.
 *
 * @author   Feross Aboukhadijeh <feross@feross.org> <http://feross.org>
 * @license  MIT
 */

var base64 = require('base64-js')
var ieee754 = require('ieee754')
var isArray = require('is-array')

exports.Buffer = Buffer
exports.SlowBuffer = Buffer
exports.INSPECT_MAX_BYTES = 50
Buffer.poolSize = 8192 // not used by this implementation

var kMaxLength = 0x3fffffff

/**
 * If `Buffer.TYPED_ARRAY_SUPPORT`:
 *   === true    Use Uint8Array implementation (fastest)
 *   === false   Use Object implementation (most compatible, even IE6)
 *
 * Browsers that support typed arrays are IE 10+, Firefox 4+, Chrome 7+, Safari 5.1+,
 * Opera 11.6+, iOS 4.2+.
 *
 * Note:
 *
 * - Implementation must support adding new properties to `Uint8Array` instances.
 *   Firefox 4-29 lacked support, fixed in Firefox 30+.
 *   See: https://bugzilla.mozilla.org/show_bug.cgi?id=695438.
 *
 *  - Chrome 9-10 is missing the `TypedArray.prototype.subarray` function.
 *
 *  - IE10 has a broken `TypedArray.prototype.subarray` function which returns arrays of
 *    incorrect length in some situations.
 *
 * We detect these buggy browsers and set `Buffer.TYPED_ARRAY_SUPPORT` to `false` so they will
 * get the Object implementation, which is slower but will work correctly.
 */
Buffer.TYPED_ARRAY_SUPPORT = (function () {
  try {
    var buf = new ArrayBuffer(0)
    var arr = new Uint8Array(buf)
    arr.foo = function () { return 42 }
    return 42 === arr.foo() && // typed array instances can be augmented
        typeof arr.subarray === 'function' && // chrome 9-10 lack `subarray`
        new Uint8Array(1).subarray(1, 1).byteLength === 0 // ie10 has broken `subarray`
  } catch (e) {
    return false
  }
})()

/**
 * Class: Buffer
 * =============
 *
 * The Buffer constructor returns instances of `Uint8Array` that are augmented
 * with function properties for all the node `Buffer` API functions. We use
 * `Uint8Array` so that square bracket notation works as expected -- it returns
 * a single octet.
 *
 * By augmenting the instances, we can avoid modifying the `Uint8Array`
 * prototype.
 */
function Buffer (subject, encoding, noZero) {
  if (!(this instanceof Buffer))
    return new Buffer(subject, encoding, noZero)

  var type = typeof subject

  // Find the length
  var length
  if (type === 'number')
    length = subject > 0 ? subject >>> 0 : 0
  else if (type === 'string') {
    if (encoding === 'base64')
      subject = base64clean(subject)
    length = Buffer.byteLength(subject, encoding)
  } else if (type === 'object' && subject !== null) { // assume object is array-like
    if (subject.type === 'Buffer' && isArray(subject.data))
      subject = subject.data
    length = +subject.length > 0 ? Math.floor(+subject.length) : 0
  } else
    throw new TypeError('must start with number, buffer, array or string')

  if (this.length > kMaxLength)
    throw new RangeError('Attempt to allocate Buffer larger than maximum ' +
      'size: 0x' + kMaxLength.toString(16) + ' bytes')

  var buf
  if (Buffer.TYPED_ARRAY_SUPPORT) {
    // Preferred: Return an augmented `Uint8Array` instance for best performance
    buf = Buffer._augment(new Uint8Array(length))
  } else {
    // Fallback: Return THIS instance of Buffer (created by `new`)
    buf = this
    buf.length = length
    buf._isBuffer = true
  }

  var i
  if (Buffer.TYPED_ARRAY_SUPPORT && typeof subject.byteLength === 'number') {
    // Speed optimization -- use set if we're copying from a typed array
    buf._set(subject)
  } else if (isArrayish(subject)) {
    // Treat array-ish objects as a byte array
    if (Buffer.isBuffer(subject)) {
      for (i = 0; i < length; i++)
        buf[i] = subject.readUInt8(i)
    } else {
      for (i = 0; i < length; i++)
        buf[i] = ((subject[i] % 256) + 256) % 256
    }
  } else if (type === 'string') {
    buf.write(subject, 0, encoding)
  } else if (type === 'number' && !Buffer.TYPED_ARRAY_SUPPORT && !noZero) {
    for (i = 0; i < length; i++) {
      buf[i] = 0
    }
  }

  return buf
}

Buffer.isBuffer = function (b) {
  return !!(b != null && b._isBuffer)
}

Buffer.compare = function (a, b) {
  if (!Buffer.isBuffer(a) || !Buffer.isBuffer(b))
    throw new TypeError('Arguments must be Buffers')

  var x = a.length
  var y = b.length
  for (var i = 0, len = Math.min(x, y); i < len && a[i] === b[i]; i++) {}
  if (i !== len) {
    x = a[i]
    y = b[i]
  }
  if (x < y) return -1
  if (y < x) return 1
  return 0
}

Buffer.isEncoding = function (encoding) {
  switch (String(encoding).toLowerCase()) {
    case 'hex':
    case 'utf8':
    case 'utf-8':
    case 'ascii':
    case 'binary':
    case 'base64':
    case 'raw':
    case 'ucs2':
    case 'ucs-2':
    case 'utf16le':
    case 'utf-16le':
      return true
    default:
      return false
  }
}

Buffer.concat = function (list, totalLength) {
  if (!isArray(list)) throw new TypeError('Usage: Buffer.concat(list[, length])')

  if (list.length === 0) {
    return new Buffer(0)
  } else if (list.length === 1) {
    return list[0]
  }

  var i
  if (totalLength === undefined) {
    totalLength = 0
    for (i = 0; i < list.length; i++) {
      totalLength += list[i].length
    }
  }

  var buf = new Buffer(totalLength)
  var pos = 0
  for (i = 0; i < list.length; i++) {
    var item = list[i]
    item.copy(buf, pos)
    pos += item.length
  }
  return buf
}

Buffer.byteLength = function (str, encoding) {
  var ret
  str = str + ''
  switch (encoding || 'utf8') {
    case 'ascii':
    case 'binary':
    case 'raw':
      ret = str.length
      break
    case 'ucs2':
    case 'ucs-2':
    case 'utf16le':
    case 'utf-16le':
      ret = str.length * 2
      break
    case 'hex':
      ret = str.length >>> 1
      break
    case 'utf8':
    case 'utf-8':
      ret = utf8ToBytes(str).length
      break
    case 'base64':
      ret = base64ToBytes(str).length
      break
    default:
      ret = str.length
  }
  return ret
}

// pre-set for values that may exist in the future
Buffer.prototype.length = undefined
Buffer.prototype.parent = undefined

// toString(encoding, start=0, end=buffer.length)
Buffer.prototype.toString = function (encoding, start, end) {
  var loweredCase = false

  start = start >>> 0
  end = end === undefined || end === Infinity ? this.length : end >>> 0

  if (!encoding) encoding = 'utf8'
  if (start < 0) start = 0
  if (end > this.length) end = this.length
  if (end <= start) return ''

  while (true) {
    switch (encoding) {
      case 'hex':
        return hexSlice(this, start, end)

      case 'utf8':
      case 'utf-8':
        return utf8Slice(this, start, end)

      case 'ascii':
        return asciiSlice(this, start, end)

      case 'binary':
        return binarySlice(this, start, end)

      case 'base64':
        return base64Slice(this, start, end)

      case 'ucs2':
      case 'ucs-2':
      case 'utf16le':
      case 'utf-16le':
        return utf16leSlice(this, start, end)

      default:
        if (loweredCase)
          throw new TypeError('Unknown encoding: ' + encoding)
        encoding = (encoding + '').toLowerCase()
        loweredCase = true
    }
  }
}

Buffer.prototype.equals = function (b) {
  if(!Buffer.isBuffer(b)) throw new TypeError('Argument must be a Buffer')
  return Buffer.compare(this, b) === 0
}

Buffer.prototype.inspect = function () {
  var str = ''
  var max = exports.INSPECT_MAX_BYTES
  if (this.length > 0) {
    str = this.toString('hex', 0, max).match(/.{2}/g).join(' ')
    if (this.length > max)
      str += ' ... '
  }
  return '<Buffer ' + str + '>'
}

Buffer.prototype.compare = function (b) {
  if (!Buffer.isBuffer(b)) throw new TypeError('Argument must be a Buffer')
  return Buffer.compare(this, b)
}

// `get` will be removed in Node 0.13+
Buffer.prototype.get = function (offset) {
  console.log('.get() is deprecated. Access using array indexes instead.')
  return this.readUInt8(offset)
}

// `set` will be removed in Node 0.13+
Buffer.prototype.set = function (v, offset) {
  console.log('.set() is deprecated. Access using array indexes instead.')
  return this.writeUInt8(v, offset)
}

function hexWrite (buf, string, offset, length) {
  offset = Number(offset) || 0
  var remaining = buf.length - offset
  if (!length) {
    length = remaining
  } else {
    length = Number(length)
    if (length > remaining) {
      length = remaining
    }
  }

  // must be an even number of digits
  var strLen = string.length
  if (strLen % 2 !== 0) throw new Error('Invalid hex string')

  if (length > strLen / 2) {
    length = strLen / 2
  }
  for (var i = 0; i < length; i++) {
    var byte = parseInt(string.substr(i * 2, 2), 16)
    if (isNaN(byte)) throw new Error('Invalid hex string')
    buf[offset + i] = byte
  }
  return i
}

function utf8Write (buf, string, offset, length) {
  var charsWritten = blitBuffer(utf8ToBytes(string), buf, offset, length)
  return charsWritten
}

function asciiWrite (buf, string, offset, length) {
  var charsWritten = blitBuffer(asciiToBytes(string), buf, offset, length)
  return charsWritten
}

function binaryWrite (buf, string, offset, length) {
  return asciiWrite(buf, string, offset, length)
}

function base64Write (buf, string, offset, length) {
  var charsWritten = blitBuffer(base64ToBytes(string), buf, offset, length)
  return charsWritten
}

function utf16leWrite (buf, string, offset, length) {
  var charsWritten = blitBuffer(utf16leToBytes(string), buf, offset, length)
  return charsWritten
}

Buffer.prototype.write = function (string, offset, length, encoding) {
  // Support both (string, offset, length, encoding)
  // and the legacy (string, encoding, offset, length)
  if (isFinite(offset)) {
    if (!isFinite(length)) {
      encoding = length
      length = undefined
    }
  } else {  // legacy
    var swap = encoding
    encoding = offset
    offset = length
    length = swap
  }

  offset = Number(offset) || 0
  var remaining = this.length - offset
  if (!length) {
    length = remaining
  } else {
    length = Number(length)
    if (length > remaining) {
      length = remaining
    }
  }
  encoding = String(encoding || 'utf8').toLowerCase()

  var ret
  switch (encoding) {
    case 'hex':
      ret = hexWrite(this, string, offset, length)
      break
    case 'utf8':
    case 'utf-8':
      ret = utf8Write(this, string, offset, length)
      break
    case 'ascii':
      ret = asciiWrite(this, string, offset, length)
      break
    case 'binary':
      ret = binaryWrite(this, string, offset, length)
      break
    case 'base64':
      ret = base64Write(this, string, offset, length)
      break
    case 'ucs2':
    case 'ucs-2':
    case 'utf16le':
    case 'utf-16le':
      ret = utf16leWrite(this, string, offset, length)
      break
    default:
      throw new TypeError('Unknown encoding: ' + encoding)
  }
  return ret
}

Buffer.prototype.toJSON = function () {
  return {
    type: 'Buffer',
    data: Array.prototype.slice.call(this._arr || this, 0)
  }
}

function base64Slice (buf, start, end) {
  if (start === 0 && end === buf.length) {
    return base64.fromByteArray(buf)
  } else {
    return base64.fromByteArray(buf.slice(start, end))
  }
}

function utf8Slice (buf, start, end) {
  var res = ''
  var tmp = ''
  end = Math.min(buf.length, end)

  for (var i = start; i < end; i++) {
    if (buf[i] <= 0x7F) {
      res += decodeUtf8Char(tmp) + String.fromCharCode(buf[i])
      tmp = ''
    } else {
      tmp += '%' + buf[i].toString(16)
    }
  }

  return res + decodeUtf8Char(tmp)
}

function asciiSlice (buf, start, end) {
  var ret = ''
  end = Math.min(buf.length, end)

  for (var i = start; i < end; i++) {
    ret += String.fromCharCode(buf[i])
  }
  return ret
}

function binarySlice (buf, start, end) {
  return asciiSlice(buf, start, end)
}

function hexSlice (buf, start, end) {
  var len = buf.length

  if (!start || start < 0) start = 0
  if (!end || end < 0 || end > len) end = len

  var out = ''
  for (var i = start; i < end; i++) {
    out += toHex(buf[i])
  }
  return out
}

function utf16leSlice (buf, start, end) {
  var bytes = buf.slice(start, end)
  var res = ''
  for (var i = 0; i < bytes.length; i += 2) {
    res += String.fromCharCode(bytes[i] + bytes[i + 1] * 256)
  }
  return res
}

Buffer.prototype.slice = function (start, end) {
  var len = this.length
  start = ~~start
  end = end === undefined ? len : ~~end

  if (start < 0) {
    start += len;
    if (start < 0)
      start = 0
  } else if (start > len) {
    start = len
  }

  if (end < 0) {
    end += len
    if (end < 0)
      end = 0
  } else if (end > len) {
    end = len
  }

  if (end < start)
    end = start

  if (Buffer.TYPED_ARRAY_SUPPORT) {
    return Buffer._augment(this.subarray(start, end))
  } else {
    var sliceLen = end - start
    var newBuf = new Buffer(sliceLen, undefined, true)
    for (var i = 0; i < sliceLen; i++) {
      newBuf[i] = this[i + start]
    }
    return newBuf
  }
}

/*
 * Need to make sure that buffer isn't trying to write out of bounds.
 */
function checkOffset (offset, ext, length) {
  if ((offset % 1) !== 0 || offset < 0)
    throw new RangeError('offset is not uint')
  if (offset + ext > length)
    throw new RangeError('Trying to access beyond buffer length')
}

Buffer.prototype.readUInt8 = function (offset, noAssert) {
  if (!noAssert)
    checkOffset(offset, 1, this.length)
  return this[offset]
}

Buffer.prototype.readUInt16LE = function (offset, noAssert) {
  if (!noAssert)
    checkOffset(offset, 2, this.length)
  return this[offset] | (this[offset + 1] << 8)
}

Buffer.prototype.readUInt16BE = function (offset, noAssert) {
  if (!noAssert)
    checkOffset(offset, 2, this.length)
  return (this[offset] << 8) | this[offset + 1]
}

Buffer.prototype.readUInt32LE = function (offset, noAssert) {
  if (!noAssert)
    checkOffset(offset, 4, this.length)

  return ((this[offset]) |
      (this[offset + 1] << 8) |
      (this[offset + 2] << 16)) +
      (this[offset + 3] * 0x1000000)
}

Buffer.prototype.readUInt32BE = function (offset, noAssert) {
  if (!noAssert)
    checkOffset(offset, 4, this.length)

  return (this[offset] * 0x1000000) +
      ((this[offset + 1] << 16) |
      (this[offset + 2] << 8) |
      this[offset + 3])
}

Buffer.prototype.readInt8 = function (offset, noAssert) {
  if (!noAssert)
    checkOffset(offset, 1, this.length)
  if (!(this[offset] & 0x80))
    return (this[offset])
  return ((0xff - this[offset] + 1) * -1)
}

Buffer.prototype.readInt16LE = function (offset, noAssert) {
  if (!noAssert)
    checkOffset(offset, 2, this.length)
  var val = this[offset] | (this[offset + 1] << 8)
  return (val & 0x8000) ? val | 0xFFFF0000 : val
}

Buffer.prototype.readInt16BE = function (offset, noAssert) {
  if (!noAssert)
    checkOffset(offset, 2, this.length)
  var val = this[offset + 1] | (this[offset] << 8)
  return (val & 0x8000) ? val | 0xFFFF0000 : val
}

Buffer.prototype.readInt32LE = function (offset, noAssert) {
  if (!noAssert)
    checkOffset(offset, 4, this.length)

  return (this[offset]) |
      (this[offset + 1] << 8) |
      (this[offset + 2] << 16) |
      (this[offset + 3] << 24)
}

Buffer.prototype.readInt32BE = function (offset, noAssert) {
  if (!noAssert)
    checkOffset(offset, 4, this.length)

  return (this[offset] << 24) |
      (this[offset + 1] << 16) |
      (this[offset + 2] << 8) |
      (this[offset + 3])
}

Buffer.prototype.readFloatLE = function (offset, noAssert) {
  if (!noAssert)
    checkOffset(offset, 4, this.length)
  return ieee754.read(this, offset, true, 23, 4)
}

Buffer.prototype.readFloatBE = function (offset, noAssert) {
  if (!noAssert)
    checkOffset(offset, 4, this.length)
  return ieee754.read(this, offset, false, 23, 4)
}

Buffer.prototype.readDoubleLE = function (offset, noAssert) {
  if (!noAssert)
    checkOffset(offset, 8, this.length)
  return ieee754.read(this, offset, true, 52, 8)
}

Buffer.prototype.readDoubleBE = function (offset, noAssert) {
  if (!noAssert)
    checkOffset(offset, 8, this.length)
  return ieee754.read(this, offset, false, 52, 8)
}

function checkInt (buf, value, offset, ext, max, min) {
  if (!Buffer.isBuffer(buf)) throw new TypeError('buffer must be a Buffer instance')
  if (value > max || value < min) throw new TypeError('value is out of bounds')
  if (offset + ext > buf.length) throw new TypeError('index out of range')
}

Buffer.prototype.writeUInt8 = function (value, offset, noAssert) {
  value = +value
  offset = offset >>> 0
  if (!noAssert)
    checkInt(this, value, offset, 1, 0xff, 0)
  if (!Buffer.TYPED_ARRAY_SUPPORT) value = Math.floor(value)
  this[offset] = value
  return offset + 1
}

function objectWriteUInt16 (buf, value, offset, littleEndian) {
  if (value < 0) value = 0xffff + value + 1
  for (var i = 0, j = Math.min(buf.length - offset, 2); i < j; i++) {
    buf[offset + i] = (value & (0xff << (8 * (littleEndian ? i : 1 - i)))) >>>
      (littleEndian ? i : 1 - i) * 8
  }
}

Buffer.prototype.writeUInt16LE = function (value, offset, noAssert) {
  value = +value
  offset = offset >>> 0
  if (!noAssert)
    checkInt(this, value, offset, 2, 0xffff, 0)
  if (Buffer.TYPED_ARRAY_SUPPORT) {
    this[offset] = value
    this[offset + 1] = (value >>> 8)
  } else objectWriteUInt16(this, value, offset, true)
  return offset + 2
}

Buffer.prototype.writeUInt16BE = function (value, offset, noAssert) {
  value = +value
  offset = offset >>> 0
  if (!noAssert)
    checkInt(this, value, offset, 2, 0xffff, 0)
  if (Buffer.TYPED_ARRAY_SUPPORT) {
    this[offset] = (value >>> 8)
    this[offset + 1] = value
  } else objectWriteUInt16(this, value, offset, false)
  return offset + 2
}

function objectWriteUInt32 (buf, value, offset, littleEndian) {
  if (value < 0) value = 0xffffffff + value + 1
  for (var i = 0, j = Math.min(buf.length - offset, 4); i < j; i++) {
    buf[offset + i] = (value >>> (littleEndian ? i : 3 - i) * 8) & 0xff
  }
}

Buffer.prototype.writeUInt32LE = function (value, offset, noAssert) {
  value = +value
  offset = offset >>> 0
  if (!noAssert)
    checkInt(this, value, offset, 4, 0xffffffff, 0)
  if (Buffer.TYPED_ARRAY_SUPPORT) {
    this[offset + 3] = (value >>> 24)
    this[offset + 2] = (value >>> 16)
    this[offset + 1] = (value >>> 8)
    this[offset] = value
  } else objectWriteUInt32(this, value, offset, true)
  return offset + 4
}

Buffer.prototype.writeUInt32BE = function (value, offset, noAssert) {
  value = +value
  offset = offset >>> 0
  if (!noAssert)
    checkInt(this, value, offset, 4, 0xffffffff, 0)
  if (Buffer.TYPED_ARRAY_SUPPORT) {
    this[offset] = (value >>> 24)
    this[offset + 1] = (value >>> 16)
    this[offset + 2] = (value >>> 8)
    this[offset + 3] = value
  } else objectWriteUInt32(this, value, offset, false)
  return offset + 4
}

Buffer.prototype.writeInt8 = function (value, offset, noAssert) {
  value = +value
  offset = offset >>> 0
  if (!noAssert)
    checkInt(this, value, offset, 1, 0x7f, -0x80)
  if (!Buffer.TYPED_ARRAY_SUPPORT) value = Math.floor(value)
  if (value < 0) value = 0xff + value + 1
  this[offset] = value
  return offset + 1
}

Buffer.prototype.writeInt16LE = function (value, offset, noAssert) {
  value = +value
  offset = offset >>> 0
  if (!noAssert)
    checkInt(this, value, offset, 2, 0x7fff, -0x8000)
  if (Buffer.TYPED_ARRAY_SUPPORT) {
    this[offset] = value
    this[offset + 1] = (value >>> 8)
  } else objectWriteUInt16(this, value, offset, true)
  return offset + 2
}

Buffer.prototype.writeInt16BE = function (value, offset, noAssert) {
  value = +value
  offset = offset >>> 0
  if (!noAssert)
    checkInt(this, value, offset, 2, 0x7fff, -0x8000)
  if (Buffer.TYPED_ARRAY_SUPPORT) {
    this[offset] = (value >>> 8)
    this[offset + 1] = value
  } else objectWriteUInt16(this, value, offset, false)
  return offset + 2
}

Buffer.prototype.writeInt32LE = function (value, offset, noAssert) {
  value = +value
  offset = offset >>> 0
  if (!noAssert)
    checkInt(this, value, offset, 4, 0x7fffffff, -0x80000000)
  if (Buffer.TYPED_ARRAY_SUPPORT) {
    this[offset] = value
    this[offset + 1] = (value >>> 8)
    this[offset + 2] = (value >>> 16)
    this[offset + 3] = (value >>> 24)
  } else objectWriteUInt32(this, value, offset, true)
  return offset + 4
}

Buffer.prototype.writeInt32BE = function (value, offset, noAssert) {
  value = +value
  offset = offset >>> 0
  if (!noAssert)
    checkInt(this, value, offset, 4, 0x7fffffff, -0x80000000)
  if (value < 0) value = 0xffffffff + value + 1
  if (Buffer.TYPED_ARRAY_SUPPORT) {
    this[offset] = (value >>> 24)
    this[offset + 1] = (value >>> 16)
    this[offset + 2] = (value >>> 8)
    this[offset + 3] = value
  } else objectWriteUInt32(this, value, offset, false)
  return offset + 4
}

function checkIEEE754 (buf, value, offset, ext, max, min) {
  if (value > max || value < min) throw new TypeError('value is out of bounds')
  if (offset + ext > buf.length) throw new TypeError('index out of range')
}

function writeFloat (buf, value, offset, littleEndian, noAssert) {
  if (!noAssert)
    checkIEEE754(buf, value, offset, 4, 3.4028234663852886e+38, -3.4028234663852886e+38)
  ieee754.write(buf, value, offset, littleEndian, 23, 4)
  return offset + 4
}

Buffer.prototype.writeFloatLE = function (value, offset, noAssert) {
  return writeFloat(this, value, offset, true, noAssert)
}

Buffer.prototype.writeFloatBE = function (value, offset, noAssert) {
  return writeFloat(this, value, offset, false, noAssert)
}

function writeDouble (buf, value, offset, littleEndian, noAssert) {
  if (!noAssert)
    checkIEEE754(buf, value, offset, 8, 1.7976931348623157E+308, -1.7976931348623157E+308)
  ieee754.write(buf, value, offset, littleEndian, 52, 8)
  return offset + 8
}

Buffer.prototype.writeDoubleLE = function (value, offset, noAssert) {
  return writeDouble(this, value, offset, true, noAssert)
}

Buffer.prototype.writeDoubleBE = function (value, offset, noAssert) {
  return writeDouble(this, value, offset, false, noAssert)
}

// copy(targetBuffer, targetStart=0, sourceStart=0, sourceEnd=buffer.length)
Buffer.prototype.copy = function (target, target_start, start, end) {
  var source = this

  if (!start) start = 0
  if (!end && end !== 0) end = this.length
  if (!target_start) target_start = 0

  // Copy 0 bytes; we're done
  if (end === start) return
  if (target.length === 0 || source.length === 0) return

  // Fatal error conditions
  if (end < start) throw new TypeError('sourceEnd < sourceStart')
  if (target_start < 0 || target_start >= target.length)
    throw new TypeError('targetStart out of bounds')
  if (start < 0 || start >= source.length) throw new TypeError('sourceStart out of bounds')
  if (end < 0 || end > source.length) throw new TypeError('sourceEnd out of bounds')

  // Are we oob?
  if (end > this.length)
    end = this.length
  if (target.length - target_start < end - start)
    end = target.length - target_start + start

  var len = end - start

  if (len < 1000 || !Buffer.TYPED_ARRAY_SUPPORT) {
    for (var i = 0; i < len; i++) {
      target[i + target_start] = this[i + start]
    }
  } else {
    target._set(this.subarray(start, start + len), target_start)
  }
}

// fill(value, start=0, end=buffer.length)
Buffer.prototype.fill = function (value, start, end) {
  if (!value) value = 0
  if (!start) start = 0
  if (!end) end = this.length

  if (end < start) throw new TypeError('end < start')

  // Fill 0 bytes; we're done
  if (end === start) return
  if (this.length === 0) return

  if (start < 0 || start >= this.length) throw new TypeError('start out of bounds')
  if (end < 0 || end > this.length) throw new TypeError('end out of bounds')

  var i
  if (typeof value === 'number') {
    for (i = start; i < end; i++) {
      this[i] = value
    }
  } else {
    var bytes = utf8ToBytes(value.toString())
    var len = bytes.length
    for (i = start; i < end; i++) {
      this[i] = bytes[i % len]
    }
  }

  return this
}

/**
 * Creates a new `ArrayBuffer` with the *copied* memory of the buffer instance.
 * Added in Node 0.12. Only available in browsers that support ArrayBuffer.
 */
Buffer.prototype.toArrayBuffer = function () {
  if (typeof Uint8Array !== 'undefined') {
    if (Buffer.TYPED_ARRAY_SUPPORT) {
      return (new Buffer(this)).buffer
    } else {
      var buf = new Uint8Array(this.length)
      for (var i = 0, len = buf.length; i < len; i += 1) {
        buf[i] = this[i]
      }
      return buf.buffer
    }
  } else {
    throw new TypeError('Buffer.toArrayBuffer not supported in this browser')
  }
}

// HELPER FUNCTIONS
// ================

var BP = Buffer.prototype

/**
 * Augment a Uint8Array *instance* (not the Uint8Array class!) with Buffer methods
 */
Buffer._augment = function (arr) {
  arr.constructor = Buffer
  arr._isBuffer = true

  // save reference to original Uint8Array get/set methods before overwriting
  arr._get = arr.get
  arr._set = arr.set

  // deprecated, will be removed in node 0.13+
  arr.get = BP.get
  arr.set = BP.set

  arr.write = BP.write
  arr.toString = BP.toString
  arr.toLocaleString = BP.toString
  arr.toJSON = BP.toJSON
  arr.equals = BP.equals
  arr.compare = BP.compare
  arr.copy = BP.copy
  arr.slice = BP.slice
  arr.readUInt8 = BP.readUInt8
  arr.readUInt16LE = BP.readUInt16LE
  arr.readUInt16BE = BP.readUInt16BE
  arr.readUInt32LE = BP.readUInt32LE
  arr.readUInt32BE = BP.readUInt32BE
  arr.readInt8 = BP.readInt8
  arr.readInt16LE = BP.readInt16LE
  arr.readInt16BE = BP.readInt16BE
  arr.readInt32LE = BP.readInt32LE
  arr.readInt32BE = BP.readInt32BE
  arr.readFloatLE = BP.readFloatLE
  arr.readFloatBE = BP.readFloatBE
  arr.readDoubleLE = BP.readDoubleLE
  arr.readDoubleBE = BP.readDoubleBE
  arr.writeUInt8 = BP.writeUInt8
  arr.writeUInt16LE = BP.writeUInt16LE
  arr.writeUInt16BE = BP.writeUInt16BE
  arr.writeUInt32LE = BP.writeUInt32LE
  arr.writeUInt32BE = BP.writeUInt32BE
  arr.writeInt8 = BP.writeInt8
  arr.writeInt16LE = BP.writeInt16LE
  arr.writeInt16BE = BP.writeInt16BE
  arr.writeInt32LE = BP.writeInt32LE
  arr.writeInt32BE = BP.writeInt32BE
  arr.writeFloatLE = BP.writeFloatLE
  arr.writeFloatBE = BP.writeFloatBE
  arr.writeDoubleLE = BP.writeDoubleLE
  arr.writeDoubleBE = BP.writeDoubleBE
  arr.fill = BP.fill
  arr.inspect = BP.inspect
  arr.toArrayBuffer = BP.toArrayBuffer

  return arr
}

var INVALID_BASE64_RE = /[^+\/0-9A-z]/g

function base64clean (str) {
  // Node strips out invalid characters like \n and \t from the string, base64-js does not
  str = stringtrim(str).replace(INVALID_BASE64_RE, '')
  // Node allows for non-padded base64 strings (missing trailing ===), base64-js does not
  while (str.length % 4 !== 0) {
    str = str + '='
  }
  return str
}

function stringtrim (str) {
  if (str.trim) return str.trim()
  return str.replace(/^\s+|\s+$/g, '')
}

function isArrayish (subject) {
  return isArray(subject) || Buffer.isBuffer(subject) ||
      subject && typeof subject === 'object' &&
      typeof subject.length === 'number'
}

function toHex (n) {
  if (n < 16) return '0' + n.toString(16)
  return n.toString(16)
}

function utf8ToBytes (str) {
  var byteArray = []
  for (var i = 0; i < str.length; i++) {
    var b = str.charCodeAt(i)
    if (b <= 0x7F) {
      byteArray.push(b)
    } else {
      var start = i
      if (b >= 0xD800 && b <= 0xDFFF) i++
      var h = encodeURIComponent(str.slice(start, i+1)).substr(1).split('%')
      for (var j = 0; j < h.length; j++) {
        byteArray.push(parseInt(h[j], 16))
      }
    }
  }
  return byteArray
}

function asciiToBytes (str) {
  var byteArray = []
  for (var i = 0; i < str.length; i++) {
    // Node's code seems to be doing this and not & 0x7F..
    byteArray.push(str.charCodeAt(i) & 0xFF)
  }
  return byteArray
}

function utf16leToBytes (str) {
  var c, hi, lo
  var byteArray = []
  for (var i = 0; i < str.length; i++) {
    c = str.charCodeAt(i)
    hi = c >> 8
    lo = c % 256
    byteArray.push(lo)
    byteArray.push(hi)
  }

  return byteArray
}

function base64ToBytes (str) {
  return base64.toByteArray(str)
}

function blitBuffer (src, dst, offset, length) {
  for (var i = 0; i < length; i++) {
    if ((i + offset >= dst.length) || (i >= src.length))
      break
    dst[i + offset] = src[i]
  }
  return i
}

function decodeUtf8Char (str) {
  try {
    return decodeURIComponent(str)
  } catch (err) {
    return String.fromCharCode(0xFFFD) // UTF 8 invalid char
  }
}

},{"base64-js":2,"ieee754":3,"is-array":4}],2:[function(require,module,exports){
var lookup = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/';

;(function (exports) {
	'use strict';

  var Arr = (typeof Uint8Array !== 'undefined')
    ? Uint8Array
    : Array

	var PLUS   = '+'.charCodeAt(0)
	var SLASH  = '/'.charCodeAt(0)
	var NUMBER = '0'.charCodeAt(0)
	var LOWER  = 'a'.charCodeAt(0)
	var UPPER  = 'A'.charCodeAt(0)

	function decode (elt) {
		var code = elt.charCodeAt(0)
		if (code === PLUS)
			return 62 // '+'
		if (code === SLASH)
			return 63 // '/'
		if (code < NUMBER)
			return -1 //no match
		if (code < NUMBER + 10)
			return code - NUMBER + 26 + 26
		if (code < UPPER + 26)
			return code - UPPER
		if (code < LOWER + 26)
			return code - LOWER + 26
	}

	function b64ToByteArray (b64) {
		var i, j, l, tmp, placeHolders, arr

		if (b64.length % 4 > 0) {
			throw new Error('Invalid string. Length must be a multiple of 4')
		}

		// the number of equal signs (place holders)
		// if there are two placeholders, than the two characters before it
		// represent one byte
		// if there is only one, then the three characters before it represent 2 bytes
		// this is just a cheap hack to not do indexOf twice
		var len = b64.length
		placeHolders = '=' === b64.charAt(len - 2) ? 2 : '=' === b64.charAt(len - 1) ? 1 : 0

		// base64 is 4/3 + up to two characters of the original data
		arr = new Arr(b64.length * 3 / 4 - placeHolders)

		// if there are placeholders, only get up to the last complete 4 chars
		l = placeHolders > 0 ? b64.length - 4 : b64.length

		var L = 0

		function push (v) {
			arr[L++] = v
		}

		for (i = 0, j = 0; i < l; i += 4, j += 3) {
			tmp = (decode(b64.charAt(i)) << 18) | (decode(b64.charAt(i + 1)) << 12) | (decode(b64.charAt(i + 2)) << 6) | decode(b64.charAt(i + 3))
			push((tmp & 0xFF0000) >> 16)
			push((tmp & 0xFF00) >> 8)
			push(tmp & 0xFF)
		}

		if (placeHolders === 2) {
			tmp = (decode(b64.charAt(i)) << 2) | (decode(b64.charAt(i + 1)) >> 4)
			push(tmp & 0xFF)
		} else if (placeHolders === 1) {
			tmp = (decode(b64.charAt(i)) << 10) | (decode(b64.charAt(i + 1)) << 4) | (decode(b64.charAt(i + 2)) >> 2)
			push((tmp >> 8) & 0xFF)
			push(tmp & 0xFF)
		}

		return arr
	}

	function uint8ToBase64 (uint8) {
		var i,
			extraBytes = uint8.length % 3, // if we have 1 byte left, pad 2 bytes
			output = "",
			temp, length

		function encode (num) {
			return lookup.charAt(num)
		}

		function tripletToBase64 (num) {
			return encode(num >> 18 & 0x3F) + encode(num >> 12 & 0x3F) + encode(num >> 6 & 0x3F) + encode(num & 0x3F)
		}

		// go through the array every three bytes, we'll deal with trailing stuff later
		for (i = 0, length = uint8.length - extraBytes; i < length; i += 3) {
			temp = (uint8[i] << 16) + (uint8[i + 1] << 8) + (uint8[i + 2])
			output += tripletToBase64(temp)
		}

		// pad the end with zeros, but make sure to not forget the extra bytes
		switch (extraBytes) {
			case 1:
				temp = uint8[uint8.length - 1]
				output += encode(temp >> 2)
				output += encode((temp << 4) & 0x3F)
				output += '=='
				break
			case 2:
				temp = (uint8[uint8.length - 2] << 8) + (uint8[uint8.length - 1])
				output += encode(temp >> 10)
				output += encode((temp >> 4) & 0x3F)
				output += encode((temp << 2) & 0x3F)
				output += '='
				break
		}

		return output
	}

	exports.toByteArray = b64ToByteArray
	exports.fromByteArray = uint8ToBase64
}(typeof exports === 'undefined' ? (this.base64js = {}) : exports))

},{}],3:[function(require,module,exports){
exports.read = function(buffer, offset, isLE, mLen, nBytes) {
  var e, m,
      eLen = nBytes * 8 - mLen - 1,
      eMax = (1 << eLen) - 1,
      eBias = eMax >> 1,
      nBits = -7,
      i = isLE ? (nBytes - 1) : 0,
      d = isLE ? -1 : 1,
      s = buffer[offset + i];

  i += d;

  e = s & ((1 << (-nBits)) - 1);
  s >>= (-nBits);
  nBits += eLen;
  for (; nBits > 0; e = e * 256 + buffer[offset + i], i += d, nBits -= 8);

  m = e & ((1 << (-nBits)) - 1);
  e >>= (-nBits);
  nBits += mLen;
  for (; nBits > 0; m = m * 256 + buffer[offset + i], i += d, nBits -= 8);

  if (e === 0) {
    e = 1 - eBias;
  } else if (e === eMax) {
    return m ? NaN : ((s ? -1 : 1) * Infinity);
  } else {
    m = m + Math.pow(2, mLen);
    e = e - eBias;
  }
  return (s ? -1 : 1) * m * Math.pow(2, e - mLen);
};

exports.write = function(buffer, value, offset, isLE, mLen, nBytes) {
  var e, m, c,
      eLen = nBytes * 8 - mLen - 1,
      eMax = (1 << eLen) - 1,
      eBias = eMax >> 1,
      rt = (mLen === 23 ? Math.pow(2, -24) - Math.pow(2, -77) : 0),
      i = isLE ? 0 : (nBytes - 1),
      d = isLE ? 1 : -1,
      s = value < 0 || (value === 0 && 1 / value < 0) ? 1 : 0;

  value = Math.abs(value);

  if (isNaN(value) || value === Infinity) {
    m = isNaN(value) ? 1 : 0;
    e = eMax;
  } else {
    e = Math.floor(Math.log(value) / Math.LN2);
    if (value * (c = Math.pow(2, -e)) < 1) {
      e--;
      c *= 2;
    }
    if (e + eBias >= 1) {
      value += rt / c;
    } else {
      value += rt * Math.pow(2, 1 - eBias);
    }
    if (value * c >= 2) {
      e++;
      c /= 2;
    }

    if (e + eBias >= eMax) {
      m = 0;
      e = eMax;
    } else if (e + eBias >= 1) {
      m = (value * c - 1) * Math.pow(2, mLen);
      e = e + eBias;
    } else {
      m = value * Math.pow(2, eBias - 1) * Math.pow(2, mLen);
      e = 0;
    }
  }

  for (; mLen >= 8; buffer[offset + i] = m & 0xff, i += d, m /= 256, mLen -= 8);

  e = (e << mLen) | m;
  eLen += mLen;
  for (; eLen > 0; buffer[offset + i] = e & 0xff, i += d, e /= 256, eLen -= 8);

  buffer[offset + i - d] |= s * 128;
};

},{}],4:[function(require,module,exports){

/**
 * isArray
 */

var isArray = Array.isArray;

/**
 * toString
 */

var str = Object.prototype.toString;

/**
 * Whether or not the given `val`
 * is an array.
 *
 * example:
 *
 *        isArray([]);
 *        // > true
 *        isArray(arguments);
 *        // > false
 *        isArray('');
 *        // > false
 *
 * @param {mixed} val
 * @return {bool}
 */

module.exports = isArray || function (val) {
  return !! val && '[object Array]' == str.call(val);
};

},{}],5:[function(require,module,exports){
if (typeof Object.create === 'function') {
  // implementation from standard node.js 'util' module
  module.exports = function inherits(ctor, superCtor) {
    ctor.super_ = superCtor
    ctor.prototype = Object.create(superCtor.prototype, {
      constructor: {
        value: ctor,
        enumerable: false,
        writable: true,
        configurable: true
      }
    });
  };
} else {
  // old school shim for old browsers
  module.exports = function inherits(ctor, superCtor) {
    ctor.super_ = superCtor
    var TempCtor = function () {}
    TempCtor.prototype = superCtor.prototype
    ctor.prototype = new TempCtor()
    ctor.prototype.constructor = ctor
  }
}

},{}],6:[function(require,module,exports){
// shim for using process in browser

var process = module.exports = {};

process.nextTick = (function () {
    var canSetImmediate = typeof window !== 'undefined'
    && window.setImmediate;
    var canMutationObserver = typeof window !== 'undefined'
    && window.MutationObserver;
    var canPost = typeof window !== 'undefined'
    && window.postMessage && window.addEventListener
    ;

    if (canSetImmediate) {
        return function (f) { return window.setImmediate(f) };
    }

    var queue = [];

    if (canMutationObserver) {
        var hiddenDiv = document.createElement("div");
        var observer = new MutationObserver(function () {
            var queueList = queue.slice();
            queue.length = 0;
            queueList.forEach(function (fn) {
                fn();
            });
        });

        observer.observe(hiddenDiv, { attributes: true });

        return function nextTick(fn) {
            if (!queue.length) {
                hiddenDiv.setAttribute('yes', 'no');
            }
            queue.push(fn);
        };
    }

    if (canPost) {
        window.addEventListener('message', function (ev) {
            var source = ev.source;
            if ((source === window || source === null) && ev.data === 'process-tick') {
                ev.stopPropagation();
                if (queue.length > 0) {
                    var fn = queue.shift();
                    fn();
                }
            }
        }, true);

        return function nextTick(fn) {
            queue.push(fn);
            window.postMessage('process-tick', '*');
        };
    }

    return function nextTick(fn) {
        setTimeout(fn, 0);
    };
})();

process.title = 'browser';
process.browser = true;
process.env = {};
process.argv = [];

function noop() {}

process.on = noop;
process.addListener = noop;
process.once = noop;
process.off = noop;
process.removeListener = noop;
process.removeAllListeners = noop;
process.emit = noop;

process.binding = function (name) {
    throw new Error('process.binding is not supported');
};

// TODO(shtylman)
process.cwd = function () { return '/' };
process.chdir = function (dir) {
    throw new Error('process.chdir is not supported');
};

},{}],7:[function(require,module,exports){
module.exports = function isBuffer(arg) {
  return arg && typeof arg === 'object'
    && typeof arg.copy === 'function'
    && typeof arg.fill === 'function'
    && typeof arg.readUInt8 === 'function';
}
},{}],8:[function(require,module,exports){
(function (process,global){
// Copyright Joyent, Inc. and other Node contributors.
//
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the
// following conditions:
//
// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
// OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
// NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
// USE OR OTHER DEALINGS IN THE SOFTWARE.

var formatRegExp = /%[sdj%]/g;
exports.format = function(f) {
  if (!isString(f)) {
    var objects = [];
    for (var i = 0; i < arguments.length; i++) {
      objects.push(inspect(arguments[i]));
    }
    return objects.join(' ');
  }

  var i = 1;
  var args = arguments;
  var len = args.length;
  var str = String(f).replace(formatRegExp, function(x) {
    if (x === '%%') return '%';
    if (i >= len) return x;
    switch (x) {
      case '%s': return String(args[i++]);
      case '%d': return Number(args[i++]);
      case '%j':
        try {
          return JSON.stringify(args[i++]);
        } catch (_) {
          return '[Circular]';
        }
      default:
        return x;
    }
  });
  for (var x = args[i]; i < len; x = args[++i]) {
    if (isNull(x) || !isObject(x)) {
      str += ' ' + x;
    } else {
      str += ' ' + inspect(x);
    }
  }
  return str;
};


// Mark that a method should not be used.
// Returns a modified function which warns once by default.
// If --no-deprecation is set, then it is a no-op.
exports.deprecate = function(fn, msg) {
  // Allow for deprecating things in the process of starting up.
  if (isUndefined(global.process)) {
    return function() {
      return exports.deprecate(fn, msg).apply(this, arguments);
    };
  }

  if (process.noDeprecation === true) {
    return fn;
  }

  var warned = false;
  function deprecated() {
    if (!warned) {
      if (process.throwDeprecation) {
        throw new Error(msg);
      } else if (process.traceDeprecation) {
        console.trace(msg);
      } else {
        console.error(msg);
      }
      warned = true;
    }
    return fn.apply(this, arguments);
  }

  return deprecated;
};


var debugs = {};
var debugEnviron;
exports.debuglog = function(set) {
  if (isUndefined(debugEnviron))
    debugEnviron = process.env.NODE_DEBUG || '';
  set = set.toUpperCase();
  if (!debugs[set]) {
    if (new RegExp('\\b' + set + '\\b', 'i').test(debugEnviron)) {
      var pid = process.pid;
      debugs[set] = function() {
        var msg = exports.format.apply(exports, arguments);
        console.error('%s %d: %s', set, pid, msg);
      };
    } else {
      debugs[set] = function() {};
    }
  }
  return debugs[set];
};


/**
 * Echos the value of a value. Trys to print the value out
 * in the best way possible given the different types.
 *
 * @param {Object} obj The object to print out.
 * @param {Object} opts Optional options object that alters the output.
 */
/* legacy: obj, showHidden, depth, colors*/
function inspect(obj, opts) {
  // default options
  var ctx = {
    seen: [],
    stylize: stylizeNoColor
  };
  // legacy...
  if (arguments.length >= 3) ctx.depth = arguments[2];
  if (arguments.length >= 4) ctx.colors = arguments[3];
  if (isBoolean(opts)) {
    // legacy...
    ctx.showHidden = opts;
  } else if (opts) {
    // got an "options" object
    exports._extend(ctx, opts);
  }
  // set default options
  if (isUndefined(ctx.showHidden)) ctx.showHidden = false;
  if (isUndefined(ctx.depth)) ctx.depth = 2;
  if (isUndefined(ctx.colors)) ctx.colors = false;
  if (isUndefined(ctx.customInspect)) ctx.customInspect = true;
  if (ctx.colors) ctx.stylize = stylizeWithColor;
  return formatValue(ctx, obj, ctx.depth);
}
exports.inspect = inspect;


// http://en.wikipedia.org/wiki/ANSI_escape_code#graphics
inspect.colors = {
  'bold' : [1, 22],
  'italic' : [3, 23],
  'underline' : [4, 24],
  'inverse' : [7, 27],
  'white' : [37, 39],
  'grey' : [90, 39],
  'black' : [30, 39],
  'blue' : [34, 39],
  'cyan' : [36, 39],
  'green' : [32, 39],
  'magenta' : [35, 39],
  'red' : [31, 39],
  'yellow' : [33, 39]
};

// Don't use 'blue' not visible on cmd.exe
inspect.styles = {
  'special': 'cyan',
  'number': 'yellow',
  'boolean': 'yellow',
  'undefined': 'grey',
  'null': 'bold',
  'string': 'green',
  'date': 'magenta',
  // "name": intentionally not styling
  'regexp': 'red'
};


function stylizeWithColor(str, styleType) {
  var style = inspect.styles[styleType];

  if (style) {
    return '\u001b[' + inspect.colors[style][0] + 'm' + str +
           '\u001b[' + inspect.colors[style][1] + 'm';
  } else {
    return str;
  }
}


function stylizeNoColor(str, styleType) {
  return str;
}


function arrayToHash(array) {
  var hash = {};

  array.forEach(function(val, idx) {
    hash[val] = true;
  });

  return hash;
}


function formatValue(ctx, value, recurseTimes) {
  // Provide a hook for user-specified inspect functions.
  // Check that value is an object with an inspect function on it
  if (ctx.customInspect &&
      value &&
      isFunction(value.inspect) &&
      // Filter out the util module, it's inspect function is special
      value.inspect !== exports.inspect &&
      // Also filter out any prototype objects using the circular check.
      !(value.constructor && value.constructor.prototype === value)) {
    var ret = value.inspect(recurseTimes, ctx);
    if (!isString(ret)) {
      ret = formatValue(ctx, ret, recurseTimes);
    }
    return ret;
  }

  // Primitive types cannot have properties
  var primitive = formatPrimitive(ctx, value);
  if (primitive) {
    return primitive;
  }

  // Look up the keys of the object.
  var keys = Object.keys(value);
  var visibleKeys = arrayToHash(keys);

  if (ctx.showHidden) {
    keys = Object.getOwnPropertyNames(value);
  }

  // IE doesn't make error fields non-enumerable
  // http://msdn.microsoft.com/en-us/library/ie/dww52sbt(v=vs.94).aspx
  if (isError(value)
      && (keys.indexOf('message') >= 0 || keys.indexOf('description') >= 0)) {
    return formatError(value);
  }

  // Some type of object without properties can be shortcutted.
  if (keys.length === 0) {
    if (isFunction(value)) {
      var name = value.name ? ': ' + value.name : '';
      return ctx.stylize('[Function' + name + ']', 'special');
    }
    if (isRegExp(value)) {
      return ctx.stylize(RegExp.prototype.toString.call(value), 'regexp');
    }
    if (isDate(value)) {
      return ctx.stylize(Date.prototype.toString.call(value), 'date');
    }
    if (isError(value)) {
      return formatError(value);
    }
  }

  var base = '', array = false, braces = ['{', '}'];

  // Make Array say that they are Array
  if (isArray(value)) {
    array = true;
    braces = ['[', ']'];
  }

  // Make functions say that they are functions
  if (isFunction(value)) {
    var n = value.name ? ': ' + value.name : '';
    base = ' [Function' + n + ']';
  }

  // Make RegExps say that they are RegExps
  if (isRegExp(value)) {
    base = ' ' + RegExp.prototype.toString.call(value);
  }

  // Make dates with properties first say the date
  if (isDate(value)) {
    base = ' ' + Date.prototype.toUTCString.call(value);
  }

  // Make error with message first say the error
  if (isError(value)) {
    base = ' ' + formatError(value);
  }

  if (keys.length === 0 && (!array || value.length == 0)) {
    return braces[0] + base + braces[1];
  }

  if (recurseTimes < 0) {
    if (isRegExp(value)) {
      return ctx.stylize(RegExp.prototype.toString.call(value), 'regexp');
    } else {
      return ctx.stylize('[Object]', 'special');
    }
  }

  ctx.seen.push(value);

  var output;
  if (array) {
    output = formatArray(ctx, value, recurseTimes, visibleKeys, keys);
  } else {
    output = keys.map(function(key) {
      return formatProperty(ctx, value, recurseTimes, visibleKeys, key, array);
    });
  }

  ctx.seen.pop();

  return reduceToSingleString(output, base, braces);
}


function formatPrimitive(ctx, value) {
  if (isUndefined(value))
    return ctx.stylize('undefined', 'undefined');
  if (isString(value)) {
    var simple = '\'' + JSON.stringify(value).replace(/^"|"$/g, '')
                                             .replace(/'/g, "\\'")
                                             .replace(/\\"/g, '"') + '\'';
    return ctx.stylize(simple, 'string');
  }
  if (isNumber(value))
    return ctx.stylize('' + value, 'number');
  if (isBoolean(value))
    return ctx.stylize('' + value, 'boolean');
  // For some reason typeof null is "object", so special case here.
  if (isNull(value))
    return ctx.stylize('null', 'null');
}


function formatError(value) {
  return '[' + Error.prototype.toString.call(value) + ']';
}


function formatArray(ctx, value, recurseTimes, visibleKeys, keys) {
  var output = [];
  for (var i = 0, l = value.length; i < l; ++i) {
    if (hasOwnProperty(value, String(i))) {
      output.push(formatProperty(ctx, value, recurseTimes, visibleKeys,
          String(i), true));
    } else {
      output.push('');
    }
  }
  keys.forEach(function(key) {
    if (!key.match(/^\d+$/)) {
      output.push(formatProperty(ctx, value, recurseTimes, visibleKeys,
          key, true));
    }
  });
  return output;
}


function formatProperty(ctx, value, recurseTimes, visibleKeys, key, array) {
  var name, str, desc;
  desc = Object.getOwnPropertyDescriptor(value, key) || { value: value[key] };
  if (desc.get) {
    if (desc.set) {
      str = ctx.stylize('[Getter/Setter]', 'special');
    } else {
      str = ctx.stylize('[Getter]', 'special');
    }
  } else {
    if (desc.set) {
      str = ctx.stylize('[Setter]', 'special');
    }
  }
  if (!hasOwnProperty(visibleKeys, key)) {
    name = '[' + key + ']';
  }
  if (!str) {
    if (ctx.seen.indexOf(desc.value) < 0) {
      if (isNull(recurseTimes)) {
        str = formatValue(ctx, desc.value, null);
      } else {
        str = formatValue(ctx, desc.value, recurseTimes - 1);
      }
      if (str.indexOf('\n') > -1) {
        if (array) {
          str = str.split('\n').map(function(line) {
            return '  ' + line;
          }).join('\n').substr(2);
        } else {
          str = '\n' + str.split('\n').map(function(line) {
            return '   ' + line;
          }).join('\n');
        }
      }
    } else {
      str = ctx.stylize('[Circular]', 'special');
    }
  }
  if (isUndefined(name)) {
    if (array && key.match(/^\d+$/)) {
      return str;
    }
    name = JSON.stringify('' + key);
    if (name.match(/^"([a-zA-Z_][a-zA-Z_0-9]*)"$/)) {
      name = name.substr(1, name.length - 2);
      name = ctx.stylize(name, 'name');
    } else {
      name = name.replace(/'/g, "\\'")
                 .replace(/\\"/g, '"')
                 .replace(/(^"|"$)/g, "'");
      name = ctx.stylize(name, 'string');
    }
  }

  return name + ': ' + str;
}


function reduceToSingleString(output, base, braces) {
  var numLinesEst = 0;
  var length = output.reduce(function(prev, cur) {
    numLinesEst++;
    if (cur.indexOf('\n') >= 0) numLinesEst++;
    return prev + cur.replace(/\u001b\[\d\d?m/g, '').length + 1;
  }, 0);

  if (length > 60) {
    return braces[0] +
           (base === '' ? '' : base + '\n ') +
           ' ' +
           output.join(',\n  ') +
           ' ' +
           braces[1];
  }

  return braces[0] + base + ' ' + output.join(', ') + ' ' + braces[1];
}


// NOTE: These type checking functions intentionally don't use `instanceof`
// because it is fragile and can be easily faked with `Object.create()`.
function isArray(ar) {
  return Array.isArray(ar);
}
exports.isArray = isArray;

function isBoolean(arg) {
  return typeof arg === 'boolean';
}
exports.isBoolean = isBoolean;

function isNull(arg) {
  return arg === null;
}
exports.isNull = isNull;

function isNullOrUndefined(arg) {
  return arg == null;
}
exports.isNullOrUndefined = isNullOrUndefined;

function isNumber(arg) {
  return typeof arg === 'number';
}
exports.isNumber = isNumber;

function isString(arg) {
  return typeof arg === 'string';
}
exports.isString = isString;

function isSymbol(arg) {
  return typeof arg === 'symbol';
}
exports.isSymbol = isSymbol;

function isUndefined(arg) {
  return arg === void 0;
}
exports.isUndefined = isUndefined;

function isRegExp(re) {
  return isObject(re) && objectToString(re) === '[object RegExp]';
}
exports.isRegExp = isRegExp;

function isObject(arg) {
  return typeof arg === 'object' && arg !== null;
}
exports.isObject = isObject;

function isDate(d) {
  return isObject(d) && objectToString(d) === '[object Date]';
}
exports.isDate = isDate;

function isError(e) {
  return isObject(e) &&
      (objectToString(e) === '[object Error]' || e instanceof Error);
}
exports.isError = isError;

function isFunction(arg) {
  return typeof arg === 'function';
}
exports.isFunction = isFunction;

function isPrimitive(arg) {
  return arg === null ||
         typeof arg === 'boolean' ||
         typeof arg === 'number' ||
         typeof arg === 'string' ||
         typeof arg === 'symbol' ||  // ES6 symbol
         typeof arg === 'undefined';
}
exports.isPrimitive = isPrimitive;

exports.isBuffer = require('./support/isBuffer');

function objectToString(o) {
  return Object.prototype.toString.call(o);
}


function pad(n) {
  return n < 10 ? '0' + n.toString(10) : n.toString(10);
}


var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep',
              'Oct', 'Nov', 'Dec'];

// 26 Feb 16:19:34
function timestamp() {
  var d = new Date();
  var time = [pad(d.getHours()),
              pad(d.getMinutes()),
              pad(d.getSeconds())].join(':');
  return [d.getDate(), months[d.getMonth()], time].join(' ');
}


// log is just a thin wrapper to console.log that prepends a timestamp
exports.log = function() {
  console.log('%s - %s', timestamp(), exports.format.apply(exports, arguments));
};


/**
 * Inherit the prototype methods from one constructor into another.
 *
 * The Function.prototype.inherits from lang.js rewritten as a standalone
 * function (not on Function.prototype). NOTE: If this file is to be loaded
 * during bootstrapping this function needs to be rewritten using some native
 * functions as prototype setup using normal JavaScript does not work as
 * expected during bootstrapping (see mirror.js in r114903).
 *
 * @param {function} ctor Constructor function which needs to inherit the
 *     prototype.
 * @param {function} superCtor Constructor function to inherit prototype from.
 */
exports.inherits = require('inherits');

exports._extend = function(origin, add) {
  // Don't do anything if add isn't an object
  if (!add || !isObject(add)) return origin;

  var keys = Object.keys(add);
  var i = keys.length;
  while (i--) {
    origin[keys[i]] = add[keys[i]];
  }
  return origin;
};

function hasOwnProperty(obj, prop) {
  return Object.prototype.hasOwnProperty.call(obj, prop);
}

}).call(this,require('_process'),typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{"./support/isBuffer":7,"_process":6,"inherits":5}],9:[function(require,module,exports){
// Generated by CoffeeScript 1.6.3
(function() {
  var Color, Sketch, Vec2;

  Color = require('../color').Color;

  Vec2 = require('../math/vector').Vec2;

  /*
  
    FIELDKIT SKETCH CLASS
  */


  Sketch = (function() {
    var TWO_PI, computeStyle, isFillEnabled, isStrokeEnabled;

    Sketch.prototype.width = -1;

    Sketch.prototype.height = -1;

    Sketch.prototype.canvasId = "sketch";

    Sketch.prototype.domObjectId = "container";

    Sketch.prototype.g = null;

    Sketch.prototype.mouseX = 0;

    Sketch.prototype.mouseY = 0;

    function Sketch(options) {
      var canvas, domObject,
        _this = this;
      domObject = document.getElementById(this.domObjectId);
      if (this.width === -1) {
        this.width = domObject.offsetWidth;
      }
      if (this.height === -1) {
        this.height = domObject.offsetHeight;
      }
      canvas = document.createElement("canvas");
      canvas.id = this.canvasId;
      canvas.width = this.width;
      canvas.height = this.height;
      domObject.appendChild(canvas);
      this.g = canvas.getContext("2d");
      this.setup();
      this.start();
      domObject.onmousemove = function(e) {
        _this.mouseX = e.x;
        return _this.mouseY = e.y;
      };
      domObject.onmousedown = function(e) {
        return _this.mouseDown();
      };
      domObject.onmouseup = function(e) {
        return _this.mouseUp();
      };
    }

    Sketch.prototype.isRunning = false;

    Sketch.prototype.isFinishedCallback = null;

    Sketch.prototype.start = function() {
      var render, requestId,
        _this = this;
      this.isRunning = true;
      requestId = null;
      render = function() {
        _this.draw();
        if (_this.isRunning) {
          return requestId = window.requestAnimationFrame(render);
        } else {
          window.cancelAnimationFrame(requestId);
          if (_this.isFinishedCallback != null) {
            return _this.isFinishedCallback();
          }
        }
      };
      return requestId = window.requestAnimationFrame(render);
    };

    Sketch.prototype.stop = function(callback) {
      this.isRunning = false;
      return this.isFinishedCallback = callback;
    };

    Sketch.prototype.toString = function() {
      return "Sketch";
    };

    /*
      Sketch API
    */


    Sketch.prototype.setup = function() {};

    Sketch.prototype.draw = function() {};

    Sketch.prototype.mouseDown = function() {};

    Sketch.prototype.mouseUp = function() {};

    /*
      2D Drawing API
    */


    isFillEnabled = true;

    isStrokeEnabled = false;

    TWO_PI = Math.PI * 2;

    computeStyle = function(args) {
      var arg, grey;
      switch (args.length) {
        case 1:
          arg = args[0];
          if (typeof arg === "string") {
            return arg;
          } else if (arg instanceof Color) {
            return arg.toCSS();
          } else {
            return "rgba(" + arg + ", " + arg + ", " + arg + ", 1)";
          }
          break;
        case 2:
          grey = args[0];
          return "rgba(" + grey + ", " + grey + ", " + grey + ", " + args[1] + ")";
        case 3:
          return "rgba(" + args[0] + ", " + args[1] + ", " + args[2] + ", 1)";
        case 4:
          return "rgba(" + args[0] + ", " + args[1] + ", " + args[2] + ", " + args[3] + ")";
        default:
          return "#FF0000";
      }
    };

    Sketch.prototype.color = function() {
      return computeStyle(arguments);
    };

    Sketch.prototype.background = function() {
      var style;
      this.g.clearRect(0, 0, this.width, this.height);
      style = this.g.fillStyle;
      this.g.fillStyle = computeStyle(arguments);
      this.g.fillRect(0, 0, this.width, this.height);
      return this.g.fillStyle = style;
    };

    Sketch.prototype.fill = function() {
      isFillEnabled = true;
      return this.g.fillStyle = computeStyle(arguments);
    };

    Sketch.prototype.stroke = function() {
      isStrokeEnabled = true;
      return this.g.strokeStyle = computeStyle(arguments);
    };

    Sketch.prototype.lineWidth = function(width) {
      return this.g.lineWidth = width;
    };

    Sketch.prototype.noFill = function() {
      return isFillEnabled = false;
    };

    Sketch.prototype.noStroke = function() {
      return isStrokeEnabled = false;
    };

    Sketch.prototype.rect = function(x, y, w, h) {
      if (isFillEnabled) {
        this.g.fillRect(x, y, w, h);
      }
      if (isStrokeEnabled) {
        return this.g.strokeRect(x, y, w, h);
      }
    };

    Sketch.prototype.circle = function(x, y, r) {
      this.g.beginPath();
      this.g.arc(x, y, r, 0, TWO_PI, false);
      if (isFillEnabled) {
        this.g.fill();
      }
      if (isStrokeEnabled) {
        this.g.stroke();
      }
      return this.g.closePath();
    };

    Sketch.prototype.line = function(x1, y1, x2, y2) {
      this.g.beginPath();
      if (x1 instanceof Vec2) {
        this.g.moveTo(x1.x, x1.y);
        this.g.lineTo(y1.x, y1.y);
      } else {
        this.g.moveTo(x1, y1);
        this.g.lineTo(x2, y2);
      }
      this.g.stroke();
      return this.g.closePath();
    };

    Sketch.prototype.polygon = function(points) {
      var p, _i, _len, _ref;
      this.g.beginPath();
      this.g.moveTo(points[0].x, points[0].y);
      _ref = points.slice(1);
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        p = _ref[_i];
        this.g.lineTo(p.x, p.y);
      }
      if (isFillEnabled) {
        this.g.fill();
      }
      if (isStrokeEnabled) {
        this.g.stroke();
      }
      return this.g.closePath();
    };

    Sketch.prototype.createImage = function(width, height) {
      var imageData;
      imageData = this.g.createImageData(width, height);
      imageData.setPixel = function(x, y, r, g, b, a) {
        var index;
        if (a == null) {
          a = 255;
        }
        index = (x + y * this.width) * 4;
        this.data[index + 0] = r;
        this.data[index + 1] = g;
        this.data[index + 2] = b;
        return this.data[index + 3] = a;
      };
      return imageData;
    };

    Sketch.prototype.drawImage = function(imageData, x, y) {
      return this.g.putImageData(imageData, x, y);
    };

    return Sketch;

  })();

  module.exports = {
    Sketch: Sketch
  };

}).call(this);

},{"../color":10,"../math/vector":18}],10:[function(require,module,exports){
// Generated by CoffeeScript 1.6.3
/*
*/


(function() {
  var Color;

  Color = (function() {
    Color.prototype.r = 1;

    Color.prototype.g = 1;

    Color.prototype.b = 1;

    Color.prototype.a = 1;

    function Color(r, g, b, a) {
      this.r = r != null ? r : 1;
      this.g = g != null ? g : 1;
      this.b = b != null ? b : 1;
      this.a = a != null ? a : 1;
    }

    Color.prototype.set = function(color) {
      this.r = color.r;
      this.g = color.g;
      this.b = color.b;
      this.a = color.a;
      return this;
    };

    Color.prototype.set3 = function(r, g, b) {
      this.r = r;
      this.g = g;
      this.b = b;
      return this;
    };

    Color.prototype.randomize = function() {
      this.r = Math.random();
      this.g = Math.random();
      this.b = Math.random();
      return this;
    };

    Color.prototype.clone = function() {
      return new Color(this.r, this.g, this.b, this.a);
    };

    Color.prototype.equals = function(other) {
      if (other == null) {
        return false;
      }
      return this.r === other.r && this.g === other.g && this.b === other.b && this.a === other.a;
    };

    Color.prototype.toCSS = function() {
      var b, g, r;
      r = Math.floor(255 * this.r);
      g = Math.floor(255 * this.g);
      b = Math.floor(255 * this.b);
      return "rgba(" + r + "," + g + "," + b + "," + this.a + ")";
    };

    Color.prototype.toString = function() {
      return "fk.Color(" + this.r + "," + this.g + "," + this.b + "," + this.a + ")";
    };

    return Color;

  })();

  module.exports = {
    Color: Color
  };

}).call(this);

},{}],11:[function(require,module,exports){
// Generated by CoffeeScript 1.6.3
/*
      _____  __  _____  __     ____
     / ___/ / / /____/ / /    /    \   FieldKit
    / ___/ /_/ /____/ / /__  /  /  /   (c) 2013, FIELD. All rights reserved.
   /_/        /____/ /____/ /_____/    http://www.field.io

   Created by Marcus Wendt on 07/03/2013
*/


(function() {
  var extend, fk, util;

  util = require('./util');

  extend = function() {
    var pkg;
    switch (arguments.length) {
      case 1:
        return util.extend(fk, arguments[0]);
      case 2:
        pkg = arguments[0];
        if (fk[pkg] == null) {
          fk[pkg] = {};
        }
        return util.extend(fk[pkg], arguments[1]);
    }
  };

  fk = {};

  extend(require('./color'));

  extend(require('./time'));

  extend('math', require('./math/math'));

  extend('math', require('./math/random'));

  extend('math', require('./math/noise'));

  extend('math', require('./math/vector'));

  extend('math', require('./math/rect'));

  extend('math', require('./math/box'));

  extend('math', require('./math/line'));

  extend('util', util);

  extend('physics', require('./physics/physics'));

  extend('physics', require('./physics/particle'));

  extend('physics', require('./physics/behaviours'));

  extend('physics', require('./physics/constraints'));

  extend('client', require('./client/sketch'));

  module.exports = fk;

  if (typeof window !== "undefined" && window !== null) {
    window.fk = fk;
  }

}).call(this);

},{"./client/sketch":9,"./color":10,"./math/box":12,"./math/line":13,"./math/math":14,"./math/noise":15,"./math/random":16,"./math/rect":17,"./math/vector":18,"./physics/behaviours":19,"./physics/constraints":20,"./physics/particle":21,"./physics/physics":22,"./time":23,"./util":24}],12:[function(require,module,exports){
// Generated by CoffeeScript 1.6.3
(function() {
  var Box, Vec3;

  Vec3 = require("./vector").Vec3;

  Box = (function() {
    Box.prototype.x1 = 0;

    Box.prototype.y1 = 0;

    Box.prototype.z1 = 0;

    Box.prototype.x2 = 0;

    Box.prototype.y2 = 0;

    Box.prototype.z2 = 0;

    function Box(x1, y1, z1, x2, y2, z2) {
      this.x1 = x1 != null ? x1 : 0;
      this.y1 = y1 != null ? y1 : 0;
      this.z1 = z1 != null ? z1 : 0;
      this.x2 = x2 != null ? x2 : 0;
      this.y2 = y2 != null ? y2 : 0;
      this.z2 = z2 != null ? z2 : 0;
    }

    Box.prototype.center = function() {
      var x, y, z;
      x = Math.min(this.x1, this.x2) + this.width() * 0.5;
      y = Math.min(this.y1, this.y2) + this.height() * 0.5;
      z = Math.min(this.z1, this.z2) + this.depth() * 0.5;
      return new Vec3(x, y, z);
    };

    Box.prototype.width = function() {
      return Math.abs(this.x1 - this.x2);
    };

    Box.prototype.height = function() {
      return Math.abs(this.y1 - this.y2);
    };

    Box.prototype.depth = function() {
      return Math.abs(this.z1 - this.z2);
    };

    return Box;

  })();

  module.exports.Box = Box;

}).call(this);

},{"./vector":18}],13:[function(require,module,exports){
// Generated by CoffeeScript 1.6.3
/*
  A simple line from multiple segments, can be sampled at any point.
  NOTE this curve does not always run through all given points.
  Work with any object type as long as it has x and y number properties.
*/


(function() {
  var Polyline, Spline;

  Polyline = (function() {
    function Polyline(points) {
      this.points = points != null ? points : [];
    }

    Polyline.prototype.add = function(point) {
      return points.push(point);
    };

    Polyline.prototype.size = function() {
      return points.length;
    };

    Polyline.prototype.clear = function() {
      var points;
      return points = [];
    };

    Polyline.prototype.point = function(time) {
      var median, next, prev;
      if (time <= 0) {
        return points[0];
      } else if (time >= 1) {
        return points[points.length - 1];
      } else {
        median = time * (points.length - 1);
        prev = points[Math.floor(median)];
        next = points[Math.ceil(median)];
        return {
          x: prev.x + (next.x - prev.x) * 0.5,
          y: prev.y + (next.y - prev.y) * 0.5
        };
      }
    };

    return Polyline;

  })();

  /*
  A Catmull-Rom spline (which is a special form of the cubic hermite curve) implementation,
  generates a smooth curve/interpolation from a number of Vec2 points.
  */


  Spline = (function() {
    var beforeLast, first, last, second;

    first = second = beforeLast = last = void 0;

    Spline.prototype.points = [];

    Spline.prototype.needsUpdate = false;

    function Spline(points) {
      this.points = points != null ? points : [];
      this.needsUpdate = this.points.length > 0;
    }

    Spline.prototype.add = function(point) {
      this.points.push(point);
      return this.needsUpdate = true;
    };

    Spline.prototype.size = function() {
      return this.points.length;
    };

    Spline.prototype.clear = function() {
      return this.points = [];
    };

    Spline.prototype.update = function() {
      if (this.points.length < 4) {
        return;
      }
      first = this.points[0];
      second = this.points[1];
      beforeLast = this.points[this.points.length - 2];
      last = this.points[this.points.length - 1];
      return this.needsUpdate = false;
    };

    Spline.prototype.point = function(time) {
      var i, normalizedTime, partPercentage, result, size, t, t2, t3, timeBetween, tmp1, tmp2, tmp3, tmp4;
      if (this.points.length < 4) {
        return;
      }
      if (time <= 0) {
        return this.points[0];
      } else if (time >= 1) {
        return this.points[this.points.length - 1];
      } else {
        if (this.needsUpdate) {
          update();
        }
        size = this.points.length;
        partPercentage = 1.0 / (size - 1);
        timeBetween = time / partPercentage;
        i = Math.floor(timeBetween);
        normalizedTime = timeBetween - i;
        t = normalizedTime * 0.5;
        t2 = t * normalizedTime;
        t3 = t2 * normalizedTime;
        tmp1 = void 0;
        if (--i === -1) {
          tmp1 = first.clone();
        } else {
          tmp1 = this.points[i].clone();
        }
        tmp2 = this.points[++i].clone();
        tmp3 = this.points[++i].clone();
        tmp4 = void 0;
        if (++i === size) {
          tmp4 = last.clone();
        } else {
          tmp4 = this.points[i].clone();
        }
        tmp1.scale(-t3 + 2 * t2 - t);
        result = tmp1;
        tmp2.scale(3 * t3 - 5 * t2 + 1);
        result.add(tmp2);
        tmp3.scale(-3 * t3 + 4 * t2 + t);
        result.add(tmp3);
        tmp4.scale(t3 - t2);
        result.add(tmp4);
        return result;
      }
    };

    return Spline;

  })();

  module.exports = {
    Polyline: Polyline,
    Spline: Spline
  };

}).call(this);

},{}],14:[function(require,module,exports){
// Generated by CoffeeScript 1.6.3
(function() {
  var random, randomPkg;

  randomPkg = require('./random');

  random = new randomPkg.Random();

  module.exports = {
    fit: function(value, inMin, inMax, outMin, outMax) {
      value = Math.max(inMin, Math.min(inMax, value));
      return ((value - inMin) / (inMax - inMin)) * (outMax - outMin) + outMin;
    },
    fit01: function(value, min, max) {
      return value * (max - min) + min;
    },
    clamp: function(value, min, max) {
      return Math.max(min, Math.min(max, value));
    },
    clamp01: function(value) {
      return Math.max(0, Math.min(1, value));
    },
    random: random,
    randf: function(min, max) {
      return random.randf(min, max);
    },
    randi: function(min, max) {
      return random.randi(min, max);
    },
    flipCoin: function(chance) {
      return random.flipCoin(chance);
    },
    round: function(value, decimals) {
      var exp;
      if (decimals == null) {
        decimals = 0;
      }
      if (decimals === 0) {
        return Math.round(value);
      } else {
        exp = Math.pow(10, decimals);
        return Math.round(value * exp) / exp;
      }
    },
    interpolate: {
      spherical: function(current, target, delta) {
        return current * (1.0 - delta) + target * delta;
      },
      linear: function(current, target, delta) {
        return current + (target - current) * delta;
      }
    },
    /*
      Easing Functions - inspired by http://easings.net
      only considering the t value for the range [0, 1] => [0, 1]
    */

    ease: {
      linear: function(t) {
        return t;
      },
      inQuad: function(t) {
        return Math.pow(t, 2);
      },
      outQuad: function(t) {
        return -(Math.pow(t - 1, 2) - 1);
      },
      inOutQuad: function(t) {
        if ((t /= 0.5) < 1) {
          return 0.5 * Math.pow(t, 2);
        }
        return -0.5 * ((t -= 2) * t - 2);
      },
      inCubic: function(t) {
        return Math.pow(t, 3);
      },
      outCubic: function(t) {
        return Math.pow(t - 1, 3) + 1;
      },
      inOutCubic: function(t) {
        if ((t /= 0.5) < 1) {
          return 0.5 * Math.pow(t, 3);
        }
        return 0.5 * (Math.pow(t - 2, 3) + 2);
      },
      inQuart: function(t) {
        return Math.pow(t, 4);
      },
      outQuart: function(t) {
        return -(Math.pow(t - 1, 4) - 1);
      },
      inOutQuart: function(t) {
        if ((t /= 0.5) < 1) {
          return 0.5 * Math.pow(t, 4);
        }
        return -0.5 * ((t -= 2) * Math.pow(t, 3) - 2);
      },
      inQuint: function(t) {
        return Math.pow(t, 5);
      },
      outQuint: function(t) {
        return Math.pow(t - 1, 5) + 1;
      },
      inOutQuint: function(t) {
        if ((t /= 0.5) < 1) {
          return 0.5 * Math.pow(t, 5);
        }
        return 0.5 * (Math.pow(t - 2, 5) + 2);
      },
      inSine: function(t) {
        return -Math.cos(t * (Math.PI / 2)) + 1;
      },
      outSine: function(t) {
        return Math.sin(t * (Math.PI / 2));
      },
      inOutSine: function(t) {
        return -0.5 * (Math.cos(Math.PI * t) - 1);
      },
      inExpo: function(t) {
        if (t === 0) {
          return 0;
        } else {
          return Math.pow(2, 10 * (t - 1));
        }
      },
      outExpo: function(t) {
        if (t === 1) {
          return 1;
        } else {
          return -Math.pow(2, -10 * t) + 1;
        }
      },
      inOutExpo: function(t) {
        if (t === 0) {
          return 0;
        }
        if (t === 1) {
          return 1;
        }
        if ((t /= 0.5) < 1) {
          return 0.5 * Math.pow(2, 10 * (t - 1));
        }
        return 0.5 * (-Math.pow(2, -10 * --t) + 2);
      },
      inCirc: function(t) {
        return -(Math.sqrt(1 - (t * t)) - 1);
      },
      outCirc: function(t) {
        return Math.sqrt(1 - Math.pow(t - 1, 2));
      },
      inOutCirc: function(t) {
        if ((t /= 0.5) < 1) {
          return -0.5 * (Math.sqrt(1 - t * t) - 1);
        }
        return 0.5 * (Math.sqrt(1 - (t -= 2) * t) + 1);
      },
      outBounce: function(t) {
        if (t < (1 / 2.75)) {
          return 7.5625 * t * t;
        } else if (t < (2 / 2.75)) {
          return 7.5625 * (t -= 1.5 / 2.75) * t + 0.75;
        } else if (t < (2.5 / 2.75)) {
          return 7.5625 * (t -= 2.25 / 2.75) * t + 0.9375;
        } else {
          return 7.5625 * (t -= 2.625 / 2.75) * t + 0.984375;
        }
      },
      inBack: function(t) {
        var s;
        s = 1.70158;
        return t * t * ((s + 1) * t - s);
      },
      outBack: function(t) {
        var s;
        s = 1.70158;
        return (t = t - 1) * t * ((s + 1) * t + s) + 1;
      },
      inOutBack: function(t) {
        var s;
        s = 1.70158;
        if ((t /= 0.5) < 1) {
          return 0.5 * (t * t * (((s *= 1.525) + 1) * t - s));
        }
        return 0.5 * ((t -= 2) * t * (((s *= 1.525) + 1) * t + s) + 2);
      },
      elastic: function(t) {
        return -1 * Math.pow(4, -8 * t) * Math.sin((t * 6 - 1) * (2 * Math.PI) / 2) + 1;
      },
      swingFromTo: function(t) {
        var s;
        s = 1.70158;
        if ((t /= 0.5) < 1) {
          return 0.5 * (t * t * (((s *= 1.525) + 1) * t - s));
        } else {
          return 0.5 * ((t -= 2) * t * (((s *= 1.525) + 1) * t + s) + 2);
        }
      },
      swingFrom: function(t) {
        var s;
        s = 1.70158;
        return t * t * ((s + 1) * t - s);
      },
      swingTo: function(t) {
        var s;
        s = 1.70158;
        return (t -= 1) * t * ((s + 1) * t + s) + 1;
      },
      bounce: function(t) {
        if (t < (1 / 2.75)) {
          return 7.5625 * t * t;
        } else if (t < (2 / 2.75)) {
          return 7.5625 * (t -= 1.5 / 2.75) * t + 0.75;
        } else if (t < (2.5 / 2.75)) {
          return 7.5625 * (t -= 2.25 / 2.75) * t + 0.9375;
        } else {
          return 7.5625 * (t -= 2.625 / 2.75) * t + 0.984375;
        }
      },
      bouncePast: function(t) {
        if (t < (1 / 2.75)) {
          return 7.5625 * t * t;
        } else if (t < (2 / 2.75)) {
          return 2 - (7.5625 * (t -= 1.5 / 2.75) * t + 0.75);
        } else if (t < (2.5 / 2.75)) {
          return 2 - (7.5625 * (t -= 2.25 / 2.75) * t + 0.9375);
        } else {
          return 2 - (7.5625 * (t -= 2.625 / 2.75) * t + 0.984375);
        }
      },
      fromTo: function(t) {
        if ((t /= 0.5) < 1) {
          return 0.5 * Math.pow(t, 4);
        }
        return -0.5 * ((t -= 2) * Math.pow(t, 3) - 2);
      },
      from: function(t) {
        return Math.pow(t, 4);
      },
      to: function(t) {
        return Math.pow(t, 0.25);
      }
    }
  };

}).call(this);

},{"./random":16}],15:[function(require,module,exports){
// Generated by CoffeeScript 1.6.3
/*
  Base class for all types of Noise generators
*/


(function() {
  var FlowNoise, Noise, SimplexNoise,
    __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  Noise = (function() {
    function Noise() {}

    Noise.prototype.noise = function(x, y) {
      return 0;
    };

    Noise.prototype.noise2 = function(x, y) {
      return 0;
    };

    Noise.prototype.noise3 = function(x, y, z) {
      return 0;
    };

    Noise.prototype.noise4 = function(x, y, z, w) {
      return 0;
    };

    return Noise;

  })();

  /*
    Ported from Stefan Gustavson's java implementation
    http://staffwww.itn.liu.se/~stegu/simplexnoise/simplexnoise.pdf
    Read Stefan's excellent paper for details on how this code works.
  
    Sean McCullough banksean@gmail.com
  
    Added 4D noise
    Joshua Koo zz85nus@gmail.com
  */


  SimplexNoise = (function(_super) {
    var dot;

    __extends(SimplexNoise, _super);

    function SimplexNoise(rng) {
      var i, _i, _j;
      if (rng == null) {
        rng = Math;
      }
      this.grad3 = [[1, 1, 0], [-1, 1, 0], [1, -1, 0], [-1, -1, 0], [1, 0, 1], [-1, 0, 1], [1, 0, -1], [-1, 0, -1], [0, 1, 1], [0, -1, 1], [0, 1, -1], [0, -1, -1]];
      this.grad4 = [[0, 1, 1, 1], [0, 1, 1, -1], [0, 1, -1, 1], [0, 1, -1, -1], [0, -1, 1, 1], [0, -1, 1, -1], [0, -1, -1, 1], [0, -1, -1, -1], [1, 0, 1, 1], [1, 0, 1, -1], [1, 0, -1, 1], [1, 0, -1, -1], [-1, 0, 1, 1], [-1, 0, 1, -1], [-1, 0, -1, 1], [-1, 0, -1, -1], [1, 1, 0, 1], [1, 1, 0, -1], [1, -1, 0, 1], [1, -1, 0, -1], [-1, 1, 0, 1], [-1, 1, 0, -1], [-1, -1, 0, 1], [-1, -1, 0, -1], [1, 1, 1, 0], [1, 1, -1, 0], [1, -1, 1, 0], [1, -1, -1, 0], [-1, 1, 1, 0], [-1, 1, -1, 0], [-1, -1, 1, 0], [-1, -1, -1, 0]];
      this.p = [];
      i = 0;
      for (i = _i = 0; _i <= 256; i = ++_i) {
        this.p[i] = Math.floor(rng.random() * 256);
      }
      this.perm = [];
      for (i = _j = 0; _j <= 512; i = ++_j) {
        this.perm[i] = this.p[i & 255];
      }
      this.simplex = [[0, 1, 2, 3], [0, 1, 3, 2], [0, 0, 0, 0], [0, 2, 3, 1], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [1, 2, 3, 0], [0, 2, 1, 3], [0, 0, 0, 0], [0, 3, 1, 2], [0, 3, 2, 1], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [1, 3, 2, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [1, 2, 0, 3], [0, 0, 0, 0], [1, 3, 0, 2], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [2, 3, 0, 1], [2, 3, 1, 0], [1, 0, 2, 3], [1, 0, 3, 2], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [2, 0, 3, 1], [0, 0, 0, 0], [2, 1, 3, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [2, 0, 1, 3], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [3, 0, 1, 2], [3, 0, 2, 1], [0, 0, 0, 0], [3, 1, 2, 0], [2, 1, 0, 3], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [3, 1, 0, 2], [0, 0, 0, 0], [3, 2, 0, 1], [3, 2, 1, 0]];
    }

    dot = function(g, x, y) {
      return g[0] * x + g[1] * y;
    };

    SimplexNoise.prototype.noise2 = function(xin, yin) {
      var F2, G2, X0, Y0, gi0, gi1, gi2, i, i1, ii, j, j1, jj, n0, n1, n2, s, t, t0, t1, t2, x0, x1, x2, y0, y1, y2;
      n0 = 0;
      n1 = 0;
      n2 = 0;
      F2 = 0.5 * (Math.sqrt(3.0) - 1.0);
      s = (xin + yin) * F2;
      i = Math.floor(xin + s);
      j = Math.floor(yin + s);
      G2 = (3.0 - Math.sqrt(3.0)) / 6.0;
      t = (i + j) * G2;
      X0 = i - t;
      Y0 = j - t;
      x0 = xin - X0;
      y0 = yin - Y0;
      i1 = 0;
      j1 = 0;
      if (x0 > y0) {
        i1 = 1;
        j1 = 0;
      } else {
        i1 = 0;
        j1 = 1;
      }
      x1 = x0 - i1 + G2;
      y1 = y0 - j1 + G2;
      x2 = x0 - 1.0 + 2.0 * G2;
      y2 = y0 - 1.0 + 2.0 * G2;
      ii = i & 255;
      jj = j & 255;
      gi0 = this.perm[ii + this.perm[jj]] % 12;
      gi1 = this.perm[ii + i1 + this.perm[jj + j1]] % 12;
      gi2 = this.perm[ii + 1 + this.perm[jj + 1]] % 12;
      t0 = 0.5 - x0 * x0 - y0 * y0;
      if (!(t0 < 0)) {
        t0 *= t0;
        n0 = t0 * t0 * dot(this.grad3[gi0], x0, y0);
      }
      t1 = 0.5 - x1 * x1 - y1 * y1;
      if (!(t1 < 0)) {
        t1 *= t1;
        n1 = t1 * t1 * dot(this.grad3[gi1], x1, y1);
      }
      t2 = 0.5 - x2 * x2 - y2 * y2;
      if (!(t2 < 0)) {
        t2 *= t2;
        n2 = t2 * t2 * dot(this.grad3[gi2], x2, y2);
      }
      return 70.0 * (n0 + n1 + n2);
    };

    SimplexNoise.prototype.noise3 = function(xin, yin, zin) {
      var F3, G3, X0, Y0, Z0, gi0, gi1, gi2, gi3, i, i1, i2, ii, j, j1, j2, jj, k, k1, k2, kk, n0, n1, n2, n3, s, t, t0, t1, t2, t3, x0, x1, x2, x3, y0, y1, y2, y3, z0, z1, z2, z3;
      n0 = 0;
      n1 = 0;
      n2 = 0;
      n3 = 0;
      F3 = 1.0 / 3.0;
      s = (xin + yin + zin) * F3;
      i = Math.floor(xin + s);
      j = Math.floor(yin + s);
      k = Math.floor(zin + s);
      G3 = 1.0 / 6.0;
      t = (i + j + k) * G3;
      X0 = i - t;
      Y0 = j - t;
      Z0 = k - t;
      x0 = xin - X0;
      y0 = yin - Y0;
      z0 = zin - Z0;
      i1 = 0;
      j1 = 0;
      k1 = 0;
      i2 = 0;
      j2 = 0;
      k2 = 0;
      if (x0 >= y0) {
        if (y0 >= z0) {
          i1 = 1;
          j1 = 0;
          k1 = 0;
          i2 = 1;
          j2 = 1;
          k2 = 0;
        } else if (x0 >= z0) {
          i1 = 1;
          j1 = 0;
          k1 = 0;
          i2 = 1;
          j2 = 0;
          k2 = 1;
        } else {
          i1 = 0;
          j1 = 0;
          k1 = 1;
          i2 = 1;
          j2 = 0;
          k2 = 1;
        }
      } else {
        if (y0 < z0) {
          i1 = 0;
          j1 = 0;
          k1 = 1;
          i2 = 0;
          j2 = 1;
          k2 = 1;
        } else if (x0 < z0) {
          i1 = 0;
          j1 = 1;
          k1 = 0;
          i2 = 0;
          j2 = 1;
          k2 = 1;
        } else {
          i1 = 0;
          j1 = 1;
          k1 = 0;
          i2 = 1;
          j2 = 1;
          k2 = 0;
        }
      }
      x1 = x0 - i1 + G3;
      y1 = y0 - j1 + G3;
      z1 = z0 - k1 + G3;
      x2 = x0 - i2 + 2.0 * G3;
      y2 = y0 - j2 + 2.0 * G3;
      z2 = z0 - k2 + 2.0 * G3;
      x3 = x0 - 1.0 + 3.0 * G3;
      y3 = y0 - 1.0 + 3.0 * G3;
      z3 = z0 - 1.0 + 3.0 * G3;
      ii = i & 255;
      jj = j & 255;
      kk = k & 255;
      gi0 = this.perm[ii + this.perm[jj + this.perm[kk]]] % 12;
      gi1 = this.perm[ii + i1 + this.perm[jj + j1 + this.perm[kk + k1]]] % 12;
      gi2 = this.perm[ii + i2 + this.perm[jj + j2 + this.perm[kk + k2]]] % 12;
      gi3 = this.perm[ii + 1 + this.perm[jj + 1 + this.perm[kk + 1]]] % 12;
      t0 = 0.6 - x0 * x0 - y0 * y0 - z0 * z0;
      if (!(t0 < 0)) {
        t0 *= t0;
        n0 = t0 * t0 * dot(this.grad3[gi0], x0, y0, z0);
      }
      t1 = 0.6 - x1 * x1 - y1 * y1 - z1 * z1;
      if (!(t1 < 0)) {
        t1 *= t1;
        n1 = t1 * t1 * dot(this.grad3[gi1], x1, y1, z1);
      }
      t2 = 0.6 - x2 * x2 - y2 * y2 - z2 * z2;
      if (!(t2 < 0)) {
        t2 *= t2;
        n2 = t2 * t2 * dot(this.grad3[gi2], x2, y2, z2);
      }
      t3 = 0.6 - x3 * x3 - y3 * y3 - z3 * z3;
      if (!(t3 < 0)) {
        t3 *= t3;
        n3 = t3 * t3 * dot(this.grad3[gi3], x3, y3, z3);
      }
      return 32.0 * (n0 + n1 + n2 + n3);
    };

    SimplexNoise.prototype.noise4 = function(x, y, z, w) {
      var F4, G4, W0, X0, Y0, Z0, c, c1, c2, c3, c4, c5, c6, gi0, gi1, gi2, gi3, gi4, grad4, i, i1, i2, i3, ii, j, j1, j2, j3, jj, k, k1, k2, k3, kk, l, l1, l2, l3, ll, n0, n1, n2, n3, n4, perm, s, simplex, t, t0, t1, t2, t3, t4, w0, w1, w2, w3, w4, x0, x1, x2, x3, x4, y0, y1, y2, y3, y4, z0, z1, z2, z3, z4;
      grad4 = this.grad4;
      simplex = this.simplex;
      perm = this.perm;
      F4 = (Math.sqrt(5.0) - 1.0) / 4.0;
      G4 = (5.0 - Math.sqrt(5.0)) / 20.0;
      n0 = 0;
      n1 = 0;
      n2 = 0;
      n3 = 0;
      n4 = 0;
      s = (x + y + z + w) * F4;
      i = Math.floor(x + s);
      j = Math.floor(y + s);
      k = Math.floor(z + s);
      l = Math.floor(w + s);
      t = (i + j + k + l) * G4;
      X0 = i - t;
      Y0 = j - t;
      Z0 = k - t;
      W0 = l - t;
      x0 = x - X0;
      y0 = y - Y0;
      z0 = z - Z0;
      w0 = w - W0;
      c1 = (x0 > y0 ? 32 : 0);
      c2 = (x0 > z0 ? 16 : 0);
      c3 = (y0 > z0 ? 8 : 0);
      c4 = (x0 > w0 ? 4 : 0);
      c5 = (y0 > w0 ? 2 : 0);
      c6 = (z0 > w0 ? 1 : 0);
      c = c1 + c2 + c3 + c4 + c5 + c6;
      i1 = 0;
      j1 = 0;
      k1 = 0;
      l1 = 0;
      i2 = 0;
      j2 = 0;
      k2 = 0;
      l2 = 0;
      i3 = 0;
      j3 = 0;
      k3 = 0;
      l3 = 0;
      i1 = (simplex[c][0] >= 3 ? 1 : 0);
      j1 = (simplex[c][1] >= 3 ? 1 : 0);
      k1 = (simplex[c][2] >= 3 ? 1 : 0);
      l1 = (simplex[c][3] >= 3 ? 1 : 0);
      i2 = (simplex[c][0] >= 2 ? 1 : 0);
      j2 = (simplex[c][1] >= 2 ? 1 : 0);
      k2 = (simplex[c][2] >= 2 ? 1 : 0);
      l2 = (simplex[c][3] >= 2 ? 1 : 0);
      i3 = (simplex[c][0] >= 1 ? 1 : 0);
      j3 = (simplex[c][1] >= 1 ? 1 : 0);
      k3 = (simplex[c][2] >= 1 ? 1 : 0);
      l3 = (simplex[c][3] >= 1 ? 1 : 0);
      x1 = x0 - i1 + G4;
      y1 = y0 - j1 + G4;
      z1 = z0 - k1 + G4;
      w1 = w0 - l1 + G4;
      x2 = x0 - i2 + 2.0 * G4;
      y2 = y0 - j2 + 2.0 * G4;
      z2 = z0 - k2 + 2.0 * G4;
      w2 = w0 - l2 + 2.0 * G4;
      x3 = x0 - i3 + 3.0 * G4;
      y3 = y0 - j3 + 3.0 * G4;
      z3 = z0 - k3 + 3.0 * G4;
      w3 = w0 - l3 + 3.0 * G4;
      x4 = x0 - 1.0 + 4.0 * G4;
      y4 = y0 - 1.0 + 4.0 * G4;
      z4 = z0 - 1.0 + 4.0 * G4;
      w4 = w0 - 1.0 + 4.0 * G4;
      ii = i & 255;
      jj = j & 255;
      kk = k & 255;
      ll = l & 255;
      gi0 = perm[ii + perm[jj + perm[kk + perm[ll]]]] % 32;
      gi1 = perm[ii + i1 + perm[jj + j1 + perm[kk + k1 + perm[ll + l1]]]] % 32;
      gi2 = perm[ii + i2 + perm[jj + j2 + perm[kk + k2 + perm[ll + l2]]]] % 32;
      gi3 = perm[ii + i3 + perm[jj + j3 + perm[kk + k3 + perm[ll + l3]]]] % 32;
      gi4 = perm[ii + 1 + perm[jj + 1 + perm[kk + 1 + perm[ll + 1]]]] % 32;
      t0 = 0.6 - x0 * x0 - y0 * y0 - z0 * z0 - w0 * w0;
      if (!(t0 < 0)) {
        t0 *= t0;
        n0 = t0 * t0 * dot(grad4[gi0], x0, y0, z0, w0);
      }
      t1 = 0.6 - x1 * x1 - y1 * y1 - z1 * z1 - w1 * w1;
      if (!(t1 < 0)) {
        t1 *= t1;
        n1 = t1 * t1 * dot(grad4[gi1], x1, y1, z1, w1);
      }
      t2 = 0.6 - x2 * x2 - y2 * y2 - z2 * z2 - w2 * w2;
      if (!(t2 < 0)) {
        t2 *= t2;
        n2 = t2 * t2 * dot(grad4[gi2], x2, y2, z2, w2);
      }
      t3 = 0.6 - x3 * x3 - y3 * y3 - z3 * z3 - w3 * w3;
      if (!(t3 < 0)) {
        t3 *= t3;
        n3 = t3 * t3 * dot(grad4[gi3], x3, y3, z3, w3);
      }
      t4 = 0.6 - x4 * x4 - y4 * y4 - z4 * z4 - w4 * w4;
      if (!(t4 < 0)) {
        t4 *= t4;
        n4 = t4 * t4 * dot(grad4[gi4], x4, y4, z4, w4);
      }
      return 27.0 * (n0 + n1 + n2 + n3 + n4);
    };

    return SimplexNoise;

  })(Noise);

  FlowNoise = (function(_super) {
    var TWO_PI, n;

    __extends(FlowNoise, _super);

    n = 128;

    TWO_PI = Math.PI * 2;

    FlowNoise.prototype.basis = [];

    FlowNoise.prototype.perm = [];

    function FlowNoise(rng) {
      var i, theta, _i;
      if (rng == null) {
        rng = Math;
      }
      for (i = _i = 0; 0 <= n ? _i <= n : _i >= n; i = 0 <= n ? ++_i : --_i) {
        theta = i * TWO_PI / n;
        this.basis[i] = [Math.cos(theta, Math.sin(theta))];
        this.perm[i] = i;
      }
      reinitialize((rng.random() * 1000) | 0);
    }

    FlowNoise.prototype.reinitialize = function(seed) {
      var i, j, _i, _results;
      _results = [];
      for (i = _i = 1; 1 <= n ? _i <= n : _i >= n; i = 1 <= n ? ++_i : --_i) {
        j = (Math.random() * seed) % (i + 1);
        _results.push(seed += 1);
      }
      return _results;
    };

    return FlowNoise;

  })(Noise);

  module.exports = {
    SimplexNoise: SimplexNoise
  };

}).call(this);

},{}],16:[function(require,module,exports){
// Generated by CoffeeScript 1.6.3
/*

 Random based on Jon Watte's mersenne twister package.
 Adds several utility methods under a unified interface, that'll allow to use
 different number generators at some point.
*/


(function() {
  var Random, mersenne, util;

  mersenne = require('mersenne');

  util = require('../util');

  Random = (function() {
    function Random(initialSeed) {
      if (initialSeed == null) {
        initialSeed = 0;
      }
      this.seed(initialSeed);
    }

    Random.prototype.seed = function(value) {
      this.seedValue = value;
      return mersenne.seed(this.seedValue);
    };

    Random.prototype.toString = function() {
      return "Random(" + this.seedValue + ")";
    };

    Random.prototype.random = function() {
      var k;
      k = 1000000;
      return mersenne.rand(k) / k;
    };

    Random.prototype.randi = function(min, max) {
      return Math.floor(this.random() * (max - min) + min);
    };

    Random.prototype.randf = function(min, max) {
      return this.random() * (max - min) + min;
    };

    Random.prototype.randn = function() {
      return this.random() * 2 - 1;
    };

    Random.prototype.flipCoin = function(chance) {
      if (chance == null) {
        chance = 0.5;
      }
      return this.random() < chance;
    };

    Random.prototype.shuffle = function(list) {
      return list.sort(function() {
        return 0.5 - this.random();
      });
    };

    Random.prototype.pick = function(list, count) {
      var i, indices, result, _i, _j, _ref;
      if (count == null) {
        count = 1;
      }
      switch (list.length) {
        case 0:
          return null;
        case 1:
          return list[0];
        default:
          if (count === 1) {
            return list[this.randi(0, list.length)];
          } else {
            indices = [];
            for (i = _i = 0, _ref = list.length; 0 <= _ref ? _i < _ref : _i > _ref; i = 0 <= _ref ? ++_i : --_i) {
              indices.push(i);
            }
            util.shuffle(indices);
            result = [];
            for (i = _j = 0; 0 <= count ? _j < count : _j > count; i = 0 <= count ? ++_j : --_j) {
              result.push(list[indices[i]]);
            }
            return result;
          }
      }
    };

    Random.prototype.pickAB = function(listA, listB, chance) {
      var list;
      list = this.next() < chance ? listA : listB;
      return this.pick(list);
    };

    return Random;

  })();

  module.exports.Random = Random;

}).call(this);

},{"../util":24,"mersenne":25}],17:[function(require,module,exports){
// Generated by CoffeeScript 1.6.3
(function() {
  var Rect, Vec2;

  Vec2 = require("./vector").Vec2;

  Rect = (function() {
    Rect.prototype.x1 = 0;

    Rect.prototype.y1 = 0;

    Rect.prototype.x2 = 0;

    Rect.prototype.y2 = 0;

    function Rect(x1, y1, x2, y2) {
      this.x1 = x1 != null ? x1 : 0;
      this.y1 = y1 != null ? y1 : 0;
      this.x2 = x2 != null ? x2 : 0;
      this.y2 = y2 != null ? y2 : 0;
    }

    Rect.prototype.center = function() {
      var x, y;
      x = Math.min(this.x1, this.x2) + this.width() * 0.5;
      y = Math.min(this.y1, this.y2) + this.height() * 0.5;
      return new Vec2(x, y);
    };

    Rect.prototype.width = function() {
      return Math.abs(this.x1 - this.x2);
    };

    Rect.prototype.height = function() {
      return Math.abs(this.y1 - this.y2);
    };

    return Rect;

  })();

  module.exports.Rect = Rect;

}).call(this);

},{"./vector":18}],18:[function(require,module,exports){
// Generated by CoffeeScript 1.6.3
(function() {
  var Vec2, Vec3;

  Vec2 = (function() {
    Vec2.prototype.x = 0;

    Vec2.prototype.y = 0;

    function Vec2(x, y) {
      this.x = x != null ? x : 0;
      this.y = y != null ? y : 0;
    }

    Vec2.prototype.set = function(v) {
      this.x = v.x;
      this.y = v.y;
      return this;
    };

    Vec2.prototype.set2 = function(x, y) {
      this.x = x;
      this.y = y;
      return this;
    };

    Vec2.prototype.zero = function() {
      this.x = this.y = 0;
      return this;
    };

    Vec2.prototype.add = function(v) {
      this.x += v.x;
      this.y += v.y;
      return this;
    };

    Vec2.prototype.add_ = function(v) {
      return new Vec2(this.x + v.x, this.y + v.y);
    };

    Vec2.prototype.sub = function(v) {
      this.x -= v.x;
      this.y -= v.y;
      return this;
    };

    Vec2.prototype.sub_ = function(v) {
      return new Vec2(this.x - v.x, this.y - v.y);
    };

    Vec2.prototype.mul = function(v) {
      this.x *= v.x;
      this.y *= v.y;
      return this;
    };

    Vec2.prototype.mul_ = function(v) {
      return new Vec2(this.x * v.x, this.y * v.y);
    };

    Vec2.prototype.div = function(v) {
      this.x /= v.x;
      this.y /= v.y;
      return this;
    };

    Vec2.prototype.div_ = function(v) {
      return new Vec2(this.x / v.x, this.y / v.y);
    };

    Vec2.prototype.scale = function(value) {
      this.x *= value;
      this.y *= value;
      return this;
    };

    Vec2.prototype.scale_ = function(value) {
      return new Vec2(this.x * value, this.y * value);
    };

    Vec2.prototype.length = function() {
      return Math.sqrt(this.x * this.x + this.y * this.y);
    };

    Vec2.prototype.lengthSquared = function() {
      return this.x * this.x + this.y * this.y;
    };

    Vec2.prototype.normalize = function() {
      var l;
      l = this.length();
      if (l !== 0) {
        this.x /= l;
        this.y /= l;
      }
      return this;
    };

    Vec2.prototype.normalize_ = function() {
      return this.clone().normalize();
    };

    Vec2.prototype.normalizeTo = function(length) {
      var magnitude;
      magnitude = Math.sqrt(this.x * this.x + this.y * this.y);
      if (magnitude > 0) {
        magnitude = length / magnitude;
        this.x *= magnitude;
        this.y *= magnitude;
      }
      return this;
    };

    Vec2.prototype.normalizeTo_ = function(length) {
      return this.clone().normalizeTo_(length);
    };

    Vec2.prototype.distance = function(v) {
      return Math.sqrt(this.distanceSquared2(v.x, v.y));
    };

    Vec2.prototype.distanceSquared = function(v) {
      return this.distanceSquared2(v.x, v.y);
    };

    Vec2.prototype.distanceSquared2 = function(x, y) {
      var dx, dy;
      dx = this.x - x;
      dy = this.y - y;
      return dx * dx + dy * dy;
    };

    Vec2.prototype.dot = function(v) {
      return this.x * v.x + this.y * v.y;
    };

    Vec2.prototype.rotate = function(angle) {
      var cosa, rx, ry, sina;
      sina = Math.sin(angle);
      cosa = Math.cos(angle);
      rx = this.x * cosa - this.y * sina;
      ry = this.x * sina + this.y * cosa;
      this.x = rx;
      return this.y = ry;
    };

    Vec2.prototype.jitter = function(amount) {
      this.x += (Math.random() * 2 - 1) * amount;
      this.y += (Math.random() * 2 - 1) * amount;
      return this;
    };

    Vec2.prototype.jitter_ = function(amount) {
      return (new Vec2(this.x, this.y)).jitter(amount);
    };

    Vec2.prototype.lerp = function(target, delta) {
      this.x = this.x * (1 - delta) + target.x * delta;
      this.y = this.y * (1 - delta) + target.y * delta;
      return this;
    };

    Vec2.prototype.lerp_ = function(target, delta) {
      return (new Vec2(this.x, this.y)).lerp(target, delta);
    };

    Vec2.prototype.equals = function(v) {
      return this.x === v.x && this.y === v.y;
    };

    Vec2.prototype.clone = function() {
      return new Vec2(this.x, this.y);
    };

    Vec2.prototype.toString = function() {
      return "Vec2(" + this.x + ", " + this.y + ")";
    };

    return Vec2;

  })();

  Vec3 = (function() {
    Vec3.prototype.x = 0;

    Vec3.prototype.y = 0;

    Vec3.prototype.z = 0;

    function Vec3(x, y, z) {
      this.x = x != null ? x : 0;
      this.y = y != null ? y : 0;
      this.z = z != null ? z : 0;
    }

    Vec3.prototype.set = function(v) {
      this.x = v.x;
      this.y = v.y;
      this.z = v.z;
      return this;
    };

    Vec3.prototype.set3 = function(x, y, z) {
      this.x = x;
      this.y = y;
      this.z = z;
      return this;
    };

    Vec3.prototype.zero = function() {
      this.x = this.y = this.z = 0;
      return this;
    };

    Vec3.prototype.add = function(v) {
      this.x += v.x;
      this.y += v.y;
      this.z += v.z;
      return this;
    };

    Vec3.prototype.add_ = function(v) {
      return new Vec3(this.x + v.x, this.y + v.y, this.z + v.z);
    };

    Vec3.prototype.sub = function(v) {
      this.x -= v.x;
      this.y -= v.y;
      this.z -= v.z;
      return this;
    };

    Vec3.prototype.sub_ = function(v) {
      return new Vec3(this.x - v.x, this.y - v.y, this.z - v.z);
    };

    Vec3.prototype.mul = function(v) {
      this.x *= v.x;
      this.y *= v.y;
      return this;
    };

    Vec3.prototype.mul_ = function(v) {
      return new Vec3(this.x * v.x, this.y * v.y, this.z * v.z);
    };

    Vec3.prototype.div = function(v) {
      this.x /= v.x;
      this.y /= v.y;
      this.z /= v.z;
      return this;
    };

    Vec3.prototype.div_ = function(v) {
      return new Vec3(this.x / v.x, this.y / v.y, this.z = v.z);
    };

    Vec3.prototype.scale = function(value) {
      this.x *= value;
      this.y *= value;
      this.z *= value;
      return this;
    };

    Vec3.prototype.scale_ = function(value) {
      return new Vec3(this.x * value, this.y * value, this.z * value);
    };

    Vec3.prototype.length = function() {
      return Math.sqrt(this.x * this.x + this.y * this.y + this.z * this.z);
    };

    Vec3.prototype.lengthSquared = function() {
      return this.x * this.x + this.y * this.y + this.z * this.z;
    };

    Vec3.prototype.normalize = function() {
      var l;
      l = this.length();
      if (l !== 0) {
        this.x /= l;
        this.y /= l;
        this.z /= l;
      }
      return this;
    };

    Vec3.prototype.normalize_ = function() {
      return this.clone().normalize();
    };

    Vec3.prototype.normalizeTo = function(length) {
      var magnitude;
      magnitude = Math.sqrt(this.x * this.x + this.y * this.y + this.z * this.z);
      if (magnitude > 0) {
        magnitude = length / magnitude;
        this.x *= magnitude;
        this.y *= magnitude;
      }
      return this;
    };

    Vec3.prototype.normalizeTo_ = function(length) {
      return this.clone().normalizeTo_(length);
    };

    Vec3.prototype.distance = function(v) {
      return Math.sqrt(this.distanceSquared3(v.x, v.y, v.z));
    };

    Vec3.prototype.distanceSquared = function(v) {
      return this.distanceSquared3(v.x, v.y, v.z);
    };

    Vec3.prototype.distanceSquared3 = function(x, y, z) {
      var dx, dy, dz;
      dx = this.x - x;
      dy = this.y - y;
      dz = this.z - z;
      return dx * dx + dy * dy + dz * dz;
    };

    Vec3.prototype.dot = function(v) {
      return this.x * v.x + this.y * v.y + this.z * v.z;
    };

    Vec3.prototype.jitter = function(amount) {
      this.x += (Math.random() * 2 - 1) * amount;
      this.y += (Math.random() * 2 - 1) * amount;
      this.z += (Math.random() * 2 - 1) * amount;
      return this;
    };

    Vec3.prototype.jitter_ = function(amount) {
      return (new Vec3(this.x, this.y, this.z)).jitter(amount);
    };

    Vec3.prototype.lerp = function(target, delta) {
      this.x = this.x * (1 - delta) + target.x * delta;
      this.y = this.y * (1 - delta) + target.y * delta;
      this.z = this.z * (1 - delta) + target.z * delta;
      return this;
    };

    Vec3.prototype.lerp_ = function(target, delta) {
      return (new Vec3(this.x, this.y, this.z)).lerp(target, delta);
    };

    Vec3.prototype.equals = function(v) {
      return this.x === v.x && this.y === v.y && this.z === v.z;
    };

    Vec3.prototype.clone = function() {
      return new Vec3(this.x, this.y, this.z);
    };

    Vec3.prototype.toString = function() {
      return "Vec3(" + this.x + ", " + this.y + ", " + this.z + ")";
    };

    return Vec3;

  })();

  module.exports = {
    Vec2: Vec2,
    Vec3: Vec3
  };

}).call(this);

},{}],19:[function(require,module,exports){
// Generated by CoffeeScript 1.6.3
(function() {
  var Attractor, Behaviour, Force, Vec2, Vec3, physics, vector,
    __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  vector = require('../math/vector');

  Vec2 = vector.Vec2;

  Vec3 = vector.Vec3;

  physics = require('./physics');

  Behaviour = physics.Behaviour;

  /*
  
    A constant force along a vector e.g. Gravity.
    Works in 2D + 3D.
  */


  Force = (function(_super) {
    var force;

    __extends(Force, _super);

    force = null;

    function Force(direction, weight) {
      this.direction = direction;
      this.weight = weight != null ? weight : 1;
    }

    Force.prototype.prepare = function() {
      return force = this.direction.normalizeTo(this.weight);
    };

    Force.prototype.apply = function(particle) {
      return particle.position.add(force);
    };

    Force.prototype.toString = function() {
      return "Force(" + force + ")";
    };

    return Force;

  })(Behaviour);

  /*
  
    Attracts each particle within range to a target point.
    Works in 2D + 3D.
  */


  Attractor = (function() {
    var rangeSq, tmp;

    tmp = null;

    rangeSq = 0;

    function Attractor(target, range, weight) {
      this.target = target;
      this.range = range;
      this.weight = weight != null ? weight : 1;
      tmp = this.target.clone();
    }

    Attractor.prototype.prepare = function() {
      return rangeSq = this.range * this.range;
    };

    Attractor.prototype.apply = function(particle) {
      var dist, distSq;
      tmp.set(this.target).sub(particle.position);
      distSq = tmp.lengthSquared();
      if (distSq > 0 && distSq < rangeSq) {
        dist = Math.sqrt(distSq);
        tmp.scale((1 / dist) * (1 - dist / this.range) * this.weight);
        return particle.force.add(tmp);
      }
    };

    Attractor.prototype.toString = function() {
      return "Attractor(" + this.target + ", " + this.range + ", " + this.weight + ")";
    };

    return Attractor;

  })();

  module.exports = {
    Force: Force,
    Attractor: Attractor
  };

}).call(this);

},{"../math/vector":18,"./physics":22}],20:[function(require,module,exports){
// Generated by CoffeeScript 1.6.3
(function() {
  var Area, Box, Collision, Constraint, Vec2, Vec3, Wrap2, Wrap3, physics, vector,
    __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  vector = require('../math/vector');

  Vec2 = vector.Vec2;

  Vec3 = vector.Vec3;

  physics = require('./physics');

  Constraint = physics.Constraint;

  /*
    Keeps the particle inside the given 3D box
  */


  Box = (function(_super) {
    __extends(Box, _super);

    function Box(min, max) {
      this.min = min != null ? min : new Vec3();
      this.max = max != null ? max : new Vec3(100, 100, 100);
    }

    Box.prototype.apply = function(particle) {
      var pos;
      pos = particle.position;
      if (pos.x < this.min.x) {
        pos.x = this.min.x;
      }
      if (pos.y < this.min.y) {
        pos.y = this.min.y;
      }
      if (pos.z < this.min.z) {
        pos.z = this.min.z;
      }
      if (pos.x > this.max.x) {
        pos.x = this.max.x;
      }
      if (pos.y > this.max.y) {
        pos.y = this.max.y;
      }
      if (pos.z > this.max.z) {
        return pos.z = this.max.z;
      }
    };

    Box.prototype.toString = function() {
      return "Box(" + this.min + ", " + this.max + ")";
    };

    return Box;

  })(Constraint);

  /*
    2D version of Box.
  */


  Area = (function(_super) {
    __extends(Area, _super);

    function Area(min, max) {
      this.min = min != null ? min : new Vec2();
      this.max = max != null ? max : new Vec2(100, 100);
    }

    Area.prototype.apply = function(particle) {
      var pos;
      pos = particle.position;
      if (pos.x < this.min.x) {
        pos.x = this.min.x;
      }
      if (pos.y < this.min.y) {
        pos.y = this.min.y;
      }
      if (pos.x > this.max.x) {
        pos.x = this.max.x;
      }
      if (pos.y > this.max.y) {
        return pos.y = this.max.y;
      }
    };

    Area.prototype.toString = function() {
      return "Area(" + this.min + ", " + this.max + ")";
    };

    return Area;

  })(Constraint);

  /*
    Keeps a particle within a certain 2D region by wrapping it around a given area.
  */


  Wrap2 = (function(_super) {
    var delta;

    __extends(Wrap2, _super);

    delta = new Vec2();

    function Wrap2(min, max) {
      this.min = min != null ? min : new Vec2();
      this.max = max != null ? max : new Vec2(100, 100);
    }

    Wrap2.prototype.prepare = function() {
      return delta.set(this.max).sub(this.min);
    };

    Wrap2.prototype.apply = function(particle) {
      var pos, prev;
      pos = particle.position;
      prev = particle.prev;
      if (pos.x < this.min.x) {
        pos.x += delta.x;
        prev.x += delta.x;
      }
      if (pos.y < this.min.y) {
        pos.y += delta.y;
        prev.y += delta.y;
      }
      if (pos.x > this.max.x) {
        pos.x -= delta.x;
        prev.x -= delta.x;
      }
      if (pos.y > this.max.y) {
        pos.y -= delta.y;
        return prev.y -= delta.y;
      }
    };

    Wrap2.prototype.toString = function() {
      return "Wrap2(" + this.min + ", " + this.max + ")";
    };

    return Wrap2;

  })(Constraint);

  /*
    Keeps a particle within a certain 2D region by wrapping it around a given area.
  */


  Wrap3 = (function(_super) {
    var delta;

    __extends(Wrap3, _super);

    delta = new Vec3();

    function Wrap3(min, max) {
      this.min = min != null ? min : new Vec3();
      this.max = max != null ? max : new Vec3(100, 100, 100);
    }

    Wrap3.prototype.prepare = function() {
      return delta.set(this.max).sub(this.min);
    };

    Wrap3.prototype.apply = function(particle) {
      var pos, prev;
      pos = particle.position;
      prev = particle.prev;
      if (pos.x < this.min.x) {
        pos.x += delta.x;
        prev.x += delta.x;
      }
      if (pos.y < this.min.y) {
        pos.y += delta.y;
        prev.y += delta.y;
      }
      if (pos.z < this.min.z) {
        pos.z += delta.z;
        prev.z += delta.z;
      }
      if (pos.x > this.max.x) {
        pos.x -= delta.x;
        prev.x -= delta.x;
      }
      if (pos.y > this.max.y) {
        pos.y -= delta.y;
        prev.y -= delta.y;
      }
      if (pos.z > this.max.z) {
        pos.z -= delta.z;
        return prev.z -= delta.z;
      }
    };

    Wrap3.prototype.toString = function() {
      return "Wrap3(" + this.min + ", " + this.max + ")";
    };

    return Wrap3;

  })(Constraint);

  /*
    Stops particles from colliding with each other.
  */


  Collision = (function(_super) {
    __extends(Collision, _super);

    Collision.prototype.physics = null;

    function Collision(physics, searchRadius) {
      this.physics = physics;
      this.searchRadius = searchRadius != null ? searchRadius : 100;
    }

    Collision.prototype.apply = function(particle) {
      var delta, dist, distSq, neighbour, neighbours, position, radius, radiusSq, _i, _len;
      position = particle.position;
      delta = position.clone();
      neighbours = this.physics.space.search(position, this.searchRadius);
      for (_i = 0, _len = neighbours.length; _i < _len; _i++) {
        neighbour = neighbours[_i];
        if (neighbour === particle) {
          continue;
        }
        delta.set(position).sub(neighbour.position);
        distSq = delta.lengthSquared();
        radius = particle.size + neighbour.size;
        radiusSq = radius * radius;
        if (distSq < radiusSq) {
          dist = Math.sqrt(distSq);
          delta.scale((dist - radius) / radius * 0.5);
          particle.position.sub(delta);
          neighbour.position.add(delta);
        }
        void 0;
      }
      return void 0;
    };

    Collision.prototype.toString = function() {
      return "Collision(" + this.searchRadius + ")";
    };

    return Collision;

  })(Constraint);

  module.exports = {
    Box: Box,
    Area: Area,
    Wrap2: Wrap2,
    Wrap3: Wrap3,
    Collision: Collision
  };

}).call(this);

},{"../math/vector":18,"./physics":22}],21:[function(require,module,exports){
// Generated by CoffeeScript 1.6.3
(function() {
  var Particle, Particle2, Particle3, State, Vec2, Vec3, vector,
    __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  vector = require('../math/vector');

  Vec2 = vector.Vec2;

  Vec3 = vector.Vec3;

  State = {
    ALIVE: 0,
    LOCKED: 1,
    IDLE: 2,
    DEAD: 3
  };

  /*
  
    VerLer Particle Baseclass
  
    FIELD flavoured particle integrator.
    Supports Verlet-style integration for 'strict' relationships e.g. Springs + Constraints
    and also Euler-style continous force integration for smooth/ flowing behaviour e.g. Flocking
  */


  Particle = (function() {
    Particle.prototype.id = 0;

    Particle.prototype.state = State.ALIVE;

    Particle.prototype.age = 0;

    Particle.prototype.lifetime = -1;

    Particle.prototype.position = null;

    Particle.prototype.drag = 0.03;

    Particle.prototype.prev = null;

    Particle.prototype.force = null;

    Particle.prototype.velocity = null;

    function Particle(id) {
      this.id = id;
      this.size = 1;
      this.isLocked = false;
    }

    Particle.prototype.clearVelocity = function() {
      return this.prev.set(this.position);
    };

    Particle.prototype.scaleVelocity = function(amount) {
      return this.prev.lerp(this.position, 1.0 - amount);
    };

    Particle.prototype.setPosition = function(v) {
      this.position.set(v);
      return this.prev.set(v);
    };

    Particle.prototype.lock = function() {
      return this.state = State.LOCKED;
    };

    Particle.prototype.unlock = function() {
      return this.state = State.ALIVE;
    };

    Particle.prototype.die = function() {
      return this.state = State.DEAD;
    };

    Particle.prototype.idle = function() {
      return this.state = State.IDLE;
    };

    Particle.prototype.toString = function() {
      return "Particle(" + this.position + ")";
    };

    return Particle;

  })();

  /*
  
    3D VerLer Particle
  */


  Particle3 = (function(_super) {
    var tmp;

    __extends(Particle3, _super);

    tmp = new Vec3();

    function Particle3(id) {
      this.id = id;
      this.position = new Vec3();
      this.prev = new Vec3();
      this.force = new Vec3();
      this.velocity = new Vec3();
    }

    Particle3.prototype.update = function() {
      this.age++;
      if (this.state > State.ALIVE) {
        return;
      }
      if (this.lifetime > 0 && this.age === this.lifetime) {
        this.state = State.DEAD;
      }
      tmp.set(this.position);
      this.position.x += (this.position.x - this.prev.x) + this.force.x;
      this.position.y += (this.position.y - this.prev.y) + this.force.y;
      this.position.z += (this.position.z - this.prev.z) + this.force.z;
      this.prev.set(tmp);
      this.prev.lerp(this.position, this.drag);
      return this.force.zero();
    };

    Particle3.prototype.setPosition3 = function(x, y, z) {
      this.position.set3(x, y, z);
      return this.prev.set3(x, y, z);
    };

    return Particle3;

  })(Particle);

  /*
  
    2D VerLer Particle
  */


  Particle2 = (function(_super) {
    var tmp;

    __extends(Particle2, _super);

    tmp = new Vec2();

    function Particle2(id) {
      this.id = id;
      this.position = new Vec2();
      this.prev = new Vec2();
      this.force = new Vec2();
      this.velocity = new Vec2();
    }

    Particle2.prototype.update = function() {
      this.age++;
      if (this.state > State.ALIVE) {
        return;
      }
      if (this.lifetime > 0 && this.age === this.lifetime) {
        this.state = State.DEAD;
      }
      tmp.set(this.position);
      this.position.x += (this.position.x - this.prev.x) + this.force.x;
      this.position.y += (this.position.y - this.prev.y) + this.force.y;
      this.prev.set(tmp);
      this.prev.lerp(this.position, this.drag);
      return this.force.zero();
    };

    Particle2.prototype.setPosition2 = function(x, y) {
      this.position.set2(x, y);
      return this.prev.set2(x, y);
    };

    return Particle2;

  })(Particle);

  module.exports = {
    Particle: Particle,
    Particle2: Particle2,
    Particle3: Particle3,
    State: State
  };

}).call(this);

},{"../math/vector":18}],22:[function(require,module,exports){
// Generated by CoffeeScript 1.6.3
(function() {
  var Behaviour, Constraint, Emitter, Particle, Physics, Space, Spring, particleModule, util, _ref,
    __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  util = require('../util');

  particleModule = require('./particle');

  Particle = particleModule.Particle;

  /*
  
    A Particle Physics Simulation
  */


  Physics = (function() {
    function Physics() {
      this.particles = [];
      this.springs = [];
      this.behaviours = [];
      this.constraints = [];
      this.emitter = null;
      this.space = null;
      this.constraintIterations = 1;
      this.springIterations = 1;
      this.space = new Space();
      this.emitter = new Emitter(this);
      this.clear();
    }

    Physics.prototype.clear = function() {
      this.particles = [];
      this.behaviours = [];
      return this.constraints = [];
    };

    Physics.prototype.add = function() {
      var arg, state;
      if (arguments.length === 0) {
        return;
      }
      arg = arguments[0];
      if (arg instanceof Particle) {
        this.addParticle(arg);
      } else if (arg instanceof Behaviour) {
        console.log("adding behaviour " + arg);
        if (arguments.length > 1) {
          state = arguments[1];
        }
        this.addBehaviour(arg, state);
      } else if (arg instanceof Spring) {
        console.log("adding spring " + arg);
        this.addSpring(arg);
      } else {
        "Cannot add " + arg;
      }
      return arg;
    };

    Physics.prototype.addParticle = function(particle) {
      this.particles.push(particle);
      return particle;
    };

    Physics.prototype.addSpring = function(spring) {
      this.springs.push(spring);
      return spring;
    };

    Physics.prototype.addBehaviour = function(effector, state) {
      var list;
      if (state == null) {
        state = particleModule.State.ALIVE;
      }
      list = effector instanceof Constraint ? this.constraints : this.behaviours;
      if (!list[state]) {
        list[state] = [];
      }
      return list[state].push(effector);
    };

    Physics.prototype.update = function() {
      var dead, i, j, particle, particles, spring, stateDead, _i, _j, _k, _l, _len, _len1, _ref, _ref1, _ref2;
      this.emitter.update();
      this.space.update(this);
      particles = this.particles;
      this.applyEffectors(this.behaviours, particles);
      for (j = _i = 0, _ref = this.constraintIterations; 0 <= _ref ? _i <= _ref : _i >= _ref; j = 0 <= _ref ? ++_i : --_i) {
        this.applyEffectors(this.constraints, particles);
        for (i = _j = 0, _ref1 = this.springIterations; 0 <= _ref1 ? _j <= _ref1 : _j >= _ref1; i = 0 <= _ref1 ? ++_j : --_j) {
          _ref2 = this.springs;
          for (_k = 0, _len = _ref2.length; _k < _len; _k++) {
            spring = _ref2[_k];
            spring.update();
          }
        }
      }
      dead = [];
      stateDead = particleModule.State.DEAD;
      for (_l = 0, _len1 = particles.length; _l < _len1; _l++) {
        particle = particles[_l];
        particle.update();
        if (particle.state === stateDead) {
          dead.push(particle);
        }
        void 0;
      }
      i = dead.length;
      while (i--) {
        particle = dead[i];
        util.removeElement(particle, particles);
        void 0;
      }
      return void 0;
    };

    Physics.prototype.applyEffectors = function(effectors, particles) {
      var effector, particle, state, stateEffectors, _i, _j, _len, _len1;
      state = effectors.length;
      while (state--) {
        stateEffectors = effectors[state];
        for (_i = 0, _len = stateEffectors.length; _i < _len; _i++) {
          effector = stateEffectors[_i];
          effector.prepare(this);
          for (_j = 0, _len1 = particles.length; _j < _len1; _j++) {
            particle = particles[_j];
            if (particle.state === state && !particle.isLocked) {
              effector.apply(particle);
            }
            void 0;
          }
          void 0;
        }
        void 0;
      }
      return void 0;
    };

    Physics.prototype.size = function() {
      return this.particles.length;
    };

    return Physics;

  })();

  /*
    Spatial Search
  
    Simple brute force spatial searches.
    Subclasses may override this to organise particles so they can be found quicker later
  */


  Space = (function() {
    var physics;

    physics = null;

    function Space() {}

    Space.prototype.update = function(physics_) {
      return physics = physics_;
    };

    Space.prototype.search = function(center, radius) {
      var particle, radiusSq, result, _i, _len, _ref;
      result = [];
      radiusSq = radius * radius;
      _ref = physics.particles;
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        particle = _ref[_i];
        if (center.distanceSquared(particle.position) < radiusSq) {
          result.push(particle);
        }
      }
      return result;
    };

    return Space;

  })();

  /*
    Particle Emitter
  */


  Emitter = (function() {
    var id, timer;

    timer = -1;

    id = 0;

    function Emitter(physics) {
      this.physics = physics;
      this.rate = 1;
      this.interval = 1;
      this.max = 100;
      this.type = particleModule.Particle3;
    }

    Emitter.prototype.update = function() {
      var i, p;
      if (timer === -1 || timer >= this.interval) {
        timer = 0;
        i = 0;
        while (i < this.rate && this.physics.size() < this.max) {
          p = this.create();
          this.init(p);
          i++;
        }
      }
      return timer++;
    };

    Emitter.prototype.create = function() {
      var p;
      p = new this.type(id++);
      this.physics.addParticle(p);
      return p;
    };

    Emitter.prototype.init = function(particle) {};

    return Emitter;

  })();

  /*
    Base class for all physical forces, behaviours & constraints
  */


  Behaviour = (function() {
    function Behaviour() {}

    Behaviour.prototype.prepare = function() {};

    Behaviour.prototype.apply = function(particle) {};

    return Behaviour;

  })();

  Constraint = (function(_super) {
    __extends(Constraint, _super);

    function Constraint() {
      _ref = Constraint.__super__.constructor.apply(this, arguments);
      return _ref;
    }

    Constraint.prototype.prepare = function() {};

    Constraint.prototype.apply = function(particle) {};

    return Constraint;

  })(Behaviour);

  /*
    Verlet Spring
  */


  Spring = (function() {
    function Spring(a, b, strength) {
      this.a = a;
      this.b = b;
      this.strength = strength != null ? strength : 0.5;
      this.restLength = this.a.position.distance(this.b.position);
    }

    Spring.prototype.update = function() {
      var delta, dist, normDistStrength;
      delta = this.b.position.sub_(this.a.position);
      dist = delta.length() + Number.MIN_VALUE;
      normDistStrength = (dist - this.restLength) / dist * this.strength;
      if (normDistStrength === 0) {
        return;
      }
      delta.scale(normDistStrength);
      if (!this.a.isLocked) {
        this.a.position.add(delta);
      }
      if (!this.b.isLocked) {
        return this.b.position.sub(delta);
      }
    };

    Spring.prototype.toString = function() {
      return "Spring(" + a + ", " + b + ")";
    };

    return Spring;

  })();

  module.exports = {
    Physics: Physics,
    Emitter: Emitter,
    Space: Space,
    Behaviour: Behaviour,
    Constraint: Constraint,
    Spring: Spring
  };

}).call(this);

},{"../util":24,"./particle":21}],23:[function(require,module,exports){
// Generated by CoffeeScript 1.6.3
(function() {
  var Tempo, Time, Timer, Timespan, math, util,
    __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  util = require('./util');

  math = require('./math/math');

  /*
    Timer: keeps track of time, measures intervals etc.
    ------------------------------------------------------------------------------
  */


  Timer = (function() {
    Timer.prototype.now = null;

    Timer.prototype.prev = null;

    function Timer() {
      this.reset();
    }

    Timer.prototype.update = function() {
      var dt;
      this.now = Date.now();
      dt = this.now - this.prev;
      this.prev = this.now;
      return dt;
    };

    Timer.prototype.elapsed = function() {
      return Date.now() - this.prev;
    };

    Timer.prototype.reset = function() {
      return this.now = this.prev = Date.now();
    };

    return Timer;

  })();

  /*
    Tempo: keeps track of rhythm, converts between beats, bars, time, tempo etc
    ------------------------------------------------------------------------------
  */


  Tempo = (function() {
    Tempo.prototype.bpm = 120;

    Tempo.prototype.sigNum = 4;

    Tempo.prototype.sigDenom = 4;

    Tempo.prototype.resolution = 32;

    Tempo.prototype.beatInterval = 0;

    Tempo.prototype.gridInterval = 0;

    Tempo.prototype.time = 0;

    Tempo.prototype.prevEvent = 0;

    Tempo.prototype.beats = 0;

    Tempo.prototype.bars = 0;

    Tempo.prototype.beat = 0;

    Tempo.prototype.onBeat = false;

    Tempo.prototype.onBar = false;

    Tempo.prototype.on64 = false;

    Tempo.prototype.on32 = false;

    Tempo.prototype.on16 = false;

    Tempo.prototype.on8 = false;

    Tempo.prototype.on4 = false;

    Tempo.prototype.on2 = false;

    function Tempo(bpm, sigNum, sigDenom, resolution) {
      this.bpm = bpm != null ? bpm : 120;
      this.sigNum = sigNum != null ? sigNum : 4;
      this.sigDenom = sigDenom != null ? sigDenom : 4;
      this.resolution = resolution != null ? resolution : 32;
      this.reset();
    }

    Tempo.prototype.reset = function() {
      this.beatInterval = 1000 / (this.bpm / 60);
      this.gridInterval = this.beatInterval * this.sigNum / this.resolution;
      this.time = this.prevEvent = 0;
      this.beats = 0;
      this.beat = this.bars = 0;
      this.onBeat = this.onBar = false;
      return this.on64 = this.on32 = this.on16 = this.on8 = this.on4 = this.on2 = false;
    };

    Tempo.prototype.update = function(dt) {
      var forceOnGrid;
      forceOnGrid = this.time - this.prevEvent >= this.gridInterval;
      this.setTime(this.time + dt, forceOnGrid);
      return this.beat;
    };

    Tempo.prototype.setTime = function(time, forceOnGrid) {
      var gridUnits, r, u;
      this.time = time;
      if (this.time % this.gridInterval === 0 || forceOnGrid) {
        this.prevEvent = time;
        gridUnits = Math.floor(this.time / this.gridInterval);
        u = gridUnits;
        r = this.resolution;
        this.beats = Math.floor(this.time / this.beatInterval);
        this.bars = Math.floor(this.beats / this.sigDenom);
        this.beat = this.beats % this.sigNum;
        this.onBeat = u % (r / this.sigNum) === 0;
        this.onBar = (u % this.resolution) === 0;
        this.on64 = u % (r / 64) === 0;
        this.on32 = u % (r / 32) === 0;
        this.on16 = u % (r / 16) === 0;
        this.on8 = u % (r / 8) === 0;
        this.on4 = u % (r / 4) === 0;
        this.on2 = u % (r / 2) === 0;
      } else {
        this.onBeat = this.onBar = false;
        this.on64 = this.on32 = this.on16 = this.on8 = this.on4 = this.on2 = false;
      }
      return this.beat;
    };

    return Tempo;

  })();

  /*
    Time: Represents a single moment in time
    ------------------------------------------------------------------------------
  */


  Time = (function(_super) {
    __extends(Time, _super);

    Time.prototype.value = 0;

    function Time(arg, fps, tempo) {
      this.set(arg, fps, tempo);
    }

    Time.prototype.set = function(arg, fps, tempo) {
      if (arg == null) {
        arg = 0;
      }
      return this.value = (function() {
        switch (typeof arg) {
          case 'number':
            return arg;
          case 'string':
            return this["eval"](arg, fps, tempo);
          default:
            return arg.value;
        }
      }).call(this);
    };

    Time.prototype.add = function(time) {
      return this.value += typeof time === 'number' ? time : time.value;
    };

    Time.prototype.add_ = function(time) {
      return new Time(this.value + (typeof time === 'number' ? time : time.value));
    };

    Time.prototype.sub = function(time) {
      return this.time -= typeof time === 'number' ? time : time.value;
    };

    Time.prototype.sub_ = function(time) {
      return new Time(this.value - (typeof time === 'number' ? time : time.value));
    };

    Time.prototype.scale = function(factor) {
      return this.value *= factor;
    };

    Time.prototype.scale_ = function(factor) {
      return new Time(this.value * factor);
    };

    Time.prototype.clone = function() {
      return new Time(this.value);
    };

    Time.prototype.equals = function(time) {
      return this.value === (typeof time === 'number' ? time : time.value);
    };

    Time.prototype.toString = function() {
      return "" + this.value + "ms";
    };

    Time.prototype.normalizedTo = function(span) {
      return math.fit(this.value, span.from.value, span.to.value, 0, 1);
    };

    Time.set('s', function(seconds) {
      return this.value = seconds * 1000;
    });

    Time.get('s', function() {
      return this.value / 1000;
    });

    Time.prototype.toFrame = function(fps) {
      if (fps == null) {
        fps = 60;
      }
      return Math.floor(this.value / (1000 / fps));
    };

    Time.prototype["eval"] = function(string, fps, tempo) {
      var interval, re, unit, units, _i, _len;
      if (tempo == null) {
        tempo = null;
      }
      units = [
        {
          symbol: 'ms',
          factor: 1
        }, {
          symbol: 's',
          factor: 1000
        }, {
          symbol: 'm',
          factor: 60000
        }, {
          symbol: 'f',
          factor: 1000 / fps
        }
      ];
      if (tempo != null) {
        interval = tempo.gridInterval;
        units.push({
          symbol: 'i',
          factor: interval
        });
        units.push({
          symbol: 'n',
          factor: interval * tempo.resolution
        });
      }
      for (_i = 0, _len = units.length; _i < _len; _i++) {
        unit = units[_i];
        re = new RegExp("\\d+(?=" + unit.symbol + ")", "g");
        string = string.replace(re, function(value) {
          return value * unit.factor;
        });
        re = new RegExp(unit.symbol, "g");
        string = string.replace(re, '');
      }
      return eval(string);
    };

    Time.s = function(seconds) {
      return new Time(seconds * 1000);
    };

    Time.ms = function(milliseconds) {
      return new Time(milliseconds);
    };

    Time.f = function(frame, fps) {
      return new Time(frame / (1000 / fps));
    };

    Time.i = function(intervals, tempo) {
      return new Time(intervals * tempo.gridInterval);
    };

    Time.str = function(string, fps, tempo) {
      return new Time(string, fps, tempo);
    };

    return Time;

  })(util.EXObject);

  /*
    Timespan: Represents a duration of time between two moments
    ------------------------------------------------------------------------------
  */


  Timespan = (function(_super) {
    __extends(Timespan, _super);

    function Timespan() {
      switch (arguments.length) {
        case 0:
          this.from = new Time(0);
          this.to = new Time(0);
          break;
        case 1:
          this.from = new Time(0);
          this.to = new Time(arguments[0]);
          break;
        case 2:
          this.from = new Time(arguments[0]);
          this.to = new Time(arguments[1]);
      }
    }

    Timespan.prototype.segmentByInterval = function(interval, snapEnd) {
      var current, halfInterval, last, segments;
      if (snapEnd == null) {
        snapEnd = false;
      }
      interval = typeof interval === 'number' ? new Time(interval) : interval;
      segments = [];
      current = new Timespan(this.from, this.from.add_(interval));
      segments.push(current.clone());
      while (current.to.value < this.to.value) {
        current.from.add(interval);
        current.to.add(interval);
        segments.push(current.clone());
      }
      if (snapEnd && segments.length > 0) {
        last = segments[segments.length - 1];
        halfInterval = interval.scale_(0.5);
        if (this.to.value - last.to.value > halfInterval) {
          segments.push(new Timespan(last.clone(), this.to.clone()));
        } else {
          last.to.value = this.to.value;
        }
      }
      return segments;
    };

    Timespan.prototype.overlaps = function(other) {
      var from, from2, to, to2;
      from = this.from.value;
      to = this.to.value;
      from2 = other.from.value;
      to2 = other.to.value;
      return (to2 >= from && to2 <= to) || (from2 >= from && from2 <= to) || (from2 <= from && to2 >= to);
    };

    Timespan.prototype.clone = function() {
      return new Timespan(this.from, this.to);
    };

    Timespan.prototype.toString = function() {
      return "Timespan(" + this.from + " - " + this.to + ")";
    };

    Timespan.set('length', function(length) {
      return this.to.value = this.from.value + length;
    });

    Timespan.get('length', function() {
      return new Time(this.to.value - this.from.value);
    });

    Timespan.get('s', function() {
      return this.length.s;
    });

    Timespan.s = function(seconds) {
      return new Timespan(seconds * 1000);
    };

    Timespan.str = function(string, fps, tempo) {
      var from, times, to;
      times = string.split('..');
      if (times.length !== 2) {
        throw "Invalid argument: " + string;
      }
      from = Time.str(times[0], fps, tempo);
      to = Time.str(times[1], fps, tempo);
      return new Timespan(from, to);
    };

    return Timespan;

  })(util.EXObject);

  module.exports = {
    Timer: Timer,
    Tempo: Tempo,
    Time: Time,
    Timespan: Timespan
  };

}).call(this);

},{"./math/math":14,"./util":24}],24:[function(require,module,exports){
(function (Buffer){
// Generated by CoffeeScript 1.6.3
(function() {
  var EXObject, Mixin, clone, extend, removeElement, shuffle, util,
    __indexOf = [].indexOf || function(item) { for (var i = 0, l = this.length; i < l; i++) { if (i in this && this[i] === item) return i; } return -1; };

  util = require("util");

  /*
    JavaScript Language Utilities
    ------------------------------------------------------------------------------------------
  */


  extend = function(obj, source) {
    var i, il, keys, prop, safeHasOwnProperty;
    if (Object.keys) {
      keys = Object.keys(source);
      i = 0;
      il = keys.length;
      while (i < il) {
        prop = keys[i];
        Object.defineProperty(obj, prop, Object.getOwnPropertyDescriptor(source, prop));
        i++;
      }
    } else {
      safeHasOwnProperty = {}.hasOwnProperty;
      for (prop in source) {
        if (safeHasOwnProperty.call(source, prop)) {
          obj[prop] = source[prop];
        }
      }
    }
    return obj;
  };

  /*
  
    Swappable Mixins in CoffeeScript
  
  
    Many thanks to Hashmal, who wrote this to start.
    https://gist.github.com/803816/aceed8fc57188c3a19ce2eccdb25acb64f2be94e
  
    Usage
    -----
  
    class Derp extends Mixin
      setup: ->
        @googly = "eyes"
  
    derp: ->
      alert "Herp derp! What's with your #{ @googly }?"
  
    class Herp
      constructor: ->
        Derp::augment this
  
    herp = new Herp
    herp.derp()
  
    Mixin
    -----
  
    Classes inheriting `Mixin` will become removable mixins, enabling you to
    swap them around.
  
    Limitations
    -----------
  
    * When a class is augmented, all instances of that class are augmented too,
      and when a mixin is ejected from a class, all instances lose that mixin too.
    * You can't eject a mixin from an object if that mixin was added to the object's class. Eject the mixin from the class instead.
  */


  Mixin = (function() {
    function Mixin() {}

    Mixin.prototype.augment = function(t) {
      var m, n;
      for (n in this) {
        m = this[n];
        if (!(n === 'augment' || (this[n].prototype == null))) {
          t[n] = m;
        }
      }
      return t.setup();
    };

    Mixin.prototype.eject = function(mixin) {
      var m, n, o, p, _results;
      _results = [];
      for (n in this) {
        m = this[n];
        _results.push(__indexOf.call((function() {
          var _ref, _results1;
          _ref = mixin.prototype;
          _results1 = [];
          for (o in _ref) {
            p = _ref[o];
            _results1.push(p);
          }
          return _results1;
        })(), m) >= 0 ? delete this[n] : void 0);
      }
      return _results;
    };

    Mixin.prototype.setup = function() {};

    return Mixin;

  })();

  /*
  
    Clones (copies) an Object using deep copying.
  
  
    This function supports circular references by default, but if you are certain
    there are no circular references in your object, you can save some CPU time
    by calling clone(obj, false).
  
    Caution: if `circular` is false and `parent` contains circular references,
    your program may enter an infinite loop and crash.
  
    @param `parent` - the object to be cloned
    @param `circular` - set to true if the object to be cloned may contain
    circular references. (optional - true by default)
  */


  clone = function(parent, circular) {
    var c, child, circularParent, circularReplace, circularResolved, cloned, i, useBuffer, _clone;
    if (typeof circular === "undefined") {
      circular = true;
    }
    useBuffer = typeof Buffer !== "undefined";
    i = void 0;
    if (circular) {
      _clone = function(parent, context, child, cIndex) {
        i = void 0;
        if (typeof parent === "object") {
          if (parent == null) {
            return parent;
          }
          for (i in circularParent) {
            if (circularParent[i] === parent) {
              circularReplace.push({
                resolveTo: i,
                child: child,
                i: cIndex
              });
              return null;
            }
          }
          circularParent[context] = parent;
          if (util.isArray(parent)) {
            child = [];
            for (i in parent) {
              child[i] = _clone(parent[i], context + "[" + i + "]", child, i);
            }
          } else if (util.isDate(parent)) {
            child = new Date(parent.getTime());
          } else if (util.isRegExp(parent)) {
            child = new RegExp(parent.source);
          } else if (useBuffer && Buffer.isBuffer(parent)) {
            child = new Buffer(parent.length);
            parent.copy(child);
          } else {
            child = {};
            child.__proto__ = parent.__proto__;
            for (i in parent) {
              child[i] = _clone(parent[i], context + "[" + i + "]", child, i);
            }
          }
          circularResolved[context] = child;
        } else {
          child = parent;
        }
        return child;
      };
      circularParent = {};
      circularResolved = {};
      circularReplace = [];
      cloned = _clone(parent, "*");
      for (i in circularReplace) {
        c = circularReplace[i];
        if (c && c.child && c.i in c.child) {
          c.child[c.i] = circularResolved[c.resolveTo];
        }
      }
      return cloned;
    } else {
      child = void 0;
      if (typeof parent === "object") {
        if (parent == null) {
          return parent;
        }
        if (parent.constructor.name === "Array") {
          child = [];
          for (i in parent) {
            child[i] = clone(parent[i], circular);
          }
        } else if (util.isDate(parent)) {
          child = new Date(parent.getTime());
        } else if (!util.isRegExp(parent)) {
          child = {};
          child.__proto__ = parent.__proto__;
          for (i in parent) {
            child[i] = clone(parent[i], circular);
          }
        }
      } else {
        child = parent;
      }
      return child;
    }
  };

  clone.clonePrototype = function(parent) {
    var ctor;
    if (parent === null) {
      return null;
    }
    ctor = function() {};
    ctor.prototype = parent;
    return new ctor();
  };

  /*
  
  JavaScript getter and setter support for CoffeeScript classes
  Ref: https://github.com/jashkenas/coffee-script/issues/1039
  
  Note:
  Classes using this wont work under IE6 + IE7
  
  Usage:
  class Vector3D extends EXObject
    constructor: (@x, @y, @z) ->
  
    @get 'x', -> @[0]
    @get 'y', -> @[1]
    @get 'z', -> @[2]
  
    @set 'x', (x) -> @[0] = x
    @set 'y', (y) -> @[1] = y
    @set 'z', (z) -> @[2] = z
  */


  EXObject = (function() {
    function EXObject() {}

    EXObject.get = function(propertyName, func) {
      return Object.defineProperty(this.prototype, propertyName, {
        configurable: true,
        enumerable: true,
        get: func
      });
    };

    EXObject.set = function(propertyName, func) {
      return Object.defineProperty(this.prototype, propertyName, {
        configurable: true,
        enumerable: true,
        set: func
      });
    };

    return EXObject;

  })();

  /*
    Array Utilities
    ------------------------------------------------------------------------------------------
  */


  removeElement = function(element, list) {
    var index;
    index = list.indexOf(element);
    if (index !== -1) {
      list.splice(index, 1);
    }
    return list;
  };

  shuffle = function(object, rng) {
    var i, j, x;
    if (rng == null) {
      rng = Math;
    }
    i = object.length;
    while (i) {
      j = parseInt(rng.random() * i);
      x = object[--i];
      object[i] = object[j];
      object[j] = x;
    }
    return object;
  };

  module.exports = {
    extend: extend,
    clone: clone,
    EXObject: EXObject,
    Mixin: Mixin,
    removeElement: removeElement,
    shuffle: shuffle
  };

}).call(this);

}).call(this,require("buffer").Buffer)
},{"buffer":1,"util":8}],25:[function(require,module,exports){
// this program is a JavaScript version of Mersenne Twister, with concealment and encapsulation in class,
// an almost straight conversion from the original program, mt19937ar.c,
// translated by y. okada on July 17, 2006.
// and modified a little at july 20, 2006, but there are not any substantial differences.
// in this program, procedure descriptions and comments of original source code were not removed.
// lines commented with //c// were originally descriptions of c procedure. and a few following lines are appropriate JavaScript descriptions.
// lines commented with /* and */ are original comments.
// lines commented with // are additional comments in this JavaScript version.
// before using this version, create at least one instance of MersenneTwister19937 class, and initialize the each state, given below in c comments, of all the instances.
/*
   A C-program for MT19937, with initialization improved 2002/1/26.
   Coded by Takuji Nishimura and Makoto Matsumoto.

   Before using, initialize the state by using init_genrand(seed)
   or init_by_array(init_key, key_length).

   Copyright (C) 1997 - 2002, Makoto Matsumoto and Takuji Nishimura,
   All rights reserved.

   Redistribution and use in source and binary forms, with or without
   modification, are permitted provided that the following conditions
   are met:

     1. Redistributions of source code must retain the above copyright
        notice, this list of conditions and the following disclaimer.

     2. Redistributions in binary form must reproduce the above copyright
        notice, this list of conditions and the following disclaimer in the
        documentation and/or other materials provided with the distribution.

     3. The names of its contributors may not be used to endorse or promote
        products derived from this software without specific prior written
        permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
   "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
   A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
   CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
   EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
   PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
   PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
   LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
   NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
   SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


   Any feedback is very welcome.
   http://www.math.sci.hiroshima-u.ac.jp/~m-mat/MT/emt.html
   email: m-mat @ math.sci.hiroshima-u.ac.jp (remove space)
*/

function MersenneTwister19937()
{
	/* constants should be scoped inside the class */
	var N, M, MATRIX_A, UPPER_MASK, LOWER_MASK;
	/* Period parameters */
	//c//#define N 624
	//c//#define M 397
	//c//#define MATRIX_A 0x9908b0dfUL   /* constant vector a */
	//c//#define UPPER_MASK 0x80000000UL /* most significant w-r bits */
	//c//#define LOWER_MASK 0x7fffffffUL /* least significant r bits */
	N = 624;
	M = 397;
	MATRIX_A = 0x9908b0df;   /* constant vector a */
	UPPER_MASK = 0x80000000; /* most significant w-r bits */
	LOWER_MASK = 0x7fffffff; /* least significant r bits */
	//c//static unsigned long mt[N]; /* the array for the state vector  */
	//c//static int mti=N+1; /* mti==N+1 means mt[N] is not initialized */
	var mt = new Array(N);   /* the array for the state vector  */
	var mti = N+1;           /* mti==N+1 means mt[N] is not initialized */

	function unsigned32 (n1) // returns a 32-bits unsiged integer from an operand to which applied a bit operator.
	{
		return n1 < 0 ? (n1 ^ UPPER_MASK) + UPPER_MASK : n1;
	}

	function subtraction32 (n1, n2) // emulates lowerflow of a c 32-bits unsiged integer variable, instead of the operator -. these both arguments must be non-negative integers expressible using unsigned 32 bits.
	{
		return n1 < n2 ? unsigned32((0x100000000 - (n2 - n1)) & 0xffffffff) : n1 - n2;
	}

	function addition32 (n1, n2) // emulates overflow of a c 32-bits unsiged integer variable, instead of the operator +. these both arguments must be non-negative integers expressible using unsigned 32 bits.
	{
		return unsigned32((n1 + n2) & 0xffffffff)
	}

	function multiplication32 (n1, n2) // emulates overflow of a c 32-bits unsiged integer variable, instead of the operator *. these both arguments must be non-negative integers expressible using unsigned 32 bits.
	{
		var sum = 0;
		for (var i = 0; i < 32; ++i){
			if ((n1 >>> i) & 0x1){
				sum = addition32(sum, unsigned32(n2 << i));
			}
		}
		return sum;
	}

	/* initializes mt[N] with a seed */
	//c//void init_genrand(unsigned long s)
	this.init_genrand = function (s)
	{
		//c//mt[0]= s & 0xffffffff;
		mt[0]= unsigned32(s & 0xffffffff);
		for (mti=1; mti<N; mti++) {
			mt[mti] = 
			//c//(1812433253 * (mt[mti-1] ^ (mt[mti-1] >> 30)) + mti);
			addition32(multiplication32(1812433253, unsigned32(mt[mti-1] ^ (mt[mti-1] >>> 30))), mti);
			/* See Knuth TAOCP Vol2. 3rd Ed. P.106 for multiplier. */
			/* In the previous versions, MSBs of the seed affect   */
			/* only MSBs of the array mt[].                        */
			/* 2002/01/09 modified by Makoto Matsumoto             */
			//c//mt[mti] &= 0xffffffff;
			mt[mti] = unsigned32(mt[mti] & 0xffffffff);
			/* for >32 bit machines */
		}
	}

	/* initialize by an array with array-length */
	/* init_key is the array for initializing keys */
	/* key_length is its length */
	/* slight change for C++, 2004/2/26 */
	//c//void init_by_array(unsigned long init_key[], int key_length)
	this.init_by_array = function (init_key, key_length)
	{
		//c//int i, j, k;
		var i, j, k;
		//c//init_genrand(19650218);
		this.init_genrand(19650218);
		i=1; j=0;
		k = (N>key_length ? N : key_length);
		for (; k; k--) {
			//c//mt[i] = (mt[i] ^ ((mt[i-1] ^ (mt[i-1] >> 30)) * 1664525))
			//c//	+ init_key[j] + j; /* non linear */
			mt[i] = addition32(addition32(unsigned32(mt[i] ^ multiplication32(unsigned32(mt[i-1] ^ (mt[i-1] >>> 30)), 1664525)), init_key[j]), j);
			mt[i] = 
			//c//mt[i] &= 0xffffffff; /* for WORDSIZE > 32 machines */
			unsigned32(mt[i] & 0xffffffff);
			i++; j++;
			if (i>=N) { mt[0] = mt[N-1]; i=1; }
			if (j>=key_length) j=0;
		}
		for (k=N-1; k; k--) {
			//c//mt[i] = (mt[i] ^ ((mt[i-1] ^ (mt[i-1] >> 30)) * 1566083941))
			//c//- i; /* non linear */
			mt[i] = subtraction32(unsigned32((dbg=mt[i]) ^ multiplication32(unsigned32(mt[i-1] ^ (mt[i-1] >>> 30)), 1566083941)), i);
			//c//mt[i] &= 0xffffffff; /* for WORDSIZE > 32 machines */
			mt[i] = unsigned32(mt[i] & 0xffffffff);
			i++;
			if (i>=N) { mt[0] = mt[N-1]; i=1; }
		}
		mt[0] = 0x80000000; /* MSB is 1; assuring non-zero initial array */
	}

    /* moved outside of genrand_int32() by jwatte 2010-11-17; generate less garbage */
    var mag01 = [0x0, MATRIX_A];

	/* generates a random number on [0,0xffffffff]-interval */
	//c//unsigned long genrand_int32(void)
	this.genrand_int32 = function ()
	{
		//c//unsigned long y;
		//c//static unsigned long mag01[2]={0x0UL, MATRIX_A};
		var y;
		/* mag01[x] = x * MATRIX_A  for x=0,1 */

		if (mti >= N) { /* generate N words at one time */
			//c//int kk;
			var kk;

			if (mti == N+1)   /* if init_genrand() has not been called, */
				//c//init_genrand(5489); /* a default initial seed is used */
				this.init_genrand(5489); /* a default initial seed is used */

			for (kk=0;kk<N-M;kk++) {
				//c//y = (mt[kk]&UPPER_MASK)|(mt[kk+1]&LOWER_MASK);
				//c//mt[kk] = mt[kk+M] ^ (y >> 1) ^ mag01[y & 0x1];
				y = unsigned32((mt[kk]&UPPER_MASK)|(mt[kk+1]&LOWER_MASK));
				mt[kk] = unsigned32(mt[kk+M] ^ (y >>> 1) ^ mag01[y & 0x1]);
			}
			for (;kk<N-1;kk++) {
				//c//y = (mt[kk]&UPPER_MASK)|(mt[kk+1]&LOWER_MASK);
				//c//mt[kk] = mt[kk+(M-N)] ^ (y >> 1) ^ mag01[y & 0x1];
				y = unsigned32((mt[kk]&UPPER_MASK)|(mt[kk+1]&LOWER_MASK));
				mt[kk] = unsigned32(mt[kk+(M-N)] ^ (y >>> 1) ^ mag01[y & 0x1]);
			}
			//c//y = (mt[N-1]&UPPER_MASK)|(mt[0]&LOWER_MASK);
			//c//mt[N-1] = mt[M-1] ^ (y >> 1) ^ mag01[y & 0x1];
			y = unsigned32((mt[N-1]&UPPER_MASK)|(mt[0]&LOWER_MASK));
			mt[N-1] = unsigned32(mt[M-1] ^ (y >>> 1) ^ mag01[y & 0x1]);
			mti = 0;
		}

		y = mt[mti++];

		/* Tempering */
		//c//y ^= (y >> 11);
		//c//y ^= (y << 7) & 0x9d2c5680;
		//c//y ^= (y << 15) & 0xefc60000;
		//c//y ^= (y >> 18);
		y = unsigned32(y ^ (y >>> 11));
		y = unsigned32(y ^ ((y << 7) & 0x9d2c5680));
		y = unsigned32(y ^ ((y << 15) & 0xefc60000));
		y = unsigned32(y ^ (y >>> 18));

		return y;
	}

	/* generates a random number on [0,0x7fffffff]-interval */
	//c//long genrand_int31(void)
	this.genrand_int31 = function ()
	{
		//c//return (genrand_int32()>>1);
		return (this.genrand_int32()>>>1);
	}

	/* generates a random number on [0,1]-real-interval */
	//c//double genrand_real1(void)
	this.genrand_real1 = function ()
	{
		//c//return genrand_int32()*(1.0/4294967295.0);
		return this.genrand_int32()*(1.0/4294967295.0);
		/* divided by 2^32-1 */
	}

	/* generates a random number on [0,1)-real-interval */
	//c//double genrand_real2(void)
	this.genrand_real2 = function ()
	{
		//c//return genrand_int32()*(1.0/4294967296.0);
		return this.genrand_int32()*(1.0/4294967296.0);
		/* divided by 2^32 */
	}

	/* generates a random number on (0,1)-real-interval */
	//c//double genrand_real3(void)
	this.genrand_real3 = function ()
	{
		//c//return ((genrand_int32()) + 0.5)*(1.0/4294967296.0);
		return ((this.genrand_int32()) + 0.5)*(1.0/4294967296.0);
		/* divided by 2^32 */
	}

	/* generates a random number on [0,1) with 53-bit resolution*/
	//c//double genrand_res53(void)
	this.genrand_res53 = function ()
	{
		//c//unsigned long a=genrand_int32()>>5, b=genrand_int32()>>6;
		var a=this.genrand_int32()>>>5, b=this.genrand_int32()>>>6;
		return(a*67108864.0+b)*(1.0/9007199254740992.0);
	}
	/* These real versions are due to Isaku Wada, 2002/01/09 added */
}

//  Exports: Public API

//  Export the twister class
exports.MersenneTwister19937 = MersenneTwister19937;

//  Export a simplified function to generate random numbers
var gen = new MersenneTwister19937;
gen.init_genrand((new Date).getTime() % 1000000000);
exports.rand = function(N) {
    if (!N)
        {
        N = 32768;
        }
    return Math.floor(gen.genrand_real2() * N);
}
exports.seed = function(S) {
    if (typeof(S) != 'number')
        {
        throw new Error("seed(S) must take numeric argument; is " + typeof(S));
        }
    gen.init_genrand(S);
}
exports.seed_array = function(A) {
    if (typeof(A) != 'object')
        {
        throw new Error("seed_array(A) must take array of numbers; is " + typeof(A));
        }
    gen.init_by_array(A);
}



},{}],26:[function(require,module,exports){
/*
 * A speed-improved perlin and simplex noise algorithms for 2D.
 *
 * Based on example code by Stefan Gustavson (stegu@itn.liu.se).
 * Optimisations by Peter Eastman (peastman@drizzle.stanford.edu).
 * Better rank ordering method by Stefan Gustavson in 2012.
 * Converted to Javascript by Joseph Gentle.
 *
 * Version 2012-03-09
 *
 * This code was placed in the public domain by its original author,
 * Stefan Gustavson. You may use it as you see fit, but
 * attribution is appreciated.
 *
 */

(function(global){

  // Passing in seed will seed this Noise instance
  function Noise(seed) {
    function Grad(x, y, z) {
      this.x = x; this.y = y; this.z = z;
    }

    Grad.prototype.dot2 = function(x, y) {
      return this.x*x + this.y*y;
    };

    Grad.prototype.dot3 = function(x, y, z) {
      return this.x*x + this.y*y + this.z*z;
    };

    this.grad3 = [new Grad(1,1,0),new Grad(-1,1,0),new Grad(1,-1,0),new Grad(-1,-1,0),
                 new Grad(1,0,1),new Grad(-1,0,1),new Grad(1,0,-1),new Grad(-1,0,-1),
                 new Grad(0,1,1),new Grad(0,-1,1),new Grad(0,1,-1),new Grad(0,-1,-1)];

    this.p = [151,160,137,91,90,15,
    131,13,201,95,96,53,194,233,7,225,140,36,103,30,69,142,8,99,37,240,21,10,23,
    190, 6,148,247,120,234,75,0,26,197,62,94,252,219,203,117,35,11,32,57,177,33,
    88,237,149,56,87,174,20,125,136,171,168, 68,175,74,165,71,134,139,48,27,166,
    77,146,158,231,83,111,229,122,60,211,133,230,220,105,92,41,55,46,245,40,244,
    102,143,54, 65,25,63,161, 1,216,80,73,209,76,132,187,208, 89,18,169,200,196,
    135,130,116,188,159,86,164,100,109,198,173,186, 3,64,52,217,226,250,124,123,
    5,202,38,147,118,126,255,82,85,212,207,206,59,227,47,16,58,17,182,189,28,42,
    223,183,170,213,119,248,152, 2,44,154,163, 70,221,153,101,155,167, 43,172,9,
    129,22,39,253, 19,98,108,110,79,113,224,232,178,185, 112,104,218,246,97,228,
    251,34,242,193,238,210,144,12,191,179,162,241, 81,51,145,235,249,14,239,107,
    49,192,214, 31,181,199,106,157,184, 84,204,176,115,121,50,45,127, 4,150,254,
    138,236,205,93,222,114,67,29,24,72,243,141,128,195,78,66,215,61,156,180];
    // To remove the need for index wrapping, double the permutation table length
    this.perm = new Array(512);
    this.gradP = new Array(512);

    this.seed(seed || 0);
  }

  // This isn't a very good seeding function, but it works ok. It supports 2^16
  // different seed values. Write something better if you need more seeds.
  Noise.prototype.seed = function(seed) {
    if(seed > 0 && seed < 1) {
      // Scale the seed out
      seed *= 65536;
    }

    seed = Math.floor(seed);
    if(seed < 256) {
      seed |= seed << 8;
    }

    var p = this.p;
    for(var i = 0; i < 256; i++) {
      var v;
      if (i & 1) {
        v = p[i] ^ (seed & 255);
      } else {
        v = p[i] ^ ((seed>>8) & 255);
      }

      var perm = this.perm;
      var gradP = this.gradP;
      perm[i] = perm[i + 256] = v;
      gradP[i] = gradP[i + 256] = this.grad3[v % 12];
    }
  };

  /*
  for(var i=0; i<256; i++) {
    perm[i] = perm[i + 256] = p[i];
    gradP[i] = gradP[i + 256] = grad3[perm[i] % 12];
  }*/

  // Skewing and unskewing factors for 2, 3, and 4 dimensions
  var F2 = 0.5*(Math.sqrt(3)-1);
  var G2 = (3-Math.sqrt(3))/6;

  var F3 = 1/3;
  var G3 = 1/6;

  // 2D simplex noise
  Noise.prototype.simplex2 = function(xin, yin) {
    var n0, n1, n2; // Noise contributions from the three corners
    // Skew the input space to determine which simplex cell we're in
    var s = (xin+yin)*F2; // Hairy factor for 2D
    var i = Math.floor(xin+s);
    var j = Math.floor(yin+s);
    var t = (i+j)*G2;
    var x0 = xin-i+t; // The x,y distances from the cell origin, unskewed.
    var y0 = yin-j+t;
    // For the 2D case, the simplex shape is an equilateral triangle.
    // Determine which simplex we are in.
    var i1, j1; // Offsets for second (middle) corner of simplex in (i,j) coords
    if(x0>y0) { // lower triangle, XY order: (0,0)->(1,0)->(1,1)
      i1=1; j1=0;
    } else {    // upper triangle, YX order: (0,0)->(0,1)->(1,1)
      i1=0; j1=1;
    }
    // A step of (1,0) in (i,j) means a step of (1-c,-c) in (x,y), and
    // a step of (0,1) in (i,j) means a step of (-c,1-c) in (x,y), where
    // c = (3-sqrt(3))/6
    var x1 = x0 - i1 + G2; // Offsets for middle corner in (x,y) unskewed coords
    var y1 = y0 - j1 + G2;
    var x2 = x0 - 1 + 2 * G2; // Offsets for last corner in (x,y) unskewed coords
    var y2 = y0 - 1 + 2 * G2;
    // Work out the hashed gradient indices of the three simplex corners
    i &= 255;
    j &= 255;

    var perm = this.perm;
    var gradP = this.gradP;
    var gi0 = gradP[i+perm[j]];
    var gi1 = gradP[i+i1+perm[j+j1]];
    var gi2 = gradP[i+1+perm[j+1]];
    // Calculate the contribution from the three corners
    var t0 = 0.5 - x0*x0-y0*y0;
    if(t0<0) {
      n0 = 0;
    } else {
      t0 *= t0;
      n0 = t0 * t0 * gi0.dot2(x0, y0);  // (x,y) of grad3 used for 2D gradient
    }
    var t1 = 0.5 - x1*x1-y1*y1;
    if(t1<0) {
      n1 = 0;
    } else {
      t1 *= t1;
      n1 = t1 * t1 * gi1.dot2(x1, y1);
    }
    var t2 = 0.5 - x2*x2-y2*y2;
    if(t2<0) {
      n2 = 0;
    } else {
      t2 *= t2;
      n2 = t2 * t2 * gi2.dot2(x2, y2);
    }
    // Add contributions from each corner to get the final noise value.
    // The result is scaled to return values in the interval [-1,1].
    return 70 * (n0 + n1 + n2);
  };

  // 3D simplex noise
  Noise.prototype.simplex3 = function(xin, yin, zin) {
    var n0, n1, n2, n3; // Noise contributions from the four corners

    // Skew the input space to determine which simplex cell we're in
    var s = (xin+yin+zin)*F3; // Hairy factor for 2D
    var i = Math.floor(xin+s);
    var j = Math.floor(yin+s);
    var k = Math.floor(zin+s);

    var t = (i+j+k)*G3;
    var x0 = xin-i+t; // The x,y distances from the cell origin, unskewed.
    var y0 = yin-j+t;
    var z0 = zin-k+t;

    // For the 3D case, the simplex shape is a slightly irregular tetrahedron.
    // Determine which simplex we are in.
    var i1, j1, k1; // Offsets for second corner of simplex in (i,j,k) coords
    var i2, j2, k2; // Offsets for third corner of simplex in (i,j,k) coords
    if(x0 >= y0) {
      if(y0 >= z0)      { i1=1; j1=0; k1=0; i2=1; j2=1; k2=0; }
      else if(x0 >= z0) { i1=1; j1=0; k1=0; i2=1; j2=0; k2=1; }
      else              { i1=0; j1=0; k1=1; i2=1; j2=0; k2=1; }
    } else {
      if(y0 < z0)      { i1=0; j1=0; k1=1; i2=0; j2=1; k2=1; }
      else if(x0 < z0) { i1=0; j1=1; k1=0; i2=0; j2=1; k2=1; }
      else             { i1=0; j1=1; k1=0; i2=1; j2=1; k2=0; }
    }
    // A step of (1,0,0) in (i,j,k) means a step of (1-c,-c,-c) in (x,y,z),
    // a step of (0,1,0) in (i,j,k) means a step of (-c,1-c,-c) in (x,y,z), and
    // a step of (0,0,1) in (i,j,k) means a step of (-c,-c,1-c) in (x,y,z), where
    // c = 1/6.
    var x1 = x0 - i1 + G3; // Offsets for second corner
    var y1 = y0 - j1 + G3;
    var z1 = z0 - k1 + G3;

    var x2 = x0 - i2 + 2 * G3; // Offsets for third corner
    var y2 = y0 - j2 + 2 * G3;
    var z2 = z0 - k2 + 2 * G3;

    var x3 = x0 - 1 + 3 * G3; // Offsets for fourth corner
    var y3 = y0 - 1 + 3 * G3;
    var z3 = z0 - 1 + 3 * G3;

    // Work out the hashed gradient indices of the four simplex corners
    i &= 255;
    j &= 255;
    k &= 255;

    var perm = this.perm;
    var gradP = this.gradP;
    var gi0 = gradP[i+   perm[j+   perm[k   ]]];
    var gi1 = gradP[i+i1+perm[j+j1+perm[k+k1]]];
    var gi2 = gradP[i+i2+perm[j+j2+perm[k+k2]]];
    var gi3 = gradP[i+ 1+perm[j+ 1+perm[k+ 1]]];

    // Calculate the contribution from the four corners
    var t0 = 0.5 - x0*x0-y0*y0-z0*z0;
    if(t0<0) {
      n0 = 0;
    } else {
      t0 *= t0;
      n0 = t0 * t0 * gi0.dot3(x0, y0, z0);  // (x,y) of grad3 used for 2D gradient
    }
    var t1 = 0.5 - x1*x1-y1*y1-z1*z1;
    if(t1<0) {
      n1 = 0;
    } else {
      t1 *= t1;
      n1 = t1 * t1 * gi1.dot3(x1, y1, z1);
    }
    var t2 = 0.5 - x2*x2-y2*y2-z2*z2;
    if(t2<0) {
      n2 = 0;
    } else {
      t2 *= t2;
      n2 = t2 * t2 * gi2.dot3(x2, y2, z2);
    }
    var t3 = 0.5 - x3*x3-y3*y3-z3*z3;
    if(t3<0) {
      n3 = 0;
    } else {
      t3 *= t3;
      n3 = t3 * t3 * gi3.dot3(x3, y3, z3);
    }
    // Add contributions from each corner to get the final noise value.
    // The result is scaled to return values in the interval [-1,1].
    return 32 * (n0 + n1 + n2 + n3);

  };

  // ##### Perlin noise stuff

  function fade(t) {
    return t*t*t*(t*(t*6-15)+10);
  }

  function lerp(a, b, t) {
    return (1-t)*a + t*b;
  }

  // 2D Perlin Noise
  Noise.prototype.perlin2 = function(x, y) {
    // Find unit grid cell containing point
    var X = Math.floor(x), Y = Math.floor(y);
    // Get relative xy coordinates of point within that cell
    x = x - X; y = y - Y;
    // Wrap the integer cells at 255 (smaller integer period can be introduced here)
    X = X & 255; Y = Y & 255;

    // Calculate noise contributions from each of the four corners
    var perm = this.perm;
    var gradP = this.gradP;
    var n00 = gradP[X+perm[Y]].dot2(x, y);
    var n01 = gradP[X+perm[Y+1]].dot2(x, y-1);
    var n10 = gradP[X+1+perm[Y]].dot2(x-1, y);
    var n11 = gradP[X+1+perm[Y+1]].dot2(x-1, y-1);

    // Compute the fade curve value for x
    var u = fade(x);

    // Interpolate the four results
    return lerp(
        lerp(n00, n10, u),
        lerp(n01, n11, u),
       fade(y));
  };

  // 3D Perlin Noise
  Noise.prototype.perlin3 = function(x, y, z) {
    // Find unit grid cell containing point
    var X = Math.floor(x), Y = Math.floor(y), Z = Math.floor(z);
    // Get relative xyz coordinates of point within that cell
    x = x - X; y = y - Y; z = z - Z;
    // Wrap the integer cells at 255 (smaller integer period can be introduced here)
    X = X & 255; Y = Y & 255; Z = Z & 255;

    // Calculate noise contributions from each of the eight corners
    var perm = this.perm;
    var gradP = this.gradP;
    var n000 = gradP[X+  perm[Y+  perm[Z  ]]].dot3(x,   y,     z);
    var n001 = gradP[X+  perm[Y+  perm[Z+1]]].dot3(x,   y,   z-1);
    var n010 = gradP[X+  perm[Y+1+perm[Z  ]]].dot3(x,   y-1,   z);
    var n011 = gradP[X+  perm[Y+1+perm[Z+1]]].dot3(x,   y-1, z-1);
    var n100 = gradP[X+1+perm[Y+  perm[Z  ]]].dot3(x-1,   y,   z);
    var n101 = gradP[X+1+perm[Y+  perm[Z+1]]].dot3(x-1,   y, z-1);
    var n110 = gradP[X+1+perm[Y+1+perm[Z  ]]].dot3(x-1, y-1,   z);
    var n111 = gradP[X+1+perm[Y+1+perm[Z+1]]].dot3(x-1, y-1, z-1);

    // Compute the fade curve value for x, y, z
    var u = fade(x);
    var v = fade(y);
    var w = fade(z);

    // Interpolate
    return lerp(
        lerp(
          lerp(n000, n100, u),
          lerp(n001, n101, u), w),
        lerp(
          lerp(n010, n110, u),
          lerp(n011, n111, u), w),
       v);
  };

  global.Noise = Noise;

})(typeof module === "undefined" ? this : module.exports);

},{}],27:[function(require,module,exports){
var App, GameOfLife, Renderer, Vec2, fk, noisejs;

Renderer = require('../gfx/Renderer.coffee');

fk = require('fieldkit');

Vec2 = fk.math.Vec2;

noisejs = require('noisejs');

GameOfLife = require('../game/GameOfLife.coffee');

App = (function() {
  function App(container) {
    var changeRes;
    this.container = container;
    this.renderer = new Renderer(this.container);
    this.time = 0;
    this.oTime = Date.now();
    window.addEventListener('resize', (function(_this) {
      return function(e) {
        return _this.renderer.resize();
      };
    })(this), false);
    this.noise = new noisejs.Noise(Math.random());
    this.settings = {
      res: this.renderer.grid.res,
      speed: 1,
      noise: "Game Of Life"
    };
    changeRes = (function(_this) {
      return function() {
        var grid;
        grid = _this.renderer.grid;
        grid.res = _this.settings.res;
        grid.setSize(grid.size.x, grid.size.y);
        grid.createCells();
        grid.drawBuffer("#999", 1.5);
        return _this.game = new GameOfLife(_this.renderer.grid);
      };
    })(this);
    this.gui = new dat.GUI();
    this.gui.add(this.settings, "res", 8, 128).step(1).onChange(changeRes);
    this.gui.add(this.settings, "speed", 0.1, 1);
    this.gui.add(this.settings, "noise", ["simplex", "perlin", "Game Of Life"]);
    this.game = new GameOfLife(this.renderer.grid);
  }

  App.prototype.update = function() {
    var dt, res, t, time, value, x, y, _i, _results;
    t = Date.now();
    time = (t - this.oTime) * .001;
    dt = time - this.time;
    this.time = time;
    res = this.renderer.grid.res;
    if (this.settings.noise === "Game Of Life") {
      this.game.update();
      return;
    }
    _results = [];
    for (x = _i = 0; 0 <= res ? _i < res : _i > res; x = 0 <= res ? ++_i : --_i) {
      _results.push((function() {
        var _j, _results1;
        _results1 = [];
        for (y = _j = 0; 0 <= res ? _j < res : _j > res; y = 0 <= res ? ++_j : --_j) {
          if (this.settings.noise === "simplex") {
            value = this.noise.simplex3(x, y, t * .001 * this.settings.speed);
          } else {
            value = this.noise.perlin3(x, y, t * .001 * this.settings.speed);
          }
          _results1.push(this.renderer.grid.markCell(x, y, value > 0));
        }
        return _results1;
      }).call(this));
    }
    return _results;
  };

  App.prototype.render = function() {
    return this.renderer.render();
  };

  return App;

})();

module.exports = App;



},{"../game/GameOfLife.coffee":30,"../gfx/Renderer.coffee":31,"fieldkit":11,"noisejs":26}],28:[function(require,module,exports){
var Grid, Rect2, Vec2, fk;

fk = require('fieldkit');

Vec2 = fk.math.Vec2;

Rect2 = require('./Rect2.coffee');

Grid = (function() {
  function Grid(res, size, createBuffer) {
    this.res = res;
    this.size = size;
    if (createBuffer == null) {
      createBuffer = false;
    }
    this.createCells();
    this.step = new Vec2(this.size.x / this.res, this.size.y / this.res);
    this.buffer = null;
    if (createBuffer) {
      this.buffer = el('canvas');
      this.bctx = this.buffer.getContext('2d');
      this.buffer.width = this.size.x;
      this.buffer.height = this.size.y;
    }
  }

  Grid.prototype.setSize = function(w, h) {
    this.size.set2(w, h);
    this.step = new Vec2(this.size.x / this.res, this.size.y / this.res);
    if (this.buffer !== null) {
      this.buffer.width = this.size.x;
      return this.buffer.height = this.size.y;
    }
  };

  Grid.prototype.createCells = function() {
    var x, y;
    if (this.cells) {
      this.cells = null;
    }
    return this.cells = (function() {
      var _i, _ref, _results;
      _results = [];
      for (x = _i = 1, _ref = this.res; 1 <= _ref ? _i <= _ref : _i >= _ref; x = 1 <= _ref ? ++_i : --_i) {
        _results.push((function() {
          var _j, _ref1, _results1;
          _results1 = [];
          for (y = _j = 1, _ref1 = this.res; 1 <= _ref1 ? _j <= _ref1 : _j >= _ref1; y = 1 <= _ref1 ? ++_j : --_j) {
            _results1.push(false);
          }
          return _results1;
        }).call(this));
      }
      return _results;
    }).call(this);
  };

  Grid.prototype.drawBuffer = function(col, lineWidth) {
    if (col == null) {
      col = "#666";
    }
    if (lineWidth == null) {
      lineWidth = .5;
    }
    if (this.buffer === null) {
      return;
    }
    this.bctx.clearRect(0, 0, this.buffer.width, this.buffer.height);
    this.bctx.strokeStyle = col;
    this.bctx.lineWidth = lineWidth;
    this.bctx.beginPath();
    this.drawPaths(this.bctx);
    return this.bctx.stroke();
  };

  Grid.prototype.drawPaths = function(ctx, offset, scale) {
    var i, x1, x2, y1, y2, _i, _ref, _results;
    if (offset == null) {
      offset = {
        x: 0,
        y: 0
      };
    }
    if (scale == null) {
      scale = 1;
    }
    x1 = offset.x;
    x2 = offset.x + this.size.x * scale;
    y1 = offset.y;
    y2 = offset.y + this.size.y * scale;
    _results = [];
    for (i = _i = 0, _ref = this.res; 0 <= _ref ? _i <= _ref : _i >= _ref; i = 0 <= _ref ? ++_i : --_i) {
      ctx.moveTo(x1, y1 + this.step.y * i * scale);
      ctx.lineTo(x2, y1 + this.step.y * i * scale);
      ctx.moveTo(x1 + this.step.x * i * scale, y1);
      _results.push(ctx.lineTo(x1 + this.step.x * i * scale, y2));
    }
    return _results;
  };

  Grid.prototype.getIntersectionCell = function(x, y, mark) {
    var i, j;
    if (x == null) {
      x = 0;
    }
    if (y == null) {
      y = 0;
    }
    if (mark == null) {
      mark = false;
    }
    if (x < 0 || x > this.size.x) {
      return null;
    }
    if (y < 0 || y > this.size.y) {
      return null;
    }
    i = Math.floor((x / this.size.x) * this.res);
    j = Math.floor((y / this.size.y) * this.res);
    if (mark) {
      this.cells[j][i] = true;
    }
    return this.getCellRect(j, i);
  };

  Grid.prototype.getCellRect = function(i, j) {
    if (i == null) {
      i = 0;
    }
    if (j == null) {
      j = 0;
    }
    return new Rect2(this.step.x * j, this.step.y * i, this.step.x, this.step.y);
  };

  Grid.prototype.getNewDensityGrid = function(factor) {
    if (factor == null) {
      factor = 2;
    }
    return new Grid(Math.floor(this.res * factor), this.size.clone());
  };

  Grid.prototype.clone = function() {
    var g, x, y, _i, _j, _ref, _ref1;
    g = this.getNewDensityGrid(1);
    for (x = _i = 0, _ref = this.res; 0 <= _ref ? _i < _ref : _i > _ref; x = 0 <= _ref ? ++_i : --_i) {
      for (y = _j = 0, _ref1 = this.res; 0 <= _ref1 ? _j < _ref1 : _j > _ref1; y = 0 <= _ref1 ? ++_j : --_j) {
        g.cells[x][y] = this.cells[x][y];
      }
    }
    return g;
  };

  Grid.prototype.clearCells = function() {
    var x, y, _i, _ref, _results;
    _results = [];
    for (x = _i = 0, _ref = this.res; 0 <= _ref ? _i < _ref : _i > _ref; x = 0 <= _ref ? ++_i : --_i) {
      _results.push((function() {
        var _j, _ref1, _results1;
        _results1 = [];
        for (y = _j = 0, _ref1 = this.res; 0 <= _ref1 ? _j < _ref1 : _j > _ref1; y = 0 <= _ref1 ? ++_j : --_j) {
          _results1.push(this.cells[x][y] = false);
        }
        return _results1;
      }).call(this));
    }
    return _results;
  };

  Grid.prototype.markCell = function(i, j, val) {
    if (val == null) {
      val = true;
    }
    return this.cells[i][j] = val;
  };

  Grid.prototype.markRandomCells = function(seed, threshold) {
    var Random, x, y, _i, _ref, _results;
    if (seed == null) {
      seed = Date.now();
    }
    if (threshold == null) {
      threshold = .5;
    }
    Random = new fk.math.Random(seed);
    _results = [];
    for (x = _i = 0, _ref = this.res; 0 <= _ref ? _i < _ref : _i > _ref; x = 0 <= _ref ? ++_i : --_i) {
      _results.push((function() {
        var _j, _ref1, _results1;
        _results1 = [];
        for (y = _j = 0, _ref1 = this.res; 0 <= _ref1 ? _j < _ref1 : _j > _ref1; y = 0 <= _ref1 ? ++_j : --_j) {
          _results1.push(this.cells[x][y] = Random.random() < threshold);
        }
        return _results1;
      }).call(this));
    }
    return _results;
  };

  Grid.prototype.drawMarkedCellPaths = function(ctx) {
    var i, j, r, _i, _ref, _results;
    _results = [];
    for (i = _i = 0, _ref = this.res; 0 <= _ref ? _i < _ref : _i > _ref; i = 0 <= _ref ? ++_i : --_i) {
      _results.push((function() {
        var _j, _ref1, _results1;
        _results1 = [];
        for (j = _j = 0, _ref1 = this.res; 0 <= _ref1 ? _j < _ref1 : _j > _ref1; j = 0 <= _ref1 ? ++_j : --_j) {
          if (this.cells[i][j]) {
            r = this.getCellRect(j, i);
            _results1.push(ctx.rect(r.x, r.y, r.width, r.height));
          } else {
            _results1.push(void 0);
          }
        }
        return _results1;
      }).call(this));
    }
    return _results;
  };

  Grid.prototype.getNeighbours = function(i, j) {
    var n;
    n = [null, null, null, null, null, null, null, null];
    if (i > 0) {
      n[0] = this.cells[i - 1][j];
      if (j < this.res - 1) {
        n[1] = this.cells[i - 1][j + 1];
      }
    }
    if (j < this.res - 1) {
      n[2] = this.cells[i][j + 1];
      if (i < this.res - 1) {
        n[3] = this.cells[i + 1][j + 1];
      }
    }
    if (i < this.res - 1) {
      n[4] = this.cells[i + 1][j];
      if (j > 0) {
        n[5] = this.cells[i + 1][j - 1];
      }
    }
    if (j > 0) {
      n[6] = this.cells[i][j - 1];
      if (i > 0) {
        n[7] = this.cells[i - 1][j - 1];
      }
    }
    return n;
  };

  Grid.prototype.getDifferentCellCount = function(g) {
    var d, i, j, _i, _j, _ref, _ref1;
    if (g.res !== this.res) {
      return -1;
    }
    d = 0;
    for (i = _i = 0, _ref = this.res; 0 <= _ref ? _i < _ref : _i > _ref; i = 0 <= _ref ? ++_i : --_i) {
      for (j = _j = 0, _ref1 = this.res; 0 <= _ref1 ? _j < _ref1 : _j > _ref1; j = 0 <= _ref1 ? ++_j : --_j) {
        if (this.cells[i][j] !== g.cells[i][j]) {
          d++;
        }
      }
    }
    return d;
  };

  return Grid;

})();

module.exports = Grid;



},{"./Rect2.coffee":29,"fieldkit":11}],29:[function(require,module,exports){
var Rect2;

Rect2 = (function() {
  function Rect2(x, y, width, height) {
    this.x = x != null ? x : 0;
    this.y = y != null ? y : 0;
    this.width = width != null ? width : 0;
    this.height = height != null ? height : 0;
  }

  Rect2.prototype.hitTest = function(r) {
    return (r.x + r.width >= this.x) && (r.x <= this.x + this.width) && (r.y + r.height >= this.y) && (r.y <= this.y + this.height);
  };

  return Rect2;

})();

module.exports = Rect2;



},{}],30:[function(require,module,exports){
var GameOfLife;

GameOfLife = (function() {
  function GameOfLife(grid) {
    this.grid = grid;
    this.updateStep = 8;
    this.step = 0;
    this.buffer = [0, 1, 2, 0];
    this.startTime = Date.now() * .001;
  }

  GameOfLife.prototype.update = function() {
    var alive, d, g, i, j, k, n, t, _i, _j, _k, _ref, _ref1;
    this.step = (this.step + 1) % this.updateStep;
    if (this.step > 0) {
      return;
    }
    g = this.grid.clone();
    for (i = _i = 0, _ref = g.res; 0 <= _ref ? _i < _ref : _i > _ref; i = 0 <= _ref ? ++_i : --_i) {
      for (j = _j = 0, _ref1 = g.res; 0 <= _ref1 ? _j < _ref1 : _j > _ref1; j = 0 <= _ref1 ? ++_j : --_j) {
        n = g.getNeighbours(i, j);
        alive = 0;
        for (k = _k = 0; _k <= 7; k = ++_k) {
          if (n[k] === true) {
            alive++;
          }
        }
        if (g.cells[i][j]) {
          if (alive < 2 || alive > 3) {
            this.grid.cells[i][j] = false;
          }
        } else {
          if (alive === 3) {
            this.grid.cells[i][j] = true;
          }
        }
      }
    }
    d = this.grid.getDifferentCellCount(g);
    this.buffer[0] = this.buffer[1];
    this.buffer[1] = this.buffer[2];
    this.buffer[2] = d;
    if (this.buffer[0] === this.buffer[1] || this.buffer[0] === this.buffer[2]) {
      this.buffer[3]++;
    } else {
      this.buffer[3] = 0;
    }
    t = Date.now() * .001;
    if (d < 2 || this.buffer[3] > 5 || t - this.startTime > 16) {
      return this.restart();
    }
  };

  GameOfLife.prototype.restart = function() {
    this.startTime = Date.now() * .001;
    return this.grid.markRandomCells();
  };

  GameOfLife.prototype.render = function(ctx, debug) {
    if (debug == null) {
      debug = false;
    }
    if (!debug) {
      return;
    }
    ctx.fillStyle = "rgba( 255, 255, 255, .2 )";
    ctx.beginPath();
    this.grid.drawMarkedCellPaths(ctx);
    return ctx.fill();
  };

  return GameOfLife;

})();

module.exports = GameOfLife;



},{}],31:[function(require,module,exports){
var Grid, Renderer, Vec2, fk;

Grid = require('../core/Grid.coffee');

fk = require('fieldkit');

Vec2 = fk.math.Vec2;

Renderer = (function() {
  function Renderer(container) {
    this.width = Math.max(window.innerWidth, 512);
    this.canvas = el('canvas');
    this.canvas.width = this.width;
    this.canvas.height = this.width;
    this.ctx = this.canvas.getContext('2d');
    container.appendChild(this.canvas);
    this.grid = new Grid(16, new Vec2(this.width, this.width), true);
    this.grid.drawBuffer("#999", 1.5);
  }

  Renderer.prototype.render = function() {
    this.ctx.clearRect(0, 0, this.width, this.width);
    this.ctx.drawImage(this.grid.buffer, 0, 0);
    this.ctx.fillStyle = "#AAA";
    this.ctx.beginPath();
    this.grid.drawMarkedCellPaths(this.ctx);
    return this.ctx.fill();
  };

  Renderer.prototype.resize = function() {
    this.width = Math.max(window.innerWidth, 512);
    this.canvas.width = this.width;
    this.canvas.height = this.width;
    this.grid.setSize(this.width, this.width);
    return this.grid.drawBuffer("#999", 1.5);
  };

  return Renderer;

})();

module.exports = Renderer;



},{"../core/Grid.coffee":28,"fieldkit":11}],32:[function(require,module,exports){
var App, Main, globals, k, v;

App = require('./apps/App.coffee');

Main = new function() {
  var animate, app, css3D, init, initialized, isMobile, setTransform;
  initialized = false;
  app = null;
  init = function() {
    if (!initialized) {
      initialized = true;
      app = new App($('container'));
      return animate();
    }
  };
  animate = function() {
    requestAnimationFrame(animate);
    app.update();
    return app.render();
  };
  isMobile = function() {
    if (navigator.userAgent.match(/Android/i) || navigator.userAgent.match(/webOS/i) || navigator.userAgent.match(/iPhone/i) || navigator.userAgent.match(/iPad/i) || navigator.userAgent.match(/iPod/i) || navigator.userAgent.match(/BlackBerry/i) || navigator.userAgent.match(/Windows Phone/i)) {
      return true;
    } else {
      return false;
    }
  };
  css3D = function() {
    var e, has3d, t, transforms, v;
    e = el('p');
    has3d = false;
    transforms = {
      'webkitTransform': '-webkit-transform',
      'OTransform': '-o-transform',
      'msTransform': '-ms-transform',
      'MozTransform': '-moz-transform',
      'transform': 'transform'
    };
    document.body.insertBefore(e, null);
    for (t in transforms) {
      v = transforms[t];
      if (e.style[t] !== void 0) {
        e.style[t] = "translate3d(1px,1px,1px)";
        has3d = window.getComputedStyle(e).getPropertyValue(transforms[t]);
      }
    }
    document.body.removeChild(e);
    return has3d !== void 0 && has3d.length > 0 && has3d !== "none";
  };
  setTransform = function(e, t, p) {
    var k, v;
    if (p == null) {
      p = ['webkit', 'Moz', 'ms'];
    }
    for (k in p) {
      v = p[k];
      e.style[p[k] + 'Transform'] = t;
    }
    return e.style.transform = t;
  };
  return {
    init: init,
    getContents: function() {
      return contents;
    },
    isMobile: isMobile,
    css3D: css3D,
    setTransform: setTransform
  };
};

globals = {
  $: function(id) {
    return document.getElementById(id);
  },
  el: function(type, cls) {
    var e;
    e = document.createElement(type);
    if (cls) {
      e.className = cls;
    }
    return e;
  },
  Main: Main
};

for (k in globals) {
  v = globals[k];
  window[k] = v;
}

if (window.addEventListener) {
  window.addEventListener('load', function(e) {
    return Main.init();
  }, false);
} else if (window.attachEvent) {
  window.attachEvent('load', function(e) {
    return Main.init();
  }, false);
}



},{"./apps/App.coffee":27}]},{},[32])
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5vZGVfbW9kdWxlcy9icm93c2VyaWZ5L25vZGVfbW9kdWxlcy9icm93c2VyLXBhY2svX3ByZWx1ZGUuanMiLCJub2RlX21vZHVsZXMvYnJvd3NlcmlmeS9ub2RlX21vZHVsZXMvYnVmZmVyL2luZGV4LmpzIiwibm9kZV9tb2R1bGVzL2Jyb3dzZXJpZnkvbm9kZV9tb2R1bGVzL2J1ZmZlci9ub2RlX21vZHVsZXMvYmFzZTY0LWpzL2xpYi9iNjQuanMiLCJub2RlX21vZHVsZXMvYnJvd3NlcmlmeS9ub2RlX21vZHVsZXMvYnVmZmVyL25vZGVfbW9kdWxlcy9pZWVlNzU0L2luZGV4LmpzIiwibm9kZV9tb2R1bGVzL2Jyb3dzZXJpZnkvbm9kZV9tb2R1bGVzL2J1ZmZlci9ub2RlX21vZHVsZXMvaXMtYXJyYXkvaW5kZXguanMiLCJub2RlX21vZHVsZXMvYnJvd3NlcmlmeS9ub2RlX21vZHVsZXMvaW5oZXJpdHMvaW5oZXJpdHNfYnJvd3Nlci5qcyIsIm5vZGVfbW9kdWxlcy9icm93c2VyaWZ5L25vZGVfbW9kdWxlcy9wcm9jZXNzL2Jyb3dzZXIuanMiLCJub2RlX21vZHVsZXMvYnJvd3NlcmlmeS9ub2RlX21vZHVsZXMvdXRpbC9zdXBwb3J0L2lzQnVmZmVyQnJvd3Nlci5qcyIsIm5vZGVfbW9kdWxlcy9icm93c2VyaWZ5L25vZGVfbW9kdWxlcy91dGlsL3V0aWwuanMiLCJub2RlX21vZHVsZXMvZmllbGRraXQvbGliL2NsaWVudC9za2V0Y2guanMiLCJub2RlX21vZHVsZXMvZmllbGRraXQvbGliL2NvbG9yLmpzIiwibm9kZV9tb2R1bGVzL2ZpZWxka2l0L2xpYi9maWVsZGtpdC5qcyIsIm5vZGVfbW9kdWxlcy9maWVsZGtpdC9saWIvbWF0aC9ib3guanMiLCJub2RlX21vZHVsZXMvZmllbGRraXQvbGliL21hdGgvbGluZS5qcyIsIm5vZGVfbW9kdWxlcy9maWVsZGtpdC9saWIvbWF0aC9tYXRoLmpzIiwibm9kZV9tb2R1bGVzL2ZpZWxka2l0L2xpYi9tYXRoL25vaXNlLmpzIiwibm9kZV9tb2R1bGVzL2ZpZWxka2l0L2xpYi9tYXRoL3JhbmRvbS5qcyIsIm5vZGVfbW9kdWxlcy9maWVsZGtpdC9saWIvbWF0aC9yZWN0LmpzIiwibm9kZV9tb2R1bGVzL2ZpZWxka2l0L2xpYi9tYXRoL3ZlY3Rvci5qcyIsIm5vZGVfbW9kdWxlcy9maWVsZGtpdC9saWIvcGh5c2ljcy9iZWhhdmlvdXJzLmpzIiwibm9kZV9tb2R1bGVzL2ZpZWxka2l0L2xpYi9waHlzaWNzL2NvbnN0cmFpbnRzLmpzIiwibm9kZV9tb2R1bGVzL2ZpZWxka2l0L2xpYi9waHlzaWNzL3BhcnRpY2xlLmpzIiwibm9kZV9tb2R1bGVzL2ZpZWxka2l0L2xpYi9waHlzaWNzL3BoeXNpY3MuanMiLCJub2RlX21vZHVsZXMvZmllbGRraXQvbGliL3RpbWUuanMiLCJub2RlX21vZHVsZXMvZmllbGRraXQvbGliL3V0aWwuanMiLCJub2RlX21vZHVsZXMvZmllbGRraXQvbm9kZV9tb2R1bGVzL21lcnNlbm5lL2xpYi9tZXJzZW5uZS5qcyIsIm5vZGVfbW9kdWxlcy9ub2lzZWpzL2luZGV4LmpzIiwiL1ZvbHVtZXMvRmlsZXMvd29ya3MvUmVzb25hdGUvMjAxNS9HcmlkL3NyYy9hcHBzL0FwcC5jb2ZmZWUiLCIvVm9sdW1lcy9GaWxlcy93b3Jrcy9SZXNvbmF0ZS8yMDE1L0dyaWQvc3JjL2NvcmUvR3JpZC5jb2ZmZWUiLCIvVm9sdW1lcy9GaWxlcy93b3Jrcy9SZXNvbmF0ZS8yMDE1L0dyaWQvc3JjL2NvcmUvUmVjdDIuY29mZmVlIiwiL1ZvbHVtZXMvRmlsZXMvd29ya3MvUmVzb25hdGUvMjAxNS9HcmlkL3NyYy9nYW1lL0dhbWVPZkxpZmUuY29mZmVlIiwiL1ZvbHVtZXMvRmlsZXMvd29ya3MvUmVzb25hdGUvMjAxNS9HcmlkL3NyYy9nZngvUmVuZGVyZXIuY29mZmVlIiwiL1ZvbHVtZXMvRmlsZXMvd29ya3MvUmVzb25hdGUvMjAxNS9HcmlkL3NyYy9tYWluLmNvZmZlZSJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtBQ0FBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDM2hDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUN4SEE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDcEZBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ2pDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDdkJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUN0RkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ0xBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQzVrQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUNuUUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDOUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ3JFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ3ZEQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ3ZKQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDclBBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUMzWUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDekdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUM1Q0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ25YQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDckdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDelFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ2hNQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ3pUQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUNyWkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQzdUQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUM3UkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDdlVBLElBQUEsNENBQUE7O0FBQUEsUUFBQSxHQUFXLE9BQUEsQ0FBUSx3QkFBUixDQUFYLENBQUE7O0FBQUEsRUFFQSxHQUFLLE9BQUEsQ0FBUSxVQUFSLENBRkwsQ0FBQTs7QUFBQSxJQUdBLEdBQU8sRUFBRSxDQUFDLElBQUksQ0FBQyxJQUhmLENBQUE7O0FBQUEsT0FLQSxHQUFVLE9BQUEsQ0FBUSxTQUFSLENBTFYsQ0FBQTs7QUFBQSxVQU9BLEdBQWEsT0FBQSxDQUFRLDJCQUFSLENBUGIsQ0FBQTs7QUFBQTtBQVVjLEVBQUEsYUFBRyxTQUFILEdBQUE7QUFDWixRQUFBLFNBQUE7QUFBQSxJQURjLElBQUMsQ0FBQSxZQUFBLFNBQ2YsQ0FBQTtBQUFBLElBQUEsSUFBQyxDQUFBLFFBQUQsR0FBZ0IsSUFBQSxRQUFBLENBQVMsSUFBQyxDQUFBLFNBQVYsQ0FBaEIsQ0FBQTtBQUFBLElBQ0EsSUFBQyxDQUFBLElBQUQsR0FBUSxDQURSLENBQUE7QUFBQSxJQUVBLElBQUMsQ0FBQSxLQUFELEdBQVMsSUFBSSxDQUFDLEdBQUwsQ0FBQSxDQUZULENBQUE7QUFBQSxJQUlBLE1BQU0sQ0FBQyxnQkFBUCxDQUF3QixRQUF4QixFQUFrQyxDQUFBLFNBQUEsS0FBQSxHQUFBO2FBQUEsU0FBRSxDQUFGLEdBQUE7ZUFDakMsS0FBQyxDQUFBLFFBQVEsQ0FBQyxNQUFWLENBQUEsRUFEaUM7TUFBQSxFQUFBO0lBQUEsQ0FBQSxDQUFBLENBQUEsSUFBQSxDQUFsQyxFQUVFLEtBRkYsQ0FKQSxDQUFBO0FBQUEsSUFRQSxJQUFDLENBQUEsS0FBRCxHQUFhLElBQUEsT0FBTyxDQUFDLEtBQVIsQ0FBYyxJQUFJLENBQUMsTUFBTCxDQUFBLENBQWQsQ0FSYixDQUFBO0FBQUEsSUFVQSxJQUFDLENBQUEsUUFBRCxHQUNDO0FBQUEsTUFBQSxHQUFBLEVBQUssSUFBQyxDQUFBLFFBQVEsQ0FBQyxJQUFJLENBQUMsR0FBcEI7QUFBQSxNQUNBLEtBQUEsRUFBTyxDQURQO0FBQUEsTUFFQSxLQUFBLEVBQU8sY0FGUDtLQVhELENBQUE7QUFBQSxJQWVBLFNBQUEsR0FBWSxDQUFBLFNBQUEsS0FBQSxHQUFBO2FBQUEsU0FBQSxHQUFBO0FBQ1gsWUFBQSxJQUFBO0FBQUEsUUFBQSxJQUFBLEdBQU8sS0FBQyxDQUFBLFFBQVEsQ0FBQyxJQUFqQixDQUFBO0FBQUEsUUFDQSxJQUFJLENBQUMsR0FBTCxHQUFXLEtBQUMsQ0FBQSxRQUFRLENBQUMsR0FEckIsQ0FBQTtBQUFBLFFBR0EsSUFBSSxDQUFDLE9BQUwsQ0FBYSxJQUFJLENBQUMsSUFBSSxDQUFDLENBQXZCLEVBQTBCLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBcEMsQ0FIQSxDQUFBO0FBQUEsUUFJQSxJQUFJLENBQUMsV0FBTCxDQUFBLENBSkEsQ0FBQTtBQUFBLFFBS0EsSUFBSSxDQUFDLFVBQUwsQ0FBZ0IsTUFBaEIsRUFBd0IsR0FBeEIsQ0FMQSxDQUFBO2VBT0EsS0FBQyxDQUFBLElBQUQsR0FBWSxJQUFBLFVBQUEsQ0FBVyxLQUFDLENBQUEsUUFBUSxDQUFDLElBQXJCLEVBUkQ7TUFBQSxFQUFBO0lBQUEsQ0FBQSxDQUFBLENBQUEsSUFBQSxDQWZaLENBQUE7QUFBQSxJQXlCQSxJQUFDLENBQUEsR0FBRCxHQUFXLElBQUEsR0FBRyxDQUFDLEdBQUosQ0FBQSxDQXpCWCxDQUFBO0FBQUEsSUEwQkEsSUFBQyxDQUFBLEdBQUcsQ0FBQyxHQUFMLENBQVUsSUFBQyxDQUFBLFFBQVgsRUFBcUIsS0FBckIsRUFBNEIsQ0FBNUIsRUFBK0IsR0FBL0IsQ0FBb0MsQ0FBQyxJQUFyQyxDQUEwQyxDQUExQyxDQUE0QyxDQUFDLFFBQTdDLENBQXVELFNBQXZELENBMUJBLENBQUE7QUFBQSxJQTJCQSxJQUFDLENBQUEsR0FBRyxDQUFDLEdBQUwsQ0FBVSxJQUFDLENBQUEsUUFBWCxFQUFxQixPQUFyQixFQUE4QixHQUE5QixFQUFtQyxDQUFuQyxDQTNCQSxDQUFBO0FBQUEsSUE0QkEsSUFBQyxDQUFBLEdBQUcsQ0FBQyxHQUFMLENBQVUsSUFBQyxDQUFBLFFBQVgsRUFBcUIsT0FBckIsRUFBOEIsQ0FBRSxTQUFGLEVBQWEsUUFBYixFQUF1QixjQUF2QixDQUE5QixDQTVCQSxDQUFBO0FBQUEsSUE4QkEsSUFBQyxDQUFBLElBQUQsR0FBWSxJQUFBLFVBQUEsQ0FBVyxJQUFDLENBQUEsUUFBUSxDQUFDLElBQXJCLENBOUJaLENBRFk7RUFBQSxDQUFiOztBQUFBLGdCQWlDQSxNQUFBLEdBQVEsU0FBQSxHQUFBO0FBQ1AsUUFBQSwyQ0FBQTtBQUFBLElBQUEsQ0FBQSxHQUFJLElBQUksQ0FBQyxHQUFMLENBQUEsQ0FBSixDQUFBO0FBQUEsSUFDQSxJQUFBLEdBQU8sQ0FBRSxDQUFBLEdBQUksSUFBQyxDQUFBLEtBQVAsQ0FBQSxHQUFpQixJQUR4QixDQUFBO0FBQUEsSUFFQSxFQUFBLEdBQUssSUFBQSxHQUFPLElBQUMsQ0FBQSxJQUZiLENBQUE7QUFBQSxJQUdBLElBQUMsQ0FBQSxJQUFELEdBQVEsSUFIUixDQUFBO0FBQUEsSUFLQSxHQUFBLEdBQU0sSUFBQyxDQUFBLFFBQVEsQ0FBQyxJQUFJLENBQUMsR0FMckIsQ0FBQTtBQU9BLElBQUEsSUFBRyxJQUFDLENBQUEsUUFBUSxDQUFDLEtBQVYsS0FBbUIsY0FBdEI7QUFDQyxNQUFBLElBQUMsQ0FBQSxJQUFJLENBQUMsTUFBTixDQUFBLENBQUEsQ0FBQTtBQUNBLFlBQUEsQ0FGRDtLQVBBO0FBV0E7U0FBUyxzRUFBVCxHQUFBO0FBQ0M7O0FBQUE7YUFBUyxzRUFBVCxHQUFBO0FBQ0MsVUFBQSxJQUFHLElBQUMsQ0FBQSxRQUFRLENBQUMsS0FBVixLQUFtQixTQUF0QjtBQUNDLFlBQUEsS0FBQSxHQUFRLElBQUMsQ0FBQSxLQUFLLENBQUMsUUFBUCxDQUFnQixDQUFoQixFQUFtQixDQUFuQixFQUFzQixDQUFBLEdBQUksSUFBSixHQUFXLElBQUMsQ0FBQSxRQUFRLENBQUMsS0FBM0MsQ0FBUixDQUREO1dBQUEsTUFBQTtBQUdDLFlBQUEsS0FBQSxHQUFRLElBQUMsQ0FBQSxLQUFLLENBQUMsT0FBUCxDQUFlLENBQWYsRUFBa0IsQ0FBbEIsRUFBcUIsQ0FBQSxHQUFJLElBQUosR0FBVyxJQUFDLENBQUEsUUFBUSxDQUFDLEtBQTFDLENBQVIsQ0FIRDtXQUFBO0FBQUEseUJBSUEsSUFBQyxDQUFBLFFBQVEsQ0FBQyxJQUFJLENBQUMsUUFBZixDQUF3QixDQUF4QixFQUEyQixDQUEzQixFQUE4QixLQUFBLEdBQVEsQ0FBdEMsRUFKQSxDQUREO0FBQUE7O29CQUFBLENBREQ7QUFBQTtvQkFaTztFQUFBLENBakNSLENBQUE7O0FBQUEsZ0JBcURBLE1BQUEsR0FBUSxTQUFBLEdBQUE7V0FDUCxJQUFDLENBQUEsUUFBUSxDQUFDLE1BQVYsQ0FBQSxFQURPO0VBQUEsQ0FyRFIsQ0FBQTs7YUFBQTs7SUFWRCxDQUFBOztBQUFBLE1Ba0VNLENBQUMsT0FBUCxHQUFpQixHQWxFakIsQ0FBQTs7Ozs7QUNBQSxJQUFBLHFCQUFBOztBQUFBLEVBQUEsR0FBSyxPQUFBLENBQVEsVUFBUixDQUFMLENBQUE7O0FBQUEsSUFDQSxHQUFPLEVBQUUsQ0FBQyxJQUFJLENBQUMsSUFEZixDQUFBOztBQUFBLEtBR0EsR0FBUSxPQUFBLENBQVEsZ0JBQVIsQ0FIUixDQUFBOztBQUFBO0FBT2MsRUFBQSxjQUFHLEdBQUgsRUFBUyxJQUFULEVBQWUsWUFBZixHQUFBO0FBQ1osSUFEYyxJQUFDLENBQUEsTUFBQSxHQUNmLENBQUE7QUFBQSxJQURvQixJQUFDLENBQUEsT0FBQSxJQUNyQixDQUFBOztNQUQyQixlQUFhO0tBQ3hDO0FBQUEsSUFBQSxJQUFDLENBQUEsV0FBRCxDQUFBLENBQUEsQ0FBQTtBQUFBLElBQ0EsSUFBQyxDQUFBLElBQUQsR0FBWSxJQUFBLElBQUEsQ0FBSyxJQUFDLENBQUEsSUFBSSxDQUFDLENBQU4sR0FBVSxJQUFDLENBQUEsR0FBaEIsRUFBcUIsSUFBQyxDQUFBLElBQUksQ0FBQyxDQUFOLEdBQVUsSUFBQyxDQUFBLEdBQWhDLENBRFosQ0FBQTtBQUFBLElBRUEsSUFBQyxDQUFBLE1BQUQsR0FBVSxJQUZWLENBQUE7QUFJQSxJQUFBLElBQUcsWUFBSDtBQUNDLE1BQUEsSUFBQyxDQUFBLE1BQUQsR0FBVSxFQUFBLENBQUcsUUFBSCxDQUFWLENBQUE7QUFBQSxNQUNBLElBQUMsQ0FBQSxJQUFELEdBQVEsSUFBQyxDQUFBLE1BQU0sQ0FBQyxVQUFSLENBQW1CLElBQW5CLENBRFIsQ0FBQTtBQUFBLE1BRUEsSUFBQyxDQUFBLE1BQU0sQ0FBQyxLQUFSLEdBQWdCLElBQUMsQ0FBQSxJQUFJLENBQUMsQ0FGdEIsQ0FBQTtBQUFBLE1BR0EsSUFBQyxDQUFBLE1BQU0sQ0FBQyxNQUFSLEdBQWlCLElBQUMsQ0FBQSxJQUFJLENBQUMsQ0FIdkIsQ0FERDtLQUxZO0VBQUEsQ0FBYjs7QUFBQSxpQkFXQSxPQUFBLEdBQVMsU0FBRSxDQUFGLEVBQUssQ0FBTCxHQUFBO0FBQ1IsSUFBQSxJQUFDLENBQUEsSUFBSSxDQUFDLElBQU4sQ0FBVyxDQUFYLEVBQWMsQ0FBZCxDQUFBLENBQUE7QUFBQSxJQUNBLElBQUMsQ0FBQSxJQUFELEdBQVksSUFBQSxJQUFBLENBQUssSUFBQyxDQUFBLElBQUksQ0FBQyxDQUFOLEdBQVUsSUFBQyxDQUFBLEdBQWhCLEVBQXFCLElBQUMsQ0FBQSxJQUFJLENBQUMsQ0FBTixHQUFVLElBQUMsQ0FBQSxHQUFoQyxDQURaLENBQUE7QUFFQSxJQUFBLElBQUcsSUFBQyxDQUFBLE1BQUQsS0FBVyxJQUFkO0FBQ0MsTUFBQSxJQUFDLENBQUEsTUFBTSxDQUFDLEtBQVIsR0FBZ0IsSUFBQyxDQUFBLElBQUksQ0FBQyxDQUF0QixDQUFBO2FBQ0EsSUFBQyxDQUFBLE1BQU0sQ0FBQyxNQUFSLEdBQWlCLElBQUMsQ0FBQSxJQUFJLENBQUMsRUFGeEI7S0FIUTtFQUFBLENBWFQsQ0FBQTs7QUFBQSxpQkFrQkEsV0FBQSxHQUFhLFNBQUEsR0FBQTtBQUNaLFFBQUEsSUFBQTtBQUFBLElBQUEsSUFBRyxJQUFDLENBQUEsS0FBSjtBQUNDLE1BQUEsSUFBQyxDQUFBLEtBQUQsR0FBUyxJQUFULENBREQ7S0FBQTtXQUdBLElBQUMsQ0FBQSxLQUFEOztBQUFTO1dBQVMsNkZBQVQsR0FBQTtBQUNSOztBQUFBO2VBQVMsa0dBQVQsR0FBQTtBQUNDLDJCQUFBLE1BQUEsQ0FERDtBQUFBOztzQkFBQSxDQURRO0FBQUE7O2tCQUpHO0VBQUEsQ0FsQmIsQ0FBQTs7QUFBQSxpQkEwQkEsVUFBQSxHQUFZLFNBQUUsR0FBRixFQUFjLFNBQWQsR0FBQTs7TUFBRSxNQUFJO0tBQ2pCOztNQUR5QixZQUFVO0tBQ25DO0FBQUEsSUFBQSxJQUFHLElBQUMsQ0FBQSxNQUFELEtBQVcsSUFBZDtBQUNDLFlBQUEsQ0FERDtLQUFBO0FBQUEsSUFHQSxJQUFDLENBQUEsSUFBSSxDQUFDLFNBQU4sQ0FBZ0IsQ0FBaEIsRUFBbUIsQ0FBbkIsRUFBc0IsSUFBQyxDQUFBLE1BQU0sQ0FBQyxLQUE5QixFQUFxQyxJQUFDLENBQUEsTUFBTSxDQUFDLE1BQTdDLENBSEEsQ0FBQTtBQUFBLElBSUEsSUFBQyxDQUFBLElBQUksQ0FBQyxXQUFOLEdBQW9CLEdBSnBCLENBQUE7QUFBQSxJQUtBLElBQUMsQ0FBQSxJQUFJLENBQUMsU0FBTixHQUFrQixTQUxsQixDQUFBO0FBQUEsSUFNQSxJQUFDLENBQUEsSUFBSSxDQUFDLFNBQU4sQ0FBQSxDQU5BLENBQUE7QUFBQSxJQU9BLElBQUMsQ0FBQSxTQUFELENBQVcsSUFBQyxDQUFBLElBQVosQ0FQQSxDQUFBO1dBUUEsSUFBQyxDQUFBLElBQUksQ0FBQyxNQUFOLENBQUEsRUFUVztFQUFBLENBMUJaLENBQUE7O0FBQUEsaUJBcUNBLFNBQUEsR0FBVyxTQUFFLEdBQUYsRUFBTyxNQUFQLEVBQThCLEtBQTlCLEdBQUE7QUFDVixRQUFBLHFDQUFBOztNQURpQixTQUFPO0FBQUEsUUFBRSxDQUFBLEVBQUcsQ0FBTDtBQUFBLFFBQVEsQ0FBQSxFQUFHLENBQVg7O0tBQ3hCOztNQUR3QyxRQUFNO0tBQzlDO0FBQUEsSUFBQSxFQUFBLEdBQUssTUFBTSxDQUFDLENBQVosQ0FBQTtBQUFBLElBQ0EsRUFBQSxHQUFLLE1BQU0sQ0FBQyxDQUFQLEdBQVcsSUFBQyxDQUFBLElBQUksQ0FBQyxDQUFOLEdBQVUsS0FEMUIsQ0FBQTtBQUFBLElBRUEsRUFBQSxHQUFLLE1BQU0sQ0FBQyxDQUZaLENBQUE7QUFBQSxJQUdBLEVBQUEsR0FBSyxNQUFNLENBQUMsQ0FBUCxHQUFXLElBQUMsQ0FBQSxJQUFJLENBQUMsQ0FBTixHQUFVLEtBSDFCLENBQUE7QUFLQTtTQUFTLDZGQUFULEdBQUE7QUFDQyxNQUFBLEdBQUcsQ0FBQyxNQUFKLENBQVcsRUFBWCxFQUFlLEVBQUEsR0FBSyxJQUFDLENBQUEsSUFBSSxDQUFDLENBQU4sR0FBVSxDQUFWLEdBQWMsS0FBbEMsQ0FBQSxDQUFBO0FBQUEsTUFDQSxHQUFHLENBQUMsTUFBSixDQUFXLEVBQVgsRUFBZSxFQUFBLEdBQUssSUFBQyxDQUFBLElBQUksQ0FBQyxDQUFOLEdBQVUsQ0FBVixHQUFjLEtBQWxDLENBREEsQ0FBQTtBQUFBLE1BRUEsR0FBRyxDQUFDLE1BQUosQ0FBVyxFQUFBLEdBQUssSUFBQyxDQUFBLElBQUksQ0FBQyxDQUFOLEdBQVUsQ0FBVixHQUFjLEtBQTlCLEVBQXFDLEVBQXJDLENBRkEsQ0FBQTtBQUFBLG9CQUdBLEdBQUcsQ0FBQyxNQUFKLENBQVcsRUFBQSxHQUFLLElBQUMsQ0FBQSxJQUFJLENBQUMsQ0FBTixHQUFVLENBQVYsR0FBYyxLQUE5QixFQUFxQyxFQUFyQyxFQUhBLENBREQ7QUFBQTtvQkFOVTtFQUFBLENBckNYLENBQUE7O0FBQUEsaUJBa0RBLG1CQUFBLEdBQXFCLFNBQUUsQ0FBRixFQUFPLENBQVAsRUFBWSxJQUFaLEdBQUE7QUFFcEIsUUFBQSxJQUFBOztNQUZzQixJQUFFO0tBRXhCOztNQUYyQixJQUFFO0tBRTdCOztNQUZnQyxPQUFLO0tBRXJDO0FBQUEsSUFBQSxJQUFHLENBQUEsR0FBSSxDQUFKLElBQVMsQ0FBQSxHQUFJLElBQUMsQ0FBQSxJQUFJLENBQUMsQ0FBdEI7QUFDQyxhQUFPLElBQVAsQ0FERDtLQUFBO0FBRUEsSUFBQSxJQUFHLENBQUEsR0FBSSxDQUFKLElBQVMsQ0FBQSxHQUFJLElBQUMsQ0FBQSxJQUFJLENBQUMsQ0FBdEI7QUFDQyxhQUFPLElBQVAsQ0FERDtLQUZBO0FBQUEsSUFLQSxDQUFBLEdBQUksSUFBSSxDQUFDLEtBQUwsQ0FBVyxDQUFFLENBQUEsR0FBSSxJQUFDLENBQUEsSUFBSSxDQUFDLENBQVosQ0FBQSxHQUFrQixJQUFDLENBQUEsR0FBOUIsQ0FMSixDQUFBO0FBQUEsSUFNQSxDQUFBLEdBQUksSUFBSSxDQUFDLEtBQUwsQ0FBVyxDQUFFLENBQUEsR0FBSSxJQUFDLENBQUEsSUFBSSxDQUFDLENBQVosQ0FBQSxHQUFrQixJQUFDLENBQUEsR0FBOUIsQ0FOSixDQUFBO0FBUUEsSUFBQSxJQUFHLElBQUg7QUFDQyxNQUFBLElBQUMsQ0FBQSxLQUFNLENBQUEsQ0FBQSxDQUFHLENBQUEsQ0FBQSxDQUFWLEdBQWUsSUFBZixDQUREO0tBUkE7QUFXQSxXQUFPLElBQUMsQ0FBQSxXQUFELENBQWEsQ0FBYixFQUFnQixDQUFoQixDQUFQLENBYm9CO0VBQUEsQ0FsRHJCLENBQUE7O0FBQUEsaUJBaUVBLFdBQUEsR0FBYSxTQUFFLENBQUYsRUFBTyxDQUFQLEdBQUE7O01BQUUsSUFBRTtLQUNoQjs7TUFEbUIsSUFBRTtLQUNyQjtXQUFJLElBQUEsS0FBQSxDQUFNLElBQUMsQ0FBQSxJQUFJLENBQUMsQ0FBTixHQUFVLENBQWhCLEVBQW1CLElBQUMsQ0FBQSxJQUFJLENBQUMsQ0FBTixHQUFVLENBQTdCLEVBQWdDLElBQUMsQ0FBQSxJQUFJLENBQUMsQ0FBdEMsRUFBeUMsSUFBQyxDQUFBLElBQUksQ0FBQyxDQUEvQyxFQURRO0VBQUEsQ0FqRWIsQ0FBQTs7QUFBQSxpQkFvRUEsaUJBQUEsR0FBbUIsU0FBRSxNQUFGLEdBQUE7O01BQUUsU0FBTztLQUMzQjtBQUFBLFdBQVcsSUFBQSxJQUFBLENBQUssSUFBSSxDQUFDLEtBQUwsQ0FBWSxJQUFDLENBQUEsR0FBRCxHQUFPLE1BQW5CLENBQUwsRUFBa0MsSUFBQyxDQUFBLElBQUksQ0FBQyxLQUFOLENBQUEsQ0FBbEMsQ0FBWCxDQURrQjtFQUFBLENBcEVuQixDQUFBOztBQUFBLGlCQXVFQSxLQUFBLEdBQU8sU0FBQSxHQUFBO0FBQ04sUUFBQSw0QkFBQTtBQUFBLElBQUEsQ0FBQSxHQUFJLElBQUMsQ0FBQSxpQkFBRCxDQUFtQixDQUFuQixDQUFKLENBQUE7QUFFQSxTQUFTLDJGQUFULEdBQUE7QUFDQyxXQUFTLGdHQUFULEdBQUE7QUFDQyxRQUFBLENBQUMsQ0FBQyxLQUFNLENBQUEsQ0FBQSxDQUFHLENBQUEsQ0FBQSxDQUFYLEdBQWdCLElBQUMsQ0FBQSxLQUFNLENBQUEsQ0FBQSxDQUFHLENBQUEsQ0FBQSxDQUExQixDQUREO0FBQUEsT0FERDtBQUFBLEtBRkE7QUFNQSxXQUFPLENBQVAsQ0FQTTtFQUFBLENBdkVQLENBQUE7O0FBQUEsaUJBZ0ZBLFVBQUEsR0FBWSxTQUFBLEdBQUE7QUFDWCxRQUFBLHdCQUFBO0FBQUE7U0FBUywyRkFBVCxHQUFBO0FBQ0M7O0FBQUE7YUFBUyxnR0FBVCxHQUFBO0FBQ0MseUJBQUEsSUFBQyxDQUFBLEtBQU0sQ0FBQSxDQUFBLENBQUcsQ0FBQSxDQUFBLENBQVYsR0FBZSxNQUFmLENBREQ7QUFBQTs7b0JBQUEsQ0FERDtBQUFBO29CQURXO0VBQUEsQ0FoRlosQ0FBQTs7QUFBQSxpQkFxRkEsUUFBQSxHQUFXLFNBQUUsQ0FBRixFQUFLLENBQUwsRUFBUSxHQUFSLEdBQUE7O01BQVEsTUFBSTtLQUN0QjtXQUFBLElBQUMsQ0FBQSxLQUFNLENBQUEsQ0FBQSxDQUFHLENBQUEsQ0FBQSxDQUFWLEdBQWUsSUFETDtFQUFBLENBckZYLENBQUE7O0FBQUEsaUJBd0ZBLGVBQUEsR0FBaUIsU0FBRSxJQUFGLEVBQW1CLFNBQW5CLEdBQUE7QUFDaEIsUUFBQSxnQ0FBQTs7TUFEa0IsT0FBSyxJQUFJLENBQUMsR0FBTCxDQUFBO0tBQ3ZCOztNQURtQyxZQUFVO0tBQzdDO0FBQUEsSUFBQSxNQUFBLEdBQWEsSUFBQSxFQUFFLENBQUMsSUFBSSxDQUFDLE1BQVIsQ0FBZSxJQUFmLENBQWIsQ0FBQTtBQUNBO1NBQVMsMkZBQVQsR0FBQTtBQUNDOztBQUFBO2FBQVMsZ0dBQVQsR0FBQTtBQUNDLHlCQUFBLElBQUMsQ0FBQSxLQUFNLENBQUEsQ0FBQSxDQUFHLENBQUEsQ0FBQSxDQUFWLEdBQWUsTUFBTSxDQUFDLE1BQVAsQ0FBQSxDQUFBLEdBQWtCLFVBQWpDLENBREQ7QUFBQTs7b0JBQUEsQ0FERDtBQUFBO29CQUZnQjtFQUFBLENBeEZqQixDQUFBOztBQUFBLGlCQThGQSxtQkFBQSxHQUFxQixTQUFFLEdBQUYsR0FBQTtBQUNwQixRQUFBLDJCQUFBO0FBQUE7U0FBUywyRkFBVCxHQUFBO0FBQ0M7O0FBQUE7YUFBUyxnR0FBVCxHQUFBO0FBQ0MsVUFBQSxJQUFHLElBQUMsQ0FBQSxLQUFNLENBQUEsQ0FBQSxDQUFHLENBQUEsQ0FBQSxDQUFiO0FBQ0MsWUFBQSxDQUFBLEdBQUksSUFBQyxDQUFBLFdBQUQsQ0FBYSxDQUFiLEVBQWdCLENBQWhCLENBQUosQ0FBQTtBQUFBLDJCQUNBLEdBQUcsQ0FBQyxJQUFKLENBQVMsQ0FBQyxDQUFDLENBQVgsRUFBYyxDQUFDLENBQUMsQ0FBaEIsRUFBbUIsQ0FBQyxDQUFDLEtBQXJCLEVBQTRCLENBQUMsQ0FBQyxNQUE5QixFQURBLENBREQ7V0FBQSxNQUFBO21DQUFBO1dBREQ7QUFBQTs7b0JBQUEsQ0FERDtBQUFBO29CQURvQjtFQUFBLENBOUZyQixDQUFBOztBQUFBLGlCQXFHQSxhQUFBLEdBQWUsU0FBRSxDQUFGLEVBQUssQ0FBTCxHQUFBO0FBQ2QsUUFBQSxDQUFBO0FBQUEsSUFBQSxDQUFBLEdBQUksQ0FBRSxJQUFGLEVBQVEsSUFBUixFQUFjLElBQWQsRUFBb0IsSUFBcEIsRUFBMEIsSUFBMUIsRUFBZ0MsSUFBaEMsRUFBc0MsSUFBdEMsRUFBNEMsSUFBNUMsQ0FBSixDQUFBO0FBRUEsSUFBQSxJQUFHLENBQUEsR0FBSSxDQUFQO0FBQ0MsTUFBQSxDQUFFLENBQUEsQ0FBQSxDQUFGLEdBQU8sSUFBQyxDQUFBLEtBQU0sQ0FBQSxDQUFBLEdBQUUsQ0FBRixDQUFLLENBQUEsQ0FBQSxDQUFuQixDQUFBO0FBQ0EsTUFBQSxJQUFHLENBQUEsR0FBSSxJQUFDLENBQUEsR0FBRCxHQUFLLENBQVo7QUFDQyxRQUFBLENBQUUsQ0FBQSxDQUFBLENBQUYsR0FBTyxJQUFDLENBQUEsS0FBTSxDQUFBLENBQUEsR0FBRSxDQUFGLENBQUssQ0FBQSxDQUFBLEdBQUUsQ0FBRixDQUFuQixDQUREO09BRkQ7S0FGQTtBQU9BLElBQUEsSUFBRyxDQUFBLEdBQUksSUFBQyxDQUFBLEdBQUQsR0FBSyxDQUFaO0FBQ0MsTUFBQSxDQUFFLENBQUEsQ0FBQSxDQUFGLEdBQU8sSUFBQyxDQUFBLEtBQU0sQ0FBQSxDQUFBLENBQUcsQ0FBQSxDQUFBLEdBQUUsQ0FBRixDQUFqQixDQUFBO0FBQ0EsTUFBQSxJQUFHLENBQUEsR0FBSSxJQUFDLENBQUEsR0FBRCxHQUFLLENBQVo7QUFDQyxRQUFBLENBQUUsQ0FBQSxDQUFBLENBQUYsR0FBTyxJQUFDLENBQUEsS0FBTSxDQUFBLENBQUEsR0FBRSxDQUFGLENBQUssQ0FBQSxDQUFBLEdBQUUsQ0FBRixDQUFuQixDQUREO09BRkQ7S0FQQTtBQVlBLElBQUEsSUFBRyxDQUFBLEdBQUksSUFBQyxDQUFBLEdBQUQsR0FBSyxDQUFaO0FBQ0MsTUFBQSxDQUFFLENBQUEsQ0FBQSxDQUFGLEdBQU8sSUFBQyxDQUFBLEtBQU0sQ0FBQSxDQUFBLEdBQUUsQ0FBRixDQUFLLENBQUEsQ0FBQSxDQUFuQixDQUFBO0FBQ0EsTUFBQSxJQUFHLENBQUEsR0FBSSxDQUFQO0FBQ0MsUUFBQSxDQUFFLENBQUEsQ0FBQSxDQUFGLEdBQU8sSUFBQyxDQUFBLEtBQU0sQ0FBQSxDQUFBLEdBQUUsQ0FBRixDQUFLLENBQUEsQ0FBQSxHQUFFLENBQUYsQ0FBbkIsQ0FERDtPQUZEO0tBWkE7QUFpQkEsSUFBQSxJQUFHLENBQUEsR0FBSSxDQUFQO0FBQ0MsTUFBQSxDQUFFLENBQUEsQ0FBQSxDQUFGLEdBQU8sSUFBQyxDQUFBLEtBQU0sQ0FBQSxDQUFBLENBQUcsQ0FBQSxDQUFBLEdBQUUsQ0FBRixDQUFqQixDQUFBO0FBQ0EsTUFBQSxJQUFHLENBQUEsR0FBSSxDQUFQO0FBQ0MsUUFBQSxDQUFFLENBQUEsQ0FBQSxDQUFGLEdBQU8sSUFBQyxDQUFBLEtBQU0sQ0FBQSxDQUFBLEdBQUUsQ0FBRixDQUFLLENBQUEsQ0FBQSxHQUFFLENBQUYsQ0FBbkIsQ0FERDtPQUZEO0tBakJBO0FBc0JBLFdBQU8sQ0FBUCxDQXZCYztFQUFBLENBckdmLENBQUE7O0FBQUEsaUJBOEhBLHFCQUFBLEdBQXVCLFNBQUUsQ0FBRixHQUFBO0FBQ3RCLFFBQUEsNEJBQUE7QUFBQSxJQUFBLElBQUcsQ0FBQyxDQUFDLEdBQUYsS0FBUyxJQUFDLENBQUEsR0FBYjtBQUNDLGFBQU8sQ0FBQSxDQUFQLENBREQ7S0FBQTtBQUFBLElBR0EsQ0FBQSxHQUFJLENBSEosQ0FBQTtBQUlBLFNBQVMsMkZBQVQsR0FBQTtBQUNDLFdBQVMsZ0dBQVQsR0FBQTtBQUNDLFFBQUEsSUFBRyxJQUFDLENBQUEsS0FBTSxDQUFBLENBQUEsQ0FBRyxDQUFBLENBQUEsQ0FBVixLQUFnQixDQUFDLENBQUMsS0FBTSxDQUFBLENBQUEsQ0FBRyxDQUFBLENBQUEsQ0FBOUI7QUFDQyxVQUFBLENBQUEsRUFBQSxDQUREO1NBREQ7QUFBQSxPQUREO0FBQUEsS0FKQTtBQVNBLFdBQU8sQ0FBUCxDQVZzQjtFQUFBLENBOUh2QixDQUFBOztjQUFBOztJQVBELENBQUE7O0FBQUEsTUFpSk0sQ0FBQyxPQUFQLEdBQWlCLElBakpqQixDQUFBOzs7OztBQ0VBLElBQUEsS0FBQTs7QUFBQTtBQUNjLEVBQUEsZUFBRyxDQUFILEVBQVMsQ0FBVCxFQUFlLEtBQWYsRUFBeUIsTUFBekIsR0FBQTtBQUFxQyxJQUFuQyxJQUFDLENBQUEsZ0JBQUEsSUFBRSxDQUFnQyxDQUFBO0FBQUEsSUFBN0IsSUFBQyxDQUFBLGdCQUFBLElBQUUsQ0FBMEIsQ0FBQTtBQUFBLElBQXZCLElBQUMsQ0FBQSx3QkFBQSxRQUFNLENBQWdCLENBQUE7QUFBQSxJQUFiLElBQUMsQ0FBQSwwQkFBQSxTQUFPLENBQUssQ0FBckM7RUFBQSxDQUFiOztBQUFBLGtCQUVBLE9BQUEsR0FBUyxTQUFFLENBQUYsR0FBQTtBQUNSLFdBQU8sQ0FBRSxDQUFDLENBQUMsQ0FBRixHQUFNLENBQUMsQ0FBQyxLQUFSLElBQWlCLElBQUMsQ0FBQSxDQUFwQixDQUFBLElBQTJCLENBQUMsQ0FBQyxDQUFDLENBQUYsSUFBTyxJQUFDLENBQUEsQ0FBRCxHQUFLLElBQUMsQ0FBQSxLQUFkLENBQTNCLElBQW9ELENBQUUsQ0FBQyxDQUFDLENBQUYsR0FBTSxDQUFDLENBQUMsTUFBUixJQUFrQixJQUFDLENBQUEsQ0FBckIsQ0FBcEQsSUFBZ0YsQ0FBQyxDQUFDLENBQUMsQ0FBRixJQUFPLElBQUMsQ0FBQSxDQUFELEdBQUssSUFBQyxDQUFBLE1BQWQsQ0FBdkYsQ0FEUTtFQUFBLENBRlQsQ0FBQTs7ZUFBQTs7SUFERCxDQUFBOztBQUFBLE1BTU0sQ0FBQyxPQUFQLEdBQWlCLEtBTmpCLENBQUE7Ozs7O0FDQ0EsSUFBQSxVQUFBOztBQUFBO0FBQ2MsRUFBQSxvQkFBRyxJQUFILEdBQUE7QUFDWixJQURjLElBQUMsQ0FBQSxPQUFBLElBQ2YsQ0FBQTtBQUFBLElBQUEsSUFBQyxDQUFBLFVBQUQsR0FBYyxDQUFkLENBQUE7QUFBQSxJQUNBLElBQUMsQ0FBQSxJQUFELEdBQVEsQ0FEUixDQUFBO0FBQUEsSUFFQSxJQUFDLENBQUEsTUFBRCxHQUFVLENBQUUsQ0FBRixFQUFLLENBQUwsRUFBUSxDQUFSLEVBQVcsQ0FBWCxDQUZWLENBQUE7QUFBQSxJQUdBLElBQUMsQ0FBQSxTQUFELEdBQWEsSUFBSSxDQUFDLEdBQUwsQ0FBQSxDQUFBLEdBQWEsSUFIMUIsQ0FEWTtFQUFBLENBQWI7O0FBQUEsdUJBS0EsTUFBQSxHQUFRLFNBQUEsR0FBQTtBQUNQLFFBQUEsbURBQUE7QUFBQSxJQUFBLElBQUMsQ0FBQSxJQUFELEdBQVEsQ0FBRSxJQUFDLENBQUEsSUFBRCxHQUFRLENBQVYsQ0FBQSxHQUFrQixJQUFDLENBQUEsVUFBM0IsQ0FBQTtBQUVBLElBQUEsSUFBRyxJQUFDLENBQUEsSUFBRCxHQUFRLENBQVg7QUFDQyxZQUFBLENBREQ7S0FGQTtBQUFBLElBTUEsQ0FBQSxHQUFJLElBQUMsQ0FBQSxJQUFJLENBQUMsS0FBTixDQUFBLENBTkosQ0FBQTtBQVFBLFNBQVMsd0ZBQVQsR0FBQTtBQUNDLFdBQVMsNkZBQVQsR0FBQTtBQUNDLFFBQUEsQ0FBQSxHQUFJLENBQUMsQ0FBQyxhQUFGLENBQWdCLENBQWhCLEVBQW1CLENBQW5CLENBQUosQ0FBQTtBQUFBLFFBQ0EsS0FBQSxHQUFRLENBRFIsQ0FBQTtBQUVBLGFBQVMsNkJBQVQsR0FBQTtBQUNDLFVBQUEsSUFBRyxDQUFFLENBQUEsQ0FBQSxDQUFGLEtBQVEsSUFBWDtBQUNDLFlBQUEsS0FBQSxFQUFBLENBREQ7V0FERDtBQUFBLFNBRkE7QUFNQSxRQUFBLElBQUcsQ0FBQyxDQUFDLEtBQU0sQ0FBQSxDQUFBLENBQUcsQ0FBQSxDQUFBLENBQWQ7QUFFQyxVQUFBLElBQUcsS0FBQSxHQUFRLENBQVIsSUFBYSxLQUFBLEdBQVEsQ0FBeEI7QUFFQyxZQUFBLElBQUMsQ0FBQSxJQUFJLENBQUMsS0FBTSxDQUFBLENBQUEsQ0FBRyxDQUFBLENBQUEsQ0FBZixHQUFvQixLQUFwQixDQUZEO1dBRkQ7U0FBQSxNQUFBO0FBUUMsVUFBQSxJQUFHLEtBQUEsS0FBUyxDQUFaO0FBRUMsWUFBQSxJQUFDLENBQUEsSUFBSSxDQUFDLEtBQU0sQ0FBQSxDQUFBLENBQUcsQ0FBQSxDQUFBLENBQWYsR0FBb0IsSUFBcEIsQ0FGRDtXQVJEO1NBUEQ7QUFBQSxPQUREO0FBQUEsS0FSQTtBQUFBLElBNEJBLENBQUEsR0FBSSxJQUFDLENBQUEsSUFBSSxDQUFDLHFCQUFOLENBQTZCLENBQTdCLENBNUJKLENBQUE7QUFBQSxJQTZCQSxJQUFDLENBQUEsTUFBTyxDQUFBLENBQUEsQ0FBUixHQUFhLElBQUMsQ0FBQSxNQUFPLENBQUEsQ0FBQSxDQTdCckIsQ0FBQTtBQUFBLElBOEJBLElBQUMsQ0FBQSxNQUFPLENBQUEsQ0FBQSxDQUFSLEdBQWEsSUFBQyxDQUFBLE1BQU8sQ0FBQSxDQUFBLENBOUJyQixDQUFBO0FBQUEsSUErQkEsSUFBQyxDQUFBLE1BQU8sQ0FBQSxDQUFBLENBQVIsR0FBYSxDQS9CYixDQUFBO0FBaUNBLElBQUEsSUFBRyxJQUFDLENBQUEsTUFBTyxDQUFBLENBQUEsQ0FBUixLQUFjLElBQUMsQ0FBQSxNQUFPLENBQUEsQ0FBQSxDQUF0QixJQUE0QixJQUFDLENBQUEsTUFBTyxDQUFBLENBQUEsQ0FBUixLQUFjLElBQUMsQ0FBQSxNQUFPLENBQUEsQ0FBQSxDQUFyRDtBQUNDLE1BQUEsSUFBQyxDQUFBLE1BQU8sQ0FBQSxDQUFBLENBQVIsRUFBQSxDQUREO0tBQUEsTUFBQTtBQUdDLE1BQUEsSUFBQyxDQUFBLE1BQU8sQ0FBQSxDQUFBLENBQVIsR0FBYSxDQUFiLENBSEQ7S0FqQ0E7QUFBQSxJQXNDQSxDQUFBLEdBQUksSUFBSSxDQUFDLEdBQUwsQ0FBQSxDQUFBLEdBQWEsSUF0Q2pCLENBQUE7QUF3Q0EsSUFBQSxJQUFHLENBQUEsR0FBSSxDQUFKLElBQVMsSUFBQyxDQUFBLE1BQU8sQ0FBQSxDQUFBLENBQVIsR0FBYSxDQUF0QixJQUEyQixDQUFBLEdBQUksSUFBQyxDQUFBLFNBQUwsR0FBaUIsRUFBL0M7YUFDQyxJQUFDLENBQUEsT0FBRCxDQUFBLEVBREQ7S0F6Q087RUFBQSxDQUxSLENBQUE7O0FBQUEsdUJBaURBLE9BQUEsR0FBUyxTQUFBLEdBQUE7QUFDUixJQUFBLElBQUMsQ0FBQSxTQUFELEdBQWEsSUFBSSxDQUFDLEdBQUwsQ0FBQSxDQUFBLEdBQWEsSUFBMUIsQ0FBQTtXQUNBLElBQUMsQ0FBQSxJQUFJLENBQUMsZUFBTixDQUFBLEVBRlE7RUFBQSxDQWpEVCxDQUFBOztBQUFBLHVCQXFEQSxNQUFBLEdBQVEsU0FBRSxHQUFGLEVBQU8sS0FBUCxHQUFBOztNQUFPLFFBQU07S0FDcEI7QUFBQSxJQUFBLElBQUcsQ0FBQSxLQUFIO0FBQ0MsWUFBQSxDQUREO0tBQUE7QUFBQSxJQUdBLEdBQUcsQ0FBQyxTQUFKLEdBQWdCLDJCQUhoQixDQUFBO0FBQUEsSUFJQSxHQUFHLENBQUMsU0FBSixDQUFBLENBSkEsQ0FBQTtBQUFBLElBS0EsSUFBQyxDQUFBLElBQUksQ0FBQyxtQkFBTixDQUEwQixHQUExQixDQUxBLENBQUE7V0FNQSxHQUFHLENBQUMsSUFBSixDQUFBLEVBUE87RUFBQSxDQXJEUixDQUFBOztvQkFBQTs7SUFERCxDQUFBOztBQUFBLE1BK0RNLENBQUMsT0FBUCxHQUFpQixVQS9EakIsQ0FBQTs7Ozs7QUNIQSxJQUFBLHdCQUFBOztBQUFBLElBQUEsR0FBTyxPQUFBLENBQVEscUJBQVIsQ0FBUCxDQUFBOztBQUFBLEVBRUEsR0FBSyxPQUFBLENBQVEsVUFBUixDQUZMLENBQUE7O0FBQUEsSUFHQSxHQUFPLEVBQUUsQ0FBQyxJQUFJLENBQUMsSUFIZixDQUFBOztBQUFBO0FBTWMsRUFBQSxrQkFBRSxTQUFGLEdBQUE7QUFDWixJQUFBLElBQUMsQ0FBQSxLQUFELEdBQVMsSUFBSSxDQUFDLEdBQUwsQ0FBUyxNQUFNLENBQUMsVUFBaEIsRUFBNEIsR0FBNUIsQ0FBVCxDQUFBO0FBQUEsSUFFQSxJQUFDLENBQUEsTUFBRCxHQUFVLEVBQUEsQ0FBRyxRQUFILENBRlYsQ0FBQTtBQUFBLElBR0EsSUFBQyxDQUFBLE1BQU0sQ0FBQyxLQUFSLEdBQWdCLElBQUMsQ0FBQSxLQUhqQixDQUFBO0FBQUEsSUFJQSxJQUFDLENBQUEsTUFBTSxDQUFDLE1BQVIsR0FBaUIsSUFBQyxDQUFBLEtBSmxCLENBQUE7QUFBQSxJQU1BLElBQUMsQ0FBQSxHQUFELEdBQU8sSUFBQyxDQUFBLE1BQU0sQ0FBQyxVQUFSLENBQW1CLElBQW5CLENBTlAsQ0FBQTtBQUFBLElBUUEsU0FBUyxDQUFDLFdBQVYsQ0FBc0IsSUFBQyxDQUFBLE1BQXZCLENBUkEsQ0FBQTtBQUFBLElBVUEsSUFBQyxDQUFBLElBQUQsR0FBWSxJQUFBLElBQUEsQ0FBSyxFQUFMLEVBQWEsSUFBQSxJQUFBLENBQU0sSUFBQyxDQUFBLEtBQVAsRUFBYyxJQUFDLENBQUEsS0FBZixDQUFiLEVBQXFDLElBQXJDLENBVlosQ0FBQTtBQUFBLElBWUEsSUFBQyxDQUFBLElBQUksQ0FBQyxVQUFOLENBQWlCLE1BQWpCLEVBQXlCLEdBQXpCLENBWkEsQ0FEWTtFQUFBLENBQWI7O0FBQUEscUJBZUEsTUFBQSxHQUFRLFNBQUEsR0FBQTtBQUNQLElBQUEsSUFBQyxDQUFBLEdBQUcsQ0FBQyxTQUFMLENBQWUsQ0FBZixFQUFrQixDQUFsQixFQUFxQixJQUFDLENBQUEsS0FBdEIsRUFBNkIsSUFBQyxDQUFBLEtBQTlCLENBQUEsQ0FBQTtBQUFBLElBR0EsSUFBQyxDQUFBLEdBQUcsQ0FBQyxTQUFMLENBQWUsSUFBQyxDQUFBLElBQUksQ0FBQyxNQUFyQixFQUE2QixDQUE3QixFQUFnQyxDQUFoQyxDQUhBLENBQUE7QUFBQSxJQU1BLElBQUMsQ0FBQSxHQUFHLENBQUMsU0FBTCxHQUFpQixNQU5qQixDQUFBO0FBQUEsSUFPQSxJQUFDLENBQUEsR0FBRyxDQUFDLFNBQUwsQ0FBQSxDQVBBLENBQUE7QUFBQSxJQVFBLElBQUMsQ0FBQSxJQUFJLENBQUMsbUJBQU4sQ0FBMEIsSUFBQyxDQUFBLEdBQTNCLENBUkEsQ0FBQTtXQVNBLElBQUMsQ0FBQSxHQUFHLENBQUMsSUFBTCxDQUFBLEVBVk87RUFBQSxDQWZSLENBQUE7O0FBQUEscUJBMkJBLE1BQUEsR0FBUSxTQUFBLEdBQUE7QUFDUCxJQUFBLElBQUMsQ0FBQSxLQUFELEdBQVMsSUFBSSxDQUFDLEdBQUwsQ0FBUyxNQUFNLENBQUMsVUFBaEIsRUFBNEIsR0FBNUIsQ0FBVCxDQUFBO0FBQUEsSUFFQSxJQUFDLENBQUEsTUFBTSxDQUFDLEtBQVIsR0FBZ0IsSUFBQyxDQUFBLEtBRmpCLENBQUE7QUFBQSxJQUdBLElBQUMsQ0FBQSxNQUFNLENBQUMsTUFBUixHQUFpQixJQUFDLENBQUEsS0FIbEIsQ0FBQTtBQUFBLElBSUEsSUFBQyxDQUFBLElBQUksQ0FBQyxPQUFOLENBQWMsSUFBQyxDQUFBLEtBQWYsRUFBc0IsSUFBQyxDQUFBLEtBQXZCLENBSkEsQ0FBQTtXQUtBLElBQUMsQ0FBQSxJQUFJLENBQUMsVUFBTixDQUFpQixNQUFqQixFQUF5QixHQUF6QixFQU5PO0VBQUEsQ0EzQlIsQ0FBQTs7a0JBQUE7O0lBTkQsQ0FBQTs7QUFBQSxNQXlDTSxDQUFDLE9BQVAsR0FBaUIsUUF6Q2pCLENBQUE7Ozs7O0FDQUEsSUFBQSx3QkFBQTs7QUFBQSxHQUFBLEdBQU0sT0FBQSxDQUFRLG1CQUFSLENBQU4sQ0FBQTs7QUFBQSxJQUtBLEdBQU8sR0FBQSxDQUFBLFNBQUksR0FBQTtBQUNWLE1BQUEsOERBQUE7QUFBQSxFQUFBLFdBQUEsR0FBYyxLQUFkLENBQUE7QUFBQSxFQUNBLEdBQUEsR0FBTSxJQUROLENBQUE7QUFBQSxFQUdBLElBQUEsR0FBTyxTQUFBLEdBQUE7QUFDTixJQUFBLElBQUcsQ0FBQSxXQUFIO0FBQ0MsTUFBQSxXQUFBLEdBQWMsSUFBZCxDQUFBO0FBQUEsTUFFQSxHQUFBLEdBQVUsSUFBQSxHQUFBLENBQUksQ0FBQSxDQUFHLFdBQUgsQ0FBSixDQUZWLENBQUE7YUFHQSxPQUFBLENBQUEsRUFKRDtLQURNO0VBQUEsQ0FIUCxDQUFBO0FBQUEsRUFVQSxPQUFBLEdBQVUsU0FBQSxHQUFBO0FBQ1QsSUFBQSxxQkFBQSxDQUFzQixPQUF0QixDQUFBLENBQUE7QUFBQSxJQUVBLEdBQUcsQ0FBQyxNQUFKLENBQUEsQ0FGQSxDQUFBO1dBR0EsR0FBRyxDQUFDLE1BQUosQ0FBQSxFQUpTO0VBQUEsQ0FWVixDQUFBO0FBQUEsRUFtQkEsUUFBQSxHQUFXLFNBQUEsR0FBQTtBQUNWLElBQUEsSUFBSSxTQUFTLENBQUMsU0FBUyxDQUFDLEtBQXBCLENBQTBCLFVBQTFCLENBQUEsSUFBeUMsU0FBUyxDQUFDLFNBQVMsQ0FBQyxLQUFwQixDQUEwQixRQUExQixDQUF6QyxJQUFnRixTQUFTLENBQUMsU0FBUyxDQUFDLEtBQXBCLENBQTBCLFNBQTFCLENBQWhGLElBQXdILFNBQVMsQ0FBQyxTQUFTLENBQUMsS0FBcEIsQ0FBMEIsT0FBMUIsQ0FBeEgsSUFBOEosU0FBUyxDQUFDLFNBQVMsQ0FBQyxLQUFwQixDQUEwQixPQUExQixDQUE5SixJQUFvTSxTQUFTLENBQUMsU0FBUyxDQUFDLEtBQXBCLENBQTBCLGFBQTFCLENBQXBNLElBQWdQLFNBQVMsQ0FBQyxTQUFTLENBQUMsS0FBcEIsQ0FBMEIsZ0JBQTFCLENBQXBQO0FBQ0MsYUFBTyxJQUFQLENBREQ7S0FBQSxNQUFBO0FBSUMsYUFBTyxLQUFQLENBSkQ7S0FEVTtFQUFBLENBbkJYLENBQUE7QUFBQSxFQTBCQSxLQUFBLEdBQVEsU0FBQSxHQUFBO0FBQ1AsUUFBQSwwQkFBQTtBQUFBLElBQUEsQ0FBQSxHQUFJLEVBQUEsQ0FBRyxHQUFILENBQUosQ0FBQTtBQUFBLElBQ0EsS0FBQSxHQUFRLEtBRFIsQ0FBQTtBQUFBLElBRUEsVUFBQSxHQUFhO0FBQUEsTUFDWixpQkFBQSxFQUFrQixtQkFETjtBQUFBLE1BRVosWUFBQSxFQUFhLGNBRkQ7QUFBQSxNQUdaLGFBQUEsRUFBYyxlQUhGO0FBQUEsTUFJWixjQUFBLEVBQWUsZ0JBSkg7QUFBQSxNQUtaLFdBQUEsRUFBWSxXQUxBO0tBRmIsQ0FBQTtBQUFBLElBV0EsUUFBUSxDQUFDLElBQUksQ0FBQyxZQUFkLENBQTJCLENBQTNCLEVBQThCLElBQTlCLENBWEEsQ0FBQTtBQWFBLFNBQUEsZUFBQTt3QkFBQTtBQUNJLE1BQUEsSUFBSyxDQUFDLENBQUMsS0FBTSxDQUFBLENBQUEsQ0FBUixLQUFjLE1BQW5CO0FBQ0ksUUFBQSxDQUFDLENBQUMsS0FBTSxDQUFBLENBQUEsQ0FBUixHQUFhLDBCQUFiLENBQUE7QUFBQSxRQUNBLEtBQUEsR0FBUSxNQUFNLENBQUMsZ0JBQVAsQ0FBeUIsQ0FBekIsQ0FBNEIsQ0FBQyxnQkFBN0IsQ0FBK0MsVUFBVyxDQUFBLENBQUEsQ0FBMUQsQ0FEUixDQURKO09BREo7QUFBQSxLQWJBO0FBQUEsSUFrQkEsUUFBUSxDQUFDLElBQUksQ0FBQyxXQUFkLENBQTBCLENBQTFCLENBbEJBLENBQUE7QUFvQkEsV0FBUyxLQUFBLEtBQVMsTUFBVCxJQUFzQixLQUFLLENBQUMsTUFBTixHQUFlLENBQXJDLElBQTBDLEtBQUEsS0FBUyxNQUE1RCxDQXJCTztFQUFBLENBMUJSLENBQUE7QUFBQSxFQWlEQSxZQUFBLEdBQWUsU0FBRSxDQUFGLEVBQUssQ0FBTCxFQUFRLENBQVIsR0FBQTtBQUNkLFFBQUEsSUFBQTs7TUFEc0IsSUFBSSxDQUFFLFFBQUYsRUFBWSxLQUFaLEVBQW1CLElBQW5CO0tBQzFCO0FBQUEsU0FBQSxNQUFBO2VBQUE7QUFDQyxNQUFBLENBQUMsQ0FBQyxLQUFPLENBQUEsQ0FBRSxDQUFBLENBQUEsQ0FBRixHQUFPLFdBQVAsQ0FBVCxHQUFnQyxDQUFoQyxDQUREO0FBQUEsS0FBQTtXQUVBLENBQUMsQ0FBQyxLQUFLLENBQUMsU0FBUixHQUFvQixFQUhOO0VBQUEsQ0FqRGYsQ0FBQTtBQXNEQSxTQUFPO0FBQUEsSUFDTixJQUFBLEVBQU0sSUFEQTtBQUFBLElBRU4sV0FBQSxFQUFhLFNBQUEsR0FBQTtBQUNaLGFBQU8sUUFBUCxDQURZO0lBQUEsQ0FGUDtBQUFBLElBSU4sUUFBQSxFQUFVLFFBSko7QUFBQSxJQUtOLEtBQUEsRUFBTyxLQUxEO0FBQUEsSUFNTixZQUFBLEVBQWMsWUFOUjtHQUFQLENBdkRVO0FBQUEsQ0FMWCxDQUFBOztBQUFBLE9Bc0VBLEdBRUM7QUFBQSxFQUFBLENBQUEsRUFBSSxTQUFFLEVBQUYsR0FBQTtBQUNILFdBQU8sUUFBUSxDQUFDLGNBQVQsQ0FBd0IsRUFBeEIsQ0FBUCxDQURHO0VBQUEsQ0FBSjtBQUFBLEVBR0EsRUFBQSxFQUFLLFNBQUUsSUFBRixFQUFRLEdBQVIsR0FBQTtBQUNKLFFBQUEsQ0FBQTtBQUFBLElBQUEsQ0FBQSxHQUFJLFFBQVEsQ0FBQyxhQUFULENBQXVCLElBQXZCLENBQUosQ0FBQTtBQUNBLElBQUEsSUFBSyxHQUFMO0FBQ0MsTUFBQSxDQUFDLENBQUMsU0FBRixHQUFjLEdBQWQsQ0FERDtLQURBO0FBR0EsV0FBTyxDQUFQLENBSkk7RUFBQSxDQUhMO0FBQUEsRUFTQSxJQUFBLEVBQU0sSUFUTjtDQXhFRCxDQUFBOztBQW1GQSxLQUFBLFlBQUE7aUJBQUE7QUFDQyxFQUFBLE1BQU8sQ0FBQSxDQUFBLENBQVAsR0FBWSxDQUFaLENBREQ7QUFBQSxDQW5GQTs7QUF1RkEsSUFBRyxNQUFNLENBQUMsZ0JBQVY7QUFDQyxFQUFBLE1BQU0sQ0FBQyxnQkFBUCxDQUF3QixNQUF4QixFQUFnQyxTQUFFLENBQUYsR0FBQTtXQUMvQixJQUFJLENBQUMsSUFBTCxDQUFBLEVBRCtCO0VBQUEsQ0FBaEMsRUFFRSxLQUZGLENBQUEsQ0FERDtDQUFBLE1BSUssSUFBRyxNQUFNLENBQUMsV0FBVjtBQUNKLEVBQUEsTUFBTSxDQUFDLFdBQVAsQ0FBbUIsTUFBbkIsRUFBMkIsU0FBRSxDQUFGLEdBQUE7V0FDMUIsSUFBSSxDQUFDLElBQUwsQ0FBQSxFQUQwQjtFQUFBLENBQTNCLEVBRUUsS0FGRixDQUFBLENBREk7Q0EzRkwiLCJmaWxlIjoiZ2VuZXJhdGVkLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXNDb250ZW50IjpbIihmdW5jdGlvbiBlKHQsbixyKXtmdW5jdGlvbiBzKG8sdSl7aWYoIW5bb10pe2lmKCF0W29dKXt2YXIgYT10eXBlb2YgcmVxdWlyZT09XCJmdW5jdGlvblwiJiZyZXF1aXJlO2lmKCF1JiZhKXJldHVybiBhKG8sITApO2lmKGkpcmV0dXJuIGkobywhMCk7dmFyIGY9bmV3IEVycm9yKFwiQ2Fubm90IGZpbmQgbW9kdWxlICdcIitvK1wiJ1wiKTt0aHJvdyBmLmNvZGU9XCJNT0RVTEVfTk9UX0ZPVU5EXCIsZn12YXIgbD1uW29dPXtleHBvcnRzOnt9fTt0W29dWzBdLmNhbGwobC5leHBvcnRzLGZ1bmN0aW9uKGUpe3ZhciBuPXRbb11bMV1bZV07cmV0dXJuIHMobj9uOmUpfSxsLGwuZXhwb3J0cyxlLHQsbixyKX1yZXR1cm4gbltvXS5leHBvcnRzfXZhciBpPXR5cGVvZiByZXF1aXJlPT1cImZ1bmN0aW9uXCImJnJlcXVpcmU7Zm9yKHZhciBvPTA7bzxyLmxlbmd0aDtvKyspcyhyW29dKTtyZXR1cm4gc30pIiwiLyohXG4gKiBUaGUgYnVmZmVyIG1vZHVsZSBmcm9tIG5vZGUuanMsIGZvciB0aGUgYnJvd3Nlci5cbiAqXG4gKiBAYXV0aG9yICAgRmVyb3NzIEFib3VraGFkaWplaCA8ZmVyb3NzQGZlcm9zcy5vcmc+IDxodHRwOi8vZmVyb3NzLm9yZz5cbiAqIEBsaWNlbnNlICBNSVRcbiAqL1xuXG52YXIgYmFzZTY0ID0gcmVxdWlyZSgnYmFzZTY0LWpzJylcbnZhciBpZWVlNzU0ID0gcmVxdWlyZSgnaWVlZTc1NCcpXG52YXIgaXNBcnJheSA9IHJlcXVpcmUoJ2lzLWFycmF5JylcblxuZXhwb3J0cy5CdWZmZXIgPSBCdWZmZXJcbmV4cG9ydHMuU2xvd0J1ZmZlciA9IEJ1ZmZlclxuZXhwb3J0cy5JTlNQRUNUX01BWF9CWVRFUyA9IDUwXG5CdWZmZXIucG9vbFNpemUgPSA4MTkyIC8vIG5vdCB1c2VkIGJ5IHRoaXMgaW1wbGVtZW50YXRpb25cblxudmFyIGtNYXhMZW5ndGggPSAweDNmZmZmZmZmXG5cbi8qKlxuICogSWYgYEJ1ZmZlci5UWVBFRF9BUlJBWV9TVVBQT1JUYDpcbiAqICAgPT09IHRydWUgICAgVXNlIFVpbnQ4QXJyYXkgaW1wbGVtZW50YXRpb24gKGZhc3Rlc3QpXG4gKiAgID09PSBmYWxzZSAgIFVzZSBPYmplY3QgaW1wbGVtZW50YXRpb24gKG1vc3QgY29tcGF0aWJsZSwgZXZlbiBJRTYpXG4gKlxuICogQnJvd3NlcnMgdGhhdCBzdXBwb3J0IHR5cGVkIGFycmF5cyBhcmUgSUUgMTArLCBGaXJlZm94IDQrLCBDaHJvbWUgNyssIFNhZmFyaSA1LjErLFxuICogT3BlcmEgMTEuNissIGlPUyA0LjIrLlxuICpcbiAqIE5vdGU6XG4gKlxuICogLSBJbXBsZW1lbnRhdGlvbiBtdXN0IHN1cHBvcnQgYWRkaW5nIG5ldyBwcm9wZXJ0aWVzIHRvIGBVaW50OEFycmF5YCBpbnN0YW5jZXMuXG4gKiAgIEZpcmVmb3ggNC0yOSBsYWNrZWQgc3VwcG9ydCwgZml4ZWQgaW4gRmlyZWZveCAzMCsuXG4gKiAgIFNlZTogaHR0cHM6Ly9idWd6aWxsYS5tb3ppbGxhLm9yZy9zaG93X2J1Zy5jZ2k/aWQ9Njk1NDM4LlxuICpcbiAqICAtIENocm9tZSA5LTEwIGlzIG1pc3NpbmcgdGhlIGBUeXBlZEFycmF5LnByb3RvdHlwZS5zdWJhcnJheWAgZnVuY3Rpb24uXG4gKlxuICogIC0gSUUxMCBoYXMgYSBicm9rZW4gYFR5cGVkQXJyYXkucHJvdG90eXBlLnN1YmFycmF5YCBmdW5jdGlvbiB3aGljaCByZXR1cm5zIGFycmF5cyBvZlxuICogICAgaW5jb3JyZWN0IGxlbmd0aCBpbiBzb21lIHNpdHVhdGlvbnMuXG4gKlxuICogV2UgZGV0ZWN0IHRoZXNlIGJ1Z2d5IGJyb3dzZXJzIGFuZCBzZXQgYEJ1ZmZlci5UWVBFRF9BUlJBWV9TVVBQT1JUYCB0byBgZmFsc2VgIHNvIHRoZXkgd2lsbFxuICogZ2V0IHRoZSBPYmplY3QgaW1wbGVtZW50YXRpb24sIHdoaWNoIGlzIHNsb3dlciBidXQgd2lsbCB3b3JrIGNvcnJlY3RseS5cbiAqL1xuQnVmZmVyLlRZUEVEX0FSUkFZX1NVUFBPUlQgPSAoZnVuY3Rpb24gKCkge1xuICB0cnkge1xuICAgIHZhciBidWYgPSBuZXcgQXJyYXlCdWZmZXIoMClcbiAgICB2YXIgYXJyID0gbmV3IFVpbnQ4QXJyYXkoYnVmKVxuICAgIGFyci5mb28gPSBmdW5jdGlvbiAoKSB7IHJldHVybiA0MiB9XG4gICAgcmV0dXJuIDQyID09PSBhcnIuZm9vKCkgJiYgLy8gdHlwZWQgYXJyYXkgaW5zdGFuY2VzIGNhbiBiZSBhdWdtZW50ZWRcbiAgICAgICAgdHlwZW9mIGFyci5zdWJhcnJheSA9PT0gJ2Z1bmN0aW9uJyAmJiAvLyBjaHJvbWUgOS0xMCBsYWNrIGBzdWJhcnJheWBcbiAgICAgICAgbmV3IFVpbnQ4QXJyYXkoMSkuc3ViYXJyYXkoMSwgMSkuYnl0ZUxlbmd0aCA9PT0gMCAvLyBpZTEwIGhhcyBicm9rZW4gYHN1YmFycmF5YFxuICB9IGNhdGNoIChlKSB7XG4gICAgcmV0dXJuIGZhbHNlXG4gIH1cbn0pKClcblxuLyoqXG4gKiBDbGFzczogQnVmZmVyXG4gKiA9PT09PT09PT09PT09XG4gKlxuICogVGhlIEJ1ZmZlciBjb25zdHJ1Y3RvciByZXR1cm5zIGluc3RhbmNlcyBvZiBgVWludDhBcnJheWAgdGhhdCBhcmUgYXVnbWVudGVkXG4gKiB3aXRoIGZ1bmN0aW9uIHByb3BlcnRpZXMgZm9yIGFsbCB0aGUgbm9kZSBgQnVmZmVyYCBBUEkgZnVuY3Rpb25zLiBXZSB1c2VcbiAqIGBVaW50OEFycmF5YCBzbyB0aGF0IHNxdWFyZSBicmFja2V0IG5vdGF0aW9uIHdvcmtzIGFzIGV4cGVjdGVkIC0tIGl0IHJldHVybnNcbiAqIGEgc2luZ2xlIG9jdGV0LlxuICpcbiAqIEJ5IGF1Z21lbnRpbmcgdGhlIGluc3RhbmNlcywgd2UgY2FuIGF2b2lkIG1vZGlmeWluZyB0aGUgYFVpbnQ4QXJyYXlgXG4gKiBwcm90b3R5cGUuXG4gKi9cbmZ1bmN0aW9uIEJ1ZmZlciAoc3ViamVjdCwgZW5jb2RpbmcsIG5vWmVybykge1xuICBpZiAoISh0aGlzIGluc3RhbmNlb2YgQnVmZmVyKSlcbiAgICByZXR1cm4gbmV3IEJ1ZmZlcihzdWJqZWN0LCBlbmNvZGluZywgbm9aZXJvKVxuXG4gIHZhciB0eXBlID0gdHlwZW9mIHN1YmplY3RcblxuICAvLyBGaW5kIHRoZSBsZW5ndGhcbiAgdmFyIGxlbmd0aFxuICBpZiAodHlwZSA9PT0gJ251bWJlcicpXG4gICAgbGVuZ3RoID0gc3ViamVjdCA+IDAgPyBzdWJqZWN0ID4+PiAwIDogMFxuICBlbHNlIGlmICh0eXBlID09PSAnc3RyaW5nJykge1xuICAgIGlmIChlbmNvZGluZyA9PT0gJ2Jhc2U2NCcpXG4gICAgICBzdWJqZWN0ID0gYmFzZTY0Y2xlYW4oc3ViamVjdClcbiAgICBsZW5ndGggPSBCdWZmZXIuYnl0ZUxlbmd0aChzdWJqZWN0LCBlbmNvZGluZylcbiAgfSBlbHNlIGlmICh0eXBlID09PSAnb2JqZWN0JyAmJiBzdWJqZWN0ICE9PSBudWxsKSB7IC8vIGFzc3VtZSBvYmplY3QgaXMgYXJyYXktbGlrZVxuICAgIGlmIChzdWJqZWN0LnR5cGUgPT09ICdCdWZmZXInICYmIGlzQXJyYXkoc3ViamVjdC5kYXRhKSlcbiAgICAgIHN1YmplY3QgPSBzdWJqZWN0LmRhdGFcbiAgICBsZW5ndGggPSArc3ViamVjdC5sZW5ndGggPiAwID8gTWF0aC5mbG9vcigrc3ViamVjdC5sZW5ndGgpIDogMFxuICB9IGVsc2VcbiAgICB0aHJvdyBuZXcgVHlwZUVycm9yKCdtdXN0IHN0YXJ0IHdpdGggbnVtYmVyLCBidWZmZXIsIGFycmF5IG9yIHN0cmluZycpXG5cbiAgaWYgKHRoaXMubGVuZ3RoID4ga01heExlbmd0aClcbiAgICB0aHJvdyBuZXcgUmFuZ2VFcnJvcignQXR0ZW1wdCB0byBhbGxvY2F0ZSBCdWZmZXIgbGFyZ2VyIHRoYW4gbWF4aW11bSAnICtcbiAgICAgICdzaXplOiAweCcgKyBrTWF4TGVuZ3RoLnRvU3RyaW5nKDE2KSArICcgYnl0ZXMnKVxuXG4gIHZhciBidWZcbiAgaWYgKEJ1ZmZlci5UWVBFRF9BUlJBWV9TVVBQT1JUKSB7XG4gICAgLy8gUHJlZmVycmVkOiBSZXR1cm4gYW4gYXVnbWVudGVkIGBVaW50OEFycmF5YCBpbnN0YW5jZSBmb3IgYmVzdCBwZXJmb3JtYW5jZVxuICAgIGJ1ZiA9IEJ1ZmZlci5fYXVnbWVudChuZXcgVWludDhBcnJheShsZW5ndGgpKVxuICB9IGVsc2Uge1xuICAgIC8vIEZhbGxiYWNrOiBSZXR1cm4gVEhJUyBpbnN0YW5jZSBvZiBCdWZmZXIgKGNyZWF0ZWQgYnkgYG5ld2ApXG4gICAgYnVmID0gdGhpc1xuICAgIGJ1Zi5sZW5ndGggPSBsZW5ndGhcbiAgICBidWYuX2lzQnVmZmVyID0gdHJ1ZVxuICB9XG5cbiAgdmFyIGlcbiAgaWYgKEJ1ZmZlci5UWVBFRF9BUlJBWV9TVVBQT1JUICYmIHR5cGVvZiBzdWJqZWN0LmJ5dGVMZW5ndGggPT09ICdudW1iZXInKSB7XG4gICAgLy8gU3BlZWQgb3B0aW1pemF0aW9uIC0tIHVzZSBzZXQgaWYgd2UncmUgY29weWluZyBmcm9tIGEgdHlwZWQgYXJyYXlcbiAgICBidWYuX3NldChzdWJqZWN0KVxuICB9IGVsc2UgaWYgKGlzQXJyYXlpc2goc3ViamVjdCkpIHtcbiAgICAvLyBUcmVhdCBhcnJheS1pc2ggb2JqZWN0cyBhcyBhIGJ5dGUgYXJyYXlcbiAgICBpZiAoQnVmZmVyLmlzQnVmZmVyKHN1YmplY3QpKSB7XG4gICAgICBmb3IgKGkgPSAwOyBpIDwgbGVuZ3RoOyBpKyspXG4gICAgICAgIGJ1ZltpXSA9IHN1YmplY3QucmVhZFVJbnQ4KGkpXG4gICAgfSBlbHNlIHtcbiAgICAgIGZvciAoaSA9IDA7IGkgPCBsZW5ndGg7IGkrKylcbiAgICAgICAgYnVmW2ldID0gKChzdWJqZWN0W2ldICUgMjU2KSArIDI1NikgJSAyNTZcbiAgICB9XG4gIH0gZWxzZSBpZiAodHlwZSA9PT0gJ3N0cmluZycpIHtcbiAgICBidWYud3JpdGUoc3ViamVjdCwgMCwgZW5jb2RpbmcpXG4gIH0gZWxzZSBpZiAodHlwZSA9PT0gJ251bWJlcicgJiYgIUJ1ZmZlci5UWVBFRF9BUlJBWV9TVVBQT1JUICYmICFub1plcm8pIHtcbiAgICBmb3IgKGkgPSAwOyBpIDwgbGVuZ3RoOyBpKyspIHtcbiAgICAgIGJ1ZltpXSA9IDBcbiAgICB9XG4gIH1cblxuICByZXR1cm4gYnVmXG59XG5cbkJ1ZmZlci5pc0J1ZmZlciA9IGZ1bmN0aW9uIChiKSB7XG4gIHJldHVybiAhIShiICE9IG51bGwgJiYgYi5faXNCdWZmZXIpXG59XG5cbkJ1ZmZlci5jb21wYXJlID0gZnVuY3Rpb24gKGEsIGIpIHtcbiAgaWYgKCFCdWZmZXIuaXNCdWZmZXIoYSkgfHwgIUJ1ZmZlci5pc0J1ZmZlcihiKSlcbiAgICB0aHJvdyBuZXcgVHlwZUVycm9yKCdBcmd1bWVudHMgbXVzdCBiZSBCdWZmZXJzJylcblxuICB2YXIgeCA9IGEubGVuZ3RoXG4gIHZhciB5ID0gYi5sZW5ndGhcbiAgZm9yICh2YXIgaSA9IDAsIGxlbiA9IE1hdGgubWluKHgsIHkpOyBpIDwgbGVuICYmIGFbaV0gPT09IGJbaV07IGkrKykge31cbiAgaWYgKGkgIT09IGxlbikge1xuICAgIHggPSBhW2ldXG4gICAgeSA9IGJbaV1cbiAgfVxuICBpZiAoeCA8IHkpIHJldHVybiAtMVxuICBpZiAoeSA8IHgpIHJldHVybiAxXG4gIHJldHVybiAwXG59XG5cbkJ1ZmZlci5pc0VuY29kaW5nID0gZnVuY3Rpb24gKGVuY29kaW5nKSB7XG4gIHN3aXRjaCAoU3RyaW5nKGVuY29kaW5nKS50b0xvd2VyQ2FzZSgpKSB7XG4gICAgY2FzZSAnaGV4JzpcbiAgICBjYXNlICd1dGY4JzpcbiAgICBjYXNlICd1dGYtOCc6XG4gICAgY2FzZSAnYXNjaWknOlxuICAgIGNhc2UgJ2JpbmFyeSc6XG4gICAgY2FzZSAnYmFzZTY0JzpcbiAgICBjYXNlICdyYXcnOlxuICAgIGNhc2UgJ3VjczInOlxuICAgIGNhc2UgJ3Vjcy0yJzpcbiAgICBjYXNlICd1dGYxNmxlJzpcbiAgICBjYXNlICd1dGYtMTZsZSc6XG4gICAgICByZXR1cm4gdHJ1ZVxuICAgIGRlZmF1bHQ6XG4gICAgICByZXR1cm4gZmFsc2VcbiAgfVxufVxuXG5CdWZmZXIuY29uY2F0ID0gZnVuY3Rpb24gKGxpc3QsIHRvdGFsTGVuZ3RoKSB7XG4gIGlmICghaXNBcnJheShsaXN0KSkgdGhyb3cgbmV3IFR5cGVFcnJvcignVXNhZ2U6IEJ1ZmZlci5jb25jYXQobGlzdFssIGxlbmd0aF0pJylcblxuICBpZiAobGlzdC5sZW5ndGggPT09IDApIHtcbiAgICByZXR1cm4gbmV3IEJ1ZmZlcigwKVxuICB9IGVsc2UgaWYgKGxpc3QubGVuZ3RoID09PSAxKSB7XG4gICAgcmV0dXJuIGxpc3RbMF1cbiAgfVxuXG4gIHZhciBpXG4gIGlmICh0b3RhbExlbmd0aCA9PT0gdW5kZWZpbmVkKSB7XG4gICAgdG90YWxMZW5ndGggPSAwXG4gICAgZm9yIChpID0gMDsgaSA8IGxpc3QubGVuZ3RoOyBpKyspIHtcbiAgICAgIHRvdGFsTGVuZ3RoICs9IGxpc3RbaV0ubGVuZ3RoXG4gICAgfVxuICB9XG5cbiAgdmFyIGJ1ZiA9IG5ldyBCdWZmZXIodG90YWxMZW5ndGgpXG4gIHZhciBwb3MgPSAwXG4gIGZvciAoaSA9IDA7IGkgPCBsaXN0Lmxlbmd0aDsgaSsrKSB7XG4gICAgdmFyIGl0ZW0gPSBsaXN0W2ldXG4gICAgaXRlbS5jb3B5KGJ1ZiwgcG9zKVxuICAgIHBvcyArPSBpdGVtLmxlbmd0aFxuICB9XG4gIHJldHVybiBidWZcbn1cblxuQnVmZmVyLmJ5dGVMZW5ndGggPSBmdW5jdGlvbiAoc3RyLCBlbmNvZGluZykge1xuICB2YXIgcmV0XG4gIHN0ciA9IHN0ciArICcnXG4gIHN3aXRjaCAoZW5jb2RpbmcgfHwgJ3V0ZjgnKSB7XG4gICAgY2FzZSAnYXNjaWknOlxuICAgIGNhc2UgJ2JpbmFyeSc6XG4gICAgY2FzZSAncmF3JzpcbiAgICAgIHJldCA9IHN0ci5sZW5ndGhcbiAgICAgIGJyZWFrXG4gICAgY2FzZSAndWNzMic6XG4gICAgY2FzZSAndWNzLTInOlxuICAgIGNhc2UgJ3V0ZjE2bGUnOlxuICAgIGNhc2UgJ3V0Zi0xNmxlJzpcbiAgICAgIHJldCA9IHN0ci5sZW5ndGggKiAyXG4gICAgICBicmVha1xuICAgIGNhc2UgJ2hleCc6XG4gICAgICByZXQgPSBzdHIubGVuZ3RoID4+PiAxXG4gICAgICBicmVha1xuICAgIGNhc2UgJ3V0ZjgnOlxuICAgIGNhc2UgJ3V0Zi04JzpcbiAgICAgIHJldCA9IHV0ZjhUb0J5dGVzKHN0cikubGVuZ3RoXG4gICAgICBicmVha1xuICAgIGNhc2UgJ2Jhc2U2NCc6XG4gICAgICByZXQgPSBiYXNlNjRUb0J5dGVzKHN0cikubGVuZ3RoXG4gICAgICBicmVha1xuICAgIGRlZmF1bHQ6XG4gICAgICByZXQgPSBzdHIubGVuZ3RoXG4gIH1cbiAgcmV0dXJuIHJldFxufVxuXG4vLyBwcmUtc2V0IGZvciB2YWx1ZXMgdGhhdCBtYXkgZXhpc3QgaW4gdGhlIGZ1dHVyZVxuQnVmZmVyLnByb3RvdHlwZS5sZW5ndGggPSB1bmRlZmluZWRcbkJ1ZmZlci5wcm90b3R5cGUucGFyZW50ID0gdW5kZWZpbmVkXG5cbi8vIHRvU3RyaW5nKGVuY29kaW5nLCBzdGFydD0wLCBlbmQ9YnVmZmVyLmxlbmd0aClcbkJ1ZmZlci5wcm90b3R5cGUudG9TdHJpbmcgPSBmdW5jdGlvbiAoZW5jb2RpbmcsIHN0YXJ0LCBlbmQpIHtcbiAgdmFyIGxvd2VyZWRDYXNlID0gZmFsc2VcblxuICBzdGFydCA9IHN0YXJ0ID4+PiAwXG4gIGVuZCA9IGVuZCA9PT0gdW5kZWZpbmVkIHx8IGVuZCA9PT0gSW5maW5pdHkgPyB0aGlzLmxlbmd0aCA6IGVuZCA+Pj4gMFxuXG4gIGlmICghZW5jb2RpbmcpIGVuY29kaW5nID0gJ3V0ZjgnXG4gIGlmIChzdGFydCA8IDApIHN0YXJ0ID0gMFxuICBpZiAoZW5kID4gdGhpcy5sZW5ndGgpIGVuZCA9IHRoaXMubGVuZ3RoXG4gIGlmIChlbmQgPD0gc3RhcnQpIHJldHVybiAnJ1xuXG4gIHdoaWxlICh0cnVlKSB7XG4gICAgc3dpdGNoIChlbmNvZGluZykge1xuICAgICAgY2FzZSAnaGV4JzpcbiAgICAgICAgcmV0dXJuIGhleFNsaWNlKHRoaXMsIHN0YXJ0LCBlbmQpXG5cbiAgICAgIGNhc2UgJ3V0ZjgnOlxuICAgICAgY2FzZSAndXRmLTgnOlxuICAgICAgICByZXR1cm4gdXRmOFNsaWNlKHRoaXMsIHN0YXJ0LCBlbmQpXG5cbiAgICAgIGNhc2UgJ2FzY2lpJzpcbiAgICAgICAgcmV0dXJuIGFzY2lpU2xpY2UodGhpcywgc3RhcnQsIGVuZClcblxuICAgICAgY2FzZSAnYmluYXJ5JzpcbiAgICAgICAgcmV0dXJuIGJpbmFyeVNsaWNlKHRoaXMsIHN0YXJ0LCBlbmQpXG5cbiAgICAgIGNhc2UgJ2Jhc2U2NCc6XG4gICAgICAgIHJldHVybiBiYXNlNjRTbGljZSh0aGlzLCBzdGFydCwgZW5kKVxuXG4gICAgICBjYXNlICd1Y3MyJzpcbiAgICAgIGNhc2UgJ3Vjcy0yJzpcbiAgICAgIGNhc2UgJ3V0ZjE2bGUnOlxuICAgICAgY2FzZSAndXRmLTE2bGUnOlxuICAgICAgICByZXR1cm4gdXRmMTZsZVNsaWNlKHRoaXMsIHN0YXJ0LCBlbmQpXG5cbiAgICAgIGRlZmF1bHQ6XG4gICAgICAgIGlmIChsb3dlcmVkQ2FzZSlcbiAgICAgICAgICB0aHJvdyBuZXcgVHlwZUVycm9yKCdVbmtub3duIGVuY29kaW5nOiAnICsgZW5jb2RpbmcpXG4gICAgICAgIGVuY29kaW5nID0gKGVuY29kaW5nICsgJycpLnRvTG93ZXJDYXNlKClcbiAgICAgICAgbG93ZXJlZENhc2UgPSB0cnVlXG4gICAgfVxuICB9XG59XG5cbkJ1ZmZlci5wcm90b3R5cGUuZXF1YWxzID0gZnVuY3Rpb24gKGIpIHtcbiAgaWYoIUJ1ZmZlci5pc0J1ZmZlcihiKSkgdGhyb3cgbmV3IFR5cGVFcnJvcignQXJndW1lbnQgbXVzdCBiZSBhIEJ1ZmZlcicpXG4gIHJldHVybiBCdWZmZXIuY29tcGFyZSh0aGlzLCBiKSA9PT0gMFxufVxuXG5CdWZmZXIucHJvdG90eXBlLmluc3BlY3QgPSBmdW5jdGlvbiAoKSB7XG4gIHZhciBzdHIgPSAnJ1xuICB2YXIgbWF4ID0gZXhwb3J0cy5JTlNQRUNUX01BWF9CWVRFU1xuICBpZiAodGhpcy5sZW5ndGggPiAwKSB7XG4gICAgc3RyID0gdGhpcy50b1N0cmluZygnaGV4JywgMCwgbWF4KS5tYXRjaCgvLnsyfS9nKS5qb2luKCcgJylcbiAgICBpZiAodGhpcy5sZW5ndGggPiBtYXgpXG4gICAgICBzdHIgKz0gJyAuLi4gJ1xuICB9XG4gIHJldHVybiAnPEJ1ZmZlciAnICsgc3RyICsgJz4nXG59XG5cbkJ1ZmZlci5wcm90b3R5cGUuY29tcGFyZSA9IGZ1bmN0aW9uIChiKSB7XG4gIGlmICghQnVmZmVyLmlzQnVmZmVyKGIpKSB0aHJvdyBuZXcgVHlwZUVycm9yKCdBcmd1bWVudCBtdXN0IGJlIGEgQnVmZmVyJylcbiAgcmV0dXJuIEJ1ZmZlci5jb21wYXJlKHRoaXMsIGIpXG59XG5cbi8vIGBnZXRgIHdpbGwgYmUgcmVtb3ZlZCBpbiBOb2RlIDAuMTMrXG5CdWZmZXIucHJvdG90eXBlLmdldCA9IGZ1bmN0aW9uIChvZmZzZXQpIHtcbiAgY29uc29sZS5sb2coJy5nZXQoKSBpcyBkZXByZWNhdGVkLiBBY2Nlc3MgdXNpbmcgYXJyYXkgaW5kZXhlcyBpbnN0ZWFkLicpXG4gIHJldHVybiB0aGlzLnJlYWRVSW50OChvZmZzZXQpXG59XG5cbi8vIGBzZXRgIHdpbGwgYmUgcmVtb3ZlZCBpbiBOb2RlIDAuMTMrXG5CdWZmZXIucHJvdG90eXBlLnNldCA9IGZ1bmN0aW9uICh2LCBvZmZzZXQpIHtcbiAgY29uc29sZS5sb2coJy5zZXQoKSBpcyBkZXByZWNhdGVkLiBBY2Nlc3MgdXNpbmcgYXJyYXkgaW5kZXhlcyBpbnN0ZWFkLicpXG4gIHJldHVybiB0aGlzLndyaXRlVUludDgodiwgb2Zmc2V0KVxufVxuXG5mdW5jdGlvbiBoZXhXcml0ZSAoYnVmLCBzdHJpbmcsIG9mZnNldCwgbGVuZ3RoKSB7XG4gIG9mZnNldCA9IE51bWJlcihvZmZzZXQpIHx8IDBcbiAgdmFyIHJlbWFpbmluZyA9IGJ1Zi5sZW5ndGggLSBvZmZzZXRcbiAgaWYgKCFsZW5ndGgpIHtcbiAgICBsZW5ndGggPSByZW1haW5pbmdcbiAgfSBlbHNlIHtcbiAgICBsZW5ndGggPSBOdW1iZXIobGVuZ3RoKVxuICAgIGlmIChsZW5ndGggPiByZW1haW5pbmcpIHtcbiAgICAgIGxlbmd0aCA9IHJlbWFpbmluZ1xuICAgIH1cbiAgfVxuXG4gIC8vIG11c3QgYmUgYW4gZXZlbiBudW1iZXIgb2YgZGlnaXRzXG4gIHZhciBzdHJMZW4gPSBzdHJpbmcubGVuZ3RoXG4gIGlmIChzdHJMZW4gJSAyICE9PSAwKSB0aHJvdyBuZXcgRXJyb3IoJ0ludmFsaWQgaGV4IHN0cmluZycpXG5cbiAgaWYgKGxlbmd0aCA+IHN0ckxlbiAvIDIpIHtcbiAgICBsZW5ndGggPSBzdHJMZW4gLyAyXG4gIH1cbiAgZm9yICh2YXIgaSA9IDA7IGkgPCBsZW5ndGg7IGkrKykge1xuICAgIHZhciBieXRlID0gcGFyc2VJbnQoc3RyaW5nLnN1YnN0cihpICogMiwgMiksIDE2KVxuICAgIGlmIChpc05hTihieXRlKSkgdGhyb3cgbmV3IEVycm9yKCdJbnZhbGlkIGhleCBzdHJpbmcnKVxuICAgIGJ1ZltvZmZzZXQgKyBpXSA9IGJ5dGVcbiAgfVxuICByZXR1cm4gaVxufVxuXG5mdW5jdGlvbiB1dGY4V3JpdGUgKGJ1Ziwgc3RyaW5nLCBvZmZzZXQsIGxlbmd0aCkge1xuICB2YXIgY2hhcnNXcml0dGVuID0gYmxpdEJ1ZmZlcih1dGY4VG9CeXRlcyhzdHJpbmcpLCBidWYsIG9mZnNldCwgbGVuZ3RoKVxuICByZXR1cm4gY2hhcnNXcml0dGVuXG59XG5cbmZ1bmN0aW9uIGFzY2lpV3JpdGUgKGJ1Ziwgc3RyaW5nLCBvZmZzZXQsIGxlbmd0aCkge1xuICB2YXIgY2hhcnNXcml0dGVuID0gYmxpdEJ1ZmZlcihhc2NpaVRvQnl0ZXMoc3RyaW5nKSwgYnVmLCBvZmZzZXQsIGxlbmd0aClcbiAgcmV0dXJuIGNoYXJzV3JpdHRlblxufVxuXG5mdW5jdGlvbiBiaW5hcnlXcml0ZSAoYnVmLCBzdHJpbmcsIG9mZnNldCwgbGVuZ3RoKSB7XG4gIHJldHVybiBhc2NpaVdyaXRlKGJ1Ziwgc3RyaW5nLCBvZmZzZXQsIGxlbmd0aClcbn1cblxuZnVuY3Rpb24gYmFzZTY0V3JpdGUgKGJ1Ziwgc3RyaW5nLCBvZmZzZXQsIGxlbmd0aCkge1xuICB2YXIgY2hhcnNXcml0dGVuID0gYmxpdEJ1ZmZlcihiYXNlNjRUb0J5dGVzKHN0cmluZyksIGJ1Ziwgb2Zmc2V0LCBsZW5ndGgpXG4gIHJldHVybiBjaGFyc1dyaXR0ZW5cbn1cblxuZnVuY3Rpb24gdXRmMTZsZVdyaXRlIChidWYsIHN0cmluZywgb2Zmc2V0LCBsZW5ndGgpIHtcbiAgdmFyIGNoYXJzV3JpdHRlbiA9IGJsaXRCdWZmZXIodXRmMTZsZVRvQnl0ZXMoc3RyaW5nKSwgYnVmLCBvZmZzZXQsIGxlbmd0aClcbiAgcmV0dXJuIGNoYXJzV3JpdHRlblxufVxuXG5CdWZmZXIucHJvdG90eXBlLndyaXRlID0gZnVuY3Rpb24gKHN0cmluZywgb2Zmc2V0LCBsZW5ndGgsIGVuY29kaW5nKSB7XG4gIC8vIFN1cHBvcnQgYm90aCAoc3RyaW5nLCBvZmZzZXQsIGxlbmd0aCwgZW5jb2RpbmcpXG4gIC8vIGFuZCB0aGUgbGVnYWN5IChzdHJpbmcsIGVuY29kaW5nLCBvZmZzZXQsIGxlbmd0aClcbiAgaWYgKGlzRmluaXRlKG9mZnNldCkpIHtcbiAgICBpZiAoIWlzRmluaXRlKGxlbmd0aCkpIHtcbiAgICAgIGVuY29kaW5nID0gbGVuZ3RoXG4gICAgICBsZW5ndGggPSB1bmRlZmluZWRcbiAgICB9XG4gIH0gZWxzZSB7ICAvLyBsZWdhY3lcbiAgICB2YXIgc3dhcCA9IGVuY29kaW5nXG4gICAgZW5jb2RpbmcgPSBvZmZzZXRcbiAgICBvZmZzZXQgPSBsZW5ndGhcbiAgICBsZW5ndGggPSBzd2FwXG4gIH1cblxuICBvZmZzZXQgPSBOdW1iZXIob2Zmc2V0KSB8fCAwXG4gIHZhciByZW1haW5pbmcgPSB0aGlzLmxlbmd0aCAtIG9mZnNldFxuICBpZiAoIWxlbmd0aCkge1xuICAgIGxlbmd0aCA9IHJlbWFpbmluZ1xuICB9IGVsc2Uge1xuICAgIGxlbmd0aCA9IE51bWJlcihsZW5ndGgpXG4gICAgaWYgKGxlbmd0aCA+IHJlbWFpbmluZykge1xuICAgICAgbGVuZ3RoID0gcmVtYWluaW5nXG4gICAgfVxuICB9XG4gIGVuY29kaW5nID0gU3RyaW5nKGVuY29kaW5nIHx8ICd1dGY4JykudG9Mb3dlckNhc2UoKVxuXG4gIHZhciByZXRcbiAgc3dpdGNoIChlbmNvZGluZykge1xuICAgIGNhc2UgJ2hleCc6XG4gICAgICByZXQgPSBoZXhXcml0ZSh0aGlzLCBzdHJpbmcsIG9mZnNldCwgbGVuZ3RoKVxuICAgICAgYnJlYWtcbiAgICBjYXNlICd1dGY4JzpcbiAgICBjYXNlICd1dGYtOCc6XG4gICAgICByZXQgPSB1dGY4V3JpdGUodGhpcywgc3RyaW5nLCBvZmZzZXQsIGxlbmd0aClcbiAgICAgIGJyZWFrXG4gICAgY2FzZSAnYXNjaWknOlxuICAgICAgcmV0ID0gYXNjaWlXcml0ZSh0aGlzLCBzdHJpbmcsIG9mZnNldCwgbGVuZ3RoKVxuICAgICAgYnJlYWtcbiAgICBjYXNlICdiaW5hcnknOlxuICAgICAgcmV0ID0gYmluYXJ5V3JpdGUodGhpcywgc3RyaW5nLCBvZmZzZXQsIGxlbmd0aClcbiAgICAgIGJyZWFrXG4gICAgY2FzZSAnYmFzZTY0JzpcbiAgICAgIHJldCA9IGJhc2U2NFdyaXRlKHRoaXMsIHN0cmluZywgb2Zmc2V0LCBsZW5ndGgpXG4gICAgICBicmVha1xuICAgIGNhc2UgJ3VjczInOlxuICAgIGNhc2UgJ3Vjcy0yJzpcbiAgICBjYXNlICd1dGYxNmxlJzpcbiAgICBjYXNlICd1dGYtMTZsZSc6XG4gICAgICByZXQgPSB1dGYxNmxlV3JpdGUodGhpcywgc3RyaW5nLCBvZmZzZXQsIGxlbmd0aClcbiAgICAgIGJyZWFrXG4gICAgZGVmYXVsdDpcbiAgICAgIHRocm93IG5ldyBUeXBlRXJyb3IoJ1Vua25vd24gZW5jb2Rpbmc6ICcgKyBlbmNvZGluZylcbiAgfVxuICByZXR1cm4gcmV0XG59XG5cbkJ1ZmZlci5wcm90b3R5cGUudG9KU09OID0gZnVuY3Rpb24gKCkge1xuICByZXR1cm4ge1xuICAgIHR5cGU6ICdCdWZmZXInLFxuICAgIGRhdGE6IEFycmF5LnByb3RvdHlwZS5zbGljZS5jYWxsKHRoaXMuX2FyciB8fCB0aGlzLCAwKVxuICB9XG59XG5cbmZ1bmN0aW9uIGJhc2U2NFNsaWNlIChidWYsIHN0YXJ0LCBlbmQpIHtcbiAgaWYgKHN0YXJ0ID09PSAwICYmIGVuZCA9PT0gYnVmLmxlbmd0aCkge1xuICAgIHJldHVybiBiYXNlNjQuZnJvbUJ5dGVBcnJheShidWYpXG4gIH0gZWxzZSB7XG4gICAgcmV0dXJuIGJhc2U2NC5mcm9tQnl0ZUFycmF5KGJ1Zi5zbGljZShzdGFydCwgZW5kKSlcbiAgfVxufVxuXG5mdW5jdGlvbiB1dGY4U2xpY2UgKGJ1Ziwgc3RhcnQsIGVuZCkge1xuICB2YXIgcmVzID0gJydcbiAgdmFyIHRtcCA9ICcnXG4gIGVuZCA9IE1hdGgubWluKGJ1Zi5sZW5ndGgsIGVuZClcblxuICBmb3IgKHZhciBpID0gc3RhcnQ7IGkgPCBlbmQ7IGkrKykge1xuICAgIGlmIChidWZbaV0gPD0gMHg3Rikge1xuICAgICAgcmVzICs9IGRlY29kZVV0ZjhDaGFyKHRtcCkgKyBTdHJpbmcuZnJvbUNoYXJDb2RlKGJ1ZltpXSlcbiAgICAgIHRtcCA9ICcnXG4gICAgfSBlbHNlIHtcbiAgICAgIHRtcCArPSAnJScgKyBidWZbaV0udG9TdHJpbmcoMTYpXG4gICAgfVxuICB9XG5cbiAgcmV0dXJuIHJlcyArIGRlY29kZVV0ZjhDaGFyKHRtcClcbn1cblxuZnVuY3Rpb24gYXNjaWlTbGljZSAoYnVmLCBzdGFydCwgZW5kKSB7XG4gIHZhciByZXQgPSAnJ1xuICBlbmQgPSBNYXRoLm1pbihidWYubGVuZ3RoLCBlbmQpXG5cbiAgZm9yICh2YXIgaSA9IHN0YXJ0OyBpIDwgZW5kOyBpKyspIHtcbiAgICByZXQgKz0gU3RyaW5nLmZyb21DaGFyQ29kZShidWZbaV0pXG4gIH1cbiAgcmV0dXJuIHJldFxufVxuXG5mdW5jdGlvbiBiaW5hcnlTbGljZSAoYnVmLCBzdGFydCwgZW5kKSB7XG4gIHJldHVybiBhc2NpaVNsaWNlKGJ1Ziwgc3RhcnQsIGVuZClcbn1cblxuZnVuY3Rpb24gaGV4U2xpY2UgKGJ1Ziwgc3RhcnQsIGVuZCkge1xuICB2YXIgbGVuID0gYnVmLmxlbmd0aFxuXG4gIGlmICghc3RhcnQgfHwgc3RhcnQgPCAwKSBzdGFydCA9IDBcbiAgaWYgKCFlbmQgfHwgZW5kIDwgMCB8fCBlbmQgPiBsZW4pIGVuZCA9IGxlblxuXG4gIHZhciBvdXQgPSAnJ1xuICBmb3IgKHZhciBpID0gc3RhcnQ7IGkgPCBlbmQ7IGkrKykge1xuICAgIG91dCArPSB0b0hleChidWZbaV0pXG4gIH1cbiAgcmV0dXJuIG91dFxufVxuXG5mdW5jdGlvbiB1dGYxNmxlU2xpY2UgKGJ1Ziwgc3RhcnQsIGVuZCkge1xuICB2YXIgYnl0ZXMgPSBidWYuc2xpY2Uoc3RhcnQsIGVuZClcbiAgdmFyIHJlcyA9ICcnXG4gIGZvciAodmFyIGkgPSAwOyBpIDwgYnl0ZXMubGVuZ3RoOyBpICs9IDIpIHtcbiAgICByZXMgKz0gU3RyaW5nLmZyb21DaGFyQ29kZShieXRlc1tpXSArIGJ5dGVzW2kgKyAxXSAqIDI1NilcbiAgfVxuICByZXR1cm4gcmVzXG59XG5cbkJ1ZmZlci5wcm90b3R5cGUuc2xpY2UgPSBmdW5jdGlvbiAoc3RhcnQsIGVuZCkge1xuICB2YXIgbGVuID0gdGhpcy5sZW5ndGhcbiAgc3RhcnQgPSB+fnN0YXJ0XG4gIGVuZCA9IGVuZCA9PT0gdW5kZWZpbmVkID8gbGVuIDogfn5lbmRcblxuICBpZiAoc3RhcnQgPCAwKSB7XG4gICAgc3RhcnQgKz0gbGVuO1xuICAgIGlmIChzdGFydCA8IDApXG4gICAgICBzdGFydCA9IDBcbiAgfSBlbHNlIGlmIChzdGFydCA+IGxlbikge1xuICAgIHN0YXJ0ID0gbGVuXG4gIH1cblxuICBpZiAoZW5kIDwgMCkge1xuICAgIGVuZCArPSBsZW5cbiAgICBpZiAoZW5kIDwgMClcbiAgICAgIGVuZCA9IDBcbiAgfSBlbHNlIGlmIChlbmQgPiBsZW4pIHtcbiAgICBlbmQgPSBsZW5cbiAgfVxuXG4gIGlmIChlbmQgPCBzdGFydClcbiAgICBlbmQgPSBzdGFydFxuXG4gIGlmIChCdWZmZXIuVFlQRURfQVJSQVlfU1VQUE9SVCkge1xuICAgIHJldHVybiBCdWZmZXIuX2F1Z21lbnQodGhpcy5zdWJhcnJheShzdGFydCwgZW5kKSlcbiAgfSBlbHNlIHtcbiAgICB2YXIgc2xpY2VMZW4gPSBlbmQgLSBzdGFydFxuICAgIHZhciBuZXdCdWYgPSBuZXcgQnVmZmVyKHNsaWNlTGVuLCB1bmRlZmluZWQsIHRydWUpXG4gICAgZm9yICh2YXIgaSA9IDA7IGkgPCBzbGljZUxlbjsgaSsrKSB7XG4gICAgICBuZXdCdWZbaV0gPSB0aGlzW2kgKyBzdGFydF1cbiAgICB9XG4gICAgcmV0dXJuIG5ld0J1ZlxuICB9XG59XG5cbi8qXG4gKiBOZWVkIHRvIG1ha2Ugc3VyZSB0aGF0IGJ1ZmZlciBpc24ndCB0cnlpbmcgdG8gd3JpdGUgb3V0IG9mIGJvdW5kcy5cbiAqL1xuZnVuY3Rpb24gY2hlY2tPZmZzZXQgKG9mZnNldCwgZXh0LCBsZW5ndGgpIHtcbiAgaWYgKChvZmZzZXQgJSAxKSAhPT0gMCB8fCBvZmZzZXQgPCAwKVxuICAgIHRocm93IG5ldyBSYW5nZUVycm9yKCdvZmZzZXQgaXMgbm90IHVpbnQnKVxuICBpZiAob2Zmc2V0ICsgZXh0ID4gbGVuZ3RoKVxuICAgIHRocm93IG5ldyBSYW5nZUVycm9yKCdUcnlpbmcgdG8gYWNjZXNzIGJleW9uZCBidWZmZXIgbGVuZ3RoJylcbn1cblxuQnVmZmVyLnByb3RvdHlwZS5yZWFkVUludDggPSBmdW5jdGlvbiAob2Zmc2V0LCBub0Fzc2VydCkge1xuICBpZiAoIW5vQXNzZXJ0KVxuICAgIGNoZWNrT2Zmc2V0KG9mZnNldCwgMSwgdGhpcy5sZW5ndGgpXG4gIHJldHVybiB0aGlzW29mZnNldF1cbn1cblxuQnVmZmVyLnByb3RvdHlwZS5yZWFkVUludDE2TEUgPSBmdW5jdGlvbiAob2Zmc2V0LCBub0Fzc2VydCkge1xuICBpZiAoIW5vQXNzZXJ0KVxuICAgIGNoZWNrT2Zmc2V0KG9mZnNldCwgMiwgdGhpcy5sZW5ndGgpXG4gIHJldHVybiB0aGlzW29mZnNldF0gfCAodGhpc1tvZmZzZXQgKyAxXSA8PCA4KVxufVxuXG5CdWZmZXIucHJvdG90eXBlLnJlYWRVSW50MTZCRSA9IGZ1bmN0aW9uIChvZmZzZXQsIG5vQXNzZXJ0KSB7XG4gIGlmICghbm9Bc3NlcnQpXG4gICAgY2hlY2tPZmZzZXQob2Zmc2V0LCAyLCB0aGlzLmxlbmd0aClcbiAgcmV0dXJuICh0aGlzW29mZnNldF0gPDwgOCkgfCB0aGlzW29mZnNldCArIDFdXG59XG5cbkJ1ZmZlci5wcm90b3R5cGUucmVhZFVJbnQzMkxFID0gZnVuY3Rpb24gKG9mZnNldCwgbm9Bc3NlcnQpIHtcbiAgaWYgKCFub0Fzc2VydClcbiAgICBjaGVja09mZnNldChvZmZzZXQsIDQsIHRoaXMubGVuZ3RoKVxuXG4gIHJldHVybiAoKHRoaXNbb2Zmc2V0XSkgfFxuICAgICAgKHRoaXNbb2Zmc2V0ICsgMV0gPDwgOCkgfFxuICAgICAgKHRoaXNbb2Zmc2V0ICsgMl0gPDwgMTYpKSArXG4gICAgICAodGhpc1tvZmZzZXQgKyAzXSAqIDB4MTAwMDAwMClcbn1cblxuQnVmZmVyLnByb3RvdHlwZS5yZWFkVUludDMyQkUgPSBmdW5jdGlvbiAob2Zmc2V0LCBub0Fzc2VydCkge1xuICBpZiAoIW5vQXNzZXJ0KVxuICAgIGNoZWNrT2Zmc2V0KG9mZnNldCwgNCwgdGhpcy5sZW5ndGgpXG5cbiAgcmV0dXJuICh0aGlzW29mZnNldF0gKiAweDEwMDAwMDApICtcbiAgICAgICgodGhpc1tvZmZzZXQgKyAxXSA8PCAxNikgfFxuICAgICAgKHRoaXNbb2Zmc2V0ICsgMl0gPDwgOCkgfFxuICAgICAgdGhpc1tvZmZzZXQgKyAzXSlcbn1cblxuQnVmZmVyLnByb3RvdHlwZS5yZWFkSW50OCA9IGZ1bmN0aW9uIChvZmZzZXQsIG5vQXNzZXJ0KSB7XG4gIGlmICghbm9Bc3NlcnQpXG4gICAgY2hlY2tPZmZzZXQob2Zmc2V0LCAxLCB0aGlzLmxlbmd0aClcbiAgaWYgKCEodGhpc1tvZmZzZXRdICYgMHg4MCkpXG4gICAgcmV0dXJuICh0aGlzW29mZnNldF0pXG4gIHJldHVybiAoKDB4ZmYgLSB0aGlzW29mZnNldF0gKyAxKSAqIC0xKVxufVxuXG5CdWZmZXIucHJvdG90eXBlLnJlYWRJbnQxNkxFID0gZnVuY3Rpb24gKG9mZnNldCwgbm9Bc3NlcnQpIHtcbiAgaWYgKCFub0Fzc2VydClcbiAgICBjaGVja09mZnNldChvZmZzZXQsIDIsIHRoaXMubGVuZ3RoKVxuICB2YXIgdmFsID0gdGhpc1tvZmZzZXRdIHwgKHRoaXNbb2Zmc2V0ICsgMV0gPDwgOClcbiAgcmV0dXJuICh2YWwgJiAweDgwMDApID8gdmFsIHwgMHhGRkZGMDAwMCA6IHZhbFxufVxuXG5CdWZmZXIucHJvdG90eXBlLnJlYWRJbnQxNkJFID0gZnVuY3Rpb24gKG9mZnNldCwgbm9Bc3NlcnQpIHtcbiAgaWYgKCFub0Fzc2VydClcbiAgICBjaGVja09mZnNldChvZmZzZXQsIDIsIHRoaXMubGVuZ3RoKVxuICB2YXIgdmFsID0gdGhpc1tvZmZzZXQgKyAxXSB8ICh0aGlzW29mZnNldF0gPDwgOClcbiAgcmV0dXJuICh2YWwgJiAweDgwMDApID8gdmFsIHwgMHhGRkZGMDAwMCA6IHZhbFxufVxuXG5CdWZmZXIucHJvdG90eXBlLnJlYWRJbnQzMkxFID0gZnVuY3Rpb24gKG9mZnNldCwgbm9Bc3NlcnQpIHtcbiAgaWYgKCFub0Fzc2VydClcbiAgICBjaGVja09mZnNldChvZmZzZXQsIDQsIHRoaXMubGVuZ3RoKVxuXG4gIHJldHVybiAodGhpc1tvZmZzZXRdKSB8XG4gICAgICAodGhpc1tvZmZzZXQgKyAxXSA8PCA4KSB8XG4gICAgICAodGhpc1tvZmZzZXQgKyAyXSA8PCAxNikgfFxuICAgICAgKHRoaXNbb2Zmc2V0ICsgM10gPDwgMjQpXG59XG5cbkJ1ZmZlci5wcm90b3R5cGUucmVhZEludDMyQkUgPSBmdW5jdGlvbiAob2Zmc2V0LCBub0Fzc2VydCkge1xuICBpZiAoIW5vQXNzZXJ0KVxuICAgIGNoZWNrT2Zmc2V0KG9mZnNldCwgNCwgdGhpcy5sZW5ndGgpXG5cbiAgcmV0dXJuICh0aGlzW29mZnNldF0gPDwgMjQpIHxcbiAgICAgICh0aGlzW29mZnNldCArIDFdIDw8IDE2KSB8XG4gICAgICAodGhpc1tvZmZzZXQgKyAyXSA8PCA4KSB8XG4gICAgICAodGhpc1tvZmZzZXQgKyAzXSlcbn1cblxuQnVmZmVyLnByb3RvdHlwZS5yZWFkRmxvYXRMRSA9IGZ1bmN0aW9uIChvZmZzZXQsIG5vQXNzZXJ0KSB7XG4gIGlmICghbm9Bc3NlcnQpXG4gICAgY2hlY2tPZmZzZXQob2Zmc2V0LCA0LCB0aGlzLmxlbmd0aClcbiAgcmV0dXJuIGllZWU3NTQucmVhZCh0aGlzLCBvZmZzZXQsIHRydWUsIDIzLCA0KVxufVxuXG5CdWZmZXIucHJvdG90eXBlLnJlYWRGbG9hdEJFID0gZnVuY3Rpb24gKG9mZnNldCwgbm9Bc3NlcnQpIHtcbiAgaWYgKCFub0Fzc2VydClcbiAgICBjaGVja09mZnNldChvZmZzZXQsIDQsIHRoaXMubGVuZ3RoKVxuICByZXR1cm4gaWVlZTc1NC5yZWFkKHRoaXMsIG9mZnNldCwgZmFsc2UsIDIzLCA0KVxufVxuXG5CdWZmZXIucHJvdG90eXBlLnJlYWREb3VibGVMRSA9IGZ1bmN0aW9uIChvZmZzZXQsIG5vQXNzZXJ0KSB7XG4gIGlmICghbm9Bc3NlcnQpXG4gICAgY2hlY2tPZmZzZXQob2Zmc2V0LCA4LCB0aGlzLmxlbmd0aClcbiAgcmV0dXJuIGllZWU3NTQucmVhZCh0aGlzLCBvZmZzZXQsIHRydWUsIDUyLCA4KVxufVxuXG5CdWZmZXIucHJvdG90eXBlLnJlYWREb3VibGVCRSA9IGZ1bmN0aW9uIChvZmZzZXQsIG5vQXNzZXJ0KSB7XG4gIGlmICghbm9Bc3NlcnQpXG4gICAgY2hlY2tPZmZzZXQob2Zmc2V0LCA4LCB0aGlzLmxlbmd0aClcbiAgcmV0dXJuIGllZWU3NTQucmVhZCh0aGlzLCBvZmZzZXQsIGZhbHNlLCA1MiwgOClcbn1cblxuZnVuY3Rpb24gY2hlY2tJbnQgKGJ1ZiwgdmFsdWUsIG9mZnNldCwgZXh0LCBtYXgsIG1pbikge1xuICBpZiAoIUJ1ZmZlci5pc0J1ZmZlcihidWYpKSB0aHJvdyBuZXcgVHlwZUVycm9yKCdidWZmZXIgbXVzdCBiZSBhIEJ1ZmZlciBpbnN0YW5jZScpXG4gIGlmICh2YWx1ZSA+IG1heCB8fCB2YWx1ZSA8IG1pbikgdGhyb3cgbmV3IFR5cGVFcnJvcigndmFsdWUgaXMgb3V0IG9mIGJvdW5kcycpXG4gIGlmIChvZmZzZXQgKyBleHQgPiBidWYubGVuZ3RoKSB0aHJvdyBuZXcgVHlwZUVycm9yKCdpbmRleCBvdXQgb2YgcmFuZ2UnKVxufVxuXG5CdWZmZXIucHJvdG90eXBlLndyaXRlVUludDggPSBmdW5jdGlvbiAodmFsdWUsIG9mZnNldCwgbm9Bc3NlcnQpIHtcbiAgdmFsdWUgPSArdmFsdWVcbiAgb2Zmc2V0ID0gb2Zmc2V0ID4+PiAwXG4gIGlmICghbm9Bc3NlcnQpXG4gICAgY2hlY2tJbnQodGhpcywgdmFsdWUsIG9mZnNldCwgMSwgMHhmZiwgMClcbiAgaWYgKCFCdWZmZXIuVFlQRURfQVJSQVlfU1VQUE9SVCkgdmFsdWUgPSBNYXRoLmZsb29yKHZhbHVlKVxuICB0aGlzW29mZnNldF0gPSB2YWx1ZVxuICByZXR1cm4gb2Zmc2V0ICsgMVxufVxuXG5mdW5jdGlvbiBvYmplY3RXcml0ZVVJbnQxNiAoYnVmLCB2YWx1ZSwgb2Zmc2V0LCBsaXR0bGVFbmRpYW4pIHtcbiAgaWYgKHZhbHVlIDwgMCkgdmFsdWUgPSAweGZmZmYgKyB2YWx1ZSArIDFcbiAgZm9yICh2YXIgaSA9IDAsIGogPSBNYXRoLm1pbihidWYubGVuZ3RoIC0gb2Zmc2V0LCAyKTsgaSA8IGo7IGkrKykge1xuICAgIGJ1ZltvZmZzZXQgKyBpXSA9ICh2YWx1ZSAmICgweGZmIDw8ICg4ICogKGxpdHRsZUVuZGlhbiA/IGkgOiAxIC0gaSkpKSkgPj4+XG4gICAgICAobGl0dGxlRW5kaWFuID8gaSA6IDEgLSBpKSAqIDhcbiAgfVxufVxuXG5CdWZmZXIucHJvdG90eXBlLndyaXRlVUludDE2TEUgPSBmdW5jdGlvbiAodmFsdWUsIG9mZnNldCwgbm9Bc3NlcnQpIHtcbiAgdmFsdWUgPSArdmFsdWVcbiAgb2Zmc2V0ID0gb2Zmc2V0ID4+PiAwXG4gIGlmICghbm9Bc3NlcnQpXG4gICAgY2hlY2tJbnQodGhpcywgdmFsdWUsIG9mZnNldCwgMiwgMHhmZmZmLCAwKVxuICBpZiAoQnVmZmVyLlRZUEVEX0FSUkFZX1NVUFBPUlQpIHtcbiAgICB0aGlzW29mZnNldF0gPSB2YWx1ZVxuICAgIHRoaXNbb2Zmc2V0ICsgMV0gPSAodmFsdWUgPj4+IDgpXG4gIH0gZWxzZSBvYmplY3RXcml0ZVVJbnQxNih0aGlzLCB2YWx1ZSwgb2Zmc2V0LCB0cnVlKVxuICByZXR1cm4gb2Zmc2V0ICsgMlxufVxuXG5CdWZmZXIucHJvdG90eXBlLndyaXRlVUludDE2QkUgPSBmdW5jdGlvbiAodmFsdWUsIG9mZnNldCwgbm9Bc3NlcnQpIHtcbiAgdmFsdWUgPSArdmFsdWVcbiAgb2Zmc2V0ID0gb2Zmc2V0ID4+PiAwXG4gIGlmICghbm9Bc3NlcnQpXG4gICAgY2hlY2tJbnQodGhpcywgdmFsdWUsIG9mZnNldCwgMiwgMHhmZmZmLCAwKVxuICBpZiAoQnVmZmVyLlRZUEVEX0FSUkFZX1NVUFBPUlQpIHtcbiAgICB0aGlzW29mZnNldF0gPSAodmFsdWUgPj4+IDgpXG4gICAgdGhpc1tvZmZzZXQgKyAxXSA9IHZhbHVlXG4gIH0gZWxzZSBvYmplY3RXcml0ZVVJbnQxNih0aGlzLCB2YWx1ZSwgb2Zmc2V0LCBmYWxzZSlcbiAgcmV0dXJuIG9mZnNldCArIDJcbn1cblxuZnVuY3Rpb24gb2JqZWN0V3JpdGVVSW50MzIgKGJ1ZiwgdmFsdWUsIG9mZnNldCwgbGl0dGxlRW5kaWFuKSB7XG4gIGlmICh2YWx1ZSA8IDApIHZhbHVlID0gMHhmZmZmZmZmZiArIHZhbHVlICsgMVxuICBmb3IgKHZhciBpID0gMCwgaiA9IE1hdGgubWluKGJ1Zi5sZW5ndGggLSBvZmZzZXQsIDQpOyBpIDwgajsgaSsrKSB7XG4gICAgYnVmW29mZnNldCArIGldID0gKHZhbHVlID4+PiAobGl0dGxlRW5kaWFuID8gaSA6IDMgLSBpKSAqIDgpICYgMHhmZlxuICB9XG59XG5cbkJ1ZmZlci5wcm90b3R5cGUud3JpdGVVSW50MzJMRSA9IGZ1bmN0aW9uICh2YWx1ZSwgb2Zmc2V0LCBub0Fzc2VydCkge1xuICB2YWx1ZSA9ICt2YWx1ZVxuICBvZmZzZXQgPSBvZmZzZXQgPj4+IDBcbiAgaWYgKCFub0Fzc2VydClcbiAgICBjaGVja0ludCh0aGlzLCB2YWx1ZSwgb2Zmc2V0LCA0LCAweGZmZmZmZmZmLCAwKVxuICBpZiAoQnVmZmVyLlRZUEVEX0FSUkFZX1NVUFBPUlQpIHtcbiAgICB0aGlzW29mZnNldCArIDNdID0gKHZhbHVlID4+PiAyNClcbiAgICB0aGlzW29mZnNldCArIDJdID0gKHZhbHVlID4+PiAxNilcbiAgICB0aGlzW29mZnNldCArIDFdID0gKHZhbHVlID4+PiA4KVxuICAgIHRoaXNbb2Zmc2V0XSA9IHZhbHVlXG4gIH0gZWxzZSBvYmplY3RXcml0ZVVJbnQzMih0aGlzLCB2YWx1ZSwgb2Zmc2V0LCB0cnVlKVxuICByZXR1cm4gb2Zmc2V0ICsgNFxufVxuXG5CdWZmZXIucHJvdG90eXBlLndyaXRlVUludDMyQkUgPSBmdW5jdGlvbiAodmFsdWUsIG9mZnNldCwgbm9Bc3NlcnQpIHtcbiAgdmFsdWUgPSArdmFsdWVcbiAgb2Zmc2V0ID0gb2Zmc2V0ID4+PiAwXG4gIGlmICghbm9Bc3NlcnQpXG4gICAgY2hlY2tJbnQodGhpcywgdmFsdWUsIG9mZnNldCwgNCwgMHhmZmZmZmZmZiwgMClcbiAgaWYgKEJ1ZmZlci5UWVBFRF9BUlJBWV9TVVBQT1JUKSB7XG4gICAgdGhpc1tvZmZzZXRdID0gKHZhbHVlID4+PiAyNClcbiAgICB0aGlzW29mZnNldCArIDFdID0gKHZhbHVlID4+PiAxNilcbiAgICB0aGlzW29mZnNldCArIDJdID0gKHZhbHVlID4+PiA4KVxuICAgIHRoaXNbb2Zmc2V0ICsgM10gPSB2YWx1ZVxuICB9IGVsc2Ugb2JqZWN0V3JpdGVVSW50MzIodGhpcywgdmFsdWUsIG9mZnNldCwgZmFsc2UpXG4gIHJldHVybiBvZmZzZXQgKyA0XG59XG5cbkJ1ZmZlci5wcm90b3R5cGUud3JpdGVJbnQ4ID0gZnVuY3Rpb24gKHZhbHVlLCBvZmZzZXQsIG5vQXNzZXJ0KSB7XG4gIHZhbHVlID0gK3ZhbHVlXG4gIG9mZnNldCA9IG9mZnNldCA+Pj4gMFxuICBpZiAoIW5vQXNzZXJ0KVxuICAgIGNoZWNrSW50KHRoaXMsIHZhbHVlLCBvZmZzZXQsIDEsIDB4N2YsIC0weDgwKVxuICBpZiAoIUJ1ZmZlci5UWVBFRF9BUlJBWV9TVVBQT1JUKSB2YWx1ZSA9IE1hdGguZmxvb3IodmFsdWUpXG4gIGlmICh2YWx1ZSA8IDApIHZhbHVlID0gMHhmZiArIHZhbHVlICsgMVxuICB0aGlzW29mZnNldF0gPSB2YWx1ZVxuICByZXR1cm4gb2Zmc2V0ICsgMVxufVxuXG5CdWZmZXIucHJvdG90eXBlLndyaXRlSW50MTZMRSA9IGZ1bmN0aW9uICh2YWx1ZSwgb2Zmc2V0LCBub0Fzc2VydCkge1xuICB2YWx1ZSA9ICt2YWx1ZVxuICBvZmZzZXQgPSBvZmZzZXQgPj4+IDBcbiAgaWYgKCFub0Fzc2VydClcbiAgICBjaGVja0ludCh0aGlzLCB2YWx1ZSwgb2Zmc2V0LCAyLCAweDdmZmYsIC0weDgwMDApXG4gIGlmIChCdWZmZXIuVFlQRURfQVJSQVlfU1VQUE9SVCkge1xuICAgIHRoaXNbb2Zmc2V0XSA9IHZhbHVlXG4gICAgdGhpc1tvZmZzZXQgKyAxXSA9ICh2YWx1ZSA+Pj4gOClcbiAgfSBlbHNlIG9iamVjdFdyaXRlVUludDE2KHRoaXMsIHZhbHVlLCBvZmZzZXQsIHRydWUpXG4gIHJldHVybiBvZmZzZXQgKyAyXG59XG5cbkJ1ZmZlci5wcm90b3R5cGUud3JpdGVJbnQxNkJFID0gZnVuY3Rpb24gKHZhbHVlLCBvZmZzZXQsIG5vQXNzZXJ0KSB7XG4gIHZhbHVlID0gK3ZhbHVlXG4gIG9mZnNldCA9IG9mZnNldCA+Pj4gMFxuICBpZiAoIW5vQXNzZXJ0KVxuICAgIGNoZWNrSW50KHRoaXMsIHZhbHVlLCBvZmZzZXQsIDIsIDB4N2ZmZiwgLTB4ODAwMClcbiAgaWYgKEJ1ZmZlci5UWVBFRF9BUlJBWV9TVVBQT1JUKSB7XG4gICAgdGhpc1tvZmZzZXRdID0gKHZhbHVlID4+PiA4KVxuICAgIHRoaXNbb2Zmc2V0ICsgMV0gPSB2YWx1ZVxuICB9IGVsc2Ugb2JqZWN0V3JpdGVVSW50MTYodGhpcywgdmFsdWUsIG9mZnNldCwgZmFsc2UpXG4gIHJldHVybiBvZmZzZXQgKyAyXG59XG5cbkJ1ZmZlci5wcm90b3R5cGUud3JpdGVJbnQzMkxFID0gZnVuY3Rpb24gKHZhbHVlLCBvZmZzZXQsIG5vQXNzZXJ0KSB7XG4gIHZhbHVlID0gK3ZhbHVlXG4gIG9mZnNldCA9IG9mZnNldCA+Pj4gMFxuICBpZiAoIW5vQXNzZXJ0KVxuICAgIGNoZWNrSW50KHRoaXMsIHZhbHVlLCBvZmZzZXQsIDQsIDB4N2ZmZmZmZmYsIC0weDgwMDAwMDAwKVxuICBpZiAoQnVmZmVyLlRZUEVEX0FSUkFZX1NVUFBPUlQpIHtcbiAgICB0aGlzW29mZnNldF0gPSB2YWx1ZVxuICAgIHRoaXNbb2Zmc2V0ICsgMV0gPSAodmFsdWUgPj4+IDgpXG4gICAgdGhpc1tvZmZzZXQgKyAyXSA9ICh2YWx1ZSA+Pj4gMTYpXG4gICAgdGhpc1tvZmZzZXQgKyAzXSA9ICh2YWx1ZSA+Pj4gMjQpXG4gIH0gZWxzZSBvYmplY3RXcml0ZVVJbnQzMih0aGlzLCB2YWx1ZSwgb2Zmc2V0LCB0cnVlKVxuICByZXR1cm4gb2Zmc2V0ICsgNFxufVxuXG5CdWZmZXIucHJvdG90eXBlLndyaXRlSW50MzJCRSA9IGZ1bmN0aW9uICh2YWx1ZSwgb2Zmc2V0LCBub0Fzc2VydCkge1xuICB2YWx1ZSA9ICt2YWx1ZVxuICBvZmZzZXQgPSBvZmZzZXQgPj4+IDBcbiAgaWYgKCFub0Fzc2VydClcbiAgICBjaGVja0ludCh0aGlzLCB2YWx1ZSwgb2Zmc2V0LCA0LCAweDdmZmZmZmZmLCAtMHg4MDAwMDAwMClcbiAgaWYgKHZhbHVlIDwgMCkgdmFsdWUgPSAweGZmZmZmZmZmICsgdmFsdWUgKyAxXG4gIGlmIChCdWZmZXIuVFlQRURfQVJSQVlfU1VQUE9SVCkge1xuICAgIHRoaXNbb2Zmc2V0XSA9ICh2YWx1ZSA+Pj4gMjQpXG4gICAgdGhpc1tvZmZzZXQgKyAxXSA9ICh2YWx1ZSA+Pj4gMTYpXG4gICAgdGhpc1tvZmZzZXQgKyAyXSA9ICh2YWx1ZSA+Pj4gOClcbiAgICB0aGlzW29mZnNldCArIDNdID0gdmFsdWVcbiAgfSBlbHNlIG9iamVjdFdyaXRlVUludDMyKHRoaXMsIHZhbHVlLCBvZmZzZXQsIGZhbHNlKVxuICByZXR1cm4gb2Zmc2V0ICsgNFxufVxuXG5mdW5jdGlvbiBjaGVja0lFRUU3NTQgKGJ1ZiwgdmFsdWUsIG9mZnNldCwgZXh0LCBtYXgsIG1pbikge1xuICBpZiAodmFsdWUgPiBtYXggfHwgdmFsdWUgPCBtaW4pIHRocm93IG5ldyBUeXBlRXJyb3IoJ3ZhbHVlIGlzIG91dCBvZiBib3VuZHMnKVxuICBpZiAob2Zmc2V0ICsgZXh0ID4gYnVmLmxlbmd0aCkgdGhyb3cgbmV3IFR5cGVFcnJvcignaW5kZXggb3V0IG9mIHJhbmdlJylcbn1cblxuZnVuY3Rpb24gd3JpdGVGbG9hdCAoYnVmLCB2YWx1ZSwgb2Zmc2V0LCBsaXR0bGVFbmRpYW4sIG5vQXNzZXJ0KSB7XG4gIGlmICghbm9Bc3NlcnQpXG4gICAgY2hlY2tJRUVFNzU0KGJ1ZiwgdmFsdWUsIG9mZnNldCwgNCwgMy40MDI4MjM0NjYzODUyODg2ZSszOCwgLTMuNDAyODIzNDY2Mzg1Mjg4NmUrMzgpXG4gIGllZWU3NTQud3JpdGUoYnVmLCB2YWx1ZSwgb2Zmc2V0LCBsaXR0bGVFbmRpYW4sIDIzLCA0KVxuICByZXR1cm4gb2Zmc2V0ICsgNFxufVxuXG5CdWZmZXIucHJvdG90eXBlLndyaXRlRmxvYXRMRSA9IGZ1bmN0aW9uICh2YWx1ZSwgb2Zmc2V0LCBub0Fzc2VydCkge1xuICByZXR1cm4gd3JpdGVGbG9hdCh0aGlzLCB2YWx1ZSwgb2Zmc2V0LCB0cnVlLCBub0Fzc2VydClcbn1cblxuQnVmZmVyLnByb3RvdHlwZS53cml0ZUZsb2F0QkUgPSBmdW5jdGlvbiAodmFsdWUsIG9mZnNldCwgbm9Bc3NlcnQpIHtcbiAgcmV0dXJuIHdyaXRlRmxvYXQodGhpcywgdmFsdWUsIG9mZnNldCwgZmFsc2UsIG5vQXNzZXJ0KVxufVxuXG5mdW5jdGlvbiB3cml0ZURvdWJsZSAoYnVmLCB2YWx1ZSwgb2Zmc2V0LCBsaXR0bGVFbmRpYW4sIG5vQXNzZXJ0KSB7XG4gIGlmICghbm9Bc3NlcnQpXG4gICAgY2hlY2tJRUVFNzU0KGJ1ZiwgdmFsdWUsIG9mZnNldCwgOCwgMS43OTc2OTMxMzQ4NjIzMTU3RSszMDgsIC0xLjc5NzY5MzEzNDg2MjMxNTdFKzMwOClcbiAgaWVlZTc1NC53cml0ZShidWYsIHZhbHVlLCBvZmZzZXQsIGxpdHRsZUVuZGlhbiwgNTIsIDgpXG4gIHJldHVybiBvZmZzZXQgKyA4XG59XG5cbkJ1ZmZlci5wcm90b3R5cGUud3JpdGVEb3VibGVMRSA9IGZ1bmN0aW9uICh2YWx1ZSwgb2Zmc2V0LCBub0Fzc2VydCkge1xuICByZXR1cm4gd3JpdGVEb3VibGUodGhpcywgdmFsdWUsIG9mZnNldCwgdHJ1ZSwgbm9Bc3NlcnQpXG59XG5cbkJ1ZmZlci5wcm90b3R5cGUud3JpdGVEb3VibGVCRSA9IGZ1bmN0aW9uICh2YWx1ZSwgb2Zmc2V0LCBub0Fzc2VydCkge1xuICByZXR1cm4gd3JpdGVEb3VibGUodGhpcywgdmFsdWUsIG9mZnNldCwgZmFsc2UsIG5vQXNzZXJ0KVxufVxuXG4vLyBjb3B5KHRhcmdldEJ1ZmZlciwgdGFyZ2V0U3RhcnQ9MCwgc291cmNlU3RhcnQ9MCwgc291cmNlRW5kPWJ1ZmZlci5sZW5ndGgpXG5CdWZmZXIucHJvdG90eXBlLmNvcHkgPSBmdW5jdGlvbiAodGFyZ2V0LCB0YXJnZXRfc3RhcnQsIHN0YXJ0LCBlbmQpIHtcbiAgdmFyIHNvdXJjZSA9IHRoaXNcblxuICBpZiAoIXN0YXJ0KSBzdGFydCA9IDBcbiAgaWYgKCFlbmQgJiYgZW5kICE9PSAwKSBlbmQgPSB0aGlzLmxlbmd0aFxuICBpZiAoIXRhcmdldF9zdGFydCkgdGFyZ2V0X3N0YXJ0ID0gMFxuXG4gIC8vIENvcHkgMCBieXRlczsgd2UncmUgZG9uZVxuICBpZiAoZW5kID09PSBzdGFydCkgcmV0dXJuXG4gIGlmICh0YXJnZXQubGVuZ3RoID09PSAwIHx8IHNvdXJjZS5sZW5ndGggPT09IDApIHJldHVyblxuXG4gIC8vIEZhdGFsIGVycm9yIGNvbmRpdGlvbnNcbiAgaWYgKGVuZCA8IHN0YXJ0KSB0aHJvdyBuZXcgVHlwZUVycm9yKCdzb3VyY2VFbmQgPCBzb3VyY2VTdGFydCcpXG4gIGlmICh0YXJnZXRfc3RhcnQgPCAwIHx8IHRhcmdldF9zdGFydCA+PSB0YXJnZXQubGVuZ3RoKVxuICAgIHRocm93IG5ldyBUeXBlRXJyb3IoJ3RhcmdldFN0YXJ0IG91dCBvZiBib3VuZHMnKVxuICBpZiAoc3RhcnQgPCAwIHx8IHN0YXJ0ID49IHNvdXJjZS5sZW5ndGgpIHRocm93IG5ldyBUeXBlRXJyb3IoJ3NvdXJjZVN0YXJ0IG91dCBvZiBib3VuZHMnKVxuICBpZiAoZW5kIDwgMCB8fCBlbmQgPiBzb3VyY2UubGVuZ3RoKSB0aHJvdyBuZXcgVHlwZUVycm9yKCdzb3VyY2VFbmQgb3V0IG9mIGJvdW5kcycpXG5cbiAgLy8gQXJlIHdlIG9vYj9cbiAgaWYgKGVuZCA+IHRoaXMubGVuZ3RoKVxuICAgIGVuZCA9IHRoaXMubGVuZ3RoXG4gIGlmICh0YXJnZXQubGVuZ3RoIC0gdGFyZ2V0X3N0YXJ0IDwgZW5kIC0gc3RhcnQpXG4gICAgZW5kID0gdGFyZ2V0Lmxlbmd0aCAtIHRhcmdldF9zdGFydCArIHN0YXJ0XG5cbiAgdmFyIGxlbiA9IGVuZCAtIHN0YXJ0XG5cbiAgaWYgKGxlbiA8IDEwMDAgfHwgIUJ1ZmZlci5UWVBFRF9BUlJBWV9TVVBQT1JUKSB7XG4gICAgZm9yICh2YXIgaSA9IDA7IGkgPCBsZW47IGkrKykge1xuICAgICAgdGFyZ2V0W2kgKyB0YXJnZXRfc3RhcnRdID0gdGhpc1tpICsgc3RhcnRdXG4gICAgfVxuICB9IGVsc2Uge1xuICAgIHRhcmdldC5fc2V0KHRoaXMuc3ViYXJyYXkoc3RhcnQsIHN0YXJ0ICsgbGVuKSwgdGFyZ2V0X3N0YXJ0KVxuICB9XG59XG5cbi8vIGZpbGwodmFsdWUsIHN0YXJ0PTAsIGVuZD1idWZmZXIubGVuZ3RoKVxuQnVmZmVyLnByb3RvdHlwZS5maWxsID0gZnVuY3Rpb24gKHZhbHVlLCBzdGFydCwgZW5kKSB7XG4gIGlmICghdmFsdWUpIHZhbHVlID0gMFxuICBpZiAoIXN0YXJ0KSBzdGFydCA9IDBcbiAgaWYgKCFlbmQpIGVuZCA9IHRoaXMubGVuZ3RoXG5cbiAgaWYgKGVuZCA8IHN0YXJ0KSB0aHJvdyBuZXcgVHlwZUVycm9yKCdlbmQgPCBzdGFydCcpXG5cbiAgLy8gRmlsbCAwIGJ5dGVzOyB3ZSdyZSBkb25lXG4gIGlmIChlbmQgPT09IHN0YXJ0KSByZXR1cm5cbiAgaWYgKHRoaXMubGVuZ3RoID09PSAwKSByZXR1cm5cblxuICBpZiAoc3RhcnQgPCAwIHx8IHN0YXJ0ID49IHRoaXMubGVuZ3RoKSB0aHJvdyBuZXcgVHlwZUVycm9yKCdzdGFydCBvdXQgb2YgYm91bmRzJylcbiAgaWYgKGVuZCA8IDAgfHwgZW5kID4gdGhpcy5sZW5ndGgpIHRocm93IG5ldyBUeXBlRXJyb3IoJ2VuZCBvdXQgb2YgYm91bmRzJylcblxuICB2YXIgaVxuICBpZiAodHlwZW9mIHZhbHVlID09PSAnbnVtYmVyJykge1xuICAgIGZvciAoaSA9IHN0YXJ0OyBpIDwgZW5kOyBpKyspIHtcbiAgICAgIHRoaXNbaV0gPSB2YWx1ZVxuICAgIH1cbiAgfSBlbHNlIHtcbiAgICB2YXIgYnl0ZXMgPSB1dGY4VG9CeXRlcyh2YWx1ZS50b1N0cmluZygpKVxuICAgIHZhciBsZW4gPSBieXRlcy5sZW5ndGhcbiAgICBmb3IgKGkgPSBzdGFydDsgaSA8IGVuZDsgaSsrKSB7XG4gICAgICB0aGlzW2ldID0gYnl0ZXNbaSAlIGxlbl1cbiAgICB9XG4gIH1cblxuICByZXR1cm4gdGhpc1xufVxuXG4vKipcbiAqIENyZWF0ZXMgYSBuZXcgYEFycmF5QnVmZmVyYCB3aXRoIHRoZSAqY29waWVkKiBtZW1vcnkgb2YgdGhlIGJ1ZmZlciBpbnN0YW5jZS5cbiAqIEFkZGVkIGluIE5vZGUgMC4xMi4gT25seSBhdmFpbGFibGUgaW4gYnJvd3NlcnMgdGhhdCBzdXBwb3J0IEFycmF5QnVmZmVyLlxuICovXG5CdWZmZXIucHJvdG90eXBlLnRvQXJyYXlCdWZmZXIgPSBmdW5jdGlvbiAoKSB7XG4gIGlmICh0eXBlb2YgVWludDhBcnJheSAhPT0gJ3VuZGVmaW5lZCcpIHtcbiAgICBpZiAoQnVmZmVyLlRZUEVEX0FSUkFZX1NVUFBPUlQpIHtcbiAgICAgIHJldHVybiAobmV3IEJ1ZmZlcih0aGlzKSkuYnVmZmVyXG4gICAgfSBlbHNlIHtcbiAgICAgIHZhciBidWYgPSBuZXcgVWludDhBcnJheSh0aGlzLmxlbmd0aClcbiAgICAgIGZvciAodmFyIGkgPSAwLCBsZW4gPSBidWYubGVuZ3RoOyBpIDwgbGVuOyBpICs9IDEpIHtcbiAgICAgICAgYnVmW2ldID0gdGhpc1tpXVxuICAgICAgfVxuICAgICAgcmV0dXJuIGJ1Zi5idWZmZXJcbiAgICB9XG4gIH0gZWxzZSB7XG4gICAgdGhyb3cgbmV3IFR5cGVFcnJvcignQnVmZmVyLnRvQXJyYXlCdWZmZXIgbm90IHN1cHBvcnRlZCBpbiB0aGlzIGJyb3dzZXInKVxuICB9XG59XG5cbi8vIEhFTFBFUiBGVU5DVElPTlNcbi8vID09PT09PT09PT09PT09PT1cblxudmFyIEJQID0gQnVmZmVyLnByb3RvdHlwZVxuXG4vKipcbiAqIEF1Z21lbnQgYSBVaW50OEFycmF5ICppbnN0YW5jZSogKG5vdCB0aGUgVWludDhBcnJheSBjbGFzcyEpIHdpdGggQnVmZmVyIG1ldGhvZHNcbiAqL1xuQnVmZmVyLl9hdWdtZW50ID0gZnVuY3Rpb24gKGFycikge1xuICBhcnIuY29uc3RydWN0b3IgPSBCdWZmZXJcbiAgYXJyLl9pc0J1ZmZlciA9IHRydWVcblxuICAvLyBzYXZlIHJlZmVyZW5jZSB0byBvcmlnaW5hbCBVaW50OEFycmF5IGdldC9zZXQgbWV0aG9kcyBiZWZvcmUgb3ZlcndyaXRpbmdcbiAgYXJyLl9nZXQgPSBhcnIuZ2V0XG4gIGFyci5fc2V0ID0gYXJyLnNldFxuXG4gIC8vIGRlcHJlY2F0ZWQsIHdpbGwgYmUgcmVtb3ZlZCBpbiBub2RlIDAuMTMrXG4gIGFyci5nZXQgPSBCUC5nZXRcbiAgYXJyLnNldCA9IEJQLnNldFxuXG4gIGFyci53cml0ZSA9IEJQLndyaXRlXG4gIGFyci50b1N0cmluZyA9IEJQLnRvU3RyaW5nXG4gIGFyci50b0xvY2FsZVN0cmluZyA9IEJQLnRvU3RyaW5nXG4gIGFyci50b0pTT04gPSBCUC50b0pTT05cbiAgYXJyLmVxdWFscyA9IEJQLmVxdWFsc1xuICBhcnIuY29tcGFyZSA9IEJQLmNvbXBhcmVcbiAgYXJyLmNvcHkgPSBCUC5jb3B5XG4gIGFyci5zbGljZSA9IEJQLnNsaWNlXG4gIGFyci5yZWFkVUludDggPSBCUC5yZWFkVUludDhcbiAgYXJyLnJlYWRVSW50MTZMRSA9IEJQLnJlYWRVSW50MTZMRVxuICBhcnIucmVhZFVJbnQxNkJFID0gQlAucmVhZFVJbnQxNkJFXG4gIGFyci5yZWFkVUludDMyTEUgPSBCUC5yZWFkVUludDMyTEVcbiAgYXJyLnJlYWRVSW50MzJCRSA9IEJQLnJlYWRVSW50MzJCRVxuICBhcnIucmVhZEludDggPSBCUC5yZWFkSW50OFxuICBhcnIucmVhZEludDE2TEUgPSBCUC5yZWFkSW50MTZMRVxuICBhcnIucmVhZEludDE2QkUgPSBCUC5yZWFkSW50MTZCRVxuICBhcnIucmVhZEludDMyTEUgPSBCUC5yZWFkSW50MzJMRVxuICBhcnIucmVhZEludDMyQkUgPSBCUC5yZWFkSW50MzJCRVxuICBhcnIucmVhZEZsb2F0TEUgPSBCUC5yZWFkRmxvYXRMRVxuICBhcnIucmVhZEZsb2F0QkUgPSBCUC5yZWFkRmxvYXRCRVxuICBhcnIucmVhZERvdWJsZUxFID0gQlAucmVhZERvdWJsZUxFXG4gIGFyci5yZWFkRG91YmxlQkUgPSBCUC5yZWFkRG91YmxlQkVcbiAgYXJyLndyaXRlVUludDggPSBCUC53cml0ZVVJbnQ4XG4gIGFyci53cml0ZVVJbnQxNkxFID0gQlAud3JpdGVVSW50MTZMRVxuICBhcnIud3JpdGVVSW50MTZCRSA9IEJQLndyaXRlVUludDE2QkVcbiAgYXJyLndyaXRlVUludDMyTEUgPSBCUC53cml0ZVVJbnQzMkxFXG4gIGFyci53cml0ZVVJbnQzMkJFID0gQlAud3JpdGVVSW50MzJCRVxuICBhcnIud3JpdGVJbnQ4ID0gQlAud3JpdGVJbnQ4XG4gIGFyci53cml0ZUludDE2TEUgPSBCUC53cml0ZUludDE2TEVcbiAgYXJyLndyaXRlSW50MTZCRSA9IEJQLndyaXRlSW50MTZCRVxuICBhcnIud3JpdGVJbnQzMkxFID0gQlAud3JpdGVJbnQzMkxFXG4gIGFyci53cml0ZUludDMyQkUgPSBCUC53cml0ZUludDMyQkVcbiAgYXJyLndyaXRlRmxvYXRMRSA9IEJQLndyaXRlRmxvYXRMRVxuICBhcnIud3JpdGVGbG9hdEJFID0gQlAud3JpdGVGbG9hdEJFXG4gIGFyci53cml0ZURvdWJsZUxFID0gQlAud3JpdGVEb3VibGVMRVxuICBhcnIud3JpdGVEb3VibGVCRSA9IEJQLndyaXRlRG91YmxlQkVcbiAgYXJyLmZpbGwgPSBCUC5maWxsXG4gIGFyci5pbnNwZWN0ID0gQlAuaW5zcGVjdFxuICBhcnIudG9BcnJheUJ1ZmZlciA9IEJQLnRvQXJyYXlCdWZmZXJcblxuICByZXR1cm4gYXJyXG59XG5cbnZhciBJTlZBTElEX0JBU0U2NF9SRSA9IC9bXitcXC8wLTlBLXpdL2dcblxuZnVuY3Rpb24gYmFzZTY0Y2xlYW4gKHN0cikge1xuICAvLyBOb2RlIHN0cmlwcyBvdXQgaW52YWxpZCBjaGFyYWN0ZXJzIGxpa2UgXFxuIGFuZCBcXHQgZnJvbSB0aGUgc3RyaW5nLCBiYXNlNjQtanMgZG9lcyBub3RcbiAgc3RyID0gc3RyaW5ndHJpbShzdHIpLnJlcGxhY2UoSU5WQUxJRF9CQVNFNjRfUkUsICcnKVxuICAvLyBOb2RlIGFsbG93cyBmb3Igbm9uLXBhZGRlZCBiYXNlNjQgc3RyaW5ncyAobWlzc2luZyB0cmFpbGluZyA9PT0pLCBiYXNlNjQtanMgZG9lcyBub3RcbiAgd2hpbGUgKHN0ci5sZW5ndGggJSA0ICE9PSAwKSB7XG4gICAgc3RyID0gc3RyICsgJz0nXG4gIH1cbiAgcmV0dXJuIHN0clxufVxuXG5mdW5jdGlvbiBzdHJpbmd0cmltIChzdHIpIHtcbiAgaWYgKHN0ci50cmltKSByZXR1cm4gc3RyLnRyaW0oKVxuICByZXR1cm4gc3RyLnJlcGxhY2UoL15cXHMrfFxccyskL2csICcnKVxufVxuXG5mdW5jdGlvbiBpc0FycmF5aXNoIChzdWJqZWN0KSB7XG4gIHJldHVybiBpc0FycmF5KHN1YmplY3QpIHx8IEJ1ZmZlci5pc0J1ZmZlcihzdWJqZWN0KSB8fFxuICAgICAgc3ViamVjdCAmJiB0eXBlb2Ygc3ViamVjdCA9PT0gJ29iamVjdCcgJiZcbiAgICAgIHR5cGVvZiBzdWJqZWN0Lmxlbmd0aCA9PT0gJ251bWJlcidcbn1cblxuZnVuY3Rpb24gdG9IZXggKG4pIHtcbiAgaWYgKG4gPCAxNikgcmV0dXJuICcwJyArIG4udG9TdHJpbmcoMTYpXG4gIHJldHVybiBuLnRvU3RyaW5nKDE2KVxufVxuXG5mdW5jdGlvbiB1dGY4VG9CeXRlcyAoc3RyKSB7XG4gIHZhciBieXRlQXJyYXkgPSBbXVxuICBmb3IgKHZhciBpID0gMDsgaSA8IHN0ci5sZW5ndGg7IGkrKykge1xuICAgIHZhciBiID0gc3RyLmNoYXJDb2RlQXQoaSlcbiAgICBpZiAoYiA8PSAweDdGKSB7XG4gICAgICBieXRlQXJyYXkucHVzaChiKVxuICAgIH0gZWxzZSB7XG4gICAgICB2YXIgc3RhcnQgPSBpXG4gICAgICBpZiAoYiA+PSAweEQ4MDAgJiYgYiA8PSAweERGRkYpIGkrK1xuICAgICAgdmFyIGggPSBlbmNvZGVVUklDb21wb25lbnQoc3RyLnNsaWNlKHN0YXJ0LCBpKzEpKS5zdWJzdHIoMSkuc3BsaXQoJyUnKVxuICAgICAgZm9yICh2YXIgaiA9IDA7IGogPCBoLmxlbmd0aDsgaisrKSB7XG4gICAgICAgIGJ5dGVBcnJheS5wdXNoKHBhcnNlSW50KGhbal0sIDE2KSlcbiAgICAgIH1cbiAgICB9XG4gIH1cbiAgcmV0dXJuIGJ5dGVBcnJheVxufVxuXG5mdW5jdGlvbiBhc2NpaVRvQnl0ZXMgKHN0cikge1xuICB2YXIgYnl0ZUFycmF5ID0gW11cbiAgZm9yICh2YXIgaSA9IDA7IGkgPCBzdHIubGVuZ3RoOyBpKyspIHtcbiAgICAvLyBOb2RlJ3MgY29kZSBzZWVtcyB0byBiZSBkb2luZyB0aGlzIGFuZCBub3QgJiAweDdGLi5cbiAgICBieXRlQXJyYXkucHVzaChzdHIuY2hhckNvZGVBdChpKSAmIDB4RkYpXG4gIH1cbiAgcmV0dXJuIGJ5dGVBcnJheVxufVxuXG5mdW5jdGlvbiB1dGYxNmxlVG9CeXRlcyAoc3RyKSB7XG4gIHZhciBjLCBoaSwgbG9cbiAgdmFyIGJ5dGVBcnJheSA9IFtdXG4gIGZvciAodmFyIGkgPSAwOyBpIDwgc3RyLmxlbmd0aDsgaSsrKSB7XG4gICAgYyA9IHN0ci5jaGFyQ29kZUF0KGkpXG4gICAgaGkgPSBjID4+IDhcbiAgICBsbyA9IGMgJSAyNTZcbiAgICBieXRlQXJyYXkucHVzaChsbylcbiAgICBieXRlQXJyYXkucHVzaChoaSlcbiAgfVxuXG4gIHJldHVybiBieXRlQXJyYXlcbn1cblxuZnVuY3Rpb24gYmFzZTY0VG9CeXRlcyAoc3RyKSB7XG4gIHJldHVybiBiYXNlNjQudG9CeXRlQXJyYXkoc3RyKVxufVxuXG5mdW5jdGlvbiBibGl0QnVmZmVyIChzcmMsIGRzdCwgb2Zmc2V0LCBsZW5ndGgpIHtcbiAgZm9yICh2YXIgaSA9IDA7IGkgPCBsZW5ndGg7IGkrKykge1xuICAgIGlmICgoaSArIG9mZnNldCA+PSBkc3QubGVuZ3RoKSB8fCAoaSA+PSBzcmMubGVuZ3RoKSlcbiAgICAgIGJyZWFrXG4gICAgZHN0W2kgKyBvZmZzZXRdID0gc3JjW2ldXG4gIH1cbiAgcmV0dXJuIGlcbn1cblxuZnVuY3Rpb24gZGVjb2RlVXRmOENoYXIgKHN0cikge1xuICB0cnkge1xuICAgIHJldHVybiBkZWNvZGVVUklDb21wb25lbnQoc3RyKVxuICB9IGNhdGNoIChlcnIpIHtcbiAgICByZXR1cm4gU3RyaW5nLmZyb21DaGFyQ29kZSgweEZGRkQpIC8vIFVURiA4IGludmFsaWQgY2hhclxuICB9XG59XG4iLCJ2YXIgbG9va3VwID0gJ0FCQ0RFRkdISUpLTE1OT1BRUlNUVVZXWFlaYWJjZGVmZ2hpamtsbW5vcHFyc3R1dnd4eXowMTIzNDU2Nzg5Ky8nO1xuXG47KGZ1bmN0aW9uIChleHBvcnRzKSB7XG5cdCd1c2Ugc3RyaWN0JztcblxuICB2YXIgQXJyID0gKHR5cGVvZiBVaW50OEFycmF5ICE9PSAndW5kZWZpbmVkJylcbiAgICA/IFVpbnQ4QXJyYXlcbiAgICA6IEFycmF5XG5cblx0dmFyIFBMVVMgICA9ICcrJy5jaGFyQ29kZUF0KDApXG5cdHZhciBTTEFTSCAgPSAnLycuY2hhckNvZGVBdCgwKVxuXHR2YXIgTlVNQkVSID0gJzAnLmNoYXJDb2RlQXQoMClcblx0dmFyIExPV0VSICA9ICdhJy5jaGFyQ29kZUF0KDApXG5cdHZhciBVUFBFUiAgPSAnQScuY2hhckNvZGVBdCgwKVxuXG5cdGZ1bmN0aW9uIGRlY29kZSAoZWx0KSB7XG5cdFx0dmFyIGNvZGUgPSBlbHQuY2hhckNvZGVBdCgwKVxuXHRcdGlmIChjb2RlID09PSBQTFVTKVxuXHRcdFx0cmV0dXJuIDYyIC8vICcrJ1xuXHRcdGlmIChjb2RlID09PSBTTEFTSClcblx0XHRcdHJldHVybiA2MyAvLyAnLydcblx0XHRpZiAoY29kZSA8IE5VTUJFUilcblx0XHRcdHJldHVybiAtMSAvL25vIG1hdGNoXG5cdFx0aWYgKGNvZGUgPCBOVU1CRVIgKyAxMClcblx0XHRcdHJldHVybiBjb2RlIC0gTlVNQkVSICsgMjYgKyAyNlxuXHRcdGlmIChjb2RlIDwgVVBQRVIgKyAyNilcblx0XHRcdHJldHVybiBjb2RlIC0gVVBQRVJcblx0XHRpZiAoY29kZSA8IExPV0VSICsgMjYpXG5cdFx0XHRyZXR1cm4gY29kZSAtIExPV0VSICsgMjZcblx0fVxuXG5cdGZ1bmN0aW9uIGI2NFRvQnl0ZUFycmF5IChiNjQpIHtcblx0XHR2YXIgaSwgaiwgbCwgdG1wLCBwbGFjZUhvbGRlcnMsIGFyclxuXG5cdFx0aWYgKGI2NC5sZW5ndGggJSA0ID4gMCkge1xuXHRcdFx0dGhyb3cgbmV3IEVycm9yKCdJbnZhbGlkIHN0cmluZy4gTGVuZ3RoIG11c3QgYmUgYSBtdWx0aXBsZSBvZiA0Jylcblx0XHR9XG5cblx0XHQvLyB0aGUgbnVtYmVyIG9mIGVxdWFsIHNpZ25zIChwbGFjZSBob2xkZXJzKVxuXHRcdC8vIGlmIHRoZXJlIGFyZSB0d28gcGxhY2Vob2xkZXJzLCB0aGFuIHRoZSB0d28gY2hhcmFjdGVycyBiZWZvcmUgaXRcblx0XHQvLyByZXByZXNlbnQgb25lIGJ5dGVcblx0XHQvLyBpZiB0aGVyZSBpcyBvbmx5IG9uZSwgdGhlbiB0aGUgdGhyZWUgY2hhcmFjdGVycyBiZWZvcmUgaXQgcmVwcmVzZW50IDIgYnl0ZXNcblx0XHQvLyB0aGlzIGlzIGp1c3QgYSBjaGVhcCBoYWNrIHRvIG5vdCBkbyBpbmRleE9mIHR3aWNlXG5cdFx0dmFyIGxlbiA9IGI2NC5sZW5ndGhcblx0XHRwbGFjZUhvbGRlcnMgPSAnPScgPT09IGI2NC5jaGFyQXQobGVuIC0gMikgPyAyIDogJz0nID09PSBiNjQuY2hhckF0KGxlbiAtIDEpID8gMSA6IDBcblxuXHRcdC8vIGJhc2U2NCBpcyA0LzMgKyB1cCB0byB0d28gY2hhcmFjdGVycyBvZiB0aGUgb3JpZ2luYWwgZGF0YVxuXHRcdGFyciA9IG5ldyBBcnIoYjY0Lmxlbmd0aCAqIDMgLyA0IC0gcGxhY2VIb2xkZXJzKVxuXG5cdFx0Ly8gaWYgdGhlcmUgYXJlIHBsYWNlaG9sZGVycywgb25seSBnZXQgdXAgdG8gdGhlIGxhc3QgY29tcGxldGUgNCBjaGFyc1xuXHRcdGwgPSBwbGFjZUhvbGRlcnMgPiAwID8gYjY0Lmxlbmd0aCAtIDQgOiBiNjQubGVuZ3RoXG5cblx0XHR2YXIgTCA9IDBcblxuXHRcdGZ1bmN0aW9uIHB1c2ggKHYpIHtcblx0XHRcdGFycltMKytdID0gdlxuXHRcdH1cblxuXHRcdGZvciAoaSA9IDAsIGogPSAwOyBpIDwgbDsgaSArPSA0LCBqICs9IDMpIHtcblx0XHRcdHRtcCA9IChkZWNvZGUoYjY0LmNoYXJBdChpKSkgPDwgMTgpIHwgKGRlY29kZShiNjQuY2hhckF0KGkgKyAxKSkgPDwgMTIpIHwgKGRlY29kZShiNjQuY2hhckF0KGkgKyAyKSkgPDwgNikgfCBkZWNvZGUoYjY0LmNoYXJBdChpICsgMykpXG5cdFx0XHRwdXNoKCh0bXAgJiAweEZGMDAwMCkgPj4gMTYpXG5cdFx0XHRwdXNoKCh0bXAgJiAweEZGMDApID4+IDgpXG5cdFx0XHRwdXNoKHRtcCAmIDB4RkYpXG5cdFx0fVxuXG5cdFx0aWYgKHBsYWNlSG9sZGVycyA9PT0gMikge1xuXHRcdFx0dG1wID0gKGRlY29kZShiNjQuY2hhckF0KGkpKSA8PCAyKSB8IChkZWNvZGUoYjY0LmNoYXJBdChpICsgMSkpID4+IDQpXG5cdFx0XHRwdXNoKHRtcCAmIDB4RkYpXG5cdFx0fSBlbHNlIGlmIChwbGFjZUhvbGRlcnMgPT09IDEpIHtcblx0XHRcdHRtcCA9IChkZWNvZGUoYjY0LmNoYXJBdChpKSkgPDwgMTApIHwgKGRlY29kZShiNjQuY2hhckF0KGkgKyAxKSkgPDwgNCkgfCAoZGVjb2RlKGI2NC5jaGFyQXQoaSArIDIpKSA+PiAyKVxuXHRcdFx0cHVzaCgodG1wID4+IDgpICYgMHhGRilcblx0XHRcdHB1c2godG1wICYgMHhGRilcblx0XHR9XG5cblx0XHRyZXR1cm4gYXJyXG5cdH1cblxuXHRmdW5jdGlvbiB1aW50OFRvQmFzZTY0ICh1aW50OCkge1xuXHRcdHZhciBpLFxuXHRcdFx0ZXh0cmFCeXRlcyA9IHVpbnQ4Lmxlbmd0aCAlIDMsIC8vIGlmIHdlIGhhdmUgMSBieXRlIGxlZnQsIHBhZCAyIGJ5dGVzXG5cdFx0XHRvdXRwdXQgPSBcIlwiLFxuXHRcdFx0dGVtcCwgbGVuZ3RoXG5cblx0XHRmdW5jdGlvbiBlbmNvZGUgKG51bSkge1xuXHRcdFx0cmV0dXJuIGxvb2t1cC5jaGFyQXQobnVtKVxuXHRcdH1cblxuXHRcdGZ1bmN0aW9uIHRyaXBsZXRUb0Jhc2U2NCAobnVtKSB7XG5cdFx0XHRyZXR1cm4gZW5jb2RlKG51bSA+PiAxOCAmIDB4M0YpICsgZW5jb2RlKG51bSA+PiAxMiAmIDB4M0YpICsgZW5jb2RlKG51bSA+PiA2ICYgMHgzRikgKyBlbmNvZGUobnVtICYgMHgzRilcblx0XHR9XG5cblx0XHQvLyBnbyB0aHJvdWdoIHRoZSBhcnJheSBldmVyeSB0aHJlZSBieXRlcywgd2UnbGwgZGVhbCB3aXRoIHRyYWlsaW5nIHN0dWZmIGxhdGVyXG5cdFx0Zm9yIChpID0gMCwgbGVuZ3RoID0gdWludDgubGVuZ3RoIC0gZXh0cmFCeXRlczsgaSA8IGxlbmd0aDsgaSArPSAzKSB7XG5cdFx0XHR0ZW1wID0gKHVpbnQ4W2ldIDw8IDE2KSArICh1aW50OFtpICsgMV0gPDwgOCkgKyAodWludDhbaSArIDJdKVxuXHRcdFx0b3V0cHV0ICs9IHRyaXBsZXRUb0Jhc2U2NCh0ZW1wKVxuXHRcdH1cblxuXHRcdC8vIHBhZCB0aGUgZW5kIHdpdGggemVyb3MsIGJ1dCBtYWtlIHN1cmUgdG8gbm90IGZvcmdldCB0aGUgZXh0cmEgYnl0ZXNcblx0XHRzd2l0Y2ggKGV4dHJhQnl0ZXMpIHtcblx0XHRcdGNhc2UgMTpcblx0XHRcdFx0dGVtcCA9IHVpbnQ4W3VpbnQ4Lmxlbmd0aCAtIDFdXG5cdFx0XHRcdG91dHB1dCArPSBlbmNvZGUodGVtcCA+PiAyKVxuXHRcdFx0XHRvdXRwdXQgKz0gZW5jb2RlKCh0ZW1wIDw8IDQpICYgMHgzRilcblx0XHRcdFx0b3V0cHV0ICs9ICc9PSdcblx0XHRcdFx0YnJlYWtcblx0XHRcdGNhc2UgMjpcblx0XHRcdFx0dGVtcCA9ICh1aW50OFt1aW50OC5sZW5ndGggLSAyXSA8PCA4KSArICh1aW50OFt1aW50OC5sZW5ndGggLSAxXSlcblx0XHRcdFx0b3V0cHV0ICs9IGVuY29kZSh0ZW1wID4+IDEwKVxuXHRcdFx0XHRvdXRwdXQgKz0gZW5jb2RlKCh0ZW1wID4+IDQpICYgMHgzRilcblx0XHRcdFx0b3V0cHV0ICs9IGVuY29kZSgodGVtcCA8PCAyKSAmIDB4M0YpXG5cdFx0XHRcdG91dHB1dCArPSAnPSdcblx0XHRcdFx0YnJlYWtcblx0XHR9XG5cblx0XHRyZXR1cm4gb3V0cHV0XG5cdH1cblxuXHRleHBvcnRzLnRvQnl0ZUFycmF5ID0gYjY0VG9CeXRlQXJyYXlcblx0ZXhwb3J0cy5mcm9tQnl0ZUFycmF5ID0gdWludDhUb0Jhc2U2NFxufSh0eXBlb2YgZXhwb3J0cyA9PT0gJ3VuZGVmaW5lZCcgPyAodGhpcy5iYXNlNjRqcyA9IHt9KSA6IGV4cG9ydHMpKVxuIiwiZXhwb3J0cy5yZWFkID0gZnVuY3Rpb24oYnVmZmVyLCBvZmZzZXQsIGlzTEUsIG1MZW4sIG5CeXRlcykge1xuICB2YXIgZSwgbSxcbiAgICAgIGVMZW4gPSBuQnl0ZXMgKiA4IC0gbUxlbiAtIDEsXG4gICAgICBlTWF4ID0gKDEgPDwgZUxlbikgLSAxLFxuICAgICAgZUJpYXMgPSBlTWF4ID4+IDEsXG4gICAgICBuQml0cyA9IC03LFxuICAgICAgaSA9IGlzTEUgPyAobkJ5dGVzIC0gMSkgOiAwLFxuICAgICAgZCA9IGlzTEUgPyAtMSA6IDEsXG4gICAgICBzID0gYnVmZmVyW29mZnNldCArIGldO1xuXG4gIGkgKz0gZDtcblxuICBlID0gcyAmICgoMSA8PCAoLW5CaXRzKSkgLSAxKTtcbiAgcyA+Pj0gKC1uQml0cyk7XG4gIG5CaXRzICs9IGVMZW47XG4gIGZvciAoOyBuQml0cyA+IDA7IGUgPSBlICogMjU2ICsgYnVmZmVyW29mZnNldCArIGldLCBpICs9IGQsIG5CaXRzIC09IDgpO1xuXG4gIG0gPSBlICYgKCgxIDw8ICgtbkJpdHMpKSAtIDEpO1xuICBlID4+PSAoLW5CaXRzKTtcbiAgbkJpdHMgKz0gbUxlbjtcbiAgZm9yICg7IG5CaXRzID4gMDsgbSA9IG0gKiAyNTYgKyBidWZmZXJbb2Zmc2V0ICsgaV0sIGkgKz0gZCwgbkJpdHMgLT0gOCk7XG5cbiAgaWYgKGUgPT09IDApIHtcbiAgICBlID0gMSAtIGVCaWFzO1xuICB9IGVsc2UgaWYgKGUgPT09IGVNYXgpIHtcbiAgICByZXR1cm4gbSA/IE5hTiA6ICgocyA/IC0xIDogMSkgKiBJbmZpbml0eSk7XG4gIH0gZWxzZSB7XG4gICAgbSA9IG0gKyBNYXRoLnBvdygyLCBtTGVuKTtcbiAgICBlID0gZSAtIGVCaWFzO1xuICB9XG4gIHJldHVybiAocyA/IC0xIDogMSkgKiBtICogTWF0aC5wb3coMiwgZSAtIG1MZW4pO1xufTtcblxuZXhwb3J0cy53cml0ZSA9IGZ1bmN0aW9uKGJ1ZmZlciwgdmFsdWUsIG9mZnNldCwgaXNMRSwgbUxlbiwgbkJ5dGVzKSB7XG4gIHZhciBlLCBtLCBjLFxuICAgICAgZUxlbiA9IG5CeXRlcyAqIDggLSBtTGVuIC0gMSxcbiAgICAgIGVNYXggPSAoMSA8PCBlTGVuKSAtIDEsXG4gICAgICBlQmlhcyA9IGVNYXggPj4gMSxcbiAgICAgIHJ0ID0gKG1MZW4gPT09IDIzID8gTWF0aC5wb3coMiwgLTI0KSAtIE1hdGgucG93KDIsIC03NykgOiAwKSxcbiAgICAgIGkgPSBpc0xFID8gMCA6IChuQnl0ZXMgLSAxKSxcbiAgICAgIGQgPSBpc0xFID8gMSA6IC0xLFxuICAgICAgcyA9IHZhbHVlIDwgMCB8fCAodmFsdWUgPT09IDAgJiYgMSAvIHZhbHVlIDwgMCkgPyAxIDogMDtcblxuICB2YWx1ZSA9IE1hdGguYWJzKHZhbHVlKTtcblxuICBpZiAoaXNOYU4odmFsdWUpIHx8IHZhbHVlID09PSBJbmZpbml0eSkge1xuICAgIG0gPSBpc05hTih2YWx1ZSkgPyAxIDogMDtcbiAgICBlID0gZU1heDtcbiAgfSBlbHNlIHtcbiAgICBlID0gTWF0aC5mbG9vcihNYXRoLmxvZyh2YWx1ZSkgLyBNYXRoLkxOMik7XG4gICAgaWYgKHZhbHVlICogKGMgPSBNYXRoLnBvdygyLCAtZSkpIDwgMSkge1xuICAgICAgZS0tO1xuICAgICAgYyAqPSAyO1xuICAgIH1cbiAgICBpZiAoZSArIGVCaWFzID49IDEpIHtcbiAgICAgIHZhbHVlICs9IHJ0IC8gYztcbiAgICB9IGVsc2Uge1xuICAgICAgdmFsdWUgKz0gcnQgKiBNYXRoLnBvdygyLCAxIC0gZUJpYXMpO1xuICAgIH1cbiAgICBpZiAodmFsdWUgKiBjID49IDIpIHtcbiAgICAgIGUrKztcbiAgICAgIGMgLz0gMjtcbiAgICB9XG5cbiAgICBpZiAoZSArIGVCaWFzID49IGVNYXgpIHtcbiAgICAgIG0gPSAwO1xuICAgICAgZSA9IGVNYXg7XG4gICAgfSBlbHNlIGlmIChlICsgZUJpYXMgPj0gMSkge1xuICAgICAgbSA9ICh2YWx1ZSAqIGMgLSAxKSAqIE1hdGgucG93KDIsIG1MZW4pO1xuICAgICAgZSA9IGUgKyBlQmlhcztcbiAgICB9IGVsc2Uge1xuICAgICAgbSA9IHZhbHVlICogTWF0aC5wb3coMiwgZUJpYXMgLSAxKSAqIE1hdGgucG93KDIsIG1MZW4pO1xuICAgICAgZSA9IDA7XG4gICAgfVxuICB9XG5cbiAgZm9yICg7IG1MZW4gPj0gODsgYnVmZmVyW29mZnNldCArIGldID0gbSAmIDB4ZmYsIGkgKz0gZCwgbSAvPSAyNTYsIG1MZW4gLT0gOCk7XG5cbiAgZSA9IChlIDw8IG1MZW4pIHwgbTtcbiAgZUxlbiArPSBtTGVuO1xuICBmb3IgKDsgZUxlbiA+IDA7IGJ1ZmZlcltvZmZzZXQgKyBpXSA9IGUgJiAweGZmLCBpICs9IGQsIGUgLz0gMjU2LCBlTGVuIC09IDgpO1xuXG4gIGJ1ZmZlcltvZmZzZXQgKyBpIC0gZF0gfD0gcyAqIDEyODtcbn07XG4iLCJcbi8qKlxuICogaXNBcnJheVxuICovXG5cbnZhciBpc0FycmF5ID0gQXJyYXkuaXNBcnJheTtcblxuLyoqXG4gKiB0b1N0cmluZ1xuICovXG5cbnZhciBzdHIgPSBPYmplY3QucHJvdG90eXBlLnRvU3RyaW5nO1xuXG4vKipcbiAqIFdoZXRoZXIgb3Igbm90IHRoZSBnaXZlbiBgdmFsYFxuICogaXMgYW4gYXJyYXkuXG4gKlxuICogZXhhbXBsZTpcbiAqXG4gKiAgICAgICAgaXNBcnJheShbXSk7XG4gKiAgICAgICAgLy8gPiB0cnVlXG4gKiAgICAgICAgaXNBcnJheShhcmd1bWVudHMpO1xuICogICAgICAgIC8vID4gZmFsc2VcbiAqICAgICAgICBpc0FycmF5KCcnKTtcbiAqICAgICAgICAvLyA+IGZhbHNlXG4gKlxuICogQHBhcmFtIHttaXhlZH0gdmFsXG4gKiBAcmV0dXJuIHtib29sfVxuICovXG5cbm1vZHVsZS5leHBvcnRzID0gaXNBcnJheSB8fCBmdW5jdGlvbiAodmFsKSB7XG4gIHJldHVybiAhISB2YWwgJiYgJ1tvYmplY3QgQXJyYXldJyA9PSBzdHIuY2FsbCh2YWwpO1xufTtcbiIsImlmICh0eXBlb2YgT2JqZWN0LmNyZWF0ZSA9PT0gJ2Z1bmN0aW9uJykge1xuICAvLyBpbXBsZW1lbnRhdGlvbiBmcm9tIHN0YW5kYXJkIG5vZGUuanMgJ3V0aWwnIG1vZHVsZVxuICBtb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIGluaGVyaXRzKGN0b3IsIHN1cGVyQ3Rvcikge1xuICAgIGN0b3Iuc3VwZXJfID0gc3VwZXJDdG9yXG4gICAgY3Rvci5wcm90b3R5cGUgPSBPYmplY3QuY3JlYXRlKHN1cGVyQ3Rvci5wcm90b3R5cGUsIHtcbiAgICAgIGNvbnN0cnVjdG9yOiB7XG4gICAgICAgIHZhbHVlOiBjdG9yLFxuICAgICAgICBlbnVtZXJhYmxlOiBmYWxzZSxcbiAgICAgICAgd3JpdGFibGU6IHRydWUsXG4gICAgICAgIGNvbmZpZ3VyYWJsZTogdHJ1ZVxuICAgICAgfVxuICAgIH0pO1xuICB9O1xufSBlbHNlIHtcbiAgLy8gb2xkIHNjaG9vbCBzaGltIGZvciBvbGQgYnJvd3NlcnNcbiAgbW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiBpbmhlcml0cyhjdG9yLCBzdXBlckN0b3IpIHtcbiAgICBjdG9yLnN1cGVyXyA9IHN1cGVyQ3RvclxuICAgIHZhciBUZW1wQ3RvciA9IGZ1bmN0aW9uICgpIHt9XG4gICAgVGVtcEN0b3IucHJvdG90eXBlID0gc3VwZXJDdG9yLnByb3RvdHlwZVxuICAgIGN0b3IucHJvdG90eXBlID0gbmV3IFRlbXBDdG9yKClcbiAgICBjdG9yLnByb3RvdHlwZS5jb25zdHJ1Y3RvciA9IGN0b3JcbiAgfVxufVxuIiwiLy8gc2hpbSBmb3IgdXNpbmcgcHJvY2VzcyBpbiBicm93c2VyXG5cbnZhciBwcm9jZXNzID0gbW9kdWxlLmV4cG9ydHMgPSB7fTtcblxucHJvY2Vzcy5uZXh0VGljayA9IChmdW5jdGlvbiAoKSB7XG4gICAgdmFyIGNhblNldEltbWVkaWF0ZSA9IHR5cGVvZiB3aW5kb3cgIT09ICd1bmRlZmluZWQnXG4gICAgJiYgd2luZG93LnNldEltbWVkaWF0ZTtcbiAgICB2YXIgY2FuTXV0YXRpb25PYnNlcnZlciA9IHR5cGVvZiB3aW5kb3cgIT09ICd1bmRlZmluZWQnXG4gICAgJiYgd2luZG93Lk11dGF0aW9uT2JzZXJ2ZXI7XG4gICAgdmFyIGNhblBvc3QgPSB0eXBlb2Ygd2luZG93ICE9PSAndW5kZWZpbmVkJ1xuICAgICYmIHdpbmRvdy5wb3N0TWVzc2FnZSAmJiB3aW5kb3cuYWRkRXZlbnRMaXN0ZW5lclxuICAgIDtcblxuICAgIGlmIChjYW5TZXRJbW1lZGlhdGUpIHtcbiAgICAgICAgcmV0dXJuIGZ1bmN0aW9uIChmKSB7IHJldHVybiB3aW5kb3cuc2V0SW1tZWRpYXRlKGYpIH07XG4gICAgfVxuXG4gICAgdmFyIHF1ZXVlID0gW107XG5cbiAgICBpZiAoY2FuTXV0YXRpb25PYnNlcnZlcikge1xuICAgICAgICB2YXIgaGlkZGVuRGl2ID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudChcImRpdlwiKTtcbiAgICAgICAgdmFyIG9ic2VydmVyID0gbmV3IE11dGF0aW9uT2JzZXJ2ZXIoZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgdmFyIHF1ZXVlTGlzdCA9IHF1ZXVlLnNsaWNlKCk7XG4gICAgICAgICAgICBxdWV1ZS5sZW5ndGggPSAwO1xuICAgICAgICAgICAgcXVldWVMaXN0LmZvckVhY2goZnVuY3Rpb24gKGZuKSB7XG4gICAgICAgICAgICAgICAgZm4oKTtcbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9KTtcblxuICAgICAgICBvYnNlcnZlci5vYnNlcnZlKGhpZGRlbkRpdiwgeyBhdHRyaWJ1dGVzOiB0cnVlIH0pO1xuXG4gICAgICAgIHJldHVybiBmdW5jdGlvbiBuZXh0VGljayhmbikge1xuICAgICAgICAgICAgaWYgKCFxdWV1ZS5sZW5ndGgpIHtcbiAgICAgICAgICAgICAgICBoaWRkZW5EaXYuc2V0QXR0cmlidXRlKCd5ZXMnLCAnbm8nKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHF1ZXVlLnB1c2goZm4pO1xuICAgICAgICB9O1xuICAgIH1cblxuICAgIGlmIChjYW5Qb3N0KSB7XG4gICAgICAgIHdpbmRvdy5hZGRFdmVudExpc3RlbmVyKCdtZXNzYWdlJywgZnVuY3Rpb24gKGV2KSB7XG4gICAgICAgICAgICB2YXIgc291cmNlID0gZXYuc291cmNlO1xuICAgICAgICAgICAgaWYgKChzb3VyY2UgPT09IHdpbmRvdyB8fCBzb3VyY2UgPT09IG51bGwpICYmIGV2LmRhdGEgPT09ICdwcm9jZXNzLXRpY2snKSB7XG4gICAgICAgICAgICAgICAgZXYuc3RvcFByb3BhZ2F0aW9uKCk7XG4gICAgICAgICAgICAgICAgaWYgKHF1ZXVlLmxlbmd0aCA+IDApIHtcbiAgICAgICAgICAgICAgICAgICAgdmFyIGZuID0gcXVldWUuc2hpZnQoKTtcbiAgICAgICAgICAgICAgICAgICAgZm4oKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgIH0sIHRydWUpO1xuXG4gICAgICAgIHJldHVybiBmdW5jdGlvbiBuZXh0VGljayhmbikge1xuICAgICAgICAgICAgcXVldWUucHVzaChmbik7XG4gICAgICAgICAgICB3aW5kb3cucG9zdE1lc3NhZ2UoJ3Byb2Nlc3MtdGljaycsICcqJyk7XG4gICAgICAgIH07XG4gICAgfVxuXG4gICAgcmV0dXJuIGZ1bmN0aW9uIG5leHRUaWNrKGZuKSB7XG4gICAgICAgIHNldFRpbWVvdXQoZm4sIDApO1xuICAgIH07XG59KSgpO1xuXG5wcm9jZXNzLnRpdGxlID0gJ2Jyb3dzZXInO1xucHJvY2Vzcy5icm93c2VyID0gdHJ1ZTtcbnByb2Nlc3MuZW52ID0ge307XG5wcm9jZXNzLmFyZ3YgPSBbXTtcblxuZnVuY3Rpb24gbm9vcCgpIHt9XG5cbnByb2Nlc3Mub24gPSBub29wO1xucHJvY2Vzcy5hZGRMaXN0ZW5lciA9IG5vb3A7XG5wcm9jZXNzLm9uY2UgPSBub29wO1xucHJvY2Vzcy5vZmYgPSBub29wO1xucHJvY2Vzcy5yZW1vdmVMaXN0ZW5lciA9IG5vb3A7XG5wcm9jZXNzLnJlbW92ZUFsbExpc3RlbmVycyA9IG5vb3A7XG5wcm9jZXNzLmVtaXQgPSBub29wO1xuXG5wcm9jZXNzLmJpbmRpbmcgPSBmdW5jdGlvbiAobmFtZSkge1xuICAgIHRocm93IG5ldyBFcnJvcigncHJvY2Vzcy5iaW5kaW5nIGlzIG5vdCBzdXBwb3J0ZWQnKTtcbn07XG5cbi8vIFRPRE8oc2h0eWxtYW4pXG5wcm9jZXNzLmN3ZCA9IGZ1bmN0aW9uICgpIHsgcmV0dXJuICcvJyB9O1xucHJvY2Vzcy5jaGRpciA9IGZ1bmN0aW9uIChkaXIpIHtcbiAgICB0aHJvdyBuZXcgRXJyb3IoJ3Byb2Nlc3MuY2hkaXIgaXMgbm90IHN1cHBvcnRlZCcpO1xufTtcbiIsIm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gaXNCdWZmZXIoYXJnKSB7XG4gIHJldHVybiBhcmcgJiYgdHlwZW9mIGFyZyA9PT0gJ29iamVjdCdcbiAgICAmJiB0eXBlb2YgYXJnLmNvcHkgPT09ICdmdW5jdGlvbidcbiAgICAmJiB0eXBlb2YgYXJnLmZpbGwgPT09ICdmdW5jdGlvbidcbiAgICAmJiB0eXBlb2YgYXJnLnJlYWRVSW50OCA9PT0gJ2Z1bmN0aW9uJztcbn0iLCIoZnVuY3Rpb24gKHByb2Nlc3MsZ2xvYmFsKXtcbi8vIENvcHlyaWdodCBKb3llbnQsIEluYy4gYW5kIG90aGVyIE5vZGUgY29udHJpYnV0b3JzLlxuLy9cbi8vIFBlcm1pc3Npb24gaXMgaGVyZWJ5IGdyYW50ZWQsIGZyZWUgb2YgY2hhcmdlLCB0byBhbnkgcGVyc29uIG9idGFpbmluZyBhXG4vLyBjb3B5IG9mIHRoaXMgc29mdHdhcmUgYW5kIGFzc29jaWF0ZWQgZG9jdW1lbnRhdGlvbiBmaWxlcyAodGhlXG4vLyBcIlNvZnR3YXJlXCIpLCB0byBkZWFsIGluIHRoZSBTb2Z0d2FyZSB3aXRob3V0IHJlc3RyaWN0aW9uLCBpbmNsdWRpbmdcbi8vIHdpdGhvdXQgbGltaXRhdGlvbiB0aGUgcmlnaHRzIHRvIHVzZSwgY29weSwgbW9kaWZ5LCBtZXJnZSwgcHVibGlzaCxcbi8vIGRpc3RyaWJ1dGUsIHN1YmxpY2Vuc2UsIGFuZC9vciBzZWxsIGNvcGllcyBvZiB0aGUgU29mdHdhcmUsIGFuZCB0byBwZXJtaXRcbi8vIHBlcnNvbnMgdG8gd2hvbSB0aGUgU29mdHdhcmUgaXMgZnVybmlzaGVkIHRvIGRvIHNvLCBzdWJqZWN0IHRvIHRoZVxuLy8gZm9sbG93aW5nIGNvbmRpdGlvbnM6XG4vL1xuLy8gVGhlIGFib3ZlIGNvcHlyaWdodCBub3RpY2UgYW5kIHRoaXMgcGVybWlzc2lvbiBub3RpY2Ugc2hhbGwgYmUgaW5jbHVkZWRcbi8vIGluIGFsbCBjb3BpZXMgb3Igc3Vic3RhbnRpYWwgcG9ydGlvbnMgb2YgdGhlIFNvZnR3YXJlLlxuLy9cbi8vIFRIRSBTT0ZUV0FSRSBJUyBQUk9WSURFRCBcIkFTIElTXCIsIFdJVEhPVVQgV0FSUkFOVFkgT0YgQU5ZIEtJTkQsIEVYUFJFU1Ncbi8vIE9SIElNUExJRUQsIElOQ0xVRElORyBCVVQgTk9UIExJTUlURUQgVE8gVEhFIFdBUlJBTlRJRVMgT0Zcbi8vIE1FUkNIQU5UQUJJTElUWSwgRklUTkVTUyBGT1IgQSBQQVJUSUNVTEFSIFBVUlBPU0UgQU5EIE5PTklORlJJTkdFTUVOVC4gSU5cbi8vIE5PIEVWRU5UIFNIQUxMIFRIRSBBVVRIT1JTIE9SIENPUFlSSUdIVCBIT0xERVJTIEJFIExJQUJMRSBGT1IgQU5ZIENMQUlNLFxuLy8gREFNQUdFUyBPUiBPVEhFUiBMSUFCSUxJVFksIFdIRVRIRVIgSU4gQU4gQUNUSU9OIE9GIENPTlRSQUNULCBUT1JUIE9SXG4vLyBPVEhFUldJU0UsIEFSSVNJTkcgRlJPTSwgT1VUIE9GIE9SIElOIENPTk5FQ1RJT04gV0lUSCBUSEUgU09GVFdBUkUgT1IgVEhFXG4vLyBVU0UgT1IgT1RIRVIgREVBTElOR1MgSU4gVEhFIFNPRlRXQVJFLlxuXG52YXIgZm9ybWF0UmVnRXhwID0gLyVbc2RqJV0vZztcbmV4cG9ydHMuZm9ybWF0ID0gZnVuY3Rpb24oZikge1xuICBpZiAoIWlzU3RyaW5nKGYpKSB7XG4gICAgdmFyIG9iamVjdHMgPSBbXTtcbiAgICBmb3IgKHZhciBpID0gMDsgaSA8IGFyZ3VtZW50cy5sZW5ndGg7IGkrKykge1xuICAgICAgb2JqZWN0cy5wdXNoKGluc3BlY3QoYXJndW1lbnRzW2ldKSk7XG4gICAgfVxuICAgIHJldHVybiBvYmplY3RzLmpvaW4oJyAnKTtcbiAgfVxuXG4gIHZhciBpID0gMTtcbiAgdmFyIGFyZ3MgPSBhcmd1bWVudHM7XG4gIHZhciBsZW4gPSBhcmdzLmxlbmd0aDtcbiAgdmFyIHN0ciA9IFN0cmluZyhmKS5yZXBsYWNlKGZvcm1hdFJlZ0V4cCwgZnVuY3Rpb24oeCkge1xuICAgIGlmICh4ID09PSAnJSUnKSByZXR1cm4gJyUnO1xuICAgIGlmIChpID49IGxlbikgcmV0dXJuIHg7XG4gICAgc3dpdGNoICh4KSB7XG4gICAgICBjYXNlICclcyc6IHJldHVybiBTdHJpbmcoYXJnc1tpKytdKTtcbiAgICAgIGNhc2UgJyVkJzogcmV0dXJuIE51bWJlcihhcmdzW2krK10pO1xuICAgICAgY2FzZSAnJWonOlxuICAgICAgICB0cnkge1xuICAgICAgICAgIHJldHVybiBKU09OLnN0cmluZ2lmeShhcmdzW2krK10pO1xuICAgICAgICB9IGNhdGNoIChfKSB7XG4gICAgICAgICAgcmV0dXJuICdbQ2lyY3VsYXJdJztcbiAgICAgICAgfVxuICAgICAgZGVmYXVsdDpcbiAgICAgICAgcmV0dXJuIHg7XG4gICAgfVxuICB9KTtcbiAgZm9yICh2YXIgeCA9IGFyZ3NbaV07IGkgPCBsZW47IHggPSBhcmdzWysraV0pIHtcbiAgICBpZiAoaXNOdWxsKHgpIHx8ICFpc09iamVjdCh4KSkge1xuICAgICAgc3RyICs9ICcgJyArIHg7XG4gICAgfSBlbHNlIHtcbiAgICAgIHN0ciArPSAnICcgKyBpbnNwZWN0KHgpO1xuICAgIH1cbiAgfVxuICByZXR1cm4gc3RyO1xufTtcblxuXG4vLyBNYXJrIHRoYXQgYSBtZXRob2Qgc2hvdWxkIG5vdCBiZSB1c2VkLlxuLy8gUmV0dXJucyBhIG1vZGlmaWVkIGZ1bmN0aW9uIHdoaWNoIHdhcm5zIG9uY2UgYnkgZGVmYXVsdC5cbi8vIElmIC0tbm8tZGVwcmVjYXRpb24gaXMgc2V0LCB0aGVuIGl0IGlzIGEgbm8tb3AuXG5leHBvcnRzLmRlcHJlY2F0ZSA9IGZ1bmN0aW9uKGZuLCBtc2cpIHtcbiAgLy8gQWxsb3cgZm9yIGRlcHJlY2F0aW5nIHRoaW5ncyBpbiB0aGUgcHJvY2VzcyBvZiBzdGFydGluZyB1cC5cbiAgaWYgKGlzVW5kZWZpbmVkKGdsb2JhbC5wcm9jZXNzKSkge1xuICAgIHJldHVybiBmdW5jdGlvbigpIHtcbiAgICAgIHJldHVybiBleHBvcnRzLmRlcHJlY2F0ZShmbiwgbXNnKS5hcHBseSh0aGlzLCBhcmd1bWVudHMpO1xuICAgIH07XG4gIH1cblxuICBpZiAocHJvY2Vzcy5ub0RlcHJlY2F0aW9uID09PSB0cnVlKSB7XG4gICAgcmV0dXJuIGZuO1xuICB9XG5cbiAgdmFyIHdhcm5lZCA9IGZhbHNlO1xuICBmdW5jdGlvbiBkZXByZWNhdGVkKCkge1xuICAgIGlmICghd2FybmVkKSB7XG4gICAgICBpZiAocHJvY2Vzcy50aHJvd0RlcHJlY2F0aW9uKSB7XG4gICAgICAgIHRocm93IG5ldyBFcnJvcihtc2cpO1xuICAgICAgfSBlbHNlIGlmIChwcm9jZXNzLnRyYWNlRGVwcmVjYXRpb24pIHtcbiAgICAgICAgY29uc29sZS50cmFjZShtc2cpO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgY29uc29sZS5lcnJvcihtc2cpO1xuICAgICAgfVxuICAgICAgd2FybmVkID0gdHJ1ZTtcbiAgICB9XG4gICAgcmV0dXJuIGZuLmFwcGx5KHRoaXMsIGFyZ3VtZW50cyk7XG4gIH1cblxuICByZXR1cm4gZGVwcmVjYXRlZDtcbn07XG5cblxudmFyIGRlYnVncyA9IHt9O1xudmFyIGRlYnVnRW52aXJvbjtcbmV4cG9ydHMuZGVidWdsb2cgPSBmdW5jdGlvbihzZXQpIHtcbiAgaWYgKGlzVW5kZWZpbmVkKGRlYnVnRW52aXJvbikpXG4gICAgZGVidWdFbnZpcm9uID0gcHJvY2Vzcy5lbnYuTk9ERV9ERUJVRyB8fCAnJztcbiAgc2V0ID0gc2V0LnRvVXBwZXJDYXNlKCk7XG4gIGlmICghZGVidWdzW3NldF0pIHtcbiAgICBpZiAobmV3IFJlZ0V4cCgnXFxcXGInICsgc2V0ICsgJ1xcXFxiJywgJ2knKS50ZXN0KGRlYnVnRW52aXJvbikpIHtcbiAgICAgIHZhciBwaWQgPSBwcm9jZXNzLnBpZDtcbiAgICAgIGRlYnVnc1tzZXRdID0gZnVuY3Rpb24oKSB7XG4gICAgICAgIHZhciBtc2cgPSBleHBvcnRzLmZvcm1hdC5hcHBseShleHBvcnRzLCBhcmd1bWVudHMpO1xuICAgICAgICBjb25zb2xlLmVycm9yKCclcyAlZDogJXMnLCBzZXQsIHBpZCwgbXNnKTtcbiAgICAgIH07XG4gICAgfSBlbHNlIHtcbiAgICAgIGRlYnVnc1tzZXRdID0gZnVuY3Rpb24oKSB7fTtcbiAgICB9XG4gIH1cbiAgcmV0dXJuIGRlYnVnc1tzZXRdO1xufTtcblxuXG4vKipcbiAqIEVjaG9zIHRoZSB2YWx1ZSBvZiBhIHZhbHVlLiBUcnlzIHRvIHByaW50IHRoZSB2YWx1ZSBvdXRcbiAqIGluIHRoZSBiZXN0IHdheSBwb3NzaWJsZSBnaXZlbiB0aGUgZGlmZmVyZW50IHR5cGVzLlxuICpcbiAqIEBwYXJhbSB7T2JqZWN0fSBvYmogVGhlIG9iamVjdCB0byBwcmludCBvdXQuXG4gKiBAcGFyYW0ge09iamVjdH0gb3B0cyBPcHRpb25hbCBvcHRpb25zIG9iamVjdCB0aGF0IGFsdGVycyB0aGUgb3V0cHV0LlxuICovXG4vKiBsZWdhY3k6IG9iaiwgc2hvd0hpZGRlbiwgZGVwdGgsIGNvbG9ycyovXG5mdW5jdGlvbiBpbnNwZWN0KG9iaiwgb3B0cykge1xuICAvLyBkZWZhdWx0IG9wdGlvbnNcbiAgdmFyIGN0eCA9IHtcbiAgICBzZWVuOiBbXSxcbiAgICBzdHlsaXplOiBzdHlsaXplTm9Db2xvclxuICB9O1xuICAvLyBsZWdhY3kuLi5cbiAgaWYgKGFyZ3VtZW50cy5sZW5ndGggPj0gMykgY3R4LmRlcHRoID0gYXJndW1lbnRzWzJdO1xuICBpZiAoYXJndW1lbnRzLmxlbmd0aCA+PSA0KSBjdHguY29sb3JzID0gYXJndW1lbnRzWzNdO1xuICBpZiAoaXNCb29sZWFuKG9wdHMpKSB7XG4gICAgLy8gbGVnYWN5Li4uXG4gICAgY3R4LnNob3dIaWRkZW4gPSBvcHRzO1xuICB9IGVsc2UgaWYgKG9wdHMpIHtcbiAgICAvLyBnb3QgYW4gXCJvcHRpb25zXCIgb2JqZWN0XG4gICAgZXhwb3J0cy5fZXh0ZW5kKGN0eCwgb3B0cyk7XG4gIH1cbiAgLy8gc2V0IGRlZmF1bHQgb3B0aW9uc1xuICBpZiAoaXNVbmRlZmluZWQoY3R4LnNob3dIaWRkZW4pKSBjdHguc2hvd0hpZGRlbiA9IGZhbHNlO1xuICBpZiAoaXNVbmRlZmluZWQoY3R4LmRlcHRoKSkgY3R4LmRlcHRoID0gMjtcbiAgaWYgKGlzVW5kZWZpbmVkKGN0eC5jb2xvcnMpKSBjdHguY29sb3JzID0gZmFsc2U7XG4gIGlmIChpc1VuZGVmaW5lZChjdHguY3VzdG9tSW5zcGVjdCkpIGN0eC5jdXN0b21JbnNwZWN0ID0gdHJ1ZTtcbiAgaWYgKGN0eC5jb2xvcnMpIGN0eC5zdHlsaXplID0gc3R5bGl6ZVdpdGhDb2xvcjtcbiAgcmV0dXJuIGZvcm1hdFZhbHVlKGN0eCwgb2JqLCBjdHguZGVwdGgpO1xufVxuZXhwb3J0cy5pbnNwZWN0ID0gaW5zcGVjdDtcblxuXG4vLyBodHRwOi8vZW4ud2lraXBlZGlhLm9yZy93aWtpL0FOU0lfZXNjYXBlX2NvZGUjZ3JhcGhpY3Ncbmluc3BlY3QuY29sb3JzID0ge1xuICAnYm9sZCcgOiBbMSwgMjJdLFxuICAnaXRhbGljJyA6IFszLCAyM10sXG4gICd1bmRlcmxpbmUnIDogWzQsIDI0XSxcbiAgJ2ludmVyc2UnIDogWzcsIDI3XSxcbiAgJ3doaXRlJyA6IFszNywgMzldLFxuICAnZ3JleScgOiBbOTAsIDM5XSxcbiAgJ2JsYWNrJyA6IFszMCwgMzldLFxuICAnYmx1ZScgOiBbMzQsIDM5XSxcbiAgJ2N5YW4nIDogWzM2LCAzOV0sXG4gICdncmVlbicgOiBbMzIsIDM5XSxcbiAgJ21hZ2VudGEnIDogWzM1LCAzOV0sXG4gICdyZWQnIDogWzMxLCAzOV0sXG4gICd5ZWxsb3cnIDogWzMzLCAzOV1cbn07XG5cbi8vIERvbid0IHVzZSAnYmx1ZScgbm90IHZpc2libGUgb24gY21kLmV4ZVxuaW5zcGVjdC5zdHlsZXMgPSB7XG4gICdzcGVjaWFsJzogJ2N5YW4nLFxuICAnbnVtYmVyJzogJ3llbGxvdycsXG4gICdib29sZWFuJzogJ3llbGxvdycsXG4gICd1bmRlZmluZWQnOiAnZ3JleScsXG4gICdudWxsJzogJ2JvbGQnLFxuICAnc3RyaW5nJzogJ2dyZWVuJyxcbiAgJ2RhdGUnOiAnbWFnZW50YScsXG4gIC8vIFwibmFtZVwiOiBpbnRlbnRpb25hbGx5IG5vdCBzdHlsaW5nXG4gICdyZWdleHAnOiAncmVkJ1xufTtcblxuXG5mdW5jdGlvbiBzdHlsaXplV2l0aENvbG9yKHN0ciwgc3R5bGVUeXBlKSB7XG4gIHZhciBzdHlsZSA9IGluc3BlY3Quc3R5bGVzW3N0eWxlVHlwZV07XG5cbiAgaWYgKHN0eWxlKSB7XG4gICAgcmV0dXJuICdcXHUwMDFiWycgKyBpbnNwZWN0LmNvbG9yc1tzdHlsZV1bMF0gKyAnbScgKyBzdHIgK1xuICAgICAgICAgICAnXFx1MDAxYlsnICsgaW5zcGVjdC5jb2xvcnNbc3R5bGVdWzFdICsgJ20nO1xuICB9IGVsc2Uge1xuICAgIHJldHVybiBzdHI7XG4gIH1cbn1cblxuXG5mdW5jdGlvbiBzdHlsaXplTm9Db2xvcihzdHIsIHN0eWxlVHlwZSkge1xuICByZXR1cm4gc3RyO1xufVxuXG5cbmZ1bmN0aW9uIGFycmF5VG9IYXNoKGFycmF5KSB7XG4gIHZhciBoYXNoID0ge307XG5cbiAgYXJyYXkuZm9yRWFjaChmdW5jdGlvbih2YWwsIGlkeCkge1xuICAgIGhhc2hbdmFsXSA9IHRydWU7XG4gIH0pO1xuXG4gIHJldHVybiBoYXNoO1xufVxuXG5cbmZ1bmN0aW9uIGZvcm1hdFZhbHVlKGN0eCwgdmFsdWUsIHJlY3Vyc2VUaW1lcykge1xuICAvLyBQcm92aWRlIGEgaG9vayBmb3IgdXNlci1zcGVjaWZpZWQgaW5zcGVjdCBmdW5jdGlvbnMuXG4gIC8vIENoZWNrIHRoYXQgdmFsdWUgaXMgYW4gb2JqZWN0IHdpdGggYW4gaW5zcGVjdCBmdW5jdGlvbiBvbiBpdFxuICBpZiAoY3R4LmN1c3RvbUluc3BlY3QgJiZcbiAgICAgIHZhbHVlICYmXG4gICAgICBpc0Z1bmN0aW9uKHZhbHVlLmluc3BlY3QpICYmXG4gICAgICAvLyBGaWx0ZXIgb3V0IHRoZSB1dGlsIG1vZHVsZSwgaXQncyBpbnNwZWN0IGZ1bmN0aW9uIGlzIHNwZWNpYWxcbiAgICAgIHZhbHVlLmluc3BlY3QgIT09IGV4cG9ydHMuaW5zcGVjdCAmJlxuICAgICAgLy8gQWxzbyBmaWx0ZXIgb3V0IGFueSBwcm90b3R5cGUgb2JqZWN0cyB1c2luZyB0aGUgY2lyY3VsYXIgY2hlY2suXG4gICAgICAhKHZhbHVlLmNvbnN0cnVjdG9yICYmIHZhbHVlLmNvbnN0cnVjdG9yLnByb3RvdHlwZSA9PT0gdmFsdWUpKSB7XG4gICAgdmFyIHJldCA9IHZhbHVlLmluc3BlY3QocmVjdXJzZVRpbWVzLCBjdHgpO1xuICAgIGlmICghaXNTdHJpbmcocmV0KSkge1xuICAgICAgcmV0ID0gZm9ybWF0VmFsdWUoY3R4LCByZXQsIHJlY3Vyc2VUaW1lcyk7XG4gICAgfVxuICAgIHJldHVybiByZXQ7XG4gIH1cblxuICAvLyBQcmltaXRpdmUgdHlwZXMgY2Fubm90IGhhdmUgcHJvcGVydGllc1xuICB2YXIgcHJpbWl0aXZlID0gZm9ybWF0UHJpbWl0aXZlKGN0eCwgdmFsdWUpO1xuICBpZiAocHJpbWl0aXZlKSB7XG4gICAgcmV0dXJuIHByaW1pdGl2ZTtcbiAgfVxuXG4gIC8vIExvb2sgdXAgdGhlIGtleXMgb2YgdGhlIG9iamVjdC5cbiAgdmFyIGtleXMgPSBPYmplY3Qua2V5cyh2YWx1ZSk7XG4gIHZhciB2aXNpYmxlS2V5cyA9IGFycmF5VG9IYXNoKGtleXMpO1xuXG4gIGlmIChjdHguc2hvd0hpZGRlbikge1xuICAgIGtleXMgPSBPYmplY3QuZ2V0T3duUHJvcGVydHlOYW1lcyh2YWx1ZSk7XG4gIH1cblxuICAvLyBJRSBkb2Vzbid0IG1ha2UgZXJyb3IgZmllbGRzIG5vbi1lbnVtZXJhYmxlXG4gIC8vIGh0dHA6Ly9tc2RuLm1pY3Jvc29mdC5jb20vZW4tdXMvbGlicmFyeS9pZS9kd3c1MnNidCh2PXZzLjk0KS5hc3B4XG4gIGlmIChpc0Vycm9yKHZhbHVlKVxuICAgICAgJiYgKGtleXMuaW5kZXhPZignbWVzc2FnZScpID49IDAgfHwga2V5cy5pbmRleE9mKCdkZXNjcmlwdGlvbicpID49IDApKSB7XG4gICAgcmV0dXJuIGZvcm1hdEVycm9yKHZhbHVlKTtcbiAgfVxuXG4gIC8vIFNvbWUgdHlwZSBvZiBvYmplY3Qgd2l0aG91dCBwcm9wZXJ0aWVzIGNhbiBiZSBzaG9ydGN1dHRlZC5cbiAgaWYgKGtleXMubGVuZ3RoID09PSAwKSB7XG4gICAgaWYgKGlzRnVuY3Rpb24odmFsdWUpKSB7XG4gICAgICB2YXIgbmFtZSA9IHZhbHVlLm5hbWUgPyAnOiAnICsgdmFsdWUubmFtZSA6ICcnO1xuICAgICAgcmV0dXJuIGN0eC5zdHlsaXplKCdbRnVuY3Rpb24nICsgbmFtZSArICddJywgJ3NwZWNpYWwnKTtcbiAgICB9XG4gICAgaWYgKGlzUmVnRXhwKHZhbHVlKSkge1xuICAgICAgcmV0dXJuIGN0eC5zdHlsaXplKFJlZ0V4cC5wcm90b3R5cGUudG9TdHJpbmcuY2FsbCh2YWx1ZSksICdyZWdleHAnKTtcbiAgICB9XG4gICAgaWYgKGlzRGF0ZSh2YWx1ZSkpIHtcbiAgICAgIHJldHVybiBjdHguc3R5bGl6ZShEYXRlLnByb3RvdHlwZS50b1N0cmluZy5jYWxsKHZhbHVlKSwgJ2RhdGUnKTtcbiAgICB9XG4gICAgaWYgKGlzRXJyb3IodmFsdWUpKSB7XG4gICAgICByZXR1cm4gZm9ybWF0RXJyb3IodmFsdWUpO1xuICAgIH1cbiAgfVxuXG4gIHZhciBiYXNlID0gJycsIGFycmF5ID0gZmFsc2UsIGJyYWNlcyA9IFsneycsICd9J107XG5cbiAgLy8gTWFrZSBBcnJheSBzYXkgdGhhdCB0aGV5IGFyZSBBcnJheVxuICBpZiAoaXNBcnJheSh2YWx1ZSkpIHtcbiAgICBhcnJheSA9IHRydWU7XG4gICAgYnJhY2VzID0gWydbJywgJ10nXTtcbiAgfVxuXG4gIC8vIE1ha2UgZnVuY3Rpb25zIHNheSB0aGF0IHRoZXkgYXJlIGZ1bmN0aW9uc1xuICBpZiAoaXNGdW5jdGlvbih2YWx1ZSkpIHtcbiAgICB2YXIgbiA9IHZhbHVlLm5hbWUgPyAnOiAnICsgdmFsdWUubmFtZSA6ICcnO1xuICAgIGJhc2UgPSAnIFtGdW5jdGlvbicgKyBuICsgJ10nO1xuICB9XG5cbiAgLy8gTWFrZSBSZWdFeHBzIHNheSB0aGF0IHRoZXkgYXJlIFJlZ0V4cHNcbiAgaWYgKGlzUmVnRXhwKHZhbHVlKSkge1xuICAgIGJhc2UgPSAnICcgKyBSZWdFeHAucHJvdG90eXBlLnRvU3RyaW5nLmNhbGwodmFsdWUpO1xuICB9XG5cbiAgLy8gTWFrZSBkYXRlcyB3aXRoIHByb3BlcnRpZXMgZmlyc3Qgc2F5IHRoZSBkYXRlXG4gIGlmIChpc0RhdGUodmFsdWUpKSB7XG4gICAgYmFzZSA9ICcgJyArIERhdGUucHJvdG90eXBlLnRvVVRDU3RyaW5nLmNhbGwodmFsdWUpO1xuICB9XG5cbiAgLy8gTWFrZSBlcnJvciB3aXRoIG1lc3NhZ2UgZmlyc3Qgc2F5IHRoZSBlcnJvclxuICBpZiAoaXNFcnJvcih2YWx1ZSkpIHtcbiAgICBiYXNlID0gJyAnICsgZm9ybWF0RXJyb3IodmFsdWUpO1xuICB9XG5cbiAgaWYgKGtleXMubGVuZ3RoID09PSAwICYmICghYXJyYXkgfHwgdmFsdWUubGVuZ3RoID09IDApKSB7XG4gICAgcmV0dXJuIGJyYWNlc1swXSArIGJhc2UgKyBicmFjZXNbMV07XG4gIH1cblxuICBpZiAocmVjdXJzZVRpbWVzIDwgMCkge1xuICAgIGlmIChpc1JlZ0V4cCh2YWx1ZSkpIHtcbiAgICAgIHJldHVybiBjdHguc3R5bGl6ZShSZWdFeHAucHJvdG90eXBlLnRvU3RyaW5nLmNhbGwodmFsdWUpLCAncmVnZXhwJyk7XG4gICAgfSBlbHNlIHtcbiAgICAgIHJldHVybiBjdHguc3R5bGl6ZSgnW09iamVjdF0nLCAnc3BlY2lhbCcpO1xuICAgIH1cbiAgfVxuXG4gIGN0eC5zZWVuLnB1c2godmFsdWUpO1xuXG4gIHZhciBvdXRwdXQ7XG4gIGlmIChhcnJheSkge1xuICAgIG91dHB1dCA9IGZvcm1hdEFycmF5KGN0eCwgdmFsdWUsIHJlY3Vyc2VUaW1lcywgdmlzaWJsZUtleXMsIGtleXMpO1xuICB9IGVsc2Uge1xuICAgIG91dHB1dCA9IGtleXMubWFwKGZ1bmN0aW9uKGtleSkge1xuICAgICAgcmV0dXJuIGZvcm1hdFByb3BlcnR5KGN0eCwgdmFsdWUsIHJlY3Vyc2VUaW1lcywgdmlzaWJsZUtleXMsIGtleSwgYXJyYXkpO1xuICAgIH0pO1xuICB9XG5cbiAgY3R4LnNlZW4ucG9wKCk7XG5cbiAgcmV0dXJuIHJlZHVjZVRvU2luZ2xlU3RyaW5nKG91dHB1dCwgYmFzZSwgYnJhY2VzKTtcbn1cblxuXG5mdW5jdGlvbiBmb3JtYXRQcmltaXRpdmUoY3R4LCB2YWx1ZSkge1xuICBpZiAoaXNVbmRlZmluZWQodmFsdWUpKVxuICAgIHJldHVybiBjdHguc3R5bGl6ZSgndW5kZWZpbmVkJywgJ3VuZGVmaW5lZCcpO1xuICBpZiAoaXNTdHJpbmcodmFsdWUpKSB7XG4gICAgdmFyIHNpbXBsZSA9ICdcXCcnICsgSlNPTi5zdHJpbmdpZnkodmFsdWUpLnJlcGxhY2UoL15cInxcIiQvZywgJycpXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAucmVwbGFjZSgvJy9nLCBcIlxcXFwnXCIpXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAucmVwbGFjZSgvXFxcXFwiL2csICdcIicpICsgJ1xcJyc7XG4gICAgcmV0dXJuIGN0eC5zdHlsaXplKHNpbXBsZSwgJ3N0cmluZycpO1xuICB9XG4gIGlmIChpc051bWJlcih2YWx1ZSkpXG4gICAgcmV0dXJuIGN0eC5zdHlsaXplKCcnICsgdmFsdWUsICdudW1iZXInKTtcbiAgaWYgKGlzQm9vbGVhbih2YWx1ZSkpXG4gICAgcmV0dXJuIGN0eC5zdHlsaXplKCcnICsgdmFsdWUsICdib29sZWFuJyk7XG4gIC8vIEZvciBzb21lIHJlYXNvbiB0eXBlb2YgbnVsbCBpcyBcIm9iamVjdFwiLCBzbyBzcGVjaWFsIGNhc2UgaGVyZS5cbiAgaWYgKGlzTnVsbCh2YWx1ZSkpXG4gICAgcmV0dXJuIGN0eC5zdHlsaXplKCdudWxsJywgJ251bGwnKTtcbn1cblxuXG5mdW5jdGlvbiBmb3JtYXRFcnJvcih2YWx1ZSkge1xuICByZXR1cm4gJ1snICsgRXJyb3IucHJvdG90eXBlLnRvU3RyaW5nLmNhbGwodmFsdWUpICsgJ10nO1xufVxuXG5cbmZ1bmN0aW9uIGZvcm1hdEFycmF5KGN0eCwgdmFsdWUsIHJlY3Vyc2VUaW1lcywgdmlzaWJsZUtleXMsIGtleXMpIHtcbiAgdmFyIG91dHB1dCA9IFtdO1xuICBmb3IgKHZhciBpID0gMCwgbCA9IHZhbHVlLmxlbmd0aDsgaSA8IGw7ICsraSkge1xuICAgIGlmIChoYXNPd25Qcm9wZXJ0eSh2YWx1ZSwgU3RyaW5nKGkpKSkge1xuICAgICAgb3V0cHV0LnB1c2goZm9ybWF0UHJvcGVydHkoY3R4LCB2YWx1ZSwgcmVjdXJzZVRpbWVzLCB2aXNpYmxlS2V5cyxcbiAgICAgICAgICBTdHJpbmcoaSksIHRydWUpKTtcbiAgICB9IGVsc2Uge1xuICAgICAgb3V0cHV0LnB1c2goJycpO1xuICAgIH1cbiAgfVxuICBrZXlzLmZvckVhY2goZnVuY3Rpb24oa2V5KSB7XG4gICAgaWYgKCFrZXkubWF0Y2goL15cXGQrJC8pKSB7XG4gICAgICBvdXRwdXQucHVzaChmb3JtYXRQcm9wZXJ0eShjdHgsIHZhbHVlLCByZWN1cnNlVGltZXMsIHZpc2libGVLZXlzLFxuICAgICAgICAgIGtleSwgdHJ1ZSkpO1xuICAgIH1cbiAgfSk7XG4gIHJldHVybiBvdXRwdXQ7XG59XG5cblxuZnVuY3Rpb24gZm9ybWF0UHJvcGVydHkoY3R4LCB2YWx1ZSwgcmVjdXJzZVRpbWVzLCB2aXNpYmxlS2V5cywga2V5LCBhcnJheSkge1xuICB2YXIgbmFtZSwgc3RyLCBkZXNjO1xuICBkZXNjID0gT2JqZWN0LmdldE93blByb3BlcnR5RGVzY3JpcHRvcih2YWx1ZSwga2V5KSB8fCB7IHZhbHVlOiB2YWx1ZVtrZXldIH07XG4gIGlmIChkZXNjLmdldCkge1xuICAgIGlmIChkZXNjLnNldCkge1xuICAgICAgc3RyID0gY3R4LnN0eWxpemUoJ1tHZXR0ZXIvU2V0dGVyXScsICdzcGVjaWFsJyk7XG4gICAgfSBlbHNlIHtcbiAgICAgIHN0ciA9IGN0eC5zdHlsaXplKCdbR2V0dGVyXScsICdzcGVjaWFsJyk7XG4gICAgfVxuICB9IGVsc2Uge1xuICAgIGlmIChkZXNjLnNldCkge1xuICAgICAgc3RyID0gY3R4LnN0eWxpemUoJ1tTZXR0ZXJdJywgJ3NwZWNpYWwnKTtcbiAgICB9XG4gIH1cbiAgaWYgKCFoYXNPd25Qcm9wZXJ0eSh2aXNpYmxlS2V5cywga2V5KSkge1xuICAgIG5hbWUgPSAnWycgKyBrZXkgKyAnXSc7XG4gIH1cbiAgaWYgKCFzdHIpIHtcbiAgICBpZiAoY3R4LnNlZW4uaW5kZXhPZihkZXNjLnZhbHVlKSA8IDApIHtcbiAgICAgIGlmIChpc051bGwocmVjdXJzZVRpbWVzKSkge1xuICAgICAgICBzdHIgPSBmb3JtYXRWYWx1ZShjdHgsIGRlc2MudmFsdWUsIG51bGwpO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgc3RyID0gZm9ybWF0VmFsdWUoY3R4LCBkZXNjLnZhbHVlLCByZWN1cnNlVGltZXMgLSAxKTtcbiAgICAgIH1cbiAgICAgIGlmIChzdHIuaW5kZXhPZignXFxuJykgPiAtMSkge1xuICAgICAgICBpZiAoYXJyYXkpIHtcbiAgICAgICAgICBzdHIgPSBzdHIuc3BsaXQoJ1xcbicpLm1hcChmdW5jdGlvbihsaW5lKSB7XG4gICAgICAgICAgICByZXR1cm4gJyAgJyArIGxpbmU7XG4gICAgICAgICAgfSkuam9pbignXFxuJykuc3Vic3RyKDIpO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIHN0ciA9ICdcXG4nICsgc3RyLnNwbGl0KCdcXG4nKS5tYXAoZnVuY3Rpb24obGluZSkge1xuICAgICAgICAgICAgcmV0dXJuICcgICAnICsgbGluZTtcbiAgICAgICAgICB9KS5qb2luKCdcXG4nKTtcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH0gZWxzZSB7XG4gICAgICBzdHIgPSBjdHguc3R5bGl6ZSgnW0NpcmN1bGFyXScsICdzcGVjaWFsJyk7XG4gICAgfVxuICB9XG4gIGlmIChpc1VuZGVmaW5lZChuYW1lKSkge1xuICAgIGlmIChhcnJheSAmJiBrZXkubWF0Y2goL15cXGQrJC8pKSB7XG4gICAgICByZXR1cm4gc3RyO1xuICAgIH1cbiAgICBuYW1lID0gSlNPTi5zdHJpbmdpZnkoJycgKyBrZXkpO1xuICAgIGlmIChuYW1lLm1hdGNoKC9eXCIoW2EtekEtWl9dW2EtekEtWl8wLTldKilcIiQvKSkge1xuICAgICAgbmFtZSA9IG5hbWUuc3Vic3RyKDEsIG5hbWUubGVuZ3RoIC0gMik7XG4gICAgICBuYW1lID0gY3R4LnN0eWxpemUobmFtZSwgJ25hbWUnKTtcbiAgICB9IGVsc2Uge1xuICAgICAgbmFtZSA9IG5hbWUucmVwbGFjZSgvJy9nLCBcIlxcXFwnXCIpXG4gICAgICAgICAgICAgICAgIC5yZXBsYWNlKC9cXFxcXCIvZywgJ1wiJylcbiAgICAgICAgICAgICAgICAgLnJlcGxhY2UoLyheXCJ8XCIkKS9nLCBcIidcIik7XG4gICAgICBuYW1lID0gY3R4LnN0eWxpemUobmFtZSwgJ3N0cmluZycpO1xuICAgIH1cbiAgfVxuXG4gIHJldHVybiBuYW1lICsgJzogJyArIHN0cjtcbn1cblxuXG5mdW5jdGlvbiByZWR1Y2VUb1NpbmdsZVN0cmluZyhvdXRwdXQsIGJhc2UsIGJyYWNlcykge1xuICB2YXIgbnVtTGluZXNFc3QgPSAwO1xuICB2YXIgbGVuZ3RoID0gb3V0cHV0LnJlZHVjZShmdW5jdGlvbihwcmV2LCBjdXIpIHtcbiAgICBudW1MaW5lc0VzdCsrO1xuICAgIGlmIChjdXIuaW5kZXhPZignXFxuJykgPj0gMCkgbnVtTGluZXNFc3QrKztcbiAgICByZXR1cm4gcHJldiArIGN1ci5yZXBsYWNlKC9cXHUwMDFiXFxbXFxkXFxkP20vZywgJycpLmxlbmd0aCArIDE7XG4gIH0sIDApO1xuXG4gIGlmIChsZW5ndGggPiA2MCkge1xuICAgIHJldHVybiBicmFjZXNbMF0gK1xuICAgICAgICAgICAoYmFzZSA9PT0gJycgPyAnJyA6IGJhc2UgKyAnXFxuICcpICtcbiAgICAgICAgICAgJyAnICtcbiAgICAgICAgICAgb3V0cHV0LmpvaW4oJyxcXG4gICcpICtcbiAgICAgICAgICAgJyAnICtcbiAgICAgICAgICAgYnJhY2VzWzFdO1xuICB9XG5cbiAgcmV0dXJuIGJyYWNlc1swXSArIGJhc2UgKyAnICcgKyBvdXRwdXQuam9pbignLCAnKSArICcgJyArIGJyYWNlc1sxXTtcbn1cblxuXG4vLyBOT1RFOiBUaGVzZSB0eXBlIGNoZWNraW5nIGZ1bmN0aW9ucyBpbnRlbnRpb25hbGx5IGRvbid0IHVzZSBgaW5zdGFuY2VvZmBcbi8vIGJlY2F1c2UgaXQgaXMgZnJhZ2lsZSBhbmQgY2FuIGJlIGVhc2lseSBmYWtlZCB3aXRoIGBPYmplY3QuY3JlYXRlKClgLlxuZnVuY3Rpb24gaXNBcnJheShhcikge1xuICByZXR1cm4gQXJyYXkuaXNBcnJheShhcik7XG59XG5leHBvcnRzLmlzQXJyYXkgPSBpc0FycmF5O1xuXG5mdW5jdGlvbiBpc0Jvb2xlYW4oYXJnKSB7XG4gIHJldHVybiB0eXBlb2YgYXJnID09PSAnYm9vbGVhbic7XG59XG5leHBvcnRzLmlzQm9vbGVhbiA9IGlzQm9vbGVhbjtcblxuZnVuY3Rpb24gaXNOdWxsKGFyZykge1xuICByZXR1cm4gYXJnID09PSBudWxsO1xufVxuZXhwb3J0cy5pc051bGwgPSBpc051bGw7XG5cbmZ1bmN0aW9uIGlzTnVsbE9yVW5kZWZpbmVkKGFyZykge1xuICByZXR1cm4gYXJnID09IG51bGw7XG59XG5leHBvcnRzLmlzTnVsbE9yVW5kZWZpbmVkID0gaXNOdWxsT3JVbmRlZmluZWQ7XG5cbmZ1bmN0aW9uIGlzTnVtYmVyKGFyZykge1xuICByZXR1cm4gdHlwZW9mIGFyZyA9PT0gJ251bWJlcic7XG59XG5leHBvcnRzLmlzTnVtYmVyID0gaXNOdW1iZXI7XG5cbmZ1bmN0aW9uIGlzU3RyaW5nKGFyZykge1xuICByZXR1cm4gdHlwZW9mIGFyZyA9PT0gJ3N0cmluZyc7XG59XG5leHBvcnRzLmlzU3RyaW5nID0gaXNTdHJpbmc7XG5cbmZ1bmN0aW9uIGlzU3ltYm9sKGFyZykge1xuICByZXR1cm4gdHlwZW9mIGFyZyA9PT0gJ3N5bWJvbCc7XG59XG5leHBvcnRzLmlzU3ltYm9sID0gaXNTeW1ib2w7XG5cbmZ1bmN0aW9uIGlzVW5kZWZpbmVkKGFyZykge1xuICByZXR1cm4gYXJnID09PSB2b2lkIDA7XG59XG5leHBvcnRzLmlzVW5kZWZpbmVkID0gaXNVbmRlZmluZWQ7XG5cbmZ1bmN0aW9uIGlzUmVnRXhwKHJlKSB7XG4gIHJldHVybiBpc09iamVjdChyZSkgJiYgb2JqZWN0VG9TdHJpbmcocmUpID09PSAnW29iamVjdCBSZWdFeHBdJztcbn1cbmV4cG9ydHMuaXNSZWdFeHAgPSBpc1JlZ0V4cDtcblxuZnVuY3Rpb24gaXNPYmplY3QoYXJnKSB7XG4gIHJldHVybiB0eXBlb2YgYXJnID09PSAnb2JqZWN0JyAmJiBhcmcgIT09IG51bGw7XG59XG5leHBvcnRzLmlzT2JqZWN0ID0gaXNPYmplY3Q7XG5cbmZ1bmN0aW9uIGlzRGF0ZShkKSB7XG4gIHJldHVybiBpc09iamVjdChkKSAmJiBvYmplY3RUb1N0cmluZyhkKSA9PT0gJ1tvYmplY3QgRGF0ZV0nO1xufVxuZXhwb3J0cy5pc0RhdGUgPSBpc0RhdGU7XG5cbmZ1bmN0aW9uIGlzRXJyb3IoZSkge1xuICByZXR1cm4gaXNPYmplY3QoZSkgJiZcbiAgICAgIChvYmplY3RUb1N0cmluZyhlKSA9PT0gJ1tvYmplY3QgRXJyb3JdJyB8fCBlIGluc3RhbmNlb2YgRXJyb3IpO1xufVxuZXhwb3J0cy5pc0Vycm9yID0gaXNFcnJvcjtcblxuZnVuY3Rpb24gaXNGdW5jdGlvbihhcmcpIHtcbiAgcmV0dXJuIHR5cGVvZiBhcmcgPT09ICdmdW5jdGlvbic7XG59XG5leHBvcnRzLmlzRnVuY3Rpb24gPSBpc0Z1bmN0aW9uO1xuXG5mdW5jdGlvbiBpc1ByaW1pdGl2ZShhcmcpIHtcbiAgcmV0dXJuIGFyZyA9PT0gbnVsbCB8fFxuICAgICAgICAgdHlwZW9mIGFyZyA9PT0gJ2Jvb2xlYW4nIHx8XG4gICAgICAgICB0eXBlb2YgYXJnID09PSAnbnVtYmVyJyB8fFxuICAgICAgICAgdHlwZW9mIGFyZyA9PT0gJ3N0cmluZycgfHxcbiAgICAgICAgIHR5cGVvZiBhcmcgPT09ICdzeW1ib2wnIHx8ICAvLyBFUzYgc3ltYm9sXG4gICAgICAgICB0eXBlb2YgYXJnID09PSAndW5kZWZpbmVkJztcbn1cbmV4cG9ydHMuaXNQcmltaXRpdmUgPSBpc1ByaW1pdGl2ZTtcblxuZXhwb3J0cy5pc0J1ZmZlciA9IHJlcXVpcmUoJy4vc3VwcG9ydC9pc0J1ZmZlcicpO1xuXG5mdW5jdGlvbiBvYmplY3RUb1N0cmluZyhvKSB7XG4gIHJldHVybiBPYmplY3QucHJvdG90eXBlLnRvU3RyaW5nLmNhbGwobyk7XG59XG5cblxuZnVuY3Rpb24gcGFkKG4pIHtcbiAgcmV0dXJuIG4gPCAxMCA/ICcwJyArIG4udG9TdHJpbmcoMTApIDogbi50b1N0cmluZygxMCk7XG59XG5cblxudmFyIG1vbnRocyA9IFsnSmFuJywgJ0ZlYicsICdNYXInLCAnQXByJywgJ01heScsICdKdW4nLCAnSnVsJywgJ0F1ZycsICdTZXAnLFxuICAgICAgICAgICAgICAnT2N0JywgJ05vdicsICdEZWMnXTtcblxuLy8gMjYgRmViIDE2OjE5OjM0XG5mdW5jdGlvbiB0aW1lc3RhbXAoKSB7XG4gIHZhciBkID0gbmV3IERhdGUoKTtcbiAgdmFyIHRpbWUgPSBbcGFkKGQuZ2V0SG91cnMoKSksXG4gICAgICAgICAgICAgIHBhZChkLmdldE1pbnV0ZXMoKSksXG4gICAgICAgICAgICAgIHBhZChkLmdldFNlY29uZHMoKSldLmpvaW4oJzonKTtcbiAgcmV0dXJuIFtkLmdldERhdGUoKSwgbW9udGhzW2QuZ2V0TW9udGgoKV0sIHRpbWVdLmpvaW4oJyAnKTtcbn1cblxuXG4vLyBsb2cgaXMganVzdCBhIHRoaW4gd3JhcHBlciB0byBjb25zb2xlLmxvZyB0aGF0IHByZXBlbmRzIGEgdGltZXN0YW1wXG5leHBvcnRzLmxvZyA9IGZ1bmN0aW9uKCkge1xuICBjb25zb2xlLmxvZygnJXMgLSAlcycsIHRpbWVzdGFtcCgpLCBleHBvcnRzLmZvcm1hdC5hcHBseShleHBvcnRzLCBhcmd1bWVudHMpKTtcbn07XG5cblxuLyoqXG4gKiBJbmhlcml0IHRoZSBwcm90b3R5cGUgbWV0aG9kcyBmcm9tIG9uZSBjb25zdHJ1Y3RvciBpbnRvIGFub3RoZXIuXG4gKlxuICogVGhlIEZ1bmN0aW9uLnByb3RvdHlwZS5pbmhlcml0cyBmcm9tIGxhbmcuanMgcmV3cml0dGVuIGFzIGEgc3RhbmRhbG9uZVxuICogZnVuY3Rpb24gKG5vdCBvbiBGdW5jdGlvbi5wcm90b3R5cGUpLiBOT1RFOiBJZiB0aGlzIGZpbGUgaXMgdG8gYmUgbG9hZGVkXG4gKiBkdXJpbmcgYm9vdHN0cmFwcGluZyB0aGlzIGZ1bmN0aW9uIG5lZWRzIHRvIGJlIHJld3JpdHRlbiB1c2luZyBzb21lIG5hdGl2ZVxuICogZnVuY3Rpb25zIGFzIHByb3RvdHlwZSBzZXR1cCB1c2luZyBub3JtYWwgSmF2YVNjcmlwdCBkb2VzIG5vdCB3b3JrIGFzXG4gKiBleHBlY3RlZCBkdXJpbmcgYm9vdHN0cmFwcGluZyAoc2VlIG1pcnJvci5qcyBpbiByMTE0OTAzKS5cbiAqXG4gKiBAcGFyYW0ge2Z1bmN0aW9ufSBjdG9yIENvbnN0cnVjdG9yIGZ1bmN0aW9uIHdoaWNoIG5lZWRzIHRvIGluaGVyaXQgdGhlXG4gKiAgICAgcHJvdG90eXBlLlxuICogQHBhcmFtIHtmdW5jdGlvbn0gc3VwZXJDdG9yIENvbnN0cnVjdG9yIGZ1bmN0aW9uIHRvIGluaGVyaXQgcHJvdG90eXBlIGZyb20uXG4gKi9cbmV4cG9ydHMuaW5oZXJpdHMgPSByZXF1aXJlKCdpbmhlcml0cycpO1xuXG5leHBvcnRzLl9leHRlbmQgPSBmdW5jdGlvbihvcmlnaW4sIGFkZCkge1xuICAvLyBEb24ndCBkbyBhbnl0aGluZyBpZiBhZGQgaXNuJ3QgYW4gb2JqZWN0XG4gIGlmICghYWRkIHx8ICFpc09iamVjdChhZGQpKSByZXR1cm4gb3JpZ2luO1xuXG4gIHZhciBrZXlzID0gT2JqZWN0LmtleXMoYWRkKTtcbiAgdmFyIGkgPSBrZXlzLmxlbmd0aDtcbiAgd2hpbGUgKGktLSkge1xuICAgIG9yaWdpbltrZXlzW2ldXSA9IGFkZFtrZXlzW2ldXTtcbiAgfVxuICByZXR1cm4gb3JpZ2luO1xufTtcblxuZnVuY3Rpb24gaGFzT3duUHJvcGVydHkob2JqLCBwcm9wKSB7XG4gIHJldHVybiBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwob2JqLCBwcm9wKTtcbn1cblxufSkuY2FsbCh0aGlzLHJlcXVpcmUoJ19wcm9jZXNzJyksdHlwZW9mIGdsb2JhbCAhPT0gXCJ1bmRlZmluZWRcIiA/IGdsb2JhbCA6IHR5cGVvZiBzZWxmICE9PSBcInVuZGVmaW5lZFwiID8gc2VsZiA6IHR5cGVvZiB3aW5kb3cgIT09IFwidW5kZWZpbmVkXCIgPyB3aW5kb3cgOiB7fSkiLCIvLyBHZW5lcmF0ZWQgYnkgQ29mZmVlU2NyaXB0IDEuNi4zXG4oZnVuY3Rpb24oKSB7XG4gIHZhciBDb2xvciwgU2tldGNoLCBWZWMyO1xuXG4gIENvbG9yID0gcmVxdWlyZSgnLi4vY29sb3InKS5Db2xvcjtcblxuICBWZWMyID0gcmVxdWlyZSgnLi4vbWF0aC92ZWN0b3InKS5WZWMyO1xuXG4gIC8qXG4gIFxuICAgIEZJRUxES0lUIFNLRVRDSCBDTEFTU1xuICAqL1xuXG5cbiAgU2tldGNoID0gKGZ1bmN0aW9uKCkge1xuICAgIHZhciBUV09fUEksIGNvbXB1dGVTdHlsZSwgaXNGaWxsRW5hYmxlZCwgaXNTdHJva2VFbmFibGVkO1xuXG4gICAgU2tldGNoLnByb3RvdHlwZS53aWR0aCA9IC0xO1xuXG4gICAgU2tldGNoLnByb3RvdHlwZS5oZWlnaHQgPSAtMTtcblxuICAgIFNrZXRjaC5wcm90b3R5cGUuY2FudmFzSWQgPSBcInNrZXRjaFwiO1xuXG4gICAgU2tldGNoLnByb3RvdHlwZS5kb21PYmplY3RJZCA9IFwiY29udGFpbmVyXCI7XG5cbiAgICBTa2V0Y2gucHJvdG90eXBlLmcgPSBudWxsO1xuXG4gICAgU2tldGNoLnByb3RvdHlwZS5tb3VzZVggPSAwO1xuXG4gICAgU2tldGNoLnByb3RvdHlwZS5tb3VzZVkgPSAwO1xuXG4gICAgZnVuY3Rpb24gU2tldGNoKG9wdGlvbnMpIHtcbiAgICAgIHZhciBjYW52YXMsIGRvbU9iamVjdCxcbiAgICAgICAgX3RoaXMgPSB0aGlzO1xuICAgICAgZG9tT2JqZWN0ID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQodGhpcy5kb21PYmplY3RJZCk7XG4gICAgICBpZiAodGhpcy53aWR0aCA9PT0gLTEpIHtcbiAgICAgICAgdGhpcy53aWR0aCA9IGRvbU9iamVjdC5vZmZzZXRXaWR0aDtcbiAgICAgIH1cbiAgICAgIGlmICh0aGlzLmhlaWdodCA9PT0gLTEpIHtcbiAgICAgICAgdGhpcy5oZWlnaHQgPSBkb21PYmplY3Qub2Zmc2V0SGVpZ2h0O1xuICAgICAgfVxuICAgICAgY2FudmFzID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudChcImNhbnZhc1wiKTtcbiAgICAgIGNhbnZhcy5pZCA9IHRoaXMuY2FudmFzSWQ7XG4gICAgICBjYW52YXMud2lkdGggPSB0aGlzLndpZHRoO1xuICAgICAgY2FudmFzLmhlaWdodCA9IHRoaXMuaGVpZ2h0O1xuICAgICAgZG9tT2JqZWN0LmFwcGVuZENoaWxkKGNhbnZhcyk7XG4gICAgICB0aGlzLmcgPSBjYW52YXMuZ2V0Q29udGV4dChcIjJkXCIpO1xuICAgICAgdGhpcy5zZXR1cCgpO1xuICAgICAgdGhpcy5zdGFydCgpO1xuICAgICAgZG9tT2JqZWN0Lm9ubW91c2Vtb3ZlID0gZnVuY3Rpb24oZSkge1xuICAgICAgICBfdGhpcy5tb3VzZVggPSBlLng7XG4gICAgICAgIHJldHVybiBfdGhpcy5tb3VzZVkgPSBlLnk7XG4gICAgICB9O1xuICAgICAgZG9tT2JqZWN0Lm9ubW91c2Vkb3duID0gZnVuY3Rpb24oZSkge1xuICAgICAgICByZXR1cm4gX3RoaXMubW91c2VEb3duKCk7XG4gICAgICB9O1xuICAgICAgZG9tT2JqZWN0Lm9ubW91c2V1cCA9IGZ1bmN0aW9uKGUpIHtcbiAgICAgICAgcmV0dXJuIF90aGlzLm1vdXNlVXAoKTtcbiAgICAgIH07XG4gICAgfVxuXG4gICAgU2tldGNoLnByb3RvdHlwZS5pc1J1bm5pbmcgPSBmYWxzZTtcblxuICAgIFNrZXRjaC5wcm90b3R5cGUuaXNGaW5pc2hlZENhbGxiYWNrID0gbnVsbDtcblxuICAgIFNrZXRjaC5wcm90b3R5cGUuc3RhcnQgPSBmdW5jdGlvbigpIHtcbiAgICAgIHZhciByZW5kZXIsIHJlcXVlc3RJZCxcbiAgICAgICAgX3RoaXMgPSB0aGlzO1xuICAgICAgdGhpcy5pc1J1bm5pbmcgPSB0cnVlO1xuICAgICAgcmVxdWVzdElkID0gbnVsbDtcbiAgICAgIHJlbmRlciA9IGZ1bmN0aW9uKCkge1xuICAgICAgICBfdGhpcy5kcmF3KCk7XG4gICAgICAgIGlmIChfdGhpcy5pc1J1bm5pbmcpIHtcbiAgICAgICAgICByZXR1cm4gcmVxdWVzdElkID0gd2luZG93LnJlcXVlc3RBbmltYXRpb25GcmFtZShyZW5kZXIpO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIHdpbmRvdy5jYW5jZWxBbmltYXRpb25GcmFtZShyZXF1ZXN0SWQpO1xuICAgICAgICAgIGlmIChfdGhpcy5pc0ZpbmlzaGVkQ2FsbGJhY2sgIT0gbnVsbCkge1xuICAgICAgICAgICAgcmV0dXJuIF90aGlzLmlzRmluaXNoZWRDYWxsYmFjaygpO1xuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgfTtcbiAgICAgIHJldHVybiByZXF1ZXN0SWQgPSB3aW5kb3cucmVxdWVzdEFuaW1hdGlvbkZyYW1lKHJlbmRlcik7XG4gICAgfTtcblxuICAgIFNrZXRjaC5wcm90b3R5cGUuc3RvcCA9IGZ1bmN0aW9uKGNhbGxiYWNrKSB7XG4gICAgICB0aGlzLmlzUnVubmluZyA9IGZhbHNlO1xuICAgICAgcmV0dXJuIHRoaXMuaXNGaW5pc2hlZENhbGxiYWNrID0gY2FsbGJhY2s7XG4gICAgfTtcblxuICAgIFNrZXRjaC5wcm90b3R5cGUudG9TdHJpbmcgPSBmdW5jdGlvbigpIHtcbiAgICAgIHJldHVybiBcIlNrZXRjaFwiO1xuICAgIH07XG5cbiAgICAvKlxuICAgICAgU2tldGNoIEFQSVxuICAgICovXG5cblxuICAgIFNrZXRjaC5wcm90b3R5cGUuc2V0dXAgPSBmdW5jdGlvbigpIHt9O1xuXG4gICAgU2tldGNoLnByb3RvdHlwZS5kcmF3ID0gZnVuY3Rpb24oKSB7fTtcblxuICAgIFNrZXRjaC5wcm90b3R5cGUubW91c2VEb3duID0gZnVuY3Rpb24oKSB7fTtcblxuICAgIFNrZXRjaC5wcm90b3R5cGUubW91c2VVcCA9IGZ1bmN0aW9uKCkge307XG5cbiAgICAvKlxuICAgICAgMkQgRHJhd2luZyBBUElcbiAgICAqL1xuXG5cbiAgICBpc0ZpbGxFbmFibGVkID0gdHJ1ZTtcblxuICAgIGlzU3Ryb2tlRW5hYmxlZCA9IGZhbHNlO1xuXG4gICAgVFdPX1BJID0gTWF0aC5QSSAqIDI7XG5cbiAgICBjb21wdXRlU3R5bGUgPSBmdW5jdGlvbihhcmdzKSB7XG4gICAgICB2YXIgYXJnLCBncmV5O1xuICAgICAgc3dpdGNoIChhcmdzLmxlbmd0aCkge1xuICAgICAgICBjYXNlIDE6XG4gICAgICAgICAgYXJnID0gYXJnc1swXTtcbiAgICAgICAgICBpZiAodHlwZW9mIGFyZyA9PT0gXCJzdHJpbmdcIikge1xuICAgICAgICAgICAgcmV0dXJuIGFyZztcbiAgICAgICAgICB9IGVsc2UgaWYgKGFyZyBpbnN0YW5jZW9mIENvbG9yKSB7XG4gICAgICAgICAgICByZXR1cm4gYXJnLnRvQ1NTKCk7XG4gICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIHJldHVybiBcInJnYmEoXCIgKyBhcmcgKyBcIiwgXCIgKyBhcmcgKyBcIiwgXCIgKyBhcmcgKyBcIiwgMSlcIjtcbiAgICAgICAgICB9XG4gICAgICAgICAgYnJlYWs7XG4gICAgICAgIGNhc2UgMjpcbiAgICAgICAgICBncmV5ID0gYXJnc1swXTtcbiAgICAgICAgICByZXR1cm4gXCJyZ2JhKFwiICsgZ3JleSArIFwiLCBcIiArIGdyZXkgKyBcIiwgXCIgKyBncmV5ICsgXCIsIFwiICsgYXJnc1sxXSArIFwiKVwiO1xuICAgICAgICBjYXNlIDM6XG4gICAgICAgICAgcmV0dXJuIFwicmdiYShcIiArIGFyZ3NbMF0gKyBcIiwgXCIgKyBhcmdzWzFdICsgXCIsIFwiICsgYXJnc1syXSArIFwiLCAxKVwiO1xuICAgICAgICBjYXNlIDQ6XG4gICAgICAgICAgcmV0dXJuIFwicmdiYShcIiArIGFyZ3NbMF0gKyBcIiwgXCIgKyBhcmdzWzFdICsgXCIsIFwiICsgYXJnc1syXSArIFwiLCBcIiArIGFyZ3NbM10gKyBcIilcIjtcbiAgICAgICAgZGVmYXVsdDpcbiAgICAgICAgICByZXR1cm4gXCIjRkYwMDAwXCI7XG4gICAgICB9XG4gICAgfTtcblxuICAgIFNrZXRjaC5wcm90b3R5cGUuY29sb3IgPSBmdW5jdGlvbigpIHtcbiAgICAgIHJldHVybiBjb21wdXRlU3R5bGUoYXJndW1lbnRzKTtcbiAgICB9O1xuXG4gICAgU2tldGNoLnByb3RvdHlwZS5iYWNrZ3JvdW5kID0gZnVuY3Rpb24oKSB7XG4gICAgICB2YXIgc3R5bGU7XG4gICAgICB0aGlzLmcuY2xlYXJSZWN0KDAsIDAsIHRoaXMud2lkdGgsIHRoaXMuaGVpZ2h0KTtcbiAgICAgIHN0eWxlID0gdGhpcy5nLmZpbGxTdHlsZTtcbiAgICAgIHRoaXMuZy5maWxsU3R5bGUgPSBjb21wdXRlU3R5bGUoYXJndW1lbnRzKTtcbiAgICAgIHRoaXMuZy5maWxsUmVjdCgwLCAwLCB0aGlzLndpZHRoLCB0aGlzLmhlaWdodCk7XG4gICAgICByZXR1cm4gdGhpcy5nLmZpbGxTdHlsZSA9IHN0eWxlO1xuICAgIH07XG5cbiAgICBTa2V0Y2gucHJvdG90eXBlLmZpbGwgPSBmdW5jdGlvbigpIHtcbiAgICAgIGlzRmlsbEVuYWJsZWQgPSB0cnVlO1xuICAgICAgcmV0dXJuIHRoaXMuZy5maWxsU3R5bGUgPSBjb21wdXRlU3R5bGUoYXJndW1lbnRzKTtcbiAgICB9O1xuXG4gICAgU2tldGNoLnByb3RvdHlwZS5zdHJva2UgPSBmdW5jdGlvbigpIHtcbiAgICAgIGlzU3Ryb2tlRW5hYmxlZCA9IHRydWU7XG4gICAgICByZXR1cm4gdGhpcy5nLnN0cm9rZVN0eWxlID0gY29tcHV0ZVN0eWxlKGFyZ3VtZW50cyk7XG4gICAgfTtcblxuICAgIFNrZXRjaC5wcm90b3R5cGUubGluZVdpZHRoID0gZnVuY3Rpb24od2lkdGgpIHtcbiAgICAgIHJldHVybiB0aGlzLmcubGluZVdpZHRoID0gd2lkdGg7XG4gICAgfTtcblxuICAgIFNrZXRjaC5wcm90b3R5cGUubm9GaWxsID0gZnVuY3Rpb24oKSB7XG4gICAgICByZXR1cm4gaXNGaWxsRW5hYmxlZCA9IGZhbHNlO1xuICAgIH07XG5cbiAgICBTa2V0Y2gucHJvdG90eXBlLm5vU3Ryb2tlID0gZnVuY3Rpb24oKSB7XG4gICAgICByZXR1cm4gaXNTdHJva2VFbmFibGVkID0gZmFsc2U7XG4gICAgfTtcblxuICAgIFNrZXRjaC5wcm90b3R5cGUucmVjdCA9IGZ1bmN0aW9uKHgsIHksIHcsIGgpIHtcbiAgICAgIGlmIChpc0ZpbGxFbmFibGVkKSB7XG4gICAgICAgIHRoaXMuZy5maWxsUmVjdCh4LCB5LCB3LCBoKTtcbiAgICAgIH1cbiAgICAgIGlmIChpc1N0cm9rZUVuYWJsZWQpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuZy5zdHJva2VSZWN0KHgsIHksIHcsIGgpO1xuICAgICAgfVxuICAgIH07XG5cbiAgICBTa2V0Y2gucHJvdG90eXBlLmNpcmNsZSA9IGZ1bmN0aW9uKHgsIHksIHIpIHtcbiAgICAgIHRoaXMuZy5iZWdpblBhdGgoKTtcbiAgICAgIHRoaXMuZy5hcmMoeCwgeSwgciwgMCwgVFdPX1BJLCBmYWxzZSk7XG4gICAgICBpZiAoaXNGaWxsRW5hYmxlZCkge1xuICAgICAgICB0aGlzLmcuZmlsbCgpO1xuICAgICAgfVxuICAgICAgaWYgKGlzU3Ryb2tlRW5hYmxlZCkge1xuICAgICAgICB0aGlzLmcuc3Ryb2tlKCk7XG4gICAgICB9XG4gICAgICByZXR1cm4gdGhpcy5nLmNsb3NlUGF0aCgpO1xuICAgIH07XG5cbiAgICBTa2V0Y2gucHJvdG90eXBlLmxpbmUgPSBmdW5jdGlvbih4MSwgeTEsIHgyLCB5Mikge1xuICAgICAgdGhpcy5nLmJlZ2luUGF0aCgpO1xuICAgICAgaWYgKHgxIGluc3RhbmNlb2YgVmVjMikge1xuICAgICAgICB0aGlzLmcubW92ZVRvKHgxLngsIHgxLnkpO1xuICAgICAgICB0aGlzLmcubGluZVRvKHkxLngsIHkxLnkpO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgdGhpcy5nLm1vdmVUbyh4MSwgeTEpO1xuICAgICAgICB0aGlzLmcubGluZVRvKHgyLCB5Mik7XG4gICAgICB9XG4gICAgICB0aGlzLmcuc3Ryb2tlKCk7XG4gICAgICByZXR1cm4gdGhpcy5nLmNsb3NlUGF0aCgpO1xuICAgIH07XG5cbiAgICBTa2V0Y2gucHJvdG90eXBlLnBvbHlnb24gPSBmdW5jdGlvbihwb2ludHMpIHtcbiAgICAgIHZhciBwLCBfaSwgX2xlbiwgX3JlZjtcbiAgICAgIHRoaXMuZy5iZWdpblBhdGgoKTtcbiAgICAgIHRoaXMuZy5tb3ZlVG8ocG9pbnRzWzBdLngsIHBvaW50c1swXS55KTtcbiAgICAgIF9yZWYgPSBwb2ludHMuc2xpY2UoMSk7XG4gICAgICBmb3IgKF9pID0gMCwgX2xlbiA9IF9yZWYubGVuZ3RoOyBfaSA8IF9sZW47IF9pKyspIHtcbiAgICAgICAgcCA9IF9yZWZbX2ldO1xuICAgICAgICB0aGlzLmcubGluZVRvKHAueCwgcC55KTtcbiAgICAgIH1cbiAgICAgIGlmIChpc0ZpbGxFbmFibGVkKSB7XG4gICAgICAgIHRoaXMuZy5maWxsKCk7XG4gICAgICB9XG4gICAgICBpZiAoaXNTdHJva2VFbmFibGVkKSB7XG4gICAgICAgIHRoaXMuZy5zdHJva2UoKTtcbiAgICAgIH1cbiAgICAgIHJldHVybiB0aGlzLmcuY2xvc2VQYXRoKCk7XG4gICAgfTtcblxuICAgIFNrZXRjaC5wcm90b3R5cGUuY3JlYXRlSW1hZ2UgPSBmdW5jdGlvbih3aWR0aCwgaGVpZ2h0KSB7XG4gICAgICB2YXIgaW1hZ2VEYXRhO1xuICAgICAgaW1hZ2VEYXRhID0gdGhpcy5nLmNyZWF0ZUltYWdlRGF0YSh3aWR0aCwgaGVpZ2h0KTtcbiAgICAgIGltYWdlRGF0YS5zZXRQaXhlbCA9IGZ1bmN0aW9uKHgsIHksIHIsIGcsIGIsIGEpIHtcbiAgICAgICAgdmFyIGluZGV4O1xuICAgICAgICBpZiAoYSA9PSBudWxsKSB7XG4gICAgICAgICAgYSA9IDI1NTtcbiAgICAgICAgfVxuICAgICAgICBpbmRleCA9ICh4ICsgeSAqIHRoaXMud2lkdGgpICogNDtcbiAgICAgICAgdGhpcy5kYXRhW2luZGV4ICsgMF0gPSByO1xuICAgICAgICB0aGlzLmRhdGFbaW5kZXggKyAxXSA9IGc7XG4gICAgICAgIHRoaXMuZGF0YVtpbmRleCArIDJdID0gYjtcbiAgICAgICAgcmV0dXJuIHRoaXMuZGF0YVtpbmRleCArIDNdID0gYTtcbiAgICAgIH07XG4gICAgICByZXR1cm4gaW1hZ2VEYXRhO1xuICAgIH07XG5cbiAgICBTa2V0Y2gucHJvdG90eXBlLmRyYXdJbWFnZSA9IGZ1bmN0aW9uKGltYWdlRGF0YSwgeCwgeSkge1xuICAgICAgcmV0dXJuIHRoaXMuZy5wdXRJbWFnZURhdGEoaW1hZ2VEYXRhLCB4LCB5KTtcbiAgICB9O1xuXG4gICAgcmV0dXJuIFNrZXRjaDtcblxuICB9KSgpO1xuXG4gIG1vZHVsZS5leHBvcnRzID0ge1xuICAgIFNrZXRjaDogU2tldGNoXG4gIH07XG5cbn0pLmNhbGwodGhpcyk7XG4iLCIvLyBHZW5lcmF0ZWQgYnkgQ29mZmVlU2NyaXB0IDEuNi4zXG4vKlxuKi9cblxuXG4oZnVuY3Rpb24oKSB7XG4gIHZhciBDb2xvcjtcblxuICBDb2xvciA9IChmdW5jdGlvbigpIHtcbiAgICBDb2xvci5wcm90b3R5cGUuciA9IDE7XG5cbiAgICBDb2xvci5wcm90b3R5cGUuZyA9IDE7XG5cbiAgICBDb2xvci5wcm90b3R5cGUuYiA9IDE7XG5cbiAgICBDb2xvci5wcm90b3R5cGUuYSA9IDE7XG5cbiAgICBmdW5jdGlvbiBDb2xvcihyLCBnLCBiLCBhKSB7XG4gICAgICB0aGlzLnIgPSByICE9IG51bGwgPyByIDogMTtcbiAgICAgIHRoaXMuZyA9IGcgIT0gbnVsbCA/IGcgOiAxO1xuICAgICAgdGhpcy5iID0gYiAhPSBudWxsID8gYiA6IDE7XG4gICAgICB0aGlzLmEgPSBhICE9IG51bGwgPyBhIDogMTtcbiAgICB9XG5cbiAgICBDb2xvci5wcm90b3R5cGUuc2V0ID0gZnVuY3Rpb24oY29sb3IpIHtcbiAgICAgIHRoaXMuciA9IGNvbG9yLnI7XG4gICAgICB0aGlzLmcgPSBjb2xvci5nO1xuICAgICAgdGhpcy5iID0gY29sb3IuYjtcbiAgICAgIHRoaXMuYSA9IGNvbG9yLmE7XG4gICAgICByZXR1cm4gdGhpcztcbiAgICB9O1xuXG4gICAgQ29sb3IucHJvdG90eXBlLnNldDMgPSBmdW5jdGlvbihyLCBnLCBiKSB7XG4gICAgICB0aGlzLnIgPSByO1xuICAgICAgdGhpcy5nID0gZztcbiAgICAgIHRoaXMuYiA9IGI7XG4gICAgICByZXR1cm4gdGhpcztcbiAgICB9O1xuXG4gICAgQ29sb3IucHJvdG90eXBlLnJhbmRvbWl6ZSA9IGZ1bmN0aW9uKCkge1xuICAgICAgdGhpcy5yID0gTWF0aC5yYW5kb20oKTtcbiAgICAgIHRoaXMuZyA9IE1hdGgucmFuZG9tKCk7XG4gICAgICB0aGlzLmIgPSBNYXRoLnJhbmRvbSgpO1xuICAgICAgcmV0dXJuIHRoaXM7XG4gICAgfTtcblxuICAgIENvbG9yLnByb3RvdHlwZS5jbG9uZSA9IGZ1bmN0aW9uKCkge1xuICAgICAgcmV0dXJuIG5ldyBDb2xvcih0aGlzLnIsIHRoaXMuZywgdGhpcy5iLCB0aGlzLmEpO1xuICAgIH07XG5cbiAgICBDb2xvci5wcm90b3R5cGUuZXF1YWxzID0gZnVuY3Rpb24ob3RoZXIpIHtcbiAgICAgIGlmIChvdGhlciA9PSBudWxsKSB7XG4gICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgIH1cbiAgICAgIHJldHVybiB0aGlzLnIgPT09IG90aGVyLnIgJiYgdGhpcy5nID09PSBvdGhlci5nICYmIHRoaXMuYiA9PT0gb3RoZXIuYiAmJiB0aGlzLmEgPT09IG90aGVyLmE7XG4gICAgfTtcblxuICAgIENvbG9yLnByb3RvdHlwZS50b0NTUyA9IGZ1bmN0aW9uKCkge1xuICAgICAgdmFyIGIsIGcsIHI7XG4gICAgICByID0gTWF0aC5mbG9vcigyNTUgKiB0aGlzLnIpO1xuICAgICAgZyA9IE1hdGguZmxvb3IoMjU1ICogdGhpcy5nKTtcbiAgICAgIGIgPSBNYXRoLmZsb29yKDI1NSAqIHRoaXMuYik7XG4gICAgICByZXR1cm4gXCJyZ2JhKFwiICsgciArIFwiLFwiICsgZyArIFwiLFwiICsgYiArIFwiLFwiICsgdGhpcy5hICsgXCIpXCI7XG4gICAgfTtcblxuICAgIENvbG9yLnByb3RvdHlwZS50b1N0cmluZyA9IGZ1bmN0aW9uKCkge1xuICAgICAgcmV0dXJuIFwiZmsuQ29sb3IoXCIgKyB0aGlzLnIgKyBcIixcIiArIHRoaXMuZyArIFwiLFwiICsgdGhpcy5iICsgXCIsXCIgKyB0aGlzLmEgKyBcIilcIjtcbiAgICB9O1xuXG4gICAgcmV0dXJuIENvbG9yO1xuXG4gIH0pKCk7XG5cbiAgbW9kdWxlLmV4cG9ydHMgPSB7XG4gICAgQ29sb3I6IENvbG9yXG4gIH07XG5cbn0pLmNhbGwodGhpcyk7XG4iLCIvLyBHZW5lcmF0ZWQgYnkgQ29mZmVlU2NyaXB0IDEuNi4zXG4vKlxuICAgICAgX19fX18gIF9fICBfX19fXyAgX18gICAgIF9fX19cbiAgICAgLyBfX18vIC8gLyAvX19fXy8gLyAvICAgIC8gICAgXFwgICBGaWVsZEtpdFxuICAgIC8gX19fLyAvXy8gL19fX18vIC8gL19fICAvICAvICAvICAgKGMpIDIwMTMsIEZJRUxELiBBbGwgcmlnaHRzIHJlc2VydmVkLlxuICAgL18vICAgICAgICAvX19fXy8gL19fX18vIC9fX19fXy8gICAgaHR0cDovL3d3dy5maWVsZC5pb1xuXG4gICBDcmVhdGVkIGJ5IE1hcmN1cyBXZW5kdCBvbiAwNy8wMy8yMDEzXG4qL1xuXG5cbihmdW5jdGlvbigpIHtcbiAgdmFyIGV4dGVuZCwgZmssIHV0aWw7XG5cbiAgdXRpbCA9IHJlcXVpcmUoJy4vdXRpbCcpO1xuXG4gIGV4dGVuZCA9IGZ1bmN0aW9uKCkge1xuICAgIHZhciBwa2c7XG4gICAgc3dpdGNoIChhcmd1bWVudHMubGVuZ3RoKSB7XG4gICAgICBjYXNlIDE6XG4gICAgICAgIHJldHVybiB1dGlsLmV4dGVuZChmaywgYXJndW1lbnRzWzBdKTtcbiAgICAgIGNhc2UgMjpcbiAgICAgICAgcGtnID0gYXJndW1lbnRzWzBdO1xuICAgICAgICBpZiAoZmtbcGtnXSA9PSBudWxsKSB7XG4gICAgICAgICAgZmtbcGtnXSA9IHt9O1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiB1dGlsLmV4dGVuZChma1twa2ddLCBhcmd1bWVudHNbMV0pO1xuICAgIH1cbiAgfTtcblxuICBmayA9IHt9O1xuXG4gIGV4dGVuZChyZXF1aXJlKCcuL2NvbG9yJykpO1xuXG4gIGV4dGVuZChyZXF1aXJlKCcuL3RpbWUnKSk7XG5cbiAgZXh0ZW5kKCdtYXRoJywgcmVxdWlyZSgnLi9tYXRoL21hdGgnKSk7XG5cbiAgZXh0ZW5kKCdtYXRoJywgcmVxdWlyZSgnLi9tYXRoL3JhbmRvbScpKTtcblxuICBleHRlbmQoJ21hdGgnLCByZXF1aXJlKCcuL21hdGgvbm9pc2UnKSk7XG5cbiAgZXh0ZW5kKCdtYXRoJywgcmVxdWlyZSgnLi9tYXRoL3ZlY3RvcicpKTtcblxuICBleHRlbmQoJ21hdGgnLCByZXF1aXJlKCcuL21hdGgvcmVjdCcpKTtcblxuICBleHRlbmQoJ21hdGgnLCByZXF1aXJlKCcuL21hdGgvYm94JykpO1xuXG4gIGV4dGVuZCgnbWF0aCcsIHJlcXVpcmUoJy4vbWF0aC9saW5lJykpO1xuXG4gIGV4dGVuZCgndXRpbCcsIHV0aWwpO1xuXG4gIGV4dGVuZCgncGh5c2ljcycsIHJlcXVpcmUoJy4vcGh5c2ljcy9waHlzaWNzJykpO1xuXG4gIGV4dGVuZCgncGh5c2ljcycsIHJlcXVpcmUoJy4vcGh5c2ljcy9wYXJ0aWNsZScpKTtcblxuICBleHRlbmQoJ3BoeXNpY3MnLCByZXF1aXJlKCcuL3BoeXNpY3MvYmVoYXZpb3VycycpKTtcblxuICBleHRlbmQoJ3BoeXNpY3MnLCByZXF1aXJlKCcuL3BoeXNpY3MvY29uc3RyYWludHMnKSk7XG5cbiAgZXh0ZW5kKCdjbGllbnQnLCByZXF1aXJlKCcuL2NsaWVudC9za2V0Y2gnKSk7XG5cbiAgbW9kdWxlLmV4cG9ydHMgPSBmaztcblxuICBpZiAodHlwZW9mIHdpbmRvdyAhPT0gXCJ1bmRlZmluZWRcIiAmJiB3aW5kb3cgIT09IG51bGwpIHtcbiAgICB3aW5kb3cuZmsgPSBmaztcbiAgfVxuXG59KS5jYWxsKHRoaXMpO1xuIiwiLy8gR2VuZXJhdGVkIGJ5IENvZmZlZVNjcmlwdCAxLjYuM1xuKGZ1bmN0aW9uKCkge1xuICB2YXIgQm94LCBWZWMzO1xuXG4gIFZlYzMgPSByZXF1aXJlKFwiLi92ZWN0b3JcIikuVmVjMztcblxuICBCb3ggPSAoZnVuY3Rpb24oKSB7XG4gICAgQm94LnByb3RvdHlwZS54MSA9IDA7XG5cbiAgICBCb3gucHJvdG90eXBlLnkxID0gMDtcblxuICAgIEJveC5wcm90b3R5cGUuejEgPSAwO1xuXG4gICAgQm94LnByb3RvdHlwZS54MiA9IDA7XG5cbiAgICBCb3gucHJvdG90eXBlLnkyID0gMDtcblxuICAgIEJveC5wcm90b3R5cGUuejIgPSAwO1xuXG4gICAgZnVuY3Rpb24gQm94KHgxLCB5MSwgejEsIHgyLCB5MiwgejIpIHtcbiAgICAgIHRoaXMueDEgPSB4MSAhPSBudWxsID8geDEgOiAwO1xuICAgICAgdGhpcy55MSA9IHkxICE9IG51bGwgPyB5MSA6IDA7XG4gICAgICB0aGlzLnoxID0gejEgIT0gbnVsbCA/IHoxIDogMDtcbiAgICAgIHRoaXMueDIgPSB4MiAhPSBudWxsID8geDIgOiAwO1xuICAgICAgdGhpcy55MiA9IHkyICE9IG51bGwgPyB5MiA6IDA7XG4gICAgICB0aGlzLnoyID0gejIgIT0gbnVsbCA/IHoyIDogMDtcbiAgICB9XG5cbiAgICBCb3gucHJvdG90eXBlLmNlbnRlciA9IGZ1bmN0aW9uKCkge1xuICAgICAgdmFyIHgsIHksIHo7XG4gICAgICB4ID0gTWF0aC5taW4odGhpcy54MSwgdGhpcy54MikgKyB0aGlzLndpZHRoKCkgKiAwLjU7XG4gICAgICB5ID0gTWF0aC5taW4odGhpcy55MSwgdGhpcy55MikgKyB0aGlzLmhlaWdodCgpICogMC41O1xuICAgICAgeiA9IE1hdGgubWluKHRoaXMuejEsIHRoaXMuejIpICsgdGhpcy5kZXB0aCgpICogMC41O1xuICAgICAgcmV0dXJuIG5ldyBWZWMzKHgsIHksIHopO1xuICAgIH07XG5cbiAgICBCb3gucHJvdG90eXBlLndpZHRoID0gZnVuY3Rpb24oKSB7XG4gICAgICByZXR1cm4gTWF0aC5hYnModGhpcy54MSAtIHRoaXMueDIpO1xuICAgIH07XG5cbiAgICBCb3gucHJvdG90eXBlLmhlaWdodCA9IGZ1bmN0aW9uKCkge1xuICAgICAgcmV0dXJuIE1hdGguYWJzKHRoaXMueTEgLSB0aGlzLnkyKTtcbiAgICB9O1xuXG4gICAgQm94LnByb3RvdHlwZS5kZXB0aCA9IGZ1bmN0aW9uKCkge1xuICAgICAgcmV0dXJuIE1hdGguYWJzKHRoaXMuejEgLSB0aGlzLnoyKTtcbiAgICB9O1xuXG4gICAgcmV0dXJuIEJveDtcblxuICB9KSgpO1xuXG4gIG1vZHVsZS5leHBvcnRzLkJveCA9IEJveDtcblxufSkuY2FsbCh0aGlzKTtcbiIsIi8vIEdlbmVyYXRlZCBieSBDb2ZmZWVTY3JpcHQgMS42LjNcbi8qXG4gIEEgc2ltcGxlIGxpbmUgZnJvbSBtdWx0aXBsZSBzZWdtZW50cywgY2FuIGJlIHNhbXBsZWQgYXQgYW55IHBvaW50LlxuICBOT1RFIHRoaXMgY3VydmUgZG9lcyBub3QgYWx3YXlzIHJ1biB0aHJvdWdoIGFsbCBnaXZlbiBwb2ludHMuXG4gIFdvcmsgd2l0aCBhbnkgb2JqZWN0IHR5cGUgYXMgbG9uZyBhcyBpdCBoYXMgeCBhbmQgeSBudW1iZXIgcHJvcGVydGllcy5cbiovXG5cblxuKGZ1bmN0aW9uKCkge1xuICB2YXIgUG9seWxpbmUsIFNwbGluZTtcblxuICBQb2x5bGluZSA9IChmdW5jdGlvbigpIHtcbiAgICBmdW5jdGlvbiBQb2x5bGluZShwb2ludHMpIHtcbiAgICAgIHRoaXMucG9pbnRzID0gcG9pbnRzICE9IG51bGwgPyBwb2ludHMgOiBbXTtcbiAgICB9XG5cbiAgICBQb2x5bGluZS5wcm90b3R5cGUuYWRkID0gZnVuY3Rpb24ocG9pbnQpIHtcbiAgICAgIHJldHVybiBwb2ludHMucHVzaChwb2ludCk7XG4gICAgfTtcblxuICAgIFBvbHlsaW5lLnByb3RvdHlwZS5zaXplID0gZnVuY3Rpb24oKSB7XG4gICAgICByZXR1cm4gcG9pbnRzLmxlbmd0aDtcbiAgICB9O1xuXG4gICAgUG9seWxpbmUucHJvdG90eXBlLmNsZWFyID0gZnVuY3Rpb24oKSB7XG4gICAgICB2YXIgcG9pbnRzO1xuICAgICAgcmV0dXJuIHBvaW50cyA9IFtdO1xuICAgIH07XG5cbiAgICBQb2x5bGluZS5wcm90b3R5cGUucG9pbnQgPSBmdW5jdGlvbih0aW1lKSB7XG4gICAgICB2YXIgbWVkaWFuLCBuZXh0LCBwcmV2O1xuICAgICAgaWYgKHRpbWUgPD0gMCkge1xuICAgICAgICByZXR1cm4gcG9pbnRzWzBdO1xuICAgICAgfSBlbHNlIGlmICh0aW1lID49IDEpIHtcbiAgICAgICAgcmV0dXJuIHBvaW50c1twb2ludHMubGVuZ3RoIC0gMV07XG4gICAgICB9IGVsc2Uge1xuICAgICAgICBtZWRpYW4gPSB0aW1lICogKHBvaW50cy5sZW5ndGggLSAxKTtcbiAgICAgICAgcHJldiA9IHBvaW50c1tNYXRoLmZsb29yKG1lZGlhbildO1xuICAgICAgICBuZXh0ID0gcG9pbnRzW01hdGguY2VpbChtZWRpYW4pXTtcbiAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICB4OiBwcmV2LnggKyAobmV4dC54IC0gcHJldi54KSAqIDAuNSxcbiAgICAgICAgICB5OiBwcmV2LnkgKyAobmV4dC55IC0gcHJldi55KSAqIDAuNVxuICAgICAgICB9O1xuICAgICAgfVxuICAgIH07XG5cbiAgICByZXR1cm4gUG9seWxpbmU7XG5cbiAgfSkoKTtcblxuICAvKlxuICBBIENhdG11bGwtUm9tIHNwbGluZSAod2hpY2ggaXMgYSBzcGVjaWFsIGZvcm0gb2YgdGhlIGN1YmljIGhlcm1pdGUgY3VydmUpIGltcGxlbWVudGF0aW9uLFxuICBnZW5lcmF0ZXMgYSBzbW9vdGggY3VydmUvaW50ZXJwb2xhdGlvbiBmcm9tIGEgbnVtYmVyIG9mIFZlYzIgcG9pbnRzLlxuICAqL1xuXG5cbiAgU3BsaW5lID0gKGZ1bmN0aW9uKCkge1xuICAgIHZhciBiZWZvcmVMYXN0LCBmaXJzdCwgbGFzdCwgc2Vjb25kO1xuXG4gICAgZmlyc3QgPSBzZWNvbmQgPSBiZWZvcmVMYXN0ID0gbGFzdCA9IHZvaWQgMDtcblxuICAgIFNwbGluZS5wcm90b3R5cGUucG9pbnRzID0gW107XG5cbiAgICBTcGxpbmUucHJvdG90eXBlLm5lZWRzVXBkYXRlID0gZmFsc2U7XG5cbiAgICBmdW5jdGlvbiBTcGxpbmUocG9pbnRzKSB7XG4gICAgICB0aGlzLnBvaW50cyA9IHBvaW50cyAhPSBudWxsID8gcG9pbnRzIDogW107XG4gICAgICB0aGlzLm5lZWRzVXBkYXRlID0gdGhpcy5wb2ludHMubGVuZ3RoID4gMDtcbiAgICB9XG5cbiAgICBTcGxpbmUucHJvdG90eXBlLmFkZCA9IGZ1bmN0aW9uKHBvaW50KSB7XG4gICAgICB0aGlzLnBvaW50cy5wdXNoKHBvaW50KTtcbiAgICAgIHJldHVybiB0aGlzLm5lZWRzVXBkYXRlID0gdHJ1ZTtcbiAgICB9O1xuXG4gICAgU3BsaW5lLnByb3RvdHlwZS5zaXplID0gZnVuY3Rpb24oKSB7XG4gICAgICByZXR1cm4gdGhpcy5wb2ludHMubGVuZ3RoO1xuICAgIH07XG5cbiAgICBTcGxpbmUucHJvdG90eXBlLmNsZWFyID0gZnVuY3Rpb24oKSB7XG4gICAgICByZXR1cm4gdGhpcy5wb2ludHMgPSBbXTtcbiAgICB9O1xuXG4gICAgU3BsaW5lLnByb3RvdHlwZS51cGRhdGUgPSBmdW5jdGlvbigpIHtcbiAgICAgIGlmICh0aGlzLnBvaW50cy5sZW5ndGggPCA0KSB7XG4gICAgICAgIHJldHVybjtcbiAgICAgIH1cbiAgICAgIGZpcnN0ID0gdGhpcy5wb2ludHNbMF07XG4gICAgICBzZWNvbmQgPSB0aGlzLnBvaW50c1sxXTtcbiAgICAgIGJlZm9yZUxhc3QgPSB0aGlzLnBvaW50c1t0aGlzLnBvaW50cy5sZW5ndGggLSAyXTtcbiAgICAgIGxhc3QgPSB0aGlzLnBvaW50c1t0aGlzLnBvaW50cy5sZW5ndGggLSAxXTtcbiAgICAgIHJldHVybiB0aGlzLm5lZWRzVXBkYXRlID0gZmFsc2U7XG4gICAgfTtcblxuICAgIFNwbGluZS5wcm90b3R5cGUucG9pbnQgPSBmdW5jdGlvbih0aW1lKSB7XG4gICAgICB2YXIgaSwgbm9ybWFsaXplZFRpbWUsIHBhcnRQZXJjZW50YWdlLCByZXN1bHQsIHNpemUsIHQsIHQyLCB0MywgdGltZUJldHdlZW4sIHRtcDEsIHRtcDIsIHRtcDMsIHRtcDQ7XG4gICAgICBpZiAodGhpcy5wb2ludHMubGVuZ3RoIDwgNCkge1xuICAgICAgICByZXR1cm47XG4gICAgICB9XG4gICAgICBpZiAodGltZSA8PSAwKSB7XG4gICAgICAgIHJldHVybiB0aGlzLnBvaW50c1swXTtcbiAgICAgIH0gZWxzZSBpZiAodGltZSA+PSAxKSB7XG4gICAgICAgIHJldHVybiB0aGlzLnBvaW50c1t0aGlzLnBvaW50cy5sZW5ndGggLSAxXTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIGlmICh0aGlzLm5lZWRzVXBkYXRlKSB7XG4gICAgICAgICAgdXBkYXRlKCk7XG4gICAgICAgIH1cbiAgICAgICAgc2l6ZSA9IHRoaXMucG9pbnRzLmxlbmd0aDtcbiAgICAgICAgcGFydFBlcmNlbnRhZ2UgPSAxLjAgLyAoc2l6ZSAtIDEpO1xuICAgICAgICB0aW1lQmV0d2VlbiA9IHRpbWUgLyBwYXJ0UGVyY2VudGFnZTtcbiAgICAgICAgaSA9IE1hdGguZmxvb3IodGltZUJldHdlZW4pO1xuICAgICAgICBub3JtYWxpemVkVGltZSA9IHRpbWVCZXR3ZWVuIC0gaTtcbiAgICAgICAgdCA9IG5vcm1hbGl6ZWRUaW1lICogMC41O1xuICAgICAgICB0MiA9IHQgKiBub3JtYWxpemVkVGltZTtcbiAgICAgICAgdDMgPSB0MiAqIG5vcm1hbGl6ZWRUaW1lO1xuICAgICAgICB0bXAxID0gdm9pZCAwO1xuICAgICAgICBpZiAoLS1pID09PSAtMSkge1xuICAgICAgICAgIHRtcDEgPSBmaXJzdC5jbG9uZSgpO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIHRtcDEgPSB0aGlzLnBvaW50c1tpXS5jbG9uZSgpO1xuICAgICAgICB9XG4gICAgICAgIHRtcDIgPSB0aGlzLnBvaW50c1srK2ldLmNsb25lKCk7XG4gICAgICAgIHRtcDMgPSB0aGlzLnBvaW50c1srK2ldLmNsb25lKCk7XG4gICAgICAgIHRtcDQgPSB2b2lkIDA7XG4gICAgICAgIGlmICgrK2kgPT09IHNpemUpIHtcbiAgICAgICAgICB0bXA0ID0gbGFzdC5jbG9uZSgpO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIHRtcDQgPSB0aGlzLnBvaW50c1tpXS5jbG9uZSgpO1xuICAgICAgICB9XG4gICAgICAgIHRtcDEuc2NhbGUoLXQzICsgMiAqIHQyIC0gdCk7XG4gICAgICAgIHJlc3VsdCA9IHRtcDE7XG4gICAgICAgIHRtcDIuc2NhbGUoMyAqIHQzIC0gNSAqIHQyICsgMSk7XG4gICAgICAgIHJlc3VsdC5hZGQodG1wMik7XG4gICAgICAgIHRtcDMuc2NhbGUoLTMgKiB0MyArIDQgKiB0MiArIHQpO1xuICAgICAgICByZXN1bHQuYWRkKHRtcDMpO1xuICAgICAgICB0bXA0LnNjYWxlKHQzIC0gdDIpO1xuICAgICAgICByZXN1bHQuYWRkKHRtcDQpO1xuICAgICAgICByZXR1cm4gcmVzdWx0O1xuICAgICAgfVxuICAgIH07XG5cbiAgICByZXR1cm4gU3BsaW5lO1xuXG4gIH0pKCk7XG5cbiAgbW9kdWxlLmV4cG9ydHMgPSB7XG4gICAgUG9seWxpbmU6IFBvbHlsaW5lLFxuICAgIFNwbGluZTogU3BsaW5lXG4gIH07XG5cbn0pLmNhbGwodGhpcyk7XG4iLCIvLyBHZW5lcmF0ZWQgYnkgQ29mZmVlU2NyaXB0IDEuNi4zXG4oZnVuY3Rpb24oKSB7XG4gIHZhciByYW5kb20sIHJhbmRvbVBrZztcblxuICByYW5kb21Qa2cgPSByZXF1aXJlKCcuL3JhbmRvbScpO1xuXG4gIHJhbmRvbSA9IG5ldyByYW5kb21Qa2cuUmFuZG9tKCk7XG5cbiAgbW9kdWxlLmV4cG9ydHMgPSB7XG4gICAgZml0OiBmdW5jdGlvbih2YWx1ZSwgaW5NaW4sIGluTWF4LCBvdXRNaW4sIG91dE1heCkge1xuICAgICAgdmFsdWUgPSBNYXRoLm1heChpbk1pbiwgTWF0aC5taW4oaW5NYXgsIHZhbHVlKSk7XG4gICAgICByZXR1cm4gKCh2YWx1ZSAtIGluTWluKSAvIChpbk1heCAtIGluTWluKSkgKiAob3V0TWF4IC0gb3V0TWluKSArIG91dE1pbjtcbiAgICB9LFxuICAgIGZpdDAxOiBmdW5jdGlvbih2YWx1ZSwgbWluLCBtYXgpIHtcbiAgICAgIHJldHVybiB2YWx1ZSAqIChtYXggLSBtaW4pICsgbWluO1xuICAgIH0sXG4gICAgY2xhbXA6IGZ1bmN0aW9uKHZhbHVlLCBtaW4sIG1heCkge1xuICAgICAgcmV0dXJuIE1hdGgubWF4KG1pbiwgTWF0aC5taW4obWF4LCB2YWx1ZSkpO1xuICAgIH0sXG4gICAgY2xhbXAwMTogZnVuY3Rpb24odmFsdWUpIHtcbiAgICAgIHJldHVybiBNYXRoLm1heCgwLCBNYXRoLm1pbigxLCB2YWx1ZSkpO1xuICAgIH0sXG4gICAgcmFuZG9tOiByYW5kb20sXG4gICAgcmFuZGY6IGZ1bmN0aW9uKG1pbiwgbWF4KSB7XG4gICAgICByZXR1cm4gcmFuZG9tLnJhbmRmKG1pbiwgbWF4KTtcbiAgICB9LFxuICAgIHJhbmRpOiBmdW5jdGlvbihtaW4sIG1heCkge1xuICAgICAgcmV0dXJuIHJhbmRvbS5yYW5kaShtaW4sIG1heCk7XG4gICAgfSxcbiAgICBmbGlwQ29pbjogZnVuY3Rpb24oY2hhbmNlKSB7XG4gICAgICByZXR1cm4gcmFuZG9tLmZsaXBDb2luKGNoYW5jZSk7XG4gICAgfSxcbiAgICByb3VuZDogZnVuY3Rpb24odmFsdWUsIGRlY2ltYWxzKSB7XG4gICAgICB2YXIgZXhwO1xuICAgICAgaWYgKGRlY2ltYWxzID09IG51bGwpIHtcbiAgICAgICAgZGVjaW1hbHMgPSAwO1xuICAgICAgfVxuICAgICAgaWYgKGRlY2ltYWxzID09PSAwKSB7XG4gICAgICAgIHJldHVybiBNYXRoLnJvdW5kKHZhbHVlKTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIGV4cCA9IE1hdGgucG93KDEwLCBkZWNpbWFscyk7XG4gICAgICAgIHJldHVybiBNYXRoLnJvdW5kKHZhbHVlICogZXhwKSAvIGV4cDtcbiAgICAgIH1cbiAgICB9LFxuICAgIGludGVycG9sYXRlOiB7XG4gICAgICBzcGhlcmljYWw6IGZ1bmN0aW9uKGN1cnJlbnQsIHRhcmdldCwgZGVsdGEpIHtcbiAgICAgICAgcmV0dXJuIGN1cnJlbnQgKiAoMS4wIC0gZGVsdGEpICsgdGFyZ2V0ICogZGVsdGE7XG4gICAgICB9LFxuICAgICAgbGluZWFyOiBmdW5jdGlvbihjdXJyZW50LCB0YXJnZXQsIGRlbHRhKSB7XG4gICAgICAgIHJldHVybiBjdXJyZW50ICsgKHRhcmdldCAtIGN1cnJlbnQpICogZGVsdGE7XG4gICAgICB9XG4gICAgfSxcbiAgICAvKlxuICAgICAgRWFzaW5nIEZ1bmN0aW9ucyAtIGluc3BpcmVkIGJ5IGh0dHA6Ly9lYXNpbmdzLm5ldFxuICAgICAgb25seSBjb25zaWRlcmluZyB0aGUgdCB2YWx1ZSBmb3IgdGhlIHJhbmdlIFswLCAxXSA9PiBbMCwgMV1cbiAgICAqL1xuXG4gICAgZWFzZToge1xuICAgICAgbGluZWFyOiBmdW5jdGlvbih0KSB7XG4gICAgICAgIHJldHVybiB0O1xuICAgICAgfSxcbiAgICAgIGluUXVhZDogZnVuY3Rpb24odCkge1xuICAgICAgICByZXR1cm4gTWF0aC5wb3codCwgMik7XG4gICAgICB9LFxuICAgICAgb3V0UXVhZDogZnVuY3Rpb24odCkge1xuICAgICAgICByZXR1cm4gLShNYXRoLnBvdyh0IC0gMSwgMikgLSAxKTtcbiAgICAgIH0sXG4gICAgICBpbk91dFF1YWQ6IGZ1bmN0aW9uKHQpIHtcbiAgICAgICAgaWYgKCh0IC89IDAuNSkgPCAxKSB7XG4gICAgICAgICAgcmV0dXJuIDAuNSAqIE1hdGgucG93KHQsIDIpO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiAtMC41ICogKCh0IC09IDIpICogdCAtIDIpO1xuICAgICAgfSxcbiAgICAgIGluQ3ViaWM6IGZ1bmN0aW9uKHQpIHtcbiAgICAgICAgcmV0dXJuIE1hdGgucG93KHQsIDMpO1xuICAgICAgfSxcbiAgICAgIG91dEN1YmljOiBmdW5jdGlvbih0KSB7XG4gICAgICAgIHJldHVybiBNYXRoLnBvdyh0IC0gMSwgMykgKyAxO1xuICAgICAgfSxcbiAgICAgIGluT3V0Q3ViaWM6IGZ1bmN0aW9uKHQpIHtcbiAgICAgICAgaWYgKCh0IC89IDAuNSkgPCAxKSB7XG4gICAgICAgICAgcmV0dXJuIDAuNSAqIE1hdGgucG93KHQsIDMpO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiAwLjUgKiAoTWF0aC5wb3codCAtIDIsIDMpICsgMik7XG4gICAgICB9LFxuICAgICAgaW5RdWFydDogZnVuY3Rpb24odCkge1xuICAgICAgICByZXR1cm4gTWF0aC5wb3codCwgNCk7XG4gICAgICB9LFxuICAgICAgb3V0UXVhcnQ6IGZ1bmN0aW9uKHQpIHtcbiAgICAgICAgcmV0dXJuIC0oTWF0aC5wb3codCAtIDEsIDQpIC0gMSk7XG4gICAgICB9LFxuICAgICAgaW5PdXRRdWFydDogZnVuY3Rpb24odCkge1xuICAgICAgICBpZiAoKHQgLz0gMC41KSA8IDEpIHtcbiAgICAgICAgICByZXR1cm4gMC41ICogTWF0aC5wb3codCwgNCk7XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIC0wLjUgKiAoKHQgLT0gMikgKiBNYXRoLnBvdyh0LCAzKSAtIDIpO1xuICAgICAgfSxcbiAgICAgIGluUXVpbnQ6IGZ1bmN0aW9uKHQpIHtcbiAgICAgICAgcmV0dXJuIE1hdGgucG93KHQsIDUpO1xuICAgICAgfSxcbiAgICAgIG91dFF1aW50OiBmdW5jdGlvbih0KSB7XG4gICAgICAgIHJldHVybiBNYXRoLnBvdyh0IC0gMSwgNSkgKyAxO1xuICAgICAgfSxcbiAgICAgIGluT3V0UXVpbnQ6IGZ1bmN0aW9uKHQpIHtcbiAgICAgICAgaWYgKCh0IC89IDAuNSkgPCAxKSB7XG4gICAgICAgICAgcmV0dXJuIDAuNSAqIE1hdGgucG93KHQsIDUpO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiAwLjUgKiAoTWF0aC5wb3codCAtIDIsIDUpICsgMik7XG4gICAgICB9LFxuICAgICAgaW5TaW5lOiBmdW5jdGlvbih0KSB7XG4gICAgICAgIHJldHVybiAtTWF0aC5jb3ModCAqIChNYXRoLlBJIC8gMikpICsgMTtcbiAgICAgIH0sXG4gICAgICBvdXRTaW5lOiBmdW5jdGlvbih0KSB7XG4gICAgICAgIHJldHVybiBNYXRoLnNpbih0ICogKE1hdGguUEkgLyAyKSk7XG4gICAgICB9LFxuICAgICAgaW5PdXRTaW5lOiBmdW5jdGlvbih0KSB7XG4gICAgICAgIHJldHVybiAtMC41ICogKE1hdGguY29zKE1hdGguUEkgKiB0KSAtIDEpO1xuICAgICAgfSxcbiAgICAgIGluRXhwbzogZnVuY3Rpb24odCkge1xuICAgICAgICBpZiAodCA9PT0gMCkge1xuICAgICAgICAgIHJldHVybiAwO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIHJldHVybiBNYXRoLnBvdygyLCAxMCAqICh0IC0gMSkpO1xuICAgICAgICB9XG4gICAgICB9LFxuICAgICAgb3V0RXhwbzogZnVuY3Rpb24odCkge1xuICAgICAgICBpZiAodCA9PT0gMSkge1xuICAgICAgICAgIHJldHVybiAxO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIHJldHVybiAtTWF0aC5wb3coMiwgLTEwICogdCkgKyAxO1xuICAgICAgICB9XG4gICAgICB9LFxuICAgICAgaW5PdXRFeHBvOiBmdW5jdGlvbih0KSB7XG4gICAgICAgIGlmICh0ID09PSAwKSB7XG4gICAgICAgICAgcmV0dXJuIDA7XG4gICAgICAgIH1cbiAgICAgICAgaWYgKHQgPT09IDEpIHtcbiAgICAgICAgICByZXR1cm4gMTtcbiAgICAgICAgfVxuICAgICAgICBpZiAoKHQgLz0gMC41KSA8IDEpIHtcbiAgICAgICAgICByZXR1cm4gMC41ICogTWF0aC5wb3coMiwgMTAgKiAodCAtIDEpKTtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gMC41ICogKC1NYXRoLnBvdygyLCAtMTAgKiAtLXQpICsgMik7XG4gICAgICB9LFxuICAgICAgaW5DaXJjOiBmdW5jdGlvbih0KSB7XG4gICAgICAgIHJldHVybiAtKE1hdGguc3FydCgxIC0gKHQgKiB0KSkgLSAxKTtcbiAgICAgIH0sXG4gICAgICBvdXRDaXJjOiBmdW5jdGlvbih0KSB7XG4gICAgICAgIHJldHVybiBNYXRoLnNxcnQoMSAtIE1hdGgucG93KHQgLSAxLCAyKSk7XG4gICAgICB9LFxuICAgICAgaW5PdXRDaXJjOiBmdW5jdGlvbih0KSB7XG4gICAgICAgIGlmICgodCAvPSAwLjUpIDwgMSkge1xuICAgICAgICAgIHJldHVybiAtMC41ICogKE1hdGguc3FydCgxIC0gdCAqIHQpIC0gMSk7XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIDAuNSAqIChNYXRoLnNxcnQoMSAtICh0IC09IDIpICogdCkgKyAxKTtcbiAgICAgIH0sXG4gICAgICBvdXRCb3VuY2U6IGZ1bmN0aW9uKHQpIHtcbiAgICAgICAgaWYgKHQgPCAoMSAvIDIuNzUpKSB7XG4gICAgICAgICAgcmV0dXJuIDcuNTYyNSAqIHQgKiB0O1xuICAgICAgICB9IGVsc2UgaWYgKHQgPCAoMiAvIDIuNzUpKSB7XG4gICAgICAgICAgcmV0dXJuIDcuNTYyNSAqICh0IC09IDEuNSAvIDIuNzUpICogdCArIDAuNzU7XG4gICAgICAgIH0gZWxzZSBpZiAodCA8ICgyLjUgLyAyLjc1KSkge1xuICAgICAgICAgIHJldHVybiA3LjU2MjUgKiAodCAtPSAyLjI1IC8gMi43NSkgKiB0ICsgMC45Mzc1O1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIHJldHVybiA3LjU2MjUgKiAodCAtPSAyLjYyNSAvIDIuNzUpICogdCArIDAuOTg0Mzc1O1xuICAgICAgICB9XG4gICAgICB9LFxuICAgICAgaW5CYWNrOiBmdW5jdGlvbih0KSB7XG4gICAgICAgIHZhciBzO1xuICAgICAgICBzID0gMS43MDE1ODtcbiAgICAgICAgcmV0dXJuIHQgKiB0ICogKChzICsgMSkgKiB0IC0gcyk7XG4gICAgICB9LFxuICAgICAgb3V0QmFjazogZnVuY3Rpb24odCkge1xuICAgICAgICB2YXIgcztcbiAgICAgICAgcyA9IDEuNzAxNTg7XG4gICAgICAgIHJldHVybiAodCA9IHQgLSAxKSAqIHQgKiAoKHMgKyAxKSAqIHQgKyBzKSArIDE7XG4gICAgICB9LFxuICAgICAgaW5PdXRCYWNrOiBmdW5jdGlvbih0KSB7XG4gICAgICAgIHZhciBzO1xuICAgICAgICBzID0gMS43MDE1ODtcbiAgICAgICAgaWYgKCh0IC89IDAuNSkgPCAxKSB7XG4gICAgICAgICAgcmV0dXJuIDAuNSAqICh0ICogdCAqICgoKHMgKj0gMS41MjUpICsgMSkgKiB0IC0gcykpO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiAwLjUgKiAoKHQgLT0gMikgKiB0ICogKCgocyAqPSAxLjUyNSkgKyAxKSAqIHQgKyBzKSArIDIpO1xuICAgICAgfSxcbiAgICAgIGVsYXN0aWM6IGZ1bmN0aW9uKHQpIHtcbiAgICAgICAgcmV0dXJuIC0xICogTWF0aC5wb3coNCwgLTggKiB0KSAqIE1hdGguc2luKCh0ICogNiAtIDEpICogKDIgKiBNYXRoLlBJKSAvIDIpICsgMTtcbiAgICAgIH0sXG4gICAgICBzd2luZ0Zyb21UbzogZnVuY3Rpb24odCkge1xuICAgICAgICB2YXIgcztcbiAgICAgICAgcyA9IDEuNzAxNTg7XG4gICAgICAgIGlmICgodCAvPSAwLjUpIDwgMSkge1xuICAgICAgICAgIHJldHVybiAwLjUgKiAodCAqIHQgKiAoKChzICo9IDEuNTI1KSArIDEpICogdCAtIHMpKTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICByZXR1cm4gMC41ICogKCh0IC09IDIpICogdCAqICgoKHMgKj0gMS41MjUpICsgMSkgKiB0ICsgcykgKyAyKTtcbiAgICAgICAgfVxuICAgICAgfSxcbiAgICAgIHN3aW5nRnJvbTogZnVuY3Rpb24odCkge1xuICAgICAgICB2YXIgcztcbiAgICAgICAgcyA9IDEuNzAxNTg7XG4gICAgICAgIHJldHVybiB0ICogdCAqICgocyArIDEpICogdCAtIHMpO1xuICAgICAgfSxcbiAgICAgIHN3aW5nVG86IGZ1bmN0aW9uKHQpIHtcbiAgICAgICAgdmFyIHM7XG4gICAgICAgIHMgPSAxLjcwMTU4O1xuICAgICAgICByZXR1cm4gKHQgLT0gMSkgKiB0ICogKChzICsgMSkgKiB0ICsgcykgKyAxO1xuICAgICAgfSxcbiAgICAgIGJvdW5jZTogZnVuY3Rpb24odCkge1xuICAgICAgICBpZiAodCA8ICgxIC8gMi43NSkpIHtcbiAgICAgICAgICByZXR1cm4gNy41NjI1ICogdCAqIHQ7XG4gICAgICAgIH0gZWxzZSBpZiAodCA8ICgyIC8gMi43NSkpIHtcbiAgICAgICAgICByZXR1cm4gNy41NjI1ICogKHQgLT0gMS41IC8gMi43NSkgKiB0ICsgMC43NTtcbiAgICAgICAgfSBlbHNlIGlmICh0IDwgKDIuNSAvIDIuNzUpKSB7XG4gICAgICAgICAgcmV0dXJuIDcuNTYyNSAqICh0IC09IDIuMjUgLyAyLjc1KSAqIHQgKyAwLjkzNzU7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgcmV0dXJuIDcuNTYyNSAqICh0IC09IDIuNjI1IC8gMi43NSkgKiB0ICsgMC45ODQzNzU7XG4gICAgICAgIH1cbiAgICAgIH0sXG4gICAgICBib3VuY2VQYXN0OiBmdW5jdGlvbih0KSB7XG4gICAgICAgIGlmICh0IDwgKDEgLyAyLjc1KSkge1xuICAgICAgICAgIHJldHVybiA3LjU2MjUgKiB0ICogdDtcbiAgICAgICAgfSBlbHNlIGlmICh0IDwgKDIgLyAyLjc1KSkge1xuICAgICAgICAgIHJldHVybiAyIC0gKDcuNTYyNSAqICh0IC09IDEuNSAvIDIuNzUpICogdCArIDAuNzUpO1xuICAgICAgICB9IGVsc2UgaWYgKHQgPCAoMi41IC8gMi43NSkpIHtcbiAgICAgICAgICByZXR1cm4gMiAtICg3LjU2MjUgKiAodCAtPSAyLjI1IC8gMi43NSkgKiB0ICsgMC45Mzc1KTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICByZXR1cm4gMiAtICg3LjU2MjUgKiAodCAtPSAyLjYyNSAvIDIuNzUpICogdCArIDAuOTg0Mzc1KTtcbiAgICAgICAgfVxuICAgICAgfSxcbiAgICAgIGZyb21UbzogZnVuY3Rpb24odCkge1xuICAgICAgICBpZiAoKHQgLz0gMC41KSA8IDEpIHtcbiAgICAgICAgICByZXR1cm4gMC41ICogTWF0aC5wb3codCwgNCk7XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIC0wLjUgKiAoKHQgLT0gMikgKiBNYXRoLnBvdyh0LCAzKSAtIDIpO1xuICAgICAgfSxcbiAgICAgIGZyb206IGZ1bmN0aW9uKHQpIHtcbiAgICAgICAgcmV0dXJuIE1hdGgucG93KHQsIDQpO1xuICAgICAgfSxcbiAgICAgIHRvOiBmdW5jdGlvbih0KSB7XG4gICAgICAgIHJldHVybiBNYXRoLnBvdyh0LCAwLjI1KTtcbiAgICAgIH1cbiAgICB9XG4gIH07XG5cbn0pLmNhbGwodGhpcyk7XG4iLCIvLyBHZW5lcmF0ZWQgYnkgQ29mZmVlU2NyaXB0IDEuNi4zXG4vKlxuICBCYXNlIGNsYXNzIGZvciBhbGwgdHlwZXMgb2YgTm9pc2UgZ2VuZXJhdG9yc1xuKi9cblxuXG4oZnVuY3Rpb24oKSB7XG4gIHZhciBGbG93Tm9pc2UsIE5vaXNlLCBTaW1wbGV4Tm9pc2UsXG4gICAgX19oYXNQcm9wID0ge30uaGFzT3duUHJvcGVydHksXG4gICAgX19leHRlbmRzID0gZnVuY3Rpb24oY2hpbGQsIHBhcmVudCkgeyBmb3IgKHZhciBrZXkgaW4gcGFyZW50KSB7IGlmIChfX2hhc1Byb3AuY2FsbChwYXJlbnQsIGtleSkpIGNoaWxkW2tleV0gPSBwYXJlbnRba2V5XTsgfSBmdW5jdGlvbiBjdG9yKCkgeyB0aGlzLmNvbnN0cnVjdG9yID0gY2hpbGQ7IH0gY3Rvci5wcm90b3R5cGUgPSBwYXJlbnQucHJvdG90eXBlOyBjaGlsZC5wcm90b3R5cGUgPSBuZXcgY3RvcigpOyBjaGlsZC5fX3N1cGVyX18gPSBwYXJlbnQucHJvdG90eXBlOyByZXR1cm4gY2hpbGQ7IH07XG5cbiAgTm9pc2UgPSAoZnVuY3Rpb24oKSB7XG4gICAgZnVuY3Rpb24gTm9pc2UoKSB7fVxuXG4gICAgTm9pc2UucHJvdG90eXBlLm5vaXNlID0gZnVuY3Rpb24oeCwgeSkge1xuICAgICAgcmV0dXJuIDA7XG4gICAgfTtcblxuICAgIE5vaXNlLnByb3RvdHlwZS5ub2lzZTIgPSBmdW5jdGlvbih4LCB5KSB7XG4gICAgICByZXR1cm4gMDtcbiAgICB9O1xuXG4gICAgTm9pc2UucHJvdG90eXBlLm5vaXNlMyA9IGZ1bmN0aW9uKHgsIHksIHopIHtcbiAgICAgIHJldHVybiAwO1xuICAgIH07XG5cbiAgICBOb2lzZS5wcm90b3R5cGUubm9pc2U0ID0gZnVuY3Rpb24oeCwgeSwgeiwgdykge1xuICAgICAgcmV0dXJuIDA7XG4gICAgfTtcblxuICAgIHJldHVybiBOb2lzZTtcblxuICB9KSgpO1xuXG4gIC8qXG4gICAgUG9ydGVkIGZyb20gU3RlZmFuIEd1c3RhdnNvbidzIGphdmEgaW1wbGVtZW50YXRpb25cbiAgICBodHRwOi8vc3RhZmZ3d3cuaXRuLmxpdS5zZS9+c3RlZ3Uvc2ltcGxleG5vaXNlL3NpbXBsZXhub2lzZS5wZGZcbiAgICBSZWFkIFN0ZWZhbidzIGV4Y2VsbGVudCBwYXBlciBmb3IgZGV0YWlscyBvbiBob3cgdGhpcyBjb2RlIHdvcmtzLlxuICBcbiAgICBTZWFuIE1jQ3VsbG91Z2ggYmFua3NlYW5AZ21haWwuY29tXG4gIFxuICAgIEFkZGVkIDREIG5vaXNlXG4gICAgSm9zaHVhIEtvbyB6ejg1bnVzQGdtYWlsLmNvbVxuICAqL1xuXG5cbiAgU2ltcGxleE5vaXNlID0gKGZ1bmN0aW9uKF9zdXBlcikge1xuICAgIHZhciBkb3Q7XG5cbiAgICBfX2V4dGVuZHMoU2ltcGxleE5vaXNlLCBfc3VwZXIpO1xuXG4gICAgZnVuY3Rpb24gU2ltcGxleE5vaXNlKHJuZykge1xuICAgICAgdmFyIGksIF9pLCBfajtcbiAgICAgIGlmIChybmcgPT0gbnVsbCkge1xuICAgICAgICBybmcgPSBNYXRoO1xuICAgICAgfVxuICAgICAgdGhpcy5ncmFkMyA9IFtbMSwgMSwgMF0sIFstMSwgMSwgMF0sIFsxLCAtMSwgMF0sIFstMSwgLTEsIDBdLCBbMSwgMCwgMV0sIFstMSwgMCwgMV0sIFsxLCAwLCAtMV0sIFstMSwgMCwgLTFdLCBbMCwgMSwgMV0sIFswLCAtMSwgMV0sIFswLCAxLCAtMV0sIFswLCAtMSwgLTFdXTtcbiAgICAgIHRoaXMuZ3JhZDQgPSBbWzAsIDEsIDEsIDFdLCBbMCwgMSwgMSwgLTFdLCBbMCwgMSwgLTEsIDFdLCBbMCwgMSwgLTEsIC0xXSwgWzAsIC0xLCAxLCAxXSwgWzAsIC0xLCAxLCAtMV0sIFswLCAtMSwgLTEsIDFdLCBbMCwgLTEsIC0xLCAtMV0sIFsxLCAwLCAxLCAxXSwgWzEsIDAsIDEsIC0xXSwgWzEsIDAsIC0xLCAxXSwgWzEsIDAsIC0xLCAtMV0sIFstMSwgMCwgMSwgMV0sIFstMSwgMCwgMSwgLTFdLCBbLTEsIDAsIC0xLCAxXSwgWy0xLCAwLCAtMSwgLTFdLCBbMSwgMSwgMCwgMV0sIFsxLCAxLCAwLCAtMV0sIFsxLCAtMSwgMCwgMV0sIFsxLCAtMSwgMCwgLTFdLCBbLTEsIDEsIDAsIDFdLCBbLTEsIDEsIDAsIC0xXSwgWy0xLCAtMSwgMCwgMV0sIFstMSwgLTEsIDAsIC0xXSwgWzEsIDEsIDEsIDBdLCBbMSwgMSwgLTEsIDBdLCBbMSwgLTEsIDEsIDBdLCBbMSwgLTEsIC0xLCAwXSwgWy0xLCAxLCAxLCAwXSwgWy0xLCAxLCAtMSwgMF0sIFstMSwgLTEsIDEsIDBdLCBbLTEsIC0xLCAtMSwgMF1dO1xuICAgICAgdGhpcy5wID0gW107XG4gICAgICBpID0gMDtcbiAgICAgIGZvciAoaSA9IF9pID0gMDsgX2kgPD0gMjU2OyBpID0gKytfaSkge1xuICAgICAgICB0aGlzLnBbaV0gPSBNYXRoLmZsb29yKHJuZy5yYW5kb20oKSAqIDI1Nik7XG4gICAgICB9XG4gICAgICB0aGlzLnBlcm0gPSBbXTtcbiAgICAgIGZvciAoaSA9IF9qID0gMDsgX2ogPD0gNTEyOyBpID0gKytfaikge1xuICAgICAgICB0aGlzLnBlcm1baV0gPSB0aGlzLnBbaSAmIDI1NV07XG4gICAgICB9XG4gICAgICB0aGlzLnNpbXBsZXggPSBbWzAsIDEsIDIsIDNdLCBbMCwgMSwgMywgMl0sIFswLCAwLCAwLCAwXSwgWzAsIDIsIDMsIDFdLCBbMCwgMCwgMCwgMF0sIFswLCAwLCAwLCAwXSwgWzAsIDAsIDAsIDBdLCBbMSwgMiwgMywgMF0sIFswLCAyLCAxLCAzXSwgWzAsIDAsIDAsIDBdLCBbMCwgMywgMSwgMl0sIFswLCAzLCAyLCAxXSwgWzAsIDAsIDAsIDBdLCBbMCwgMCwgMCwgMF0sIFswLCAwLCAwLCAwXSwgWzEsIDMsIDIsIDBdLCBbMCwgMCwgMCwgMF0sIFswLCAwLCAwLCAwXSwgWzAsIDAsIDAsIDBdLCBbMCwgMCwgMCwgMF0sIFswLCAwLCAwLCAwXSwgWzAsIDAsIDAsIDBdLCBbMCwgMCwgMCwgMF0sIFswLCAwLCAwLCAwXSwgWzEsIDIsIDAsIDNdLCBbMCwgMCwgMCwgMF0sIFsxLCAzLCAwLCAyXSwgWzAsIDAsIDAsIDBdLCBbMCwgMCwgMCwgMF0sIFswLCAwLCAwLCAwXSwgWzIsIDMsIDAsIDFdLCBbMiwgMywgMSwgMF0sIFsxLCAwLCAyLCAzXSwgWzEsIDAsIDMsIDJdLCBbMCwgMCwgMCwgMF0sIFswLCAwLCAwLCAwXSwgWzAsIDAsIDAsIDBdLCBbMiwgMCwgMywgMV0sIFswLCAwLCAwLCAwXSwgWzIsIDEsIDMsIDBdLCBbMCwgMCwgMCwgMF0sIFswLCAwLCAwLCAwXSwgWzAsIDAsIDAsIDBdLCBbMCwgMCwgMCwgMF0sIFswLCAwLCAwLCAwXSwgWzAsIDAsIDAsIDBdLCBbMCwgMCwgMCwgMF0sIFswLCAwLCAwLCAwXSwgWzIsIDAsIDEsIDNdLCBbMCwgMCwgMCwgMF0sIFswLCAwLCAwLCAwXSwgWzAsIDAsIDAsIDBdLCBbMywgMCwgMSwgMl0sIFszLCAwLCAyLCAxXSwgWzAsIDAsIDAsIDBdLCBbMywgMSwgMiwgMF0sIFsyLCAxLCAwLCAzXSwgWzAsIDAsIDAsIDBdLCBbMCwgMCwgMCwgMF0sIFswLCAwLCAwLCAwXSwgWzMsIDEsIDAsIDJdLCBbMCwgMCwgMCwgMF0sIFszLCAyLCAwLCAxXSwgWzMsIDIsIDEsIDBdXTtcbiAgICB9XG5cbiAgICBkb3QgPSBmdW5jdGlvbihnLCB4LCB5KSB7XG4gICAgICByZXR1cm4gZ1swXSAqIHggKyBnWzFdICogeTtcbiAgICB9O1xuXG4gICAgU2ltcGxleE5vaXNlLnByb3RvdHlwZS5ub2lzZTIgPSBmdW5jdGlvbih4aW4sIHlpbikge1xuICAgICAgdmFyIEYyLCBHMiwgWDAsIFkwLCBnaTAsIGdpMSwgZ2kyLCBpLCBpMSwgaWksIGosIGoxLCBqaiwgbjAsIG4xLCBuMiwgcywgdCwgdDAsIHQxLCB0MiwgeDAsIHgxLCB4MiwgeTAsIHkxLCB5MjtcbiAgICAgIG4wID0gMDtcbiAgICAgIG4xID0gMDtcbiAgICAgIG4yID0gMDtcbiAgICAgIEYyID0gMC41ICogKE1hdGguc3FydCgzLjApIC0gMS4wKTtcbiAgICAgIHMgPSAoeGluICsgeWluKSAqIEYyO1xuICAgICAgaSA9IE1hdGguZmxvb3IoeGluICsgcyk7XG4gICAgICBqID0gTWF0aC5mbG9vcih5aW4gKyBzKTtcbiAgICAgIEcyID0gKDMuMCAtIE1hdGguc3FydCgzLjApKSAvIDYuMDtcbiAgICAgIHQgPSAoaSArIGopICogRzI7XG4gICAgICBYMCA9IGkgLSB0O1xuICAgICAgWTAgPSBqIC0gdDtcbiAgICAgIHgwID0geGluIC0gWDA7XG4gICAgICB5MCA9IHlpbiAtIFkwO1xuICAgICAgaTEgPSAwO1xuICAgICAgajEgPSAwO1xuICAgICAgaWYgKHgwID4geTApIHtcbiAgICAgICAgaTEgPSAxO1xuICAgICAgICBqMSA9IDA7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICBpMSA9IDA7XG4gICAgICAgIGoxID0gMTtcbiAgICAgIH1cbiAgICAgIHgxID0geDAgLSBpMSArIEcyO1xuICAgICAgeTEgPSB5MCAtIGoxICsgRzI7XG4gICAgICB4MiA9IHgwIC0gMS4wICsgMi4wICogRzI7XG4gICAgICB5MiA9IHkwIC0gMS4wICsgMi4wICogRzI7XG4gICAgICBpaSA9IGkgJiAyNTU7XG4gICAgICBqaiA9IGogJiAyNTU7XG4gICAgICBnaTAgPSB0aGlzLnBlcm1baWkgKyB0aGlzLnBlcm1bampdXSAlIDEyO1xuICAgICAgZ2kxID0gdGhpcy5wZXJtW2lpICsgaTEgKyB0aGlzLnBlcm1bamogKyBqMV1dICUgMTI7XG4gICAgICBnaTIgPSB0aGlzLnBlcm1baWkgKyAxICsgdGhpcy5wZXJtW2pqICsgMV1dICUgMTI7XG4gICAgICB0MCA9IDAuNSAtIHgwICogeDAgLSB5MCAqIHkwO1xuICAgICAgaWYgKCEodDAgPCAwKSkge1xuICAgICAgICB0MCAqPSB0MDtcbiAgICAgICAgbjAgPSB0MCAqIHQwICogZG90KHRoaXMuZ3JhZDNbZ2kwXSwgeDAsIHkwKTtcbiAgICAgIH1cbiAgICAgIHQxID0gMC41IC0geDEgKiB4MSAtIHkxICogeTE7XG4gICAgICBpZiAoISh0MSA8IDApKSB7XG4gICAgICAgIHQxICo9IHQxO1xuICAgICAgICBuMSA9IHQxICogdDEgKiBkb3QodGhpcy5ncmFkM1tnaTFdLCB4MSwgeTEpO1xuICAgICAgfVxuICAgICAgdDIgPSAwLjUgLSB4MiAqIHgyIC0geTIgKiB5MjtcbiAgICAgIGlmICghKHQyIDwgMCkpIHtcbiAgICAgICAgdDIgKj0gdDI7XG4gICAgICAgIG4yID0gdDIgKiB0MiAqIGRvdCh0aGlzLmdyYWQzW2dpMl0sIHgyLCB5Mik7XG4gICAgICB9XG4gICAgICByZXR1cm4gNzAuMCAqIChuMCArIG4xICsgbjIpO1xuICAgIH07XG5cbiAgICBTaW1wbGV4Tm9pc2UucHJvdG90eXBlLm5vaXNlMyA9IGZ1bmN0aW9uKHhpbiwgeWluLCB6aW4pIHtcbiAgICAgIHZhciBGMywgRzMsIFgwLCBZMCwgWjAsIGdpMCwgZ2kxLCBnaTIsIGdpMywgaSwgaTEsIGkyLCBpaSwgaiwgajEsIGoyLCBqaiwgaywgazEsIGsyLCBraywgbjAsIG4xLCBuMiwgbjMsIHMsIHQsIHQwLCB0MSwgdDIsIHQzLCB4MCwgeDEsIHgyLCB4MywgeTAsIHkxLCB5MiwgeTMsIHowLCB6MSwgejIsIHozO1xuICAgICAgbjAgPSAwO1xuICAgICAgbjEgPSAwO1xuICAgICAgbjIgPSAwO1xuICAgICAgbjMgPSAwO1xuICAgICAgRjMgPSAxLjAgLyAzLjA7XG4gICAgICBzID0gKHhpbiArIHlpbiArIHppbikgKiBGMztcbiAgICAgIGkgPSBNYXRoLmZsb29yKHhpbiArIHMpO1xuICAgICAgaiA9IE1hdGguZmxvb3IoeWluICsgcyk7XG4gICAgICBrID0gTWF0aC5mbG9vcih6aW4gKyBzKTtcbiAgICAgIEczID0gMS4wIC8gNi4wO1xuICAgICAgdCA9IChpICsgaiArIGspICogRzM7XG4gICAgICBYMCA9IGkgLSB0O1xuICAgICAgWTAgPSBqIC0gdDtcbiAgICAgIFowID0gayAtIHQ7XG4gICAgICB4MCA9IHhpbiAtIFgwO1xuICAgICAgeTAgPSB5aW4gLSBZMDtcbiAgICAgIHowID0gemluIC0gWjA7XG4gICAgICBpMSA9IDA7XG4gICAgICBqMSA9IDA7XG4gICAgICBrMSA9IDA7XG4gICAgICBpMiA9IDA7XG4gICAgICBqMiA9IDA7XG4gICAgICBrMiA9IDA7XG4gICAgICBpZiAoeDAgPj0geTApIHtcbiAgICAgICAgaWYgKHkwID49IHowKSB7XG4gICAgICAgICAgaTEgPSAxO1xuICAgICAgICAgIGoxID0gMDtcbiAgICAgICAgICBrMSA9IDA7XG4gICAgICAgICAgaTIgPSAxO1xuICAgICAgICAgIGoyID0gMTtcbiAgICAgICAgICBrMiA9IDA7XG4gICAgICAgIH0gZWxzZSBpZiAoeDAgPj0gejApIHtcbiAgICAgICAgICBpMSA9IDE7XG4gICAgICAgICAgajEgPSAwO1xuICAgICAgICAgIGsxID0gMDtcbiAgICAgICAgICBpMiA9IDE7XG4gICAgICAgICAgajIgPSAwO1xuICAgICAgICAgIGsyID0gMTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICBpMSA9IDA7XG4gICAgICAgICAgajEgPSAwO1xuICAgICAgICAgIGsxID0gMTtcbiAgICAgICAgICBpMiA9IDE7XG4gICAgICAgICAgajIgPSAwO1xuICAgICAgICAgIGsyID0gMTtcbiAgICAgICAgfVxuICAgICAgfSBlbHNlIHtcbiAgICAgICAgaWYgKHkwIDwgejApIHtcbiAgICAgICAgICBpMSA9IDA7XG4gICAgICAgICAgajEgPSAwO1xuICAgICAgICAgIGsxID0gMTtcbiAgICAgICAgICBpMiA9IDA7XG4gICAgICAgICAgajIgPSAxO1xuICAgICAgICAgIGsyID0gMTtcbiAgICAgICAgfSBlbHNlIGlmICh4MCA8IHowKSB7XG4gICAgICAgICAgaTEgPSAwO1xuICAgICAgICAgIGoxID0gMTtcbiAgICAgICAgICBrMSA9IDA7XG4gICAgICAgICAgaTIgPSAwO1xuICAgICAgICAgIGoyID0gMTtcbiAgICAgICAgICBrMiA9IDE7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgaTEgPSAwO1xuICAgICAgICAgIGoxID0gMTtcbiAgICAgICAgICBrMSA9IDA7XG4gICAgICAgICAgaTIgPSAxO1xuICAgICAgICAgIGoyID0gMTtcbiAgICAgICAgICBrMiA9IDA7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICAgIHgxID0geDAgLSBpMSArIEczO1xuICAgICAgeTEgPSB5MCAtIGoxICsgRzM7XG4gICAgICB6MSA9IHowIC0gazEgKyBHMztcbiAgICAgIHgyID0geDAgLSBpMiArIDIuMCAqIEczO1xuICAgICAgeTIgPSB5MCAtIGoyICsgMi4wICogRzM7XG4gICAgICB6MiA9IHowIC0gazIgKyAyLjAgKiBHMztcbiAgICAgIHgzID0geDAgLSAxLjAgKyAzLjAgKiBHMztcbiAgICAgIHkzID0geTAgLSAxLjAgKyAzLjAgKiBHMztcbiAgICAgIHozID0gejAgLSAxLjAgKyAzLjAgKiBHMztcbiAgICAgIGlpID0gaSAmIDI1NTtcbiAgICAgIGpqID0gaiAmIDI1NTtcbiAgICAgIGtrID0gayAmIDI1NTtcbiAgICAgIGdpMCA9IHRoaXMucGVybVtpaSArIHRoaXMucGVybVtqaiArIHRoaXMucGVybVtra11dXSAlIDEyO1xuICAgICAgZ2kxID0gdGhpcy5wZXJtW2lpICsgaTEgKyB0aGlzLnBlcm1bamogKyBqMSArIHRoaXMucGVybVtrayArIGsxXV1dICUgMTI7XG4gICAgICBnaTIgPSB0aGlzLnBlcm1baWkgKyBpMiArIHRoaXMucGVybVtqaiArIGoyICsgdGhpcy5wZXJtW2trICsgazJdXV0gJSAxMjtcbiAgICAgIGdpMyA9IHRoaXMucGVybVtpaSArIDEgKyB0aGlzLnBlcm1bamogKyAxICsgdGhpcy5wZXJtW2trICsgMV1dXSAlIDEyO1xuICAgICAgdDAgPSAwLjYgLSB4MCAqIHgwIC0geTAgKiB5MCAtIHowICogejA7XG4gICAgICBpZiAoISh0MCA8IDApKSB7XG4gICAgICAgIHQwICo9IHQwO1xuICAgICAgICBuMCA9IHQwICogdDAgKiBkb3QodGhpcy5ncmFkM1tnaTBdLCB4MCwgeTAsIHowKTtcbiAgICAgIH1cbiAgICAgIHQxID0gMC42IC0geDEgKiB4MSAtIHkxICogeTEgLSB6MSAqIHoxO1xuICAgICAgaWYgKCEodDEgPCAwKSkge1xuICAgICAgICB0MSAqPSB0MTtcbiAgICAgICAgbjEgPSB0MSAqIHQxICogZG90KHRoaXMuZ3JhZDNbZ2kxXSwgeDEsIHkxLCB6MSk7XG4gICAgICB9XG4gICAgICB0MiA9IDAuNiAtIHgyICogeDIgLSB5MiAqIHkyIC0gejIgKiB6MjtcbiAgICAgIGlmICghKHQyIDwgMCkpIHtcbiAgICAgICAgdDIgKj0gdDI7XG4gICAgICAgIG4yID0gdDIgKiB0MiAqIGRvdCh0aGlzLmdyYWQzW2dpMl0sIHgyLCB5MiwgejIpO1xuICAgICAgfVxuICAgICAgdDMgPSAwLjYgLSB4MyAqIHgzIC0geTMgKiB5MyAtIHozICogejM7XG4gICAgICBpZiAoISh0MyA8IDApKSB7XG4gICAgICAgIHQzICo9IHQzO1xuICAgICAgICBuMyA9IHQzICogdDMgKiBkb3QodGhpcy5ncmFkM1tnaTNdLCB4MywgeTMsIHozKTtcbiAgICAgIH1cbiAgICAgIHJldHVybiAzMi4wICogKG4wICsgbjEgKyBuMiArIG4zKTtcbiAgICB9O1xuXG4gICAgU2ltcGxleE5vaXNlLnByb3RvdHlwZS5ub2lzZTQgPSBmdW5jdGlvbih4LCB5LCB6LCB3KSB7XG4gICAgICB2YXIgRjQsIEc0LCBXMCwgWDAsIFkwLCBaMCwgYywgYzEsIGMyLCBjMywgYzQsIGM1LCBjNiwgZ2kwLCBnaTEsIGdpMiwgZ2kzLCBnaTQsIGdyYWQ0LCBpLCBpMSwgaTIsIGkzLCBpaSwgaiwgajEsIGoyLCBqMywgamosIGssIGsxLCBrMiwgazMsIGtrLCBsLCBsMSwgbDIsIGwzLCBsbCwgbjAsIG4xLCBuMiwgbjMsIG40LCBwZXJtLCBzLCBzaW1wbGV4LCB0LCB0MCwgdDEsIHQyLCB0MywgdDQsIHcwLCB3MSwgdzIsIHczLCB3NCwgeDAsIHgxLCB4MiwgeDMsIHg0LCB5MCwgeTEsIHkyLCB5MywgeTQsIHowLCB6MSwgejIsIHozLCB6NDtcbiAgICAgIGdyYWQ0ID0gdGhpcy5ncmFkNDtcbiAgICAgIHNpbXBsZXggPSB0aGlzLnNpbXBsZXg7XG4gICAgICBwZXJtID0gdGhpcy5wZXJtO1xuICAgICAgRjQgPSAoTWF0aC5zcXJ0KDUuMCkgLSAxLjApIC8gNC4wO1xuICAgICAgRzQgPSAoNS4wIC0gTWF0aC5zcXJ0KDUuMCkpIC8gMjAuMDtcbiAgICAgIG4wID0gMDtcbiAgICAgIG4xID0gMDtcbiAgICAgIG4yID0gMDtcbiAgICAgIG4zID0gMDtcbiAgICAgIG40ID0gMDtcbiAgICAgIHMgPSAoeCArIHkgKyB6ICsgdykgKiBGNDtcbiAgICAgIGkgPSBNYXRoLmZsb29yKHggKyBzKTtcbiAgICAgIGogPSBNYXRoLmZsb29yKHkgKyBzKTtcbiAgICAgIGsgPSBNYXRoLmZsb29yKHogKyBzKTtcbiAgICAgIGwgPSBNYXRoLmZsb29yKHcgKyBzKTtcbiAgICAgIHQgPSAoaSArIGogKyBrICsgbCkgKiBHNDtcbiAgICAgIFgwID0gaSAtIHQ7XG4gICAgICBZMCA9IGogLSB0O1xuICAgICAgWjAgPSBrIC0gdDtcbiAgICAgIFcwID0gbCAtIHQ7XG4gICAgICB4MCA9IHggLSBYMDtcbiAgICAgIHkwID0geSAtIFkwO1xuICAgICAgejAgPSB6IC0gWjA7XG4gICAgICB3MCA9IHcgLSBXMDtcbiAgICAgIGMxID0gKHgwID4geTAgPyAzMiA6IDApO1xuICAgICAgYzIgPSAoeDAgPiB6MCA/IDE2IDogMCk7XG4gICAgICBjMyA9ICh5MCA+IHowID8gOCA6IDApO1xuICAgICAgYzQgPSAoeDAgPiB3MCA/IDQgOiAwKTtcbiAgICAgIGM1ID0gKHkwID4gdzAgPyAyIDogMCk7XG4gICAgICBjNiA9ICh6MCA+IHcwID8gMSA6IDApO1xuICAgICAgYyA9IGMxICsgYzIgKyBjMyArIGM0ICsgYzUgKyBjNjtcbiAgICAgIGkxID0gMDtcbiAgICAgIGoxID0gMDtcbiAgICAgIGsxID0gMDtcbiAgICAgIGwxID0gMDtcbiAgICAgIGkyID0gMDtcbiAgICAgIGoyID0gMDtcbiAgICAgIGsyID0gMDtcbiAgICAgIGwyID0gMDtcbiAgICAgIGkzID0gMDtcbiAgICAgIGozID0gMDtcbiAgICAgIGszID0gMDtcbiAgICAgIGwzID0gMDtcbiAgICAgIGkxID0gKHNpbXBsZXhbY11bMF0gPj0gMyA/IDEgOiAwKTtcbiAgICAgIGoxID0gKHNpbXBsZXhbY11bMV0gPj0gMyA/IDEgOiAwKTtcbiAgICAgIGsxID0gKHNpbXBsZXhbY11bMl0gPj0gMyA/IDEgOiAwKTtcbiAgICAgIGwxID0gKHNpbXBsZXhbY11bM10gPj0gMyA/IDEgOiAwKTtcbiAgICAgIGkyID0gKHNpbXBsZXhbY11bMF0gPj0gMiA/IDEgOiAwKTtcbiAgICAgIGoyID0gKHNpbXBsZXhbY11bMV0gPj0gMiA/IDEgOiAwKTtcbiAgICAgIGsyID0gKHNpbXBsZXhbY11bMl0gPj0gMiA/IDEgOiAwKTtcbiAgICAgIGwyID0gKHNpbXBsZXhbY11bM10gPj0gMiA/IDEgOiAwKTtcbiAgICAgIGkzID0gKHNpbXBsZXhbY11bMF0gPj0gMSA/IDEgOiAwKTtcbiAgICAgIGozID0gKHNpbXBsZXhbY11bMV0gPj0gMSA/IDEgOiAwKTtcbiAgICAgIGszID0gKHNpbXBsZXhbY11bMl0gPj0gMSA/IDEgOiAwKTtcbiAgICAgIGwzID0gKHNpbXBsZXhbY11bM10gPj0gMSA/IDEgOiAwKTtcbiAgICAgIHgxID0geDAgLSBpMSArIEc0O1xuICAgICAgeTEgPSB5MCAtIGoxICsgRzQ7XG4gICAgICB6MSA9IHowIC0gazEgKyBHNDtcbiAgICAgIHcxID0gdzAgLSBsMSArIEc0O1xuICAgICAgeDIgPSB4MCAtIGkyICsgMi4wICogRzQ7XG4gICAgICB5MiA9IHkwIC0gajIgKyAyLjAgKiBHNDtcbiAgICAgIHoyID0gejAgLSBrMiArIDIuMCAqIEc0O1xuICAgICAgdzIgPSB3MCAtIGwyICsgMi4wICogRzQ7XG4gICAgICB4MyA9IHgwIC0gaTMgKyAzLjAgKiBHNDtcbiAgICAgIHkzID0geTAgLSBqMyArIDMuMCAqIEc0O1xuICAgICAgejMgPSB6MCAtIGszICsgMy4wICogRzQ7XG4gICAgICB3MyA9IHcwIC0gbDMgKyAzLjAgKiBHNDtcbiAgICAgIHg0ID0geDAgLSAxLjAgKyA0LjAgKiBHNDtcbiAgICAgIHk0ID0geTAgLSAxLjAgKyA0LjAgKiBHNDtcbiAgICAgIHo0ID0gejAgLSAxLjAgKyA0LjAgKiBHNDtcbiAgICAgIHc0ID0gdzAgLSAxLjAgKyA0LjAgKiBHNDtcbiAgICAgIGlpID0gaSAmIDI1NTtcbiAgICAgIGpqID0gaiAmIDI1NTtcbiAgICAgIGtrID0gayAmIDI1NTtcbiAgICAgIGxsID0gbCAmIDI1NTtcbiAgICAgIGdpMCA9IHBlcm1baWkgKyBwZXJtW2pqICsgcGVybVtrayArIHBlcm1bbGxdXV1dICUgMzI7XG4gICAgICBnaTEgPSBwZXJtW2lpICsgaTEgKyBwZXJtW2pqICsgajEgKyBwZXJtW2trICsgazEgKyBwZXJtW2xsICsgbDFdXV1dICUgMzI7XG4gICAgICBnaTIgPSBwZXJtW2lpICsgaTIgKyBwZXJtW2pqICsgajIgKyBwZXJtW2trICsgazIgKyBwZXJtW2xsICsgbDJdXV1dICUgMzI7XG4gICAgICBnaTMgPSBwZXJtW2lpICsgaTMgKyBwZXJtW2pqICsgajMgKyBwZXJtW2trICsgazMgKyBwZXJtW2xsICsgbDNdXV1dICUgMzI7XG4gICAgICBnaTQgPSBwZXJtW2lpICsgMSArIHBlcm1bamogKyAxICsgcGVybVtrayArIDEgKyBwZXJtW2xsICsgMV1dXV0gJSAzMjtcbiAgICAgIHQwID0gMC42IC0geDAgKiB4MCAtIHkwICogeTAgLSB6MCAqIHowIC0gdzAgKiB3MDtcbiAgICAgIGlmICghKHQwIDwgMCkpIHtcbiAgICAgICAgdDAgKj0gdDA7XG4gICAgICAgIG4wID0gdDAgKiB0MCAqIGRvdChncmFkNFtnaTBdLCB4MCwgeTAsIHowLCB3MCk7XG4gICAgICB9XG4gICAgICB0MSA9IDAuNiAtIHgxICogeDEgLSB5MSAqIHkxIC0gejEgKiB6MSAtIHcxICogdzE7XG4gICAgICBpZiAoISh0MSA8IDApKSB7XG4gICAgICAgIHQxICo9IHQxO1xuICAgICAgICBuMSA9IHQxICogdDEgKiBkb3QoZ3JhZDRbZ2kxXSwgeDEsIHkxLCB6MSwgdzEpO1xuICAgICAgfVxuICAgICAgdDIgPSAwLjYgLSB4MiAqIHgyIC0geTIgKiB5MiAtIHoyICogejIgLSB3MiAqIHcyO1xuICAgICAgaWYgKCEodDIgPCAwKSkge1xuICAgICAgICB0MiAqPSB0MjtcbiAgICAgICAgbjIgPSB0MiAqIHQyICogZG90KGdyYWQ0W2dpMl0sIHgyLCB5MiwgejIsIHcyKTtcbiAgICAgIH1cbiAgICAgIHQzID0gMC42IC0geDMgKiB4MyAtIHkzICogeTMgLSB6MyAqIHozIC0gdzMgKiB3MztcbiAgICAgIGlmICghKHQzIDwgMCkpIHtcbiAgICAgICAgdDMgKj0gdDM7XG4gICAgICAgIG4zID0gdDMgKiB0MyAqIGRvdChncmFkNFtnaTNdLCB4MywgeTMsIHozLCB3Myk7XG4gICAgICB9XG4gICAgICB0NCA9IDAuNiAtIHg0ICogeDQgLSB5NCAqIHk0IC0gejQgKiB6NCAtIHc0ICogdzQ7XG4gICAgICBpZiAoISh0NCA8IDApKSB7XG4gICAgICAgIHQ0ICo9IHQ0O1xuICAgICAgICBuNCA9IHQ0ICogdDQgKiBkb3QoZ3JhZDRbZ2k0XSwgeDQsIHk0LCB6NCwgdzQpO1xuICAgICAgfVxuICAgICAgcmV0dXJuIDI3LjAgKiAobjAgKyBuMSArIG4yICsgbjMgKyBuNCk7XG4gICAgfTtcblxuICAgIHJldHVybiBTaW1wbGV4Tm9pc2U7XG5cbiAgfSkoTm9pc2UpO1xuXG4gIEZsb3dOb2lzZSA9IChmdW5jdGlvbihfc3VwZXIpIHtcbiAgICB2YXIgVFdPX1BJLCBuO1xuXG4gICAgX19leHRlbmRzKEZsb3dOb2lzZSwgX3N1cGVyKTtcblxuICAgIG4gPSAxMjg7XG5cbiAgICBUV09fUEkgPSBNYXRoLlBJICogMjtcblxuICAgIEZsb3dOb2lzZS5wcm90b3R5cGUuYmFzaXMgPSBbXTtcblxuICAgIEZsb3dOb2lzZS5wcm90b3R5cGUucGVybSA9IFtdO1xuXG4gICAgZnVuY3Rpb24gRmxvd05vaXNlKHJuZykge1xuICAgICAgdmFyIGksIHRoZXRhLCBfaTtcbiAgICAgIGlmIChybmcgPT0gbnVsbCkge1xuICAgICAgICBybmcgPSBNYXRoO1xuICAgICAgfVxuICAgICAgZm9yIChpID0gX2kgPSAwOyAwIDw9IG4gPyBfaSA8PSBuIDogX2kgPj0gbjsgaSA9IDAgPD0gbiA/ICsrX2kgOiAtLV9pKSB7XG4gICAgICAgIHRoZXRhID0gaSAqIFRXT19QSSAvIG47XG4gICAgICAgIHRoaXMuYmFzaXNbaV0gPSBbTWF0aC5jb3ModGhldGEsIE1hdGguc2luKHRoZXRhKSldO1xuICAgICAgICB0aGlzLnBlcm1baV0gPSBpO1xuICAgICAgfVxuICAgICAgcmVpbml0aWFsaXplKChybmcucmFuZG9tKCkgKiAxMDAwKSB8IDApO1xuICAgIH1cblxuICAgIEZsb3dOb2lzZS5wcm90b3R5cGUucmVpbml0aWFsaXplID0gZnVuY3Rpb24oc2VlZCkge1xuICAgICAgdmFyIGksIGosIF9pLCBfcmVzdWx0cztcbiAgICAgIF9yZXN1bHRzID0gW107XG4gICAgICBmb3IgKGkgPSBfaSA9IDE7IDEgPD0gbiA/IF9pIDw9IG4gOiBfaSA+PSBuOyBpID0gMSA8PSBuID8gKytfaSA6IC0tX2kpIHtcbiAgICAgICAgaiA9IChNYXRoLnJhbmRvbSgpICogc2VlZCkgJSAoaSArIDEpO1xuICAgICAgICBfcmVzdWx0cy5wdXNoKHNlZWQgKz0gMSk7XG4gICAgICB9XG4gICAgICByZXR1cm4gX3Jlc3VsdHM7XG4gICAgfTtcblxuICAgIHJldHVybiBGbG93Tm9pc2U7XG5cbiAgfSkoTm9pc2UpO1xuXG4gIG1vZHVsZS5leHBvcnRzID0ge1xuICAgIFNpbXBsZXhOb2lzZTogU2ltcGxleE5vaXNlXG4gIH07XG5cbn0pLmNhbGwodGhpcyk7XG4iLCIvLyBHZW5lcmF0ZWQgYnkgQ29mZmVlU2NyaXB0IDEuNi4zXG4vKlxuXG4gUmFuZG9tIGJhc2VkIG9uIEpvbiBXYXR0ZSdzIG1lcnNlbm5lIHR3aXN0ZXIgcGFja2FnZS5cbiBBZGRzIHNldmVyYWwgdXRpbGl0eSBtZXRob2RzIHVuZGVyIGEgdW5pZmllZCBpbnRlcmZhY2UsIHRoYXQnbGwgYWxsb3cgdG8gdXNlXG4gZGlmZmVyZW50IG51bWJlciBnZW5lcmF0b3JzIGF0IHNvbWUgcG9pbnQuXG4qL1xuXG5cbihmdW5jdGlvbigpIHtcbiAgdmFyIFJhbmRvbSwgbWVyc2VubmUsIHV0aWw7XG5cbiAgbWVyc2VubmUgPSByZXF1aXJlKCdtZXJzZW5uZScpO1xuXG4gIHV0aWwgPSByZXF1aXJlKCcuLi91dGlsJyk7XG5cbiAgUmFuZG9tID0gKGZ1bmN0aW9uKCkge1xuICAgIGZ1bmN0aW9uIFJhbmRvbShpbml0aWFsU2VlZCkge1xuICAgICAgaWYgKGluaXRpYWxTZWVkID09IG51bGwpIHtcbiAgICAgICAgaW5pdGlhbFNlZWQgPSAwO1xuICAgICAgfVxuICAgICAgdGhpcy5zZWVkKGluaXRpYWxTZWVkKTtcbiAgICB9XG5cbiAgICBSYW5kb20ucHJvdG90eXBlLnNlZWQgPSBmdW5jdGlvbih2YWx1ZSkge1xuICAgICAgdGhpcy5zZWVkVmFsdWUgPSB2YWx1ZTtcbiAgICAgIHJldHVybiBtZXJzZW5uZS5zZWVkKHRoaXMuc2VlZFZhbHVlKTtcbiAgICB9O1xuXG4gICAgUmFuZG9tLnByb3RvdHlwZS50b1N0cmluZyA9IGZ1bmN0aW9uKCkge1xuICAgICAgcmV0dXJuIFwiUmFuZG9tKFwiICsgdGhpcy5zZWVkVmFsdWUgKyBcIilcIjtcbiAgICB9O1xuXG4gICAgUmFuZG9tLnByb3RvdHlwZS5yYW5kb20gPSBmdW5jdGlvbigpIHtcbiAgICAgIHZhciBrO1xuICAgICAgayA9IDEwMDAwMDA7XG4gICAgICByZXR1cm4gbWVyc2VubmUucmFuZChrKSAvIGs7XG4gICAgfTtcblxuICAgIFJhbmRvbS5wcm90b3R5cGUucmFuZGkgPSBmdW5jdGlvbihtaW4sIG1heCkge1xuICAgICAgcmV0dXJuIE1hdGguZmxvb3IodGhpcy5yYW5kb20oKSAqIChtYXggLSBtaW4pICsgbWluKTtcbiAgICB9O1xuXG4gICAgUmFuZG9tLnByb3RvdHlwZS5yYW5kZiA9IGZ1bmN0aW9uKG1pbiwgbWF4KSB7XG4gICAgICByZXR1cm4gdGhpcy5yYW5kb20oKSAqIChtYXggLSBtaW4pICsgbWluO1xuICAgIH07XG5cbiAgICBSYW5kb20ucHJvdG90eXBlLnJhbmRuID0gZnVuY3Rpb24oKSB7XG4gICAgICByZXR1cm4gdGhpcy5yYW5kb20oKSAqIDIgLSAxO1xuICAgIH07XG5cbiAgICBSYW5kb20ucHJvdG90eXBlLmZsaXBDb2luID0gZnVuY3Rpb24oY2hhbmNlKSB7XG4gICAgICBpZiAoY2hhbmNlID09IG51bGwpIHtcbiAgICAgICAgY2hhbmNlID0gMC41O1xuICAgICAgfVxuICAgICAgcmV0dXJuIHRoaXMucmFuZG9tKCkgPCBjaGFuY2U7XG4gICAgfTtcblxuICAgIFJhbmRvbS5wcm90b3R5cGUuc2h1ZmZsZSA9IGZ1bmN0aW9uKGxpc3QpIHtcbiAgICAgIHJldHVybiBsaXN0LnNvcnQoZnVuY3Rpb24oKSB7XG4gICAgICAgIHJldHVybiAwLjUgLSB0aGlzLnJhbmRvbSgpO1xuICAgICAgfSk7XG4gICAgfTtcblxuICAgIFJhbmRvbS5wcm90b3R5cGUucGljayA9IGZ1bmN0aW9uKGxpc3QsIGNvdW50KSB7XG4gICAgICB2YXIgaSwgaW5kaWNlcywgcmVzdWx0LCBfaSwgX2osIF9yZWY7XG4gICAgICBpZiAoY291bnQgPT0gbnVsbCkge1xuICAgICAgICBjb3VudCA9IDE7XG4gICAgICB9XG4gICAgICBzd2l0Y2ggKGxpc3QubGVuZ3RoKSB7XG4gICAgICAgIGNhc2UgMDpcbiAgICAgICAgICByZXR1cm4gbnVsbDtcbiAgICAgICAgY2FzZSAxOlxuICAgICAgICAgIHJldHVybiBsaXN0WzBdO1xuICAgICAgICBkZWZhdWx0OlxuICAgICAgICAgIGlmIChjb3VudCA9PT0gMSkge1xuICAgICAgICAgICAgcmV0dXJuIGxpc3RbdGhpcy5yYW5kaSgwLCBsaXN0Lmxlbmd0aCldO1xuICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICBpbmRpY2VzID0gW107XG4gICAgICAgICAgICBmb3IgKGkgPSBfaSA9IDAsIF9yZWYgPSBsaXN0Lmxlbmd0aDsgMCA8PSBfcmVmID8gX2kgPCBfcmVmIDogX2kgPiBfcmVmOyBpID0gMCA8PSBfcmVmID8gKytfaSA6IC0tX2kpIHtcbiAgICAgICAgICAgICAgaW5kaWNlcy5wdXNoKGkpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgdXRpbC5zaHVmZmxlKGluZGljZXMpO1xuICAgICAgICAgICAgcmVzdWx0ID0gW107XG4gICAgICAgICAgICBmb3IgKGkgPSBfaiA9IDA7IDAgPD0gY291bnQgPyBfaiA8IGNvdW50IDogX2ogPiBjb3VudDsgaSA9IDAgPD0gY291bnQgPyArK19qIDogLS1faikge1xuICAgICAgICAgICAgICByZXN1bHQucHVzaChsaXN0W2luZGljZXNbaV1dKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHJldHVybiByZXN1bHQ7XG4gICAgICAgICAgfVxuICAgICAgfVxuICAgIH07XG5cbiAgICBSYW5kb20ucHJvdG90eXBlLnBpY2tBQiA9IGZ1bmN0aW9uKGxpc3RBLCBsaXN0QiwgY2hhbmNlKSB7XG4gICAgICB2YXIgbGlzdDtcbiAgICAgIGxpc3QgPSB0aGlzLm5leHQoKSA8IGNoYW5jZSA/IGxpc3RBIDogbGlzdEI7XG4gICAgICByZXR1cm4gdGhpcy5waWNrKGxpc3QpO1xuICAgIH07XG5cbiAgICByZXR1cm4gUmFuZG9tO1xuXG4gIH0pKCk7XG5cbiAgbW9kdWxlLmV4cG9ydHMuUmFuZG9tID0gUmFuZG9tO1xuXG59KS5jYWxsKHRoaXMpO1xuIiwiLy8gR2VuZXJhdGVkIGJ5IENvZmZlZVNjcmlwdCAxLjYuM1xuKGZ1bmN0aW9uKCkge1xuICB2YXIgUmVjdCwgVmVjMjtcblxuICBWZWMyID0gcmVxdWlyZShcIi4vdmVjdG9yXCIpLlZlYzI7XG5cbiAgUmVjdCA9IChmdW5jdGlvbigpIHtcbiAgICBSZWN0LnByb3RvdHlwZS54MSA9IDA7XG5cbiAgICBSZWN0LnByb3RvdHlwZS55MSA9IDA7XG5cbiAgICBSZWN0LnByb3RvdHlwZS54MiA9IDA7XG5cbiAgICBSZWN0LnByb3RvdHlwZS55MiA9IDA7XG5cbiAgICBmdW5jdGlvbiBSZWN0KHgxLCB5MSwgeDIsIHkyKSB7XG4gICAgICB0aGlzLngxID0geDEgIT0gbnVsbCA/IHgxIDogMDtcbiAgICAgIHRoaXMueTEgPSB5MSAhPSBudWxsID8geTEgOiAwO1xuICAgICAgdGhpcy54MiA9IHgyICE9IG51bGwgPyB4MiA6IDA7XG4gICAgICB0aGlzLnkyID0geTIgIT0gbnVsbCA/IHkyIDogMDtcbiAgICB9XG5cbiAgICBSZWN0LnByb3RvdHlwZS5jZW50ZXIgPSBmdW5jdGlvbigpIHtcbiAgICAgIHZhciB4LCB5O1xuICAgICAgeCA9IE1hdGgubWluKHRoaXMueDEsIHRoaXMueDIpICsgdGhpcy53aWR0aCgpICogMC41O1xuICAgICAgeSA9IE1hdGgubWluKHRoaXMueTEsIHRoaXMueTIpICsgdGhpcy5oZWlnaHQoKSAqIDAuNTtcbiAgICAgIHJldHVybiBuZXcgVmVjMih4LCB5KTtcbiAgICB9O1xuXG4gICAgUmVjdC5wcm90b3R5cGUud2lkdGggPSBmdW5jdGlvbigpIHtcbiAgICAgIHJldHVybiBNYXRoLmFicyh0aGlzLngxIC0gdGhpcy54Mik7XG4gICAgfTtcblxuICAgIFJlY3QucHJvdG90eXBlLmhlaWdodCA9IGZ1bmN0aW9uKCkge1xuICAgICAgcmV0dXJuIE1hdGguYWJzKHRoaXMueTEgLSB0aGlzLnkyKTtcbiAgICB9O1xuXG4gICAgcmV0dXJuIFJlY3Q7XG5cbiAgfSkoKTtcblxuICBtb2R1bGUuZXhwb3J0cy5SZWN0ID0gUmVjdDtcblxufSkuY2FsbCh0aGlzKTtcbiIsIi8vIEdlbmVyYXRlZCBieSBDb2ZmZWVTY3JpcHQgMS42LjNcbihmdW5jdGlvbigpIHtcbiAgdmFyIFZlYzIsIFZlYzM7XG5cbiAgVmVjMiA9IChmdW5jdGlvbigpIHtcbiAgICBWZWMyLnByb3RvdHlwZS54ID0gMDtcblxuICAgIFZlYzIucHJvdG90eXBlLnkgPSAwO1xuXG4gICAgZnVuY3Rpb24gVmVjMih4LCB5KSB7XG4gICAgICB0aGlzLnggPSB4ICE9IG51bGwgPyB4IDogMDtcbiAgICAgIHRoaXMueSA9IHkgIT0gbnVsbCA/IHkgOiAwO1xuICAgIH1cblxuICAgIFZlYzIucHJvdG90eXBlLnNldCA9IGZ1bmN0aW9uKHYpIHtcbiAgICAgIHRoaXMueCA9IHYueDtcbiAgICAgIHRoaXMueSA9IHYueTtcbiAgICAgIHJldHVybiB0aGlzO1xuICAgIH07XG5cbiAgICBWZWMyLnByb3RvdHlwZS5zZXQyID0gZnVuY3Rpb24oeCwgeSkge1xuICAgICAgdGhpcy54ID0geDtcbiAgICAgIHRoaXMueSA9IHk7XG4gICAgICByZXR1cm4gdGhpcztcbiAgICB9O1xuXG4gICAgVmVjMi5wcm90b3R5cGUuemVybyA9IGZ1bmN0aW9uKCkge1xuICAgICAgdGhpcy54ID0gdGhpcy55ID0gMDtcbiAgICAgIHJldHVybiB0aGlzO1xuICAgIH07XG5cbiAgICBWZWMyLnByb3RvdHlwZS5hZGQgPSBmdW5jdGlvbih2KSB7XG4gICAgICB0aGlzLnggKz0gdi54O1xuICAgICAgdGhpcy55ICs9IHYueTtcbiAgICAgIHJldHVybiB0aGlzO1xuICAgIH07XG5cbiAgICBWZWMyLnByb3RvdHlwZS5hZGRfID0gZnVuY3Rpb24odikge1xuICAgICAgcmV0dXJuIG5ldyBWZWMyKHRoaXMueCArIHYueCwgdGhpcy55ICsgdi55KTtcbiAgICB9O1xuXG4gICAgVmVjMi5wcm90b3R5cGUuc3ViID0gZnVuY3Rpb24odikge1xuICAgICAgdGhpcy54IC09IHYueDtcbiAgICAgIHRoaXMueSAtPSB2Lnk7XG4gICAgICByZXR1cm4gdGhpcztcbiAgICB9O1xuXG4gICAgVmVjMi5wcm90b3R5cGUuc3ViXyA9IGZ1bmN0aW9uKHYpIHtcbiAgICAgIHJldHVybiBuZXcgVmVjMih0aGlzLnggLSB2LngsIHRoaXMueSAtIHYueSk7XG4gICAgfTtcblxuICAgIFZlYzIucHJvdG90eXBlLm11bCA9IGZ1bmN0aW9uKHYpIHtcbiAgICAgIHRoaXMueCAqPSB2Lng7XG4gICAgICB0aGlzLnkgKj0gdi55O1xuICAgICAgcmV0dXJuIHRoaXM7XG4gICAgfTtcblxuICAgIFZlYzIucHJvdG90eXBlLm11bF8gPSBmdW5jdGlvbih2KSB7XG4gICAgICByZXR1cm4gbmV3IFZlYzIodGhpcy54ICogdi54LCB0aGlzLnkgKiB2LnkpO1xuICAgIH07XG5cbiAgICBWZWMyLnByb3RvdHlwZS5kaXYgPSBmdW5jdGlvbih2KSB7XG4gICAgICB0aGlzLnggLz0gdi54O1xuICAgICAgdGhpcy55IC89IHYueTtcbiAgICAgIHJldHVybiB0aGlzO1xuICAgIH07XG5cbiAgICBWZWMyLnByb3RvdHlwZS5kaXZfID0gZnVuY3Rpb24odikge1xuICAgICAgcmV0dXJuIG5ldyBWZWMyKHRoaXMueCAvIHYueCwgdGhpcy55IC8gdi55KTtcbiAgICB9O1xuXG4gICAgVmVjMi5wcm90b3R5cGUuc2NhbGUgPSBmdW5jdGlvbih2YWx1ZSkge1xuICAgICAgdGhpcy54ICo9IHZhbHVlO1xuICAgICAgdGhpcy55ICo9IHZhbHVlO1xuICAgICAgcmV0dXJuIHRoaXM7XG4gICAgfTtcblxuICAgIFZlYzIucHJvdG90eXBlLnNjYWxlXyA9IGZ1bmN0aW9uKHZhbHVlKSB7XG4gICAgICByZXR1cm4gbmV3IFZlYzIodGhpcy54ICogdmFsdWUsIHRoaXMueSAqIHZhbHVlKTtcbiAgICB9O1xuXG4gICAgVmVjMi5wcm90b3R5cGUubGVuZ3RoID0gZnVuY3Rpb24oKSB7XG4gICAgICByZXR1cm4gTWF0aC5zcXJ0KHRoaXMueCAqIHRoaXMueCArIHRoaXMueSAqIHRoaXMueSk7XG4gICAgfTtcblxuICAgIFZlYzIucHJvdG90eXBlLmxlbmd0aFNxdWFyZWQgPSBmdW5jdGlvbigpIHtcbiAgICAgIHJldHVybiB0aGlzLnggKiB0aGlzLnggKyB0aGlzLnkgKiB0aGlzLnk7XG4gICAgfTtcblxuICAgIFZlYzIucHJvdG90eXBlLm5vcm1hbGl6ZSA9IGZ1bmN0aW9uKCkge1xuICAgICAgdmFyIGw7XG4gICAgICBsID0gdGhpcy5sZW5ndGgoKTtcbiAgICAgIGlmIChsICE9PSAwKSB7XG4gICAgICAgIHRoaXMueCAvPSBsO1xuICAgICAgICB0aGlzLnkgLz0gbDtcbiAgICAgIH1cbiAgICAgIHJldHVybiB0aGlzO1xuICAgIH07XG5cbiAgICBWZWMyLnByb3RvdHlwZS5ub3JtYWxpemVfID0gZnVuY3Rpb24oKSB7XG4gICAgICByZXR1cm4gdGhpcy5jbG9uZSgpLm5vcm1hbGl6ZSgpO1xuICAgIH07XG5cbiAgICBWZWMyLnByb3RvdHlwZS5ub3JtYWxpemVUbyA9IGZ1bmN0aW9uKGxlbmd0aCkge1xuICAgICAgdmFyIG1hZ25pdHVkZTtcbiAgICAgIG1hZ25pdHVkZSA9IE1hdGguc3FydCh0aGlzLnggKiB0aGlzLnggKyB0aGlzLnkgKiB0aGlzLnkpO1xuICAgICAgaWYgKG1hZ25pdHVkZSA+IDApIHtcbiAgICAgICAgbWFnbml0dWRlID0gbGVuZ3RoIC8gbWFnbml0dWRlO1xuICAgICAgICB0aGlzLnggKj0gbWFnbml0dWRlO1xuICAgICAgICB0aGlzLnkgKj0gbWFnbml0dWRlO1xuICAgICAgfVxuICAgICAgcmV0dXJuIHRoaXM7XG4gICAgfTtcblxuICAgIFZlYzIucHJvdG90eXBlLm5vcm1hbGl6ZVRvXyA9IGZ1bmN0aW9uKGxlbmd0aCkge1xuICAgICAgcmV0dXJuIHRoaXMuY2xvbmUoKS5ub3JtYWxpemVUb18obGVuZ3RoKTtcbiAgICB9O1xuXG4gICAgVmVjMi5wcm90b3R5cGUuZGlzdGFuY2UgPSBmdW5jdGlvbih2KSB7XG4gICAgICByZXR1cm4gTWF0aC5zcXJ0KHRoaXMuZGlzdGFuY2VTcXVhcmVkMih2LngsIHYueSkpO1xuICAgIH07XG5cbiAgICBWZWMyLnByb3RvdHlwZS5kaXN0YW5jZVNxdWFyZWQgPSBmdW5jdGlvbih2KSB7XG4gICAgICByZXR1cm4gdGhpcy5kaXN0YW5jZVNxdWFyZWQyKHYueCwgdi55KTtcbiAgICB9O1xuXG4gICAgVmVjMi5wcm90b3R5cGUuZGlzdGFuY2VTcXVhcmVkMiA9IGZ1bmN0aW9uKHgsIHkpIHtcbiAgICAgIHZhciBkeCwgZHk7XG4gICAgICBkeCA9IHRoaXMueCAtIHg7XG4gICAgICBkeSA9IHRoaXMueSAtIHk7XG4gICAgICByZXR1cm4gZHggKiBkeCArIGR5ICogZHk7XG4gICAgfTtcblxuICAgIFZlYzIucHJvdG90eXBlLmRvdCA9IGZ1bmN0aW9uKHYpIHtcbiAgICAgIHJldHVybiB0aGlzLnggKiB2LnggKyB0aGlzLnkgKiB2Lnk7XG4gICAgfTtcblxuICAgIFZlYzIucHJvdG90eXBlLnJvdGF0ZSA9IGZ1bmN0aW9uKGFuZ2xlKSB7XG4gICAgICB2YXIgY29zYSwgcngsIHJ5LCBzaW5hO1xuICAgICAgc2luYSA9IE1hdGguc2luKGFuZ2xlKTtcbiAgICAgIGNvc2EgPSBNYXRoLmNvcyhhbmdsZSk7XG4gICAgICByeCA9IHRoaXMueCAqIGNvc2EgLSB0aGlzLnkgKiBzaW5hO1xuICAgICAgcnkgPSB0aGlzLnggKiBzaW5hICsgdGhpcy55ICogY29zYTtcbiAgICAgIHRoaXMueCA9IHJ4O1xuICAgICAgcmV0dXJuIHRoaXMueSA9IHJ5O1xuICAgIH07XG5cbiAgICBWZWMyLnByb3RvdHlwZS5qaXR0ZXIgPSBmdW5jdGlvbihhbW91bnQpIHtcbiAgICAgIHRoaXMueCArPSAoTWF0aC5yYW5kb20oKSAqIDIgLSAxKSAqIGFtb3VudDtcbiAgICAgIHRoaXMueSArPSAoTWF0aC5yYW5kb20oKSAqIDIgLSAxKSAqIGFtb3VudDtcbiAgICAgIHJldHVybiB0aGlzO1xuICAgIH07XG5cbiAgICBWZWMyLnByb3RvdHlwZS5qaXR0ZXJfID0gZnVuY3Rpb24oYW1vdW50KSB7XG4gICAgICByZXR1cm4gKG5ldyBWZWMyKHRoaXMueCwgdGhpcy55KSkuaml0dGVyKGFtb3VudCk7XG4gICAgfTtcblxuICAgIFZlYzIucHJvdG90eXBlLmxlcnAgPSBmdW5jdGlvbih0YXJnZXQsIGRlbHRhKSB7XG4gICAgICB0aGlzLnggPSB0aGlzLnggKiAoMSAtIGRlbHRhKSArIHRhcmdldC54ICogZGVsdGE7XG4gICAgICB0aGlzLnkgPSB0aGlzLnkgKiAoMSAtIGRlbHRhKSArIHRhcmdldC55ICogZGVsdGE7XG4gICAgICByZXR1cm4gdGhpcztcbiAgICB9O1xuXG4gICAgVmVjMi5wcm90b3R5cGUubGVycF8gPSBmdW5jdGlvbih0YXJnZXQsIGRlbHRhKSB7XG4gICAgICByZXR1cm4gKG5ldyBWZWMyKHRoaXMueCwgdGhpcy55KSkubGVycCh0YXJnZXQsIGRlbHRhKTtcbiAgICB9O1xuXG4gICAgVmVjMi5wcm90b3R5cGUuZXF1YWxzID0gZnVuY3Rpb24odikge1xuICAgICAgcmV0dXJuIHRoaXMueCA9PT0gdi54ICYmIHRoaXMueSA9PT0gdi55O1xuICAgIH07XG5cbiAgICBWZWMyLnByb3RvdHlwZS5jbG9uZSA9IGZ1bmN0aW9uKCkge1xuICAgICAgcmV0dXJuIG5ldyBWZWMyKHRoaXMueCwgdGhpcy55KTtcbiAgICB9O1xuXG4gICAgVmVjMi5wcm90b3R5cGUudG9TdHJpbmcgPSBmdW5jdGlvbigpIHtcbiAgICAgIHJldHVybiBcIlZlYzIoXCIgKyB0aGlzLnggKyBcIiwgXCIgKyB0aGlzLnkgKyBcIilcIjtcbiAgICB9O1xuXG4gICAgcmV0dXJuIFZlYzI7XG5cbiAgfSkoKTtcblxuICBWZWMzID0gKGZ1bmN0aW9uKCkge1xuICAgIFZlYzMucHJvdG90eXBlLnggPSAwO1xuXG4gICAgVmVjMy5wcm90b3R5cGUueSA9IDA7XG5cbiAgICBWZWMzLnByb3RvdHlwZS56ID0gMDtcblxuICAgIGZ1bmN0aW9uIFZlYzMoeCwgeSwgeikge1xuICAgICAgdGhpcy54ID0geCAhPSBudWxsID8geCA6IDA7XG4gICAgICB0aGlzLnkgPSB5ICE9IG51bGwgPyB5IDogMDtcbiAgICAgIHRoaXMueiA9IHogIT0gbnVsbCA/IHogOiAwO1xuICAgIH1cblxuICAgIFZlYzMucHJvdG90eXBlLnNldCA9IGZ1bmN0aW9uKHYpIHtcbiAgICAgIHRoaXMueCA9IHYueDtcbiAgICAgIHRoaXMueSA9IHYueTtcbiAgICAgIHRoaXMueiA9IHYuejtcbiAgICAgIHJldHVybiB0aGlzO1xuICAgIH07XG5cbiAgICBWZWMzLnByb3RvdHlwZS5zZXQzID0gZnVuY3Rpb24oeCwgeSwgeikge1xuICAgICAgdGhpcy54ID0geDtcbiAgICAgIHRoaXMueSA9IHk7XG4gICAgICB0aGlzLnogPSB6O1xuICAgICAgcmV0dXJuIHRoaXM7XG4gICAgfTtcblxuICAgIFZlYzMucHJvdG90eXBlLnplcm8gPSBmdW5jdGlvbigpIHtcbiAgICAgIHRoaXMueCA9IHRoaXMueSA9IHRoaXMueiA9IDA7XG4gICAgICByZXR1cm4gdGhpcztcbiAgICB9O1xuXG4gICAgVmVjMy5wcm90b3R5cGUuYWRkID0gZnVuY3Rpb24odikge1xuICAgICAgdGhpcy54ICs9IHYueDtcbiAgICAgIHRoaXMueSArPSB2Lnk7XG4gICAgICB0aGlzLnogKz0gdi56O1xuICAgICAgcmV0dXJuIHRoaXM7XG4gICAgfTtcblxuICAgIFZlYzMucHJvdG90eXBlLmFkZF8gPSBmdW5jdGlvbih2KSB7XG4gICAgICByZXR1cm4gbmV3IFZlYzModGhpcy54ICsgdi54LCB0aGlzLnkgKyB2LnksIHRoaXMueiArIHYueik7XG4gICAgfTtcblxuICAgIFZlYzMucHJvdG90eXBlLnN1YiA9IGZ1bmN0aW9uKHYpIHtcbiAgICAgIHRoaXMueCAtPSB2Lng7XG4gICAgICB0aGlzLnkgLT0gdi55O1xuICAgICAgdGhpcy56IC09IHYuejtcbiAgICAgIHJldHVybiB0aGlzO1xuICAgIH07XG5cbiAgICBWZWMzLnByb3RvdHlwZS5zdWJfID0gZnVuY3Rpb24odikge1xuICAgICAgcmV0dXJuIG5ldyBWZWMzKHRoaXMueCAtIHYueCwgdGhpcy55IC0gdi55LCB0aGlzLnogLSB2LnopO1xuICAgIH07XG5cbiAgICBWZWMzLnByb3RvdHlwZS5tdWwgPSBmdW5jdGlvbih2KSB7XG4gICAgICB0aGlzLnggKj0gdi54O1xuICAgICAgdGhpcy55ICo9IHYueTtcbiAgICAgIHJldHVybiB0aGlzO1xuICAgIH07XG5cbiAgICBWZWMzLnByb3RvdHlwZS5tdWxfID0gZnVuY3Rpb24odikge1xuICAgICAgcmV0dXJuIG5ldyBWZWMzKHRoaXMueCAqIHYueCwgdGhpcy55ICogdi55LCB0aGlzLnogKiB2LnopO1xuICAgIH07XG5cbiAgICBWZWMzLnByb3RvdHlwZS5kaXYgPSBmdW5jdGlvbih2KSB7XG4gICAgICB0aGlzLnggLz0gdi54O1xuICAgICAgdGhpcy55IC89IHYueTtcbiAgICAgIHRoaXMueiAvPSB2Lno7XG4gICAgICByZXR1cm4gdGhpcztcbiAgICB9O1xuXG4gICAgVmVjMy5wcm90b3R5cGUuZGl2XyA9IGZ1bmN0aW9uKHYpIHtcbiAgICAgIHJldHVybiBuZXcgVmVjMyh0aGlzLnggLyB2LngsIHRoaXMueSAvIHYueSwgdGhpcy56ID0gdi56KTtcbiAgICB9O1xuXG4gICAgVmVjMy5wcm90b3R5cGUuc2NhbGUgPSBmdW5jdGlvbih2YWx1ZSkge1xuICAgICAgdGhpcy54ICo9IHZhbHVlO1xuICAgICAgdGhpcy55ICo9IHZhbHVlO1xuICAgICAgdGhpcy56ICo9IHZhbHVlO1xuICAgICAgcmV0dXJuIHRoaXM7XG4gICAgfTtcblxuICAgIFZlYzMucHJvdG90eXBlLnNjYWxlXyA9IGZ1bmN0aW9uKHZhbHVlKSB7XG4gICAgICByZXR1cm4gbmV3IFZlYzModGhpcy54ICogdmFsdWUsIHRoaXMueSAqIHZhbHVlLCB0aGlzLnogKiB2YWx1ZSk7XG4gICAgfTtcblxuICAgIFZlYzMucHJvdG90eXBlLmxlbmd0aCA9IGZ1bmN0aW9uKCkge1xuICAgICAgcmV0dXJuIE1hdGguc3FydCh0aGlzLnggKiB0aGlzLnggKyB0aGlzLnkgKiB0aGlzLnkgKyB0aGlzLnogKiB0aGlzLnopO1xuICAgIH07XG5cbiAgICBWZWMzLnByb3RvdHlwZS5sZW5ndGhTcXVhcmVkID0gZnVuY3Rpb24oKSB7XG4gICAgICByZXR1cm4gdGhpcy54ICogdGhpcy54ICsgdGhpcy55ICogdGhpcy55ICsgdGhpcy56ICogdGhpcy56O1xuICAgIH07XG5cbiAgICBWZWMzLnByb3RvdHlwZS5ub3JtYWxpemUgPSBmdW5jdGlvbigpIHtcbiAgICAgIHZhciBsO1xuICAgICAgbCA9IHRoaXMubGVuZ3RoKCk7XG4gICAgICBpZiAobCAhPT0gMCkge1xuICAgICAgICB0aGlzLnggLz0gbDtcbiAgICAgICAgdGhpcy55IC89IGw7XG4gICAgICAgIHRoaXMueiAvPSBsO1xuICAgICAgfVxuICAgICAgcmV0dXJuIHRoaXM7XG4gICAgfTtcblxuICAgIFZlYzMucHJvdG90eXBlLm5vcm1hbGl6ZV8gPSBmdW5jdGlvbigpIHtcbiAgICAgIHJldHVybiB0aGlzLmNsb25lKCkubm9ybWFsaXplKCk7XG4gICAgfTtcblxuICAgIFZlYzMucHJvdG90eXBlLm5vcm1hbGl6ZVRvID0gZnVuY3Rpb24obGVuZ3RoKSB7XG4gICAgICB2YXIgbWFnbml0dWRlO1xuICAgICAgbWFnbml0dWRlID0gTWF0aC5zcXJ0KHRoaXMueCAqIHRoaXMueCArIHRoaXMueSAqIHRoaXMueSArIHRoaXMueiAqIHRoaXMueik7XG4gICAgICBpZiAobWFnbml0dWRlID4gMCkge1xuICAgICAgICBtYWduaXR1ZGUgPSBsZW5ndGggLyBtYWduaXR1ZGU7XG4gICAgICAgIHRoaXMueCAqPSBtYWduaXR1ZGU7XG4gICAgICAgIHRoaXMueSAqPSBtYWduaXR1ZGU7XG4gICAgICB9XG4gICAgICByZXR1cm4gdGhpcztcbiAgICB9O1xuXG4gICAgVmVjMy5wcm90b3R5cGUubm9ybWFsaXplVG9fID0gZnVuY3Rpb24obGVuZ3RoKSB7XG4gICAgICByZXR1cm4gdGhpcy5jbG9uZSgpLm5vcm1hbGl6ZVRvXyhsZW5ndGgpO1xuICAgIH07XG5cbiAgICBWZWMzLnByb3RvdHlwZS5kaXN0YW5jZSA9IGZ1bmN0aW9uKHYpIHtcbiAgICAgIHJldHVybiBNYXRoLnNxcnQodGhpcy5kaXN0YW5jZVNxdWFyZWQzKHYueCwgdi55LCB2LnopKTtcbiAgICB9O1xuXG4gICAgVmVjMy5wcm90b3R5cGUuZGlzdGFuY2VTcXVhcmVkID0gZnVuY3Rpb24odikge1xuICAgICAgcmV0dXJuIHRoaXMuZGlzdGFuY2VTcXVhcmVkMyh2LngsIHYueSwgdi56KTtcbiAgICB9O1xuXG4gICAgVmVjMy5wcm90b3R5cGUuZGlzdGFuY2VTcXVhcmVkMyA9IGZ1bmN0aW9uKHgsIHksIHopIHtcbiAgICAgIHZhciBkeCwgZHksIGR6O1xuICAgICAgZHggPSB0aGlzLnggLSB4O1xuICAgICAgZHkgPSB0aGlzLnkgLSB5O1xuICAgICAgZHogPSB0aGlzLnogLSB6O1xuICAgICAgcmV0dXJuIGR4ICogZHggKyBkeSAqIGR5ICsgZHogKiBkejtcbiAgICB9O1xuXG4gICAgVmVjMy5wcm90b3R5cGUuZG90ID0gZnVuY3Rpb24odikge1xuICAgICAgcmV0dXJuIHRoaXMueCAqIHYueCArIHRoaXMueSAqIHYueSArIHRoaXMueiAqIHYuejtcbiAgICB9O1xuXG4gICAgVmVjMy5wcm90b3R5cGUuaml0dGVyID0gZnVuY3Rpb24oYW1vdW50KSB7XG4gICAgICB0aGlzLnggKz0gKE1hdGgucmFuZG9tKCkgKiAyIC0gMSkgKiBhbW91bnQ7XG4gICAgICB0aGlzLnkgKz0gKE1hdGgucmFuZG9tKCkgKiAyIC0gMSkgKiBhbW91bnQ7XG4gICAgICB0aGlzLnogKz0gKE1hdGgucmFuZG9tKCkgKiAyIC0gMSkgKiBhbW91bnQ7XG4gICAgICByZXR1cm4gdGhpcztcbiAgICB9O1xuXG4gICAgVmVjMy5wcm90b3R5cGUuaml0dGVyXyA9IGZ1bmN0aW9uKGFtb3VudCkge1xuICAgICAgcmV0dXJuIChuZXcgVmVjMyh0aGlzLngsIHRoaXMueSwgdGhpcy56KSkuaml0dGVyKGFtb3VudCk7XG4gICAgfTtcblxuICAgIFZlYzMucHJvdG90eXBlLmxlcnAgPSBmdW5jdGlvbih0YXJnZXQsIGRlbHRhKSB7XG4gICAgICB0aGlzLnggPSB0aGlzLnggKiAoMSAtIGRlbHRhKSArIHRhcmdldC54ICogZGVsdGE7XG4gICAgICB0aGlzLnkgPSB0aGlzLnkgKiAoMSAtIGRlbHRhKSArIHRhcmdldC55ICogZGVsdGE7XG4gICAgICB0aGlzLnogPSB0aGlzLnogKiAoMSAtIGRlbHRhKSArIHRhcmdldC56ICogZGVsdGE7XG4gICAgICByZXR1cm4gdGhpcztcbiAgICB9O1xuXG4gICAgVmVjMy5wcm90b3R5cGUubGVycF8gPSBmdW5jdGlvbih0YXJnZXQsIGRlbHRhKSB7XG4gICAgICByZXR1cm4gKG5ldyBWZWMzKHRoaXMueCwgdGhpcy55LCB0aGlzLnopKS5sZXJwKHRhcmdldCwgZGVsdGEpO1xuICAgIH07XG5cbiAgICBWZWMzLnByb3RvdHlwZS5lcXVhbHMgPSBmdW5jdGlvbih2KSB7XG4gICAgICByZXR1cm4gdGhpcy54ID09PSB2LnggJiYgdGhpcy55ID09PSB2LnkgJiYgdGhpcy56ID09PSB2Lno7XG4gICAgfTtcblxuICAgIFZlYzMucHJvdG90eXBlLmNsb25lID0gZnVuY3Rpb24oKSB7XG4gICAgICByZXR1cm4gbmV3IFZlYzModGhpcy54LCB0aGlzLnksIHRoaXMueik7XG4gICAgfTtcblxuICAgIFZlYzMucHJvdG90eXBlLnRvU3RyaW5nID0gZnVuY3Rpb24oKSB7XG4gICAgICByZXR1cm4gXCJWZWMzKFwiICsgdGhpcy54ICsgXCIsIFwiICsgdGhpcy55ICsgXCIsIFwiICsgdGhpcy56ICsgXCIpXCI7XG4gICAgfTtcblxuICAgIHJldHVybiBWZWMzO1xuXG4gIH0pKCk7XG5cbiAgbW9kdWxlLmV4cG9ydHMgPSB7XG4gICAgVmVjMjogVmVjMixcbiAgICBWZWMzOiBWZWMzXG4gIH07XG5cbn0pLmNhbGwodGhpcyk7XG4iLCIvLyBHZW5lcmF0ZWQgYnkgQ29mZmVlU2NyaXB0IDEuNi4zXG4oZnVuY3Rpb24oKSB7XG4gIHZhciBBdHRyYWN0b3IsIEJlaGF2aW91ciwgRm9yY2UsIFZlYzIsIFZlYzMsIHBoeXNpY3MsIHZlY3RvcixcbiAgICBfX2hhc1Byb3AgPSB7fS5oYXNPd25Qcm9wZXJ0eSxcbiAgICBfX2V4dGVuZHMgPSBmdW5jdGlvbihjaGlsZCwgcGFyZW50KSB7IGZvciAodmFyIGtleSBpbiBwYXJlbnQpIHsgaWYgKF9faGFzUHJvcC5jYWxsKHBhcmVudCwga2V5KSkgY2hpbGRba2V5XSA9IHBhcmVudFtrZXldOyB9IGZ1bmN0aW9uIGN0b3IoKSB7IHRoaXMuY29uc3RydWN0b3IgPSBjaGlsZDsgfSBjdG9yLnByb3RvdHlwZSA9IHBhcmVudC5wcm90b3R5cGU7IGNoaWxkLnByb3RvdHlwZSA9IG5ldyBjdG9yKCk7IGNoaWxkLl9fc3VwZXJfXyA9IHBhcmVudC5wcm90b3R5cGU7IHJldHVybiBjaGlsZDsgfTtcblxuICB2ZWN0b3IgPSByZXF1aXJlKCcuLi9tYXRoL3ZlY3RvcicpO1xuXG4gIFZlYzIgPSB2ZWN0b3IuVmVjMjtcblxuICBWZWMzID0gdmVjdG9yLlZlYzM7XG5cbiAgcGh5c2ljcyA9IHJlcXVpcmUoJy4vcGh5c2ljcycpO1xuXG4gIEJlaGF2aW91ciA9IHBoeXNpY3MuQmVoYXZpb3VyO1xuXG4gIC8qXG4gIFxuICAgIEEgY29uc3RhbnQgZm9yY2UgYWxvbmcgYSB2ZWN0b3IgZS5nLiBHcmF2aXR5LlxuICAgIFdvcmtzIGluIDJEICsgM0QuXG4gICovXG5cblxuICBGb3JjZSA9IChmdW5jdGlvbihfc3VwZXIpIHtcbiAgICB2YXIgZm9yY2U7XG5cbiAgICBfX2V4dGVuZHMoRm9yY2UsIF9zdXBlcik7XG5cbiAgICBmb3JjZSA9IG51bGw7XG5cbiAgICBmdW5jdGlvbiBGb3JjZShkaXJlY3Rpb24sIHdlaWdodCkge1xuICAgICAgdGhpcy5kaXJlY3Rpb24gPSBkaXJlY3Rpb247XG4gICAgICB0aGlzLndlaWdodCA9IHdlaWdodCAhPSBudWxsID8gd2VpZ2h0IDogMTtcbiAgICB9XG5cbiAgICBGb3JjZS5wcm90b3R5cGUucHJlcGFyZSA9IGZ1bmN0aW9uKCkge1xuICAgICAgcmV0dXJuIGZvcmNlID0gdGhpcy5kaXJlY3Rpb24ubm9ybWFsaXplVG8odGhpcy53ZWlnaHQpO1xuICAgIH07XG5cbiAgICBGb3JjZS5wcm90b3R5cGUuYXBwbHkgPSBmdW5jdGlvbihwYXJ0aWNsZSkge1xuICAgICAgcmV0dXJuIHBhcnRpY2xlLnBvc2l0aW9uLmFkZChmb3JjZSk7XG4gICAgfTtcblxuICAgIEZvcmNlLnByb3RvdHlwZS50b1N0cmluZyA9IGZ1bmN0aW9uKCkge1xuICAgICAgcmV0dXJuIFwiRm9yY2UoXCIgKyBmb3JjZSArIFwiKVwiO1xuICAgIH07XG5cbiAgICByZXR1cm4gRm9yY2U7XG5cbiAgfSkoQmVoYXZpb3VyKTtcblxuICAvKlxuICBcbiAgICBBdHRyYWN0cyBlYWNoIHBhcnRpY2xlIHdpdGhpbiByYW5nZSB0byBhIHRhcmdldCBwb2ludC5cbiAgICBXb3JrcyBpbiAyRCArIDNELlxuICAqL1xuXG5cbiAgQXR0cmFjdG9yID0gKGZ1bmN0aW9uKCkge1xuICAgIHZhciByYW5nZVNxLCB0bXA7XG5cbiAgICB0bXAgPSBudWxsO1xuXG4gICAgcmFuZ2VTcSA9IDA7XG5cbiAgICBmdW5jdGlvbiBBdHRyYWN0b3IodGFyZ2V0LCByYW5nZSwgd2VpZ2h0KSB7XG4gICAgICB0aGlzLnRhcmdldCA9IHRhcmdldDtcbiAgICAgIHRoaXMucmFuZ2UgPSByYW5nZTtcbiAgICAgIHRoaXMud2VpZ2h0ID0gd2VpZ2h0ICE9IG51bGwgPyB3ZWlnaHQgOiAxO1xuICAgICAgdG1wID0gdGhpcy50YXJnZXQuY2xvbmUoKTtcbiAgICB9XG5cbiAgICBBdHRyYWN0b3IucHJvdG90eXBlLnByZXBhcmUgPSBmdW5jdGlvbigpIHtcbiAgICAgIHJldHVybiByYW5nZVNxID0gdGhpcy5yYW5nZSAqIHRoaXMucmFuZ2U7XG4gICAgfTtcblxuICAgIEF0dHJhY3Rvci5wcm90b3R5cGUuYXBwbHkgPSBmdW5jdGlvbihwYXJ0aWNsZSkge1xuICAgICAgdmFyIGRpc3QsIGRpc3RTcTtcbiAgICAgIHRtcC5zZXQodGhpcy50YXJnZXQpLnN1YihwYXJ0aWNsZS5wb3NpdGlvbik7XG4gICAgICBkaXN0U3EgPSB0bXAubGVuZ3RoU3F1YXJlZCgpO1xuICAgICAgaWYgKGRpc3RTcSA+IDAgJiYgZGlzdFNxIDwgcmFuZ2VTcSkge1xuICAgICAgICBkaXN0ID0gTWF0aC5zcXJ0KGRpc3RTcSk7XG4gICAgICAgIHRtcC5zY2FsZSgoMSAvIGRpc3QpICogKDEgLSBkaXN0IC8gdGhpcy5yYW5nZSkgKiB0aGlzLndlaWdodCk7XG4gICAgICAgIHJldHVybiBwYXJ0aWNsZS5mb3JjZS5hZGQodG1wKTtcbiAgICAgIH1cbiAgICB9O1xuXG4gICAgQXR0cmFjdG9yLnByb3RvdHlwZS50b1N0cmluZyA9IGZ1bmN0aW9uKCkge1xuICAgICAgcmV0dXJuIFwiQXR0cmFjdG9yKFwiICsgdGhpcy50YXJnZXQgKyBcIiwgXCIgKyB0aGlzLnJhbmdlICsgXCIsIFwiICsgdGhpcy53ZWlnaHQgKyBcIilcIjtcbiAgICB9O1xuXG4gICAgcmV0dXJuIEF0dHJhY3RvcjtcblxuICB9KSgpO1xuXG4gIG1vZHVsZS5leHBvcnRzID0ge1xuICAgIEZvcmNlOiBGb3JjZSxcbiAgICBBdHRyYWN0b3I6IEF0dHJhY3RvclxuICB9O1xuXG59KS5jYWxsKHRoaXMpO1xuIiwiLy8gR2VuZXJhdGVkIGJ5IENvZmZlZVNjcmlwdCAxLjYuM1xuKGZ1bmN0aW9uKCkge1xuICB2YXIgQXJlYSwgQm94LCBDb2xsaXNpb24sIENvbnN0cmFpbnQsIFZlYzIsIFZlYzMsIFdyYXAyLCBXcmFwMywgcGh5c2ljcywgdmVjdG9yLFxuICAgIF9faGFzUHJvcCA9IHt9Lmhhc093blByb3BlcnR5LFxuICAgIF9fZXh0ZW5kcyA9IGZ1bmN0aW9uKGNoaWxkLCBwYXJlbnQpIHsgZm9yICh2YXIga2V5IGluIHBhcmVudCkgeyBpZiAoX19oYXNQcm9wLmNhbGwocGFyZW50LCBrZXkpKSBjaGlsZFtrZXldID0gcGFyZW50W2tleV07IH0gZnVuY3Rpb24gY3RvcigpIHsgdGhpcy5jb25zdHJ1Y3RvciA9IGNoaWxkOyB9IGN0b3IucHJvdG90eXBlID0gcGFyZW50LnByb3RvdHlwZTsgY2hpbGQucHJvdG90eXBlID0gbmV3IGN0b3IoKTsgY2hpbGQuX19zdXBlcl9fID0gcGFyZW50LnByb3RvdHlwZTsgcmV0dXJuIGNoaWxkOyB9O1xuXG4gIHZlY3RvciA9IHJlcXVpcmUoJy4uL21hdGgvdmVjdG9yJyk7XG5cbiAgVmVjMiA9IHZlY3Rvci5WZWMyO1xuXG4gIFZlYzMgPSB2ZWN0b3IuVmVjMztcblxuICBwaHlzaWNzID0gcmVxdWlyZSgnLi9waHlzaWNzJyk7XG5cbiAgQ29uc3RyYWludCA9IHBoeXNpY3MuQ29uc3RyYWludDtcblxuICAvKlxuICAgIEtlZXBzIHRoZSBwYXJ0aWNsZSBpbnNpZGUgdGhlIGdpdmVuIDNEIGJveFxuICAqL1xuXG5cbiAgQm94ID0gKGZ1bmN0aW9uKF9zdXBlcikge1xuICAgIF9fZXh0ZW5kcyhCb3gsIF9zdXBlcik7XG5cbiAgICBmdW5jdGlvbiBCb3gobWluLCBtYXgpIHtcbiAgICAgIHRoaXMubWluID0gbWluICE9IG51bGwgPyBtaW4gOiBuZXcgVmVjMygpO1xuICAgICAgdGhpcy5tYXggPSBtYXggIT0gbnVsbCA/IG1heCA6IG5ldyBWZWMzKDEwMCwgMTAwLCAxMDApO1xuICAgIH1cblxuICAgIEJveC5wcm90b3R5cGUuYXBwbHkgPSBmdW5jdGlvbihwYXJ0aWNsZSkge1xuICAgICAgdmFyIHBvcztcbiAgICAgIHBvcyA9IHBhcnRpY2xlLnBvc2l0aW9uO1xuICAgICAgaWYgKHBvcy54IDwgdGhpcy5taW4ueCkge1xuICAgICAgICBwb3MueCA9IHRoaXMubWluLng7XG4gICAgICB9XG4gICAgICBpZiAocG9zLnkgPCB0aGlzLm1pbi55KSB7XG4gICAgICAgIHBvcy55ID0gdGhpcy5taW4ueTtcbiAgICAgIH1cbiAgICAgIGlmIChwb3MueiA8IHRoaXMubWluLnopIHtcbiAgICAgICAgcG9zLnogPSB0aGlzLm1pbi56O1xuICAgICAgfVxuICAgICAgaWYgKHBvcy54ID4gdGhpcy5tYXgueCkge1xuICAgICAgICBwb3MueCA9IHRoaXMubWF4Lng7XG4gICAgICB9XG4gICAgICBpZiAocG9zLnkgPiB0aGlzLm1heC55KSB7XG4gICAgICAgIHBvcy55ID0gdGhpcy5tYXgueTtcbiAgICAgIH1cbiAgICAgIGlmIChwb3MueiA+IHRoaXMubWF4LnopIHtcbiAgICAgICAgcmV0dXJuIHBvcy56ID0gdGhpcy5tYXguejtcbiAgICAgIH1cbiAgICB9O1xuXG4gICAgQm94LnByb3RvdHlwZS50b1N0cmluZyA9IGZ1bmN0aW9uKCkge1xuICAgICAgcmV0dXJuIFwiQm94KFwiICsgdGhpcy5taW4gKyBcIiwgXCIgKyB0aGlzLm1heCArIFwiKVwiO1xuICAgIH07XG5cbiAgICByZXR1cm4gQm94O1xuXG4gIH0pKENvbnN0cmFpbnQpO1xuXG4gIC8qXG4gICAgMkQgdmVyc2lvbiBvZiBCb3guXG4gICovXG5cblxuICBBcmVhID0gKGZ1bmN0aW9uKF9zdXBlcikge1xuICAgIF9fZXh0ZW5kcyhBcmVhLCBfc3VwZXIpO1xuXG4gICAgZnVuY3Rpb24gQXJlYShtaW4sIG1heCkge1xuICAgICAgdGhpcy5taW4gPSBtaW4gIT0gbnVsbCA/IG1pbiA6IG5ldyBWZWMyKCk7XG4gICAgICB0aGlzLm1heCA9IG1heCAhPSBudWxsID8gbWF4IDogbmV3IFZlYzIoMTAwLCAxMDApO1xuICAgIH1cblxuICAgIEFyZWEucHJvdG90eXBlLmFwcGx5ID0gZnVuY3Rpb24ocGFydGljbGUpIHtcbiAgICAgIHZhciBwb3M7XG4gICAgICBwb3MgPSBwYXJ0aWNsZS5wb3NpdGlvbjtcbiAgICAgIGlmIChwb3MueCA8IHRoaXMubWluLngpIHtcbiAgICAgICAgcG9zLnggPSB0aGlzLm1pbi54O1xuICAgICAgfVxuICAgICAgaWYgKHBvcy55IDwgdGhpcy5taW4ueSkge1xuICAgICAgICBwb3MueSA9IHRoaXMubWluLnk7XG4gICAgICB9XG4gICAgICBpZiAocG9zLnggPiB0aGlzLm1heC54KSB7XG4gICAgICAgIHBvcy54ID0gdGhpcy5tYXgueDtcbiAgICAgIH1cbiAgICAgIGlmIChwb3MueSA+IHRoaXMubWF4LnkpIHtcbiAgICAgICAgcmV0dXJuIHBvcy55ID0gdGhpcy5tYXgueTtcbiAgICAgIH1cbiAgICB9O1xuXG4gICAgQXJlYS5wcm90b3R5cGUudG9TdHJpbmcgPSBmdW5jdGlvbigpIHtcbiAgICAgIHJldHVybiBcIkFyZWEoXCIgKyB0aGlzLm1pbiArIFwiLCBcIiArIHRoaXMubWF4ICsgXCIpXCI7XG4gICAgfTtcblxuICAgIHJldHVybiBBcmVhO1xuXG4gIH0pKENvbnN0cmFpbnQpO1xuXG4gIC8qXG4gICAgS2VlcHMgYSBwYXJ0aWNsZSB3aXRoaW4gYSBjZXJ0YWluIDJEIHJlZ2lvbiBieSB3cmFwcGluZyBpdCBhcm91bmQgYSBnaXZlbiBhcmVhLlxuICAqL1xuXG5cbiAgV3JhcDIgPSAoZnVuY3Rpb24oX3N1cGVyKSB7XG4gICAgdmFyIGRlbHRhO1xuXG4gICAgX19leHRlbmRzKFdyYXAyLCBfc3VwZXIpO1xuXG4gICAgZGVsdGEgPSBuZXcgVmVjMigpO1xuXG4gICAgZnVuY3Rpb24gV3JhcDIobWluLCBtYXgpIHtcbiAgICAgIHRoaXMubWluID0gbWluICE9IG51bGwgPyBtaW4gOiBuZXcgVmVjMigpO1xuICAgICAgdGhpcy5tYXggPSBtYXggIT0gbnVsbCA/IG1heCA6IG5ldyBWZWMyKDEwMCwgMTAwKTtcbiAgICB9XG5cbiAgICBXcmFwMi5wcm90b3R5cGUucHJlcGFyZSA9IGZ1bmN0aW9uKCkge1xuICAgICAgcmV0dXJuIGRlbHRhLnNldCh0aGlzLm1heCkuc3ViKHRoaXMubWluKTtcbiAgICB9O1xuXG4gICAgV3JhcDIucHJvdG90eXBlLmFwcGx5ID0gZnVuY3Rpb24ocGFydGljbGUpIHtcbiAgICAgIHZhciBwb3MsIHByZXY7XG4gICAgICBwb3MgPSBwYXJ0aWNsZS5wb3NpdGlvbjtcbiAgICAgIHByZXYgPSBwYXJ0aWNsZS5wcmV2O1xuICAgICAgaWYgKHBvcy54IDwgdGhpcy5taW4ueCkge1xuICAgICAgICBwb3MueCArPSBkZWx0YS54O1xuICAgICAgICBwcmV2LnggKz0gZGVsdGEueDtcbiAgICAgIH1cbiAgICAgIGlmIChwb3MueSA8IHRoaXMubWluLnkpIHtcbiAgICAgICAgcG9zLnkgKz0gZGVsdGEueTtcbiAgICAgICAgcHJldi55ICs9IGRlbHRhLnk7XG4gICAgICB9XG4gICAgICBpZiAocG9zLnggPiB0aGlzLm1heC54KSB7XG4gICAgICAgIHBvcy54IC09IGRlbHRhLng7XG4gICAgICAgIHByZXYueCAtPSBkZWx0YS54O1xuICAgICAgfVxuICAgICAgaWYgKHBvcy55ID4gdGhpcy5tYXgueSkge1xuICAgICAgICBwb3MueSAtPSBkZWx0YS55O1xuICAgICAgICByZXR1cm4gcHJldi55IC09IGRlbHRhLnk7XG4gICAgICB9XG4gICAgfTtcblxuICAgIFdyYXAyLnByb3RvdHlwZS50b1N0cmluZyA9IGZ1bmN0aW9uKCkge1xuICAgICAgcmV0dXJuIFwiV3JhcDIoXCIgKyB0aGlzLm1pbiArIFwiLCBcIiArIHRoaXMubWF4ICsgXCIpXCI7XG4gICAgfTtcblxuICAgIHJldHVybiBXcmFwMjtcblxuICB9KShDb25zdHJhaW50KTtcblxuICAvKlxuICAgIEtlZXBzIGEgcGFydGljbGUgd2l0aGluIGEgY2VydGFpbiAyRCByZWdpb24gYnkgd3JhcHBpbmcgaXQgYXJvdW5kIGEgZ2l2ZW4gYXJlYS5cbiAgKi9cblxuXG4gIFdyYXAzID0gKGZ1bmN0aW9uKF9zdXBlcikge1xuICAgIHZhciBkZWx0YTtcblxuICAgIF9fZXh0ZW5kcyhXcmFwMywgX3N1cGVyKTtcblxuICAgIGRlbHRhID0gbmV3IFZlYzMoKTtcblxuICAgIGZ1bmN0aW9uIFdyYXAzKG1pbiwgbWF4KSB7XG4gICAgICB0aGlzLm1pbiA9IG1pbiAhPSBudWxsID8gbWluIDogbmV3IFZlYzMoKTtcbiAgICAgIHRoaXMubWF4ID0gbWF4ICE9IG51bGwgPyBtYXggOiBuZXcgVmVjMygxMDAsIDEwMCwgMTAwKTtcbiAgICB9XG5cbiAgICBXcmFwMy5wcm90b3R5cGUucHJlcGFyZSA9IGZ1bmN0aW9uKCkge1xuICAgICAgcmV0dXJuIGRlbHRhLnNldCh0aGlzLm1heCkuc3ViKHRoaXMubWluKTtcbiAgICB9O1xuXG4gICAgV3JhcDMucHJvdG90eXBlLmFwcGx5ID0gZnVuY3Rpb24ocGFydGljbGUpIHtcbiAgICAgIHZhciBwb3MsIHByZXY7XG4gICAgICBwb3MgPSBwYXJ0aWNsZS5wb3NpdGlvbjtcbiAgICAgIHByZXYgPSBwYXJ0aWNsZS5wcmV2O1xuICAgICAgaWYgKHBvcy54IDwgdGhpcy5taW4ueCkge1xuICAgICAgICBwb3MueCArPSBkZWx0YS54O1xuICAgICAgICBwcmV2LnggKz0gZGVsdGEueDtcbiAgICAgIH1cbiAgICAgIGlmIChwb3MueSA8IHRoaXMubWluLnkpIHtcbiAgICAgICAgcG9zLnkgKz0gZGVsdGEueTtcbiAgICAgICAgcHJldi55ICs9IGRlbHRhLnk7XG4gICAgICB9XG4gICAgICBpZiAocG9zLnogPCB0aGlzLm1pbi56KSB7XG4gICAgICAgIHBvcy56ICs9IGRlbHRhLno7XG4gICAgICAgIHByZXYueiArPSBkZWx0YS56O1xuICAgICAgfVxuICAgICAgaWYgKHBvcy54ID4gdGhpcy5tYXgueCkge1xuICAgICAgICBwb3MueCAtPSBkZWx0YS54O1xuICAgICAgICBwcmV2LnggLT0gZGVsdGEueDtcbiAgICAgIH1cbiAgICAgIGlmIChwb3MueSA+IHRoaXMubWF4LnkpIHtcbiAgICAgICAgcG9zLnkgLT0gZGVsdGEueTtcbiAgICAgICAgcHJldi55IC09IGRlbHRhLnk7XG4gICAgICB9XG4gICAgICBpZiAocG9zLnogPiB0aGlzLm1heC56KSB7XG4gICAgICAgIHBvcy56IC09IGRlbHRhLno7XG4gICAgICAgIHJldHVybiBwcmV2LnogLT0gZGVsdGEuejtcbiAgICAgIH1cbiAgICB9O1xuXG4gICAgV3JhcDMucHJvdG90eXBlLnRvU3RyaW5nID0gZnVuY3Rpb24oKSB7XG4gICAgICByZXR1cm4gXCJXcmFwMyhcIiArIHRoaXMubWluICsgXCIsIFwiICsgdGhpcy5tYXggKyBcIilcIjtcbiAgICB9O1xuXG4gICAgcmV0dXJuIFdyYXAzO1xuXG4gIH0pKENvbnN0cmFpbnQpO1xuXG4gIC8qXG4gICAgU3RvcHMgcGFydGljbGVzIGZyb20gY29sbGlkaW5nIHdpdGggZWFjaCBvdGhlci5cbiAgKi9cblxuXG4gIENvbGxpc2lvbiA9IChmdW5jdGlvbihfc3VwZXIpIHtcbiAgICBfX2V4dGVuZHMoQ29sbGlzaW9uLCBfc3VwZXIpO1xuXG4gICAgQ29sbGlzaW9uLnByb3RvdHlwZS5waHlzaWNzID0gbnVsbDtcblxuICAgIGZ1bmN0aW9uIENvbGxpc2lvbihwaHlzaWNzLCBzZWFyY2hSYWRpdXMpIHtcbiAgICAgIHRoaXMucGh5c2ljcyA9IHBoeXNpY3M7XG4gICAgICB0aGlzLnNlYXJjaFJhZGl1cyA9IHNlYXJjaFJhZGl1cyAhPSBudWxsID8gc2VhcmNoUmFkaXVzIDogMTAwO1xuICAgIH1cblxuICAgIENvbGxpc2lvbi5wcm90b3R5cGUuYXBwbHkgPSBmdW5jdGlvbihwYXJ0aWNsZSkge1xuICAgICAgdmFyIGRlbHRhLCBkaXN0LCBkaXN0U3EsIG5laWdoYm91ciwgbmVpZ2hib3VycywgcG9zaXRpb24sIHJhZGl1cywgcmFkaXVzU3EsIF9pLCBfbGVuO1xuICAgICAgcG9zaXRpb24gPSBwYXJ0aWNsZS5wb3NpdGlvbjtcbiAgICAgIGRlbHRhID0gcG9zaXRpb24uY2xvbmUoKTtcbiAgICAgIG5laWdoYm91cnMgPSB0aGlzLnBoeXNpY3Muc3BhY2Uuc2VhcmNoKHBvc2l0aW9uLCB0aGlzLnNlYXJjaFJhZGl1cyk7XG4gICAgICBmb3IgKF9pID0gMCwgX2xlbiA9IG5laWdoYm91cnMubGVuZ3RoOyBfaSA8IF9sZW47IF9pKyspIHtcbiAgICAgICAgbmVpZ2hib3VyID0gbmVpZ2hib3Vyc1tfaV07XG4gICAgICAgIGlmIChuZWlnaGJvdXIgPT09IHBhcnRpY2xlKSB7XG4gICAgICAgICAgY29udGludWU7XG4gICAgICAgIH1cbiAgICAgICAgZGVsdGEuc2V0KHBvc2l0aW9uKS5zdWIobmVpZ2hib3VyLnBvc2l0aW9uKTtcbiAgICAgICAgZGlzdFNxID0gZGVsdGEubGVuZ3RoU3F1YXJlZCgpO1xuICAgICAgICByYWRpdXMgPSBwYXJ0aWNsZS5zaXplICsgbmVpZ2hib3VyLnNpemU7XG4gICAgICAgIHJhZGl1c1NxID0gcmFkaXVzICogcmFkaXVzO1xuICAgICAgICBpZiAoZGlzdFNxIDwgcmFkaXVzU3EpIHtcbiAgICAgICAgICBkaXN0ID0gTWF0aC5zcXJ0KGRpc3RTcSk7XG4gICAgICAgICAgZGVsdGEuc2NhbGUoKGRpc3QgLSByYWRpdXMpIC8gcmFkaXVzICogMC41KTtcbiAgICAgICAgICBwYXJ0aWNsZS5wb3NpdGlvbi5zdWIoZGVsdGEpO1xuICAgICAgICAgIG5laWdoYm91ci5wb3NpdGlvbi5hZGQoZGVsdGEpO1xuICAgICAgICB9XG4gICAgICAgIHZvaWQgMDtcbiAgICAgIH1cbiAgICAgIHJldHVybiB2b2lkIDA7XG4gICAgfTtcblxuICAgIENvbGxpc2lvbi5wcm90b3R5cGUudG9TdHJpbmcgPSBmdW5jdGlvbigpIHtcbiAgICAgIHJldHVybiBcIkNvbGxpc2lvbihcIiArIHRoaXMuc2VhcmNoUmFkaXVzICsgXCIpXCI7XG4gICAgfTtcblxuICAgIHJldHVybiBDb2xsaXNpb247XG5cbiAgfSkoQ29uc3RyYWludCk7XG5cbiAgbW9kdWxlLmV4cG9ydHMgPSB7XG4gICAgQm94OiBCb3gsXG4gICAgQXJlYTogQXJlYSxcbiAgICBXcmFwMjogV3JhcDIsXG4gICAgV3JhcDM6IFdyYXAzLFxuICAgIENvbGxpc2lvbjogQ29sbGlzaW9uXG4gIH07XG5cbn0pLmNhbGwodGhpcyk7XG4iLCIvLyBHZW5lcmF0ZWQgYnkgQ29mZmVlU2NyaXB0IDEuNi4zXG4oZnVuY3Rpb24oKSB7XG4gIHZhciBQYXJ0aWNsZSwgUGFydGljbGUyLCBQYXJ0aWNsZTMsIFN0YXRlLCBWZWMyLCBWZWMzLCB2ZWN0b3IsXG4gICAgX19oYXNQcm9wID0ge30uaGFzT3duUHJvcGVydHksXG4gICAgX19leHRlbmRzID0gZnVuY3Rpb24oY2hpbGQsIHBhcmVudCkgeyBmb3IgKHZhciBrZXkgaW4gcGFyZW50KSB7IGlmIChfX2hhc1Byb3AuY2FsbChwYXJlbnQsIGtleSkpIGNoaWxkW2tleV0gPSBwYXJlbnRba2V5XTsgfSBmdW5jdGlvbiBjdG9yKCkgeyB0aGlzLmNvbnN0cnVjdG9yID0gY2hpbGQ7IH0gY3Rvci5wcm90b3R5cGUgPSBwYXJlbnQucHJvdG90eXBlOyBjaGlsZC5wcm90b3R5cGUgPSBuZXcgY3RvcigpOyBjaGlsZC5fX3N1cGVyX18gPSBwYXJlbnQucHJvdG90eXBlOyByZXR1cm4gY2hpbGQ7IH07XG5cbiAgdmVjdG9yID0gcmVxdWlyZSgnLi4vbWF0aC92ZWN0b3InKTtcblxuICBWZWMyID0gdmVjdG9yLlZlYzI7XG5cbiAgVmVjMyA9IHZlY3Rvci5WZWMzO1xuXG4gIFN0YXRlID0ge1xuICAgIEFMSVZFOiAwLFxuICAgIExPQ0tFRDogMSxcbiAgICBJRExFOiAyLFxuICAgIERFQUQ6IDNcbiAgfTtcblxuICAvKlxuICBcbiAgICBWZXJMZXIgUGFydGljbGUgQmFzZWNsYXNzXG4gIFxuICAgIEZJRUxEIGZsYXZvdXJlZCBwYXJ0aWNsZSBpbnRlZ3JhdG9yLlxuICAgIFN1cHBvcnRzIFZlcmxldC1zdHlsZSBpbnRlZ3JhdGlvbiBmb3IgJ3N0cmljdCcgcmVsYXRpb25zaGlwcyBlLmcuIFNwcmluZ3MgKyBDb25zdHJhaW50c1xuICAgIGFuZCBhbHNvIEV1bGVyLXN0eWxlIGNvbnRpbm91cyBmb3JjZSBpbnRlZ3JhdGlvbiBmb3Igc21vb3RoLyBmbG93aW5nIGJlaGF2aW91ciBlLmcuIEZsb2NraW5nXG4gICovXG5cblxuICBQYXJ0aWNsZSA9IChmdW5jdGlvbigpIHtcbiAgICBQYXJ0aWNsZS5wcm90b3R5cGUuaWQgPSAwO1xuXG4gICAgUGFydGljbGUucHJvdG90eXBlLnN0YXRlID0gU3RhdGUuQUxJVkU7XG5cbiAgICBQYXJ0aWNsZS5wcm90b3R5cGUuYWdlID0gMDtcblxuICAgIFBhcnRpY2xlLnByb3RvdHlwZS5saWZldGltZSA9IC0xO1xuXG4gICAgUGFydGljbGUucHJvdG90eXBlLnBvc2l0aW9uID0gbnVsbDtcblxuICAgIFBhcnRpY2xlLnByb3RvdHlwZS5kcmFnID0gMC4wMztcblxuICAgIFBhcnRpY2xlLnByb3RvdHlwZS5wcmV2ID0gbnVsbDtcblxuICAgIFBhcnRpY2xlLnByb3RvdHlwZS5mb3JjZSA9IG51bGw7XG5cbiAgICBQYXJ0aWNsZS5wcm90b3R5cGUudmVsb2NpdHkgPSBudWxsO1xuXG4gICAgZnVuY3Rpb24gUGFydGljbGUoaWQpIHtcbiAgICAgIHRoaXMuaWQgPSBpZDtcbiAgICAgIHRoaXMuc2l6ZSA9IDE7XG4gICAgICB0aGlzLmlzTG9ja2VkID0gZmFsc2U7XG4gICAgfVxuXG4gICAgUGFydGljbGUucHJvdG90eXBlLmNsZWFyVmVsb2NpdHkgPSBmdW5jdGlvbigpIHtcbiAgICAgIHJldHVybiB0aGlzLnByZXYuc2V0KHRoaXMucG9zaXRpb24pO1xuICAgIH07XG5cbiAgICBQYXJ0aWNsZS5wcm90b3R5cGUuc2NhbGVWZWxvY2l0eSA9IGZ1bmN0aW9uKGFtb3VudCkge1xuICAgICAgcmV0dXJuIHRoaXMucHJldi5sZXJwKHRoaXMucG9zaXRpb24sIDEuMCAtIGFtb3VudCk7XG4gICAgfTtcblxuICAgIFBhcnRpY2xlLnByb3RvdHlwZS5zZXRQb3NpdGlvbiA9IGZ1bmN0aW9uKHYpIHtcbiAgICAgIHRoaXMucG9zaXRpb24uc2V0KHYpO1xuICAgICAgcmV0dXJuIHRoaXMucHJldi5zZXQodik7XG4gICAgfTtcblxuICAgIFBhcnRpY2xlLnByb3RvdHlwZS5sb2NrID0gZnVuY3Rpb24oKSB7XG4gICAgICByZXR1cm4gdGhpcy5zdGF0ZSA9IFN0YXRlLkxPQ0tFRDtcbiAgICB9O1xuXG4gICAgUGFydGljbGUucHJvdG90eXBlLnVubG9jayA9IGZ1bmN0aW9uKCkge1xuICAgICAgcmV0dXJuIHRoaXMuc3RhdGUgPSBTdGF0ZS5BTElWRTtcbiAgICB9O1xuXG4gICAgUGFydGljbGUucHJvdG90eXBlLmRpZSA9IGZ1bmN0aW9uKCkge1xuICAgICAgcmV0dXJuIHRoaXMuc3RhdGUgPSBTdGF0ZS5ERUFEO1xuICAgIH07XG5cbiAgICBQYXJ0aWNsZS5wcm90b3R5cGUuaWRsZSA9IGZ1bmN0aW9uKCkge1xuICAgICAgcmV0dXJuIHRoaXMuc3RhdGUgPSBTdGF0ZS5JRExFO1xuICAgIH07XG5cbiAgICBQYXJ0aWNsZS5wcm90b3R5cGUudG9TdHJpbmcgPSBmdW5jdGlvbigpIHtcbiAgICAgIHJldHVybiBcIlBhcnRpY2xlKFwiICsgdGhpcy5wb3NpdGlvbiArIFwiKVwiO1xuICAgIH07XG5cbiAgICByZXR1cm4gUGFydGljbGU7XG5cbiAgfSkoKTtcblxuICAvKlxuICBcbiAgICAzRCBWZXJMZXIgUGFydGljbGVcbiAgKi9cblxuXG4gIFBhcnRpY2xlMyA9IChmdW5jdGlvbihfc3VwZXIpIHtcbiAgICB2YXIgdG1wO1xuXG4gICAgX19leHRlbmRzKFBhcnRpY2xlMywgX3N1cGVyKTtcblxuICAgIHRtcCA9IG5ldyBWZWMzKCk7XG5cbiAgICBmdW5jdGlvbiBQYXJ0aWNsZTMoaWQpIHtcbiAgICAgIHRoaXMuaWQgPSBpZDtcbiAgICAgIHRoaXMucG9zaXRpb24gPSBuZXcgVmVjMygpO1xuICAgICAgdGhpcy5wcmV2ID0gbmV3IFZlYzMoKTtcbiAgICAgIHRoaXMuZm9yY2UgPSBuZXcgVmVjMygpO1xuICAgICAgdGhpcy52ZWxvY2l0eSA9IG5ldyBWZWMzKCk7XG4gICAgfVxuXG4gICAgUGFydGljbGUzLnByb3RvdHlwZS51cGRhdGUgPSBmdW5jdGlvbigpIHtcbiAgICAgIHRoaXMuYWdlKys7XG4gICAgICBpZiAodGhpcy5zdGF0ZSA+IFN0YXRlLkFMSVZFKSB7XG4gICAgICAgIHJldHVybjtcbiAgICAgIH1cbiAgICAgIGlmICh0aGlzLmxpZmV0aW1lID4gMCAmJiB0aGlzLmFnZSA9PT0gdGhpcy5saWZldGltZSkge1xuICAgICAgICB0aGlzLnN0YXRlID0gU3RhdGUuREVBRDtcbiAgICAgIH1cbiAgICAgIHRtcC5zZXQodGhpcy5wb3NpdGlvbik7XG4gICAgICB0aGlzLnBvc2l0aW9uLnggKz0gKHRoaXMucG9zaXRpb24ueCAtIHRoaXMucHJldi54KSArIHRoaXMuZm9yY2UueDtcbiAgICAgIHRoaXMucG9zaXRpb24ueSArPSAodGhpcy5wb3NpdGlvbi55IC0gdGhpcy5wcmV2LnkpICsgdGhpcy5mb3JjZS55O1xuICAgICAgdGhpcy5wb3NpdGlvbi56ICs9ICh0aGlzLnBvc2l0aW9uLnogLSB0aGlzLnByZXYueikgKyB0aGlzLmZvcmNlLno7XG4gICAgICB0aGlzLnByZXYuc2V0KHRtcCk7XG4gICAgICB0aGlzLnByZXYubGVycCh0aGlzLnBvc2l0aW9uLCB0aGlzLmRyYWcpO1xuICAgICAgcmV0dXJuIHRoaXMuZm9yY2UuemVybygpO1xuICAgIH07XG5cbiAgICBQYXJ0aWNsZTMucHJvdG90eXBlLnNldFBvc2l0aW9uMyA9IGZ1bmN0aW9uKHgsIHksIHopIHtcbiAgICAgIHRoaXMucG9zaXRpb24uc2V0Myh4LCB5LCB6KTtcbiAgICAgIHJldHVybiB0aGlzLnByZXYuc2V0Myh4LCB5LCB6KTtcbiAgICB9O1xuXG4gICAgcmV0dXJuIFBhcnRpY2xlMztcblxuICB9KShQYXJ0aWNsZSk7XG5cbiAgLypcbiAgXG4gICAgMkQgVmVyTGVyIFBhcnRpY2xlXG4gICovXG5cblxuICBQYXJ0aWNsZTIgPSAoZnVuY3Rpb24oX3N1cGVyKSB7XG4gICAgdmFyIHRtcDtcblxuICAgIF9fZXh0ZW5kcyhQYXJ0aWNsZTIsIF9zdXBlcik7XG5cbiAgICB0bXAgPSBuZXcgVmVjMigpO1xuXG4gICAgZnVuY3Rpb24gUGFydGljbGUyKGlkKSB7XG4gICAgICB0aGlzLmlkID0gaWQ7XG4gICAgICB0aGlzLnBvc2l0aW9uID0gbmV3IFZlYzIoKTtcbiAgICAgIHRoaXMucHJldiA9IG5ldyBWZWMyKCk7XG4gICAgICB0aGlzLmZvcmNlID0gbmV3IFZlYzIoKTtcbiAgICAgIHRoaXMudmVsb2NpdHkgPSBuZXcgVmVjMigpO1xuICAgIH1cblxuICAgIFBhcnRpY2xlMi5wcm90b3R5cGUudXBkYXRlID0gZnVuY3Rpb24oKSB7XG4gICAgICB0aGlzLmFnZSsrO1xuICAgICAgaWYgKHRoaXMuc3RhdGUgPiBTdGF0ZS5BTElWRSkge1xuICAgICAgICByZXR1cm47XG4gICAgICB9XG4gICAgICBpZiAodGhpcy5saWZldGltZSA+IDAgJiYgdGhpcy5hZ2UgPT09IHRoaXMubGlmZXRpbWUpIHtcbiAgICAgICAgdGhpcy5zdGF0ZSA9IFN0YXRlLkRFQUQ7XG4gICAgICB9XG4gICAgICB0bXAuc2V0KHRoaXMucG9zaXRpb24pO1xuICAgICAgdGhpcy5wb3NpdGlvbi54ICs9ICh0aGlzLnBvc2l0aW9uLnggLSB0aGlzLnByZXYueCkgKyB0aGlzLmZvcmNlLng7XG4gICAgICB0aGlzLnBvc2l0aW9uLnkgKz0gKHRoaXMucG9zaXRpb24ueSAtIHRoaXMucHJldi55KSArIHRoaXMuZm9yY2UueTtcbiAgICAgIHRoaXMucHJldi5zZXQodG1wKTtcbiAgICAgIHRoaXMucHJldi5sZXJwKHRoaXMucG9zaXRpb24sIHRoaXMuZHJhZyk7XG4gICAgICByZXR1cm4gdGhpcy5mb3JjZS56ZXJvKCk7XG4gICAgfTtcblxuICAgIFBhcnRpY2xlMi5wcm90b3R5cGUuc2V0UG9zaXRpb24yID0gZnVuY3Rpb24oeCwgeSkge1xuICAgICAgdGhpcy5wb3NpdGlvbi5zZXQyKHgsIHkpO1xuICAgICAgcmV0dXJuIHRoaXMucHJldi5zZXQyKHgsIHkpO1xuICAgIH07XG5cbiAgICByZXR1cm4gUGFydGljbGUyO1xuXG4gIH0pKFBhcnRpY2xlKTtcblxuICBtb2R1bGUuZXhwb3J0cyA9IHtcbiAgICBQYXJ0aWNsZTogUGFydGljbGUsXG4gICAgUGFydGljbGUyOiBQYXJ0aWNsZTIsXG4gICAgUGFydGljbGUzOiBQYXJ0aWNsZTMsXG4gICAgU3RhdGU6IFN0YXRlXG4gIH07XG5cbn0pLmNhbGwodGhpcyk7XG4iLCIvLyBHZW5lcmF0ZWQgYnkgQ29mZmVlU2NyaXB0IDEuNi4zXG4oZnVuY3Rpb24oKSB7XG4gIHZhciBCZWhhdmlvdXIsIENvbnN0cmFpbnQsIEVtaXR0ZXIsIFBhcnRpY2xlLCBQaHlzaWNzLCBTcGFjZSwgU3ByaW5nLCBwYXJ0aWNsZU1vZHVsZSwgdXRpbCwgX3JlZixcbiAgICBfX2hhc1Byb3AgPSB7fS5oYXNPd25Qcm9wZXJ0eSxcbiAgICBfX2V4dGVuZHMgPSBmdW5jdGlvbihjaGlsZCwgcGFyZW50KSB7IGZvciAodmFyIGtleSBpbiBwYXJlbnQpIHsgaWYgKF9faGFzUHJvcC5jYWxsKHBhcmVudCwga2V5KSkgY2hpbGRba2V5XSA9IHBhcmVudFtrZXldOyB9IGZ1bmN0aW9uIGN0b3IoKSB7IHRoaXMuY29uc3RydWN0b3IgPSBjaGlsZDsgfSBjdG9yLnByb3RvdHlwZSA9IHBhcmVudC5wcm90b3R5cGU7IGNoaWxkLnByb3RvdHlwZSA9IG5ldyBjdG9yKCk7IGNoaWxkLl9fc3VwZXJfXyA9IHBhcmVudC5wcm90b3R5cGU7IHJldHVybiBjaGlsZDsgfTtcblxuICB1dGlsID0gcmVxdWlyZSgnLi4vdXRpbCcpO1xuXG4gIHBhcnRpY2xlTW9kdWxlID0gcmVxdWlyZSgnLi9wYXJ0aWNsZScpO1xuXG4gIFBhcnRpY2xlID0gcGFydGljbGVNb2R1bGUuUGFydGljbGU7XG5cbiAgLypcbiAgXG4gICAgQSBQYXJ0aWNsZSBQaHlzaWNzIFNpbXVsYXRpb25cbiAgKi9cblxuXG4gIFBoeXNpY3MgPSAoZnVuY3Rpb24oKSB7XG4gICAgZnVuY3Rpb24gUGh5c2ljcygpIHtcbiAgICAgIHRoaXMucGFydGljbGVzID0gW107XG4gICAgICB0aGlzLnNwcmluZ3MgPSBbXTtcbiAgICAgIHRoaXMuYmVoYXZpb3VycyA9IFtdO1xuICAgICAgdGhpcy5jb25zdHJhaW50cyA9IFtdO1xuICAgICAgdGhpcy5lbWl0dGVyID0gbnVsbDtcbiAgICAgIHRoaXMuc3BhY2UgPSBudWxsO1xuICAgICAgdGhpcy5jb25zdHJhaW50SXRlcmF0aW9ucyA9IDE7XG4gICAgICB0aGlzLnNwcmluZ0l0ZXJhdGlvbnMgPSAxO1xuICAgICAgdGhpcy5zcGFjZSA9IG5ldyBTcGFjZSgpO1xuICAgICAgdGhpcy5lbWl0dGVyID0gbmV3IEVtaXR0ZXIodGhpcyk7XG4gICAgICB0aGlzLmNsZWFyKCk7XG4gICAgfVxuXG4gICAgUGh5c2ljcy5wcm90b3R5cGUuY2xlYXIgPSBmdW5jdGlvbigpIHtcbiAgICAgIHRoaXMucGFydGljbGVzID0gW107XG4gICAgICB0aGlzLmJlaGF2aW91cnMgPSBbXTtcbiAgICAgIHJldHVybiB0aGlzLmNvbnN0cmFpbnRzID0gW107XG4gICAgfTtcblxuICAgIFBoeXNpY3MucHJvdG90eXBlLmFkZCA9IGZ1bmN0aW9uKCkge1xuICAgICAgdmFyIGFyZywgc3RhdGU7XG4gICAgICBpZiAoYXJndW1lbnRzLmxlbmd0aCA9PT0gMCkge1xuICAgICAgICByZXR1cm47XG4gICAgICB9XG4gICAgICBhcmcgPSBhcmd1bWVudHNbMF07XG4gICAgICBpZiAoYXJnIGluc3RhbmNlb2YgUGFydGljbGUpIHtcbiAgICAgICAgdGhpcy5hZGRQYXJ0aWNsZShhcmcpO1xuICAgICAgfSBlbHNlIGlmIChhcmcgaW5zdGFuY2VvZiBCZWhhdmlvdXIpIHtcbiAgICAgICAgY29uc29sZS5sb2coXCJhZGRpbmcgYmVoYXZpb3VyIFwiICsgYXJnKTtcbiAgICAgICAgaWYgKGFyZ3VtZW50cy5sZW5ndGggPiAxKSB7XG4gICAgICAgICAgc3RhdGUgPSBhcmd1bWVudHNbMV07XG4gICAgICAgIH1cbiAgICAgICAgdGhpcy5hZGRCZWhhdmlvdXIoYXJnLCBzdGF0ZSk7XG4gICAgICB9IGVsc2UgaWYgKGFyZyBpbnN0YW5jZW9mIFNwcmluZykge1xuICAgICAgICBjb25zb2xlLmxvZyhcImFkZGluZyBzcHJpbmcgXCIgKyBhcmcpO1xuICAgICAgICB0aGlzLmFkZFNwcmluZyhhcmcpO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgXCJDYW5ub3QgYWRkIFwiICsgYXJnO1xuICAgICAgfVxuICAgICAgcmV0dXJuIGFyZztcbiAgICB9O1xuXG4gICAgUGh5c2ljcy5wcm90b3R5cGUuYWRkUGFydGljbGUgPSBmdW5jdGlvbihwYXJ0aWNsZSkge1xuICAgICAgdGhpcy5wYXJ0aWNsZXMucHVzaChwYXJ0aWNsZSk7XG4gICAgICByZXR1cm4gcGFydGljbGU7XG4gICAgfTtcblxuICAgIFBoeXNpY3MucHJvdG90eXBlLmFkZFNwcmluZyA9IGZ1bmN0aW9uKHNwcmluZykge1xuICAgICAgdGhpcy5zcHJpbmdzLnB1c2goc3ByaW5nKTtcbiAgICAgIHJldHVybiBzcHJpbmc7XG4gICAgfTtcblxuICAgIFBoeXNpY3MucHJvdG90eXBlLmFkZEJlaGF2aW91ciA9IGZ1bmN0aW9uKGVmZmVjdG9yLCBzdGF0ZSkge1xuICAgICAgdmFyIGxpc3Q7XG4gICAgICBpZiAoc3RhdGUgPT0gbnVsbCkge1xuICAgICAgICBzdGF0ZSA9IHBhcnRpY2xlTW9kdWxlLlN0YXRlLkFMSVZFO1xuICAgICAgfVxuICAgICAgbGlzdCA9IGVmZmVjdG9yIGluc3RhbmNlb2YgQ29uc3RyYWludCA/IHRoaXMuY29uc3RyYWludHMgOiB0aGlzLmJlaGF2aW91cnM7XG4gICAgICBpZiAoIWxpc3Rbc3RhdGVdKSB7XG4gICAgICAgIGxpc3Rbc3RhdGVdID0gW107XG4gICAgICB9XG4gICAgICByZXR1cm4gbGlzdFtzdGF0ZV0ucHVzaChlZmZlY3Rvcik7XG4gICAgfTtcblxuICAgIFBoeXNpY3MucHJvdG90eXBlLnVwZGF0ZSA9IGZ1bmN0aW9uKCkge1xuICAgICAgdmFyIGRlYWQsIGksIGosIHBhcnRpY2xlLCBwYXJ0aWNsZXMsIHNwcmluZywgc3RhdGVEZWFkLCBfaSwgX2osIF9rLCBfbCwgX2xlbiwgX2xlbjEsIF9yZWYsIF9yZWYxLCBfcmVmMjtcbiAgICAgIHRoaXMuZW1pdHRlci51cGRhdGUoKTtcbiAgICAgIHRoaXMuc3BhY2UudXBkYXRlKHRoaXMpO1xuICAgICAgcGFydGljbGVzID0gdGhpcy5wYXJ0aWNsZXM7XG4gICAgICB0aGlzLmFwcGx5RWZmZWN0b3JzKHRoaXMuYmVoYXZpb3VycywgcGFydGljbGVzKTtcbiAgICAgIGZvciAoaiA9IF9pID0gMCwgX3JlZiA9IHRoaXMuY29uc3RyYWludEl0ZXJhdGlvbnM7IDAgPD0gX3JlZiA/IF9pIDw9IF9yZWYgOiBfaSA+PSBfcmVmOyBqID0gMCA8PSBfcmVmID8gKytfaSA6IC0tX2kpIHtcbiAgICAgICAgdGhpcy5hcHBseUVmZmVjdG9ycyh0aGlzLmNvbnN0cmFpbnRzLCBwYXJ0aWNsZXMpO1xuICAgICAgICBmb3IgKGkgPSBfaiA9IDAsIF9yZWYxID0gdGhpcy5zcHJpbmdJdGVyYXRpb25zOyAwIDw9IF9yZWYxID8gX2ogPD0gX3JlZjEgOiBfaiA+PSBfcmVmMTsgaSA9IDAgPD0gX3JlZjEgPyArK19qIDogLS1faikge1xuICAgICAgICAgIF9yZWYyID0gdGhpcy5zcHJpbmdzO1xuICAgICAgICAgIGZvciAoX2sgPSAwLCBfbGVuID0gX3JlZjIubGVuZ3RoOyBfayA8IF9sZW47IF9rKyspIHtcbiAgICAgICAgICAgIHNwcmluZyA9IF9yZWYyW19rXTtcbiAgICAgICAgICAgIHNwcmluZy51cGRhdGUoKTtcbiAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICAgIGRlYWQgPSBbXTtcbiAgICAgIHN0YXRlRGVhZCA9IHBhcnRpY2xlTW9kdWxlLlN0YXRlLkRFQUQ7XG4gICAgICBmb3IgKF9sID0gMCwgX2xlbjEgPSBwYXJ0aWNsZXMubGVuZ3RoOyBfbCA8IF9sZW4xOyBfbCsrKSB7XG4gICAgICAgIHBhcnRpY2xlID0gcGFydGljbGVzW19sXTtcbiAgICAgICAgcGFydGljbGUudXBkYXRlKCk7XG4gICAgICAgIGlmIChwYXJ0aWNsZS5zdGF0ZSA9PT0gc3RhdGVEZWFkKSB7XG4gICAgICAgICAgZGVhZC5wdXNoKHBhcnRpY2xlKTtcbiAgICAgICAgfVxuICAgICAgICB2b2lkIDA7XG4gICAgICB9XG4gICAgICBpID0gZGVhZC5sZW5ndGg7XG4gICAgICB3aGlsZSAoaS0tKSB7XG4gICAgICAgIHBhcnRpY2xlID0gZGVhZFtpXTtcbiAgICAgICAgdXRpbC5yZW1vdmVFbGVtZW50KHBhcnRpY2xlLCBwYXJ0aWNsZXMpO1xuICAgICAgICB2b2lkIDA7XG4gICAgICB9XG4gICAgICByZXR1cm4gdm9pZCAwO1xuICAgIH07XG5cbiAgICBQaHlzaWNzLnByb3RvdHlwZS5hcHBseUVmZmVjdG9ycyA9IGZ1bmN0aW9uKGVmZmVjdG9ycywgcGFydGljbGVzKSB7XG4gICAgICB2YXIgZWZmZWN0b3IsIHBhcnRpY2xlLCBzdGF0ZSwgc3RhdGVFZmZlY3RvcnMsIF9pLCBfaiwgX2xlbiwgX2xlbjE7XG4gICAgICBzdGF0ZSA9IGVmZmVjdG9ycy5sZW5ndGg7XG4gICAgICB3aGlsZSAoc3RhdGUtLSkge1xuICAgICAgICBzdGF0ZUVmZmVjdG9ycyA9IGVmZmVjdG9yc1tzdGF0ZV07XG4gICAgICAgIGZvciAoX2kgPSAwLCBfbGVuID0gc3RhdGVFZmZlY3RvcnMubGVuZ3RoOyBfaSA8IF9sZW47IF9pKyspIHtcbiAgICAgICAgICBlZmZlY3RvciA9IHN0YXRlRWZmZWN0b3JzW19pXTtcbiAgICAgICAgICBlZmZlY3Rvci5wcmVwYXJlKHRoaXMpO1xuICAgICAgICAgIGZvciAoX2ogPSAwLCBfbGVuMSA9IHBhcnRpY2xlcy5sZW5ndGg7IF9qIDwgX2xlbjE7IF9qKyspIHtcbiAgICAgICAgICAgIHBhcnRpY2xlID0gcGFydGljbGVzW19qXTtcbiAgICAgICAgICAgIGlmIChwYXJ0aWNsZS5zdGF0ZSA9PT0gc3RhdGUgJiYgIXBhcnRpY2xlLmlzTG9ja2VkKSB7XG4gICAgICAgICAgICAgIGVmZmVjdG9yLmFwcGx5KHBhcnRpY2xlKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHZvaWQgMDtcbiAgICAgICAgICB9XG4gICAgICAgICAgdm9pZCAwO1xuICAgICAgICB9XG4gICAgICAgIHZvaWQgMDtcbiAgICAgIH1cbiAgICAgIHJldHVybiB2b2lkIDA7XG4gICAgfTtcblxuICAgIFBoeXNpY3MucHJvdG90eXBlLnNpemUgPSBmdW5jdGlvbigpIHtcbiAgICAgIHJldHVybiB0aGlzLnBhcnRpY2xlcy5sZW5ndGg7XG4gICAgfTtcblxuICAgIHJldHVybiBQaHlzaWNzO1xuXG4gIH0pKCk7XG5cbiAgLypcbiAgICBTcGF0aWFsIFNlYXJjaFxuICBcbiAgICBTaW1wbGUgYnJ1dGUgZm9yY2Ugc3BhdGlhbCBzZWFyY2hlcy5cbiAgICBTdWJjbGFzc2VzIG1heSBvdmVycmlkZSB0aGlzIHRvIG9yZ2FuaXNlIHBhcnRpY2xlcyBzbyB0aGV5IGNhbiBiZSBmb3VuZCBxdWlja2VyIGxhdGVyXG4gICovXG5cblxuICBTcGFjZSA9IChmdW5jdGlvbigpIHtcbiAgICB2YXIgcGh5c2ljcztcblxuICAgIHBoeXNpY3MgPSBudWxsO1xuXG4gICAgZnVuY3Rpb24gU3BhY2UoKSB7fVxuXG4gICAgU3BhY2UucHJvdG90eXBlLnVwZGF0ZSA9IGZ1bmN0aW9uKHBoeXNpY3NfKSB7XG4gICAgICByZXR1cm4gcGh5c2ljcyA9IHBoeXNpY3NfO1xuICAgIH07XG5cbiAgICBTcGFjZS5wcm90b3R5cGUuc2VhcmNoID0gZnVuY3Rpb24oY2VudGVyLCByYWRpdXMpIHtcbiAgICAgIHZhciBwYXJ0aWNsZSwgcmFkaXVzU3EsIHJlc3VsdCwgX2ksIF9sZW4sIF9yZWY7XG4gICAgICByZXN1bHQgPSBbXTtcbiAgICAgIHJhZGl1c1NxID0gcmFkaXVzICogcmFkaXVzO1xuICAgICAgX3JlZiA9IHBoeXNpY3MucGFydGljbGVzO1xuICAgICAgZm9yIChfaSA9IDAsIF9sZW4gPSBfcmVmLmxlbmd0aDsgX2kgPCBfbGVuOyBfaSsrKSB7XG4gICAgICAgIHBhcnRpY2xlID0gX3JlZltfaV07XG4gICAgICAgIGlmIChjZW50ZXIuZGlzdGFuY2VTcXVhcmVkKHBhcnRpY2xlLnBvc2l0aW9uKSA8IHJhZGl1c1NxKSB7XG4gICAgICAgICAgcmVzdWx0LnB1c2gocGFydGljbGUpO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgICByZXR1cm4gcmVzdWx0O1xuICAgIH07XG5cbiAgICByZXR1cm4gU3BhY2U7XG5cbiAgfSkoKTtcblxuICAvKlxuICAgIFBhcnRpY2xlIEVtaXR0ZXJcbiAgKi9cblxuXG4gIEVtaXR0ZXIgPSAoZnVuY3Rpb24oKSB7XG4gICAgdmFyIGlkLCB0aW1lcjtcblxuICAgIHRpbWVyID0gLTE7XG5cbiAgICBpZCA9IDA7XG5cbiAgICBmdW5jdGlvbiBFbWl0dGVyKHBoeXNpY3MpIHtcbiAgICAgIHRoaXMucGh5c2ljcyA9IHBoeXNpY3M7XG4gICAgICB0aGlzLnJhdGUgPSAxO1xuICAgICAgdGhpcy5pbnRlcnZhbCA9IDE7XG4gICAgICB0aGlzLm1heCA9IDEwMDtcbiAgICAgIHRoaXMudHlwZSA9IHBhcnRpY2xlTW9kdWxlLlBhcnRpY2xlMztcbiAgICB9XG5cbiAgICBFbWl0dGVyLnByb3RvdHlwZS51cGRhdGUgPSBmdW5jdGlvbigpIHtcbiAgICAgIHZhciBpLCBwO1xuICAgICAgaWYgKHRpbWVyID09PSAtMSB8fCB0aW1lciA+PSB0aGlzLmludGVydmFsKSB7XG4gICAgICAgIHRpbWVyID0gMDtcbiAgICAgICAgaSA9IDA7XG4gICAgICAgIHdoaWxlIChpIDwgdGhpcy5yYXRlICYmIHRoaXMucGh5c2ljcy5zaXplKCkgPCB0aGlzLm1heCkge1xuICAgICAgICAgIHAgPSB0aGlzLmNyZWF0ZSgpO1xuICAgICAgICAgIHRoaXMuaW5pdChwKTtcbiAgICAgICAgICBpKys7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICAgIHJldHVybiB0aW1lcisrO1xuICAgIH07XG5cbiAgICBFbWl0dGVyLnByb3RvdHlwZS5jcmVhdGUgPSBmdW5jdGlvbigpIHtcbiAgICAgIHZhciBwO1xuICAgICAgcCA9IG5ldyB0aGlzLnR5cGUoaWQrKyk7XG4gICAgICB0aGlzLnBoeXNpY3MuYWRkUGFydGljbGUocCk7XG4gICAgICByZXR1cm4gcDtcbiAgICB9O1xuXG4gICAgRW1pdHRlci5wcm90b3R5cGUuaW5pdCA9IGZ1bmN0aW9uKHBhcnRpY2xlKSB7fTtcblxuICAgIHJldHVybiBFbWl0dGVyO1xuXG4gIH0pKCk7XG5cbiAgLypcbiAgICBCYXNlIGNsYXNzIGZvciBhbGwgcGh5c2ljYWwgZm9yY2VzLCBiZWhhdmlvdXJzICYgY29uc3RyYWludHNcbiAgKi9cblxuXG4gIEJlaGF2aW91ciA9IChmdW5jdGlvbigpIHtcbiAgICBmdW5jdGlvbiBCZWhhdmlvdXIoKSB7fVxuXG4gICAgQmVoYXZpb3VyLnByb3RvdHlwZS5wcmVwYXJlID0gZnVuY3Rpb24oKSB7fTtcblxuICAgIEJlaGF2aW91ci5wcm90b3R5cGUuYXBwbHkgPSBmdW5jdGlvbihwYXJ0aWNsZSkge307XG5cbiAgICByZXR1cm4gQmVoYXZpb3VyO1xuXG4gIH0pKCk7XG5cbiAgQ29uc3RyYWludCA9IChmdW5jdGlvbihfc3VwZXIpIHtcbiAgICBfX2V4dGVuZHMoQ29uc3RyYWludCwgX3N1cGVyKTtcblxuICAgIGZ1bmN0aW9uIENvbnN0cmFpbnQoKSB7XG4gICAgICBfcmVmID0gQ29uc3RyYWludC5fX3N1cGVyX18uY29uc3RydWN0b3IuYXBwbHkodGhpcywgYXJndW1lbnRzKTtcbiAgICAgIHJldHVybiBfcmVmO1xuICAgIH1cblxuICAgIENvbnN0cmFpbnQucHJvdG90eXBlLnByZXBhcmUgPSBmdW5jdGlvbigpIHt9O1xuXG4gICAgQ29uc3RyYWludC5wcm90b3R5cGUuYXBwbHkgPSBmdW5jdGlvbihwYXJ0aWNsZSkge307XG5cbiAgICByZXR1cm4gQ29uc3RyYWludDtcblxuICB9KShCZWhhdmlvdXIpO1xuXG4gIC8qXG4gICAgVmVybGV0IFNwcmluZ1xuICAqL1xuXG5cbiAgU3ByaW5nID0gKGZ1bmN0aW9uKCkge1xuICAgIGZ1bmN0aW9uIFNwcmluZyhhLCBiLCBzdHJlbmd0aCkge1xuICAgICAgdGhpcy5hID0gYTtcbiAgICAgIHRoaXMuYiA9IGI7XG4gICAgICB0aGlzLnN0cmVuZ3RoID0gc3RyZW5ndGggIT0gbnVsbCA/IHN0cmVuZ3RoIDogMC41O1xuICAgICAgdGhpcy5yZXN0TGVuZ3RoID0gdGhpcy5hLnBvc2l0aW9uLmRpc3RhbmNlKHRoaXMuYi5wb3NpdGlvbik7XG4gICAgfVxuXG4gICAgU3ByaW5nLnByb3RvdHlwZS51cGRhdGUgPSBmdW5jdGlvbigpIHtcbiAgICAgIHZhciBkZWx0YSwgZGlzdCwgbm9ybURpc3RTdHJlbmd0aDtcbiAgICAgIGRlbHRhID0gdGhpcy5iLnBvc2l0aW9uLnN1Yl8odGhpcy5hLnBvc2l0aW9uKTtcbiAgICAgIGRpc3QgPSBkZWx0YS5sZW5ndGgoKSArIE51bWJlci5NSU5fVkFMVUU7XG4gICAgICBub3JtRGlzdFN0cmVuZ3RoID0gKGRpc3QgLSB0aGlzLnJlc3RMZW5ndGgpIC8gZGlzdCAqIHRoaXMuc3RyZW5ndGg7XG4gICAgICBpZiAobm9ybURpc3RTdHJlbmd0aCA9PT0gMCkge1xuICAgICAgICByZXR1cm47XG4gICAgICB9XG4gICAgICBkZWx0YS5zY2FsZShub3JtRGlzdFN0cmVuZ3RoKTtcbiAgICAgIGlmICghdGhpcy5hLmlzTG9ja2VkKSB7XG4gICAgICAgIHRoaXMuYS5wb3NpdGlvbi5hZGQoZGVsdGEpO1xuICAgICAgfVxuICAgICAgaWYgKCF0aGlzLmIuaXNMb2NrZWQpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuYi5wb3NpdGlvbi5zdWIoZGVsdGEpO1xuICAgICAgfVxuICAgIH07XG5cbiAgICBTcHJpbmcucHJvdG90eXBlLnRvU3RyaW5nID0gZnVuY3Rpb24oKSB7XG4gICAgICByZXR1cm4gXCJTcHJpbmcoXCIgKyBhICsgXCIsIFwiICsgYiArIFwiKVwiO1xuICAgIH07XG5cbiAgICByZXR1cm4gU3ByaW5nO1xuXG4gIH0pKCk7XG5cbiAgbW9kdWxlLmV4cG9ydHMgPSB7XG4gICAgUGh5c2ljczogUGh5c2ljcyxcbiAgICBFbWl0dGVyOiBFbWl0dGVyLFxuICAgIFNwYWNlOiBTcGFjZSxcbiAgICBCZWhhdmlvdXI6IEJlaGF2aW91cixcbiAgICBDb25zdHJhaW50OiBDb25zdHJhaW50LFxuICAgIFNwcmluZzogU3ByaW5nXG4gIH07XG5cbn0pLmNhbGwodGhpcyk7XG4iLCIvLyBHZW5lcmF0ZWQgYnkgQ29mZmVlU2NyaXB0IDEuNi4zXG4oZnVuY3Rpb24oKSB7XG4gIHZhciBUZW1wbywgVGltZSwgVGltZXIsIFRpbWVzcGFuLCBtYXRoLCB1dGlsLFxuICAgIF9faGFzUHJvcCA9IHt9Lmhhc093blByb3BlcnR5LFxuICAgIF9fZXh0ZW5kcyA9IGZ1bmN0aW9uKGNoaWxkLCBwYXJlbnQpIHsgZm9yICh2YXIga2V5IGluIHBhcmVudCkgeyBpZiAoX19oYXNQcm9wLmNhbGwocGFyZW50LCBrZXkpKSBjaGlsZFtrZXldID0gcGFyZW50W2tleV07IH0gZnVuY3Rpb24gY3RvcigpIHsgdGhpcy5jb25zdHJ1Y3RvciA9IGNoaWxkOyB9IGN0b3IucHJvdG90eXBlID0gcGFyZW50LnByb3RvdHlwZTsgY2hpbGQucHJvdG90eXBlID0gbmV3IGN0b3IoKTsgY2hpbGQuX19zdXBlcl9fID0gcGFyZW50LnByb3RvdHlwZTsgcmV0dXJuIGNoaWxkOyB9O1xuXG4gIHV0aWwgPSByZXF1aXJlKCcuL3V0aWwnKTtcblxuICBtYXRoID0gcmVxdWlyZSgnLi9tYXRoL21hdGgnKTtcblxuICAvKlxuICAgIFRpbWVyOiBrZWVwcyB0cmFjayBvZiB0aW1lLCBtZWFzdXJlcyBpbnRlcnZhbHMgZXRjLlxuICAgIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuICAqL1xuXG5cbiAgVGltZXIgPSAoZnVuY3Rpb24oKSB7XG4gICAgVGltZXIucHJvdG90eXBlLm5vdyA9IG51bGw7XG5cbiAgICBUaW1lci5wcm90b3R5cGUucHJldiA9IG51bGw7XG5cbiAgICBmdW5jdGlvbiBUaW1lcigpIHtcbiAgICAgIHRoaXMucmVzZXQoKTtcbiAgICB9XG5cbiAgICBUaW1lci5wcm90b3R5cGUudXBkYXRlID0gZnVuY3Rpb24oKSB7XG4gICAgICB2YXIgZHQ7XG4gICAgICB0aGlzLm5vdyA9IERhdGUubm93KCk7XG4gICAgICBkdCA9IHRoaXMubm93IC0gdGhpcy5wcmV2O1xuICAgICAgdGhpcy5wcmV2ID0gdGhpcy5ub3c7XG4gICAgICByZXR1cm4gZHQ7XG4gICAgfTtcblxuICAgIFRpbWVyLnByb3RvdHlwZS5lbGFwc2VkID0gZnVuY3Rpb24oKSB7XG4gICAgICByZXR1cm4gRGF0ZS5ub3coKSAtIHRoaXMucHJldjtcbiAgICB9O1xuXG4gICAgVGltZXIucHJvdG90eXBlLnJlc2V0ID0gZnVuY3Rpb24oKSB7XG4gICAgICByZXR1cm4gdGhpcy5ub3cgPSB0aGlzLnByZXYgPSBEYXRlLm5vdygpO1xuICAgIH07XG5cbiAgICByZXR1cm4gVGltZXI7XG5cbiAgfSkoKTtcblxuICAvKlxuICAgIFRlbXBvOiBrZWVwcyB0cmFjayBvZiByaHl0aG0sIGNvbnZlcnRzIGJldHdlZW4gYmVhdHMsIGJhcnMsIHRpbWUsIHRlbXBvIGV0Y1xuICAgIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuICAqL1xuXG5cbiAgVGVtcG8gPSAoZnVuY3Rpb24oKSB7XG4gICAgVGVtcG8ucHJvdG90eXBlLmJwbSA9IDEyMDtcblxuICAgIFRlbXBvLnByb3RvdHlwZS5zaWdOdW0gPSA0O1xuXG4gICAgVGVtcG8ucHJvdG90eXBlLnNpZ0Rlbm9tID0gNDtcblxuICAgIFRlbXBvLnByb3RvdHlwZS5yZXNvbHV0aW9uID0gMzI7XG5cbiAgICBUZW1wby5wcm90b3R5cGUuYmVhdEludGVydmFsID0gMDtcblxuICAgIFRlbXBvLnByb3RvdHlwZS5ncmlkSW50ZXJ2YWwgPSAwO1xuXG4gICAgVGVtcG8ucHJvdG90eXBlLnRpbWUgPSAwO1xuXG4gICAgVGVtcG8ucHJvdG90eXBlLnByZXZFdmVudCA9IDA7XG5cbiAgICBUZW1wby5wcm90b3R5cGUuYmVhdHMgPSAwO1xuXG4gICAgVGVtcG8ucHJvdG90eXBlLmJhcnMgPSAwO1xuXG4gICAgVGVtcG8ucHJvdG90eXBlLmJlYXQgPSAwO1xuXG4gICAgVGVtcG8ucHJvdG90eXBlLm9uQmVhdCA9IGZhbHNlO1xuXG4gICAgVGVtcG8ucHJvdG90eXBlLm9uQmFyID0gZmFsc2U7XG5cbiAgICBUZW1wby5wcm90b3R5cGUub242NCA9IGZhbHNlO1xuXG4gICAgVGVtcG8ucHJvdG90eXBlLm9uMzIgPSBmYWxzZTtcblxuICAgIFRlbXBvLnByb3RvdHlwZS5vbjE2ID0gZmFsc2U7XG5cbiAgICBUZW1wby5wcm90b3R5cGUub244ID0gZmFsc2U7XG5cbiAgICBUZW1wby5wcm90b3R5cGUub240ID0gZmFsc2U7XG5cbiAgICBUZW1wby5wcm90b3R5cGUub24yID0gZmFsc2U7XG5cbiAgICBmdW5jdGlvbiBUZW1wbyhicG0sIHNpZ051bSwgc2lnRGVub20sIHJlc29sdXRpb24pIHtcbiAgICAgIHRoaXMuYnBtID0gYnBtICE9IG51bGwgPyBicG0gOiAxMjA7XG4gICAgICB0aGlzLnNpZ051bSA9IHNpZ051bSAhPSBudWxsID8gc2lnTnVtIDogNDtcbiAgICAgIHRoaXMuc2lnRGVub20gPSBzaWdEZW5vbSAhPSBudWxsID8gc2lnRGVub20gOiA0O1xuICAgICAgdGhpcy5yZXNvbHV0aW9uID0gcmVzb2x1dGlvbiAhPSBudWxsID8gcmVzb2x1dGlvbiA6IDMyO1xuICAgICAgdGhpcy5yZXNldCgpO1xuICAgIH1cblxuICAgIFRlbXBvLnByb3RvdHlwZS5yZXNldCA9IGZ1bmN0aW9uKCkge1xuICAgICAgdGhpcy5iZWF0SW50ZXJ2YWwgPSAxMDAwIC8gKHRoaXMuYnBtIC8gNjApO1xuICAgICAgdGhpcy5ncmlkSW50ZXJ2YWwgPSB0aGlzLmJlYXRJbnRlcnZhbCAqIHRoaXMuc2lnTnVtIC8gdGhpcy5yZXNvbHV0aW9uO1xuICAgICAgdGhpcy50aW1lID0gdGhpcy5wcmV2RXZlbnQgPSAwO1xuICAgICAgdGhpcy5iZWF0cyA9IDA7XG4gICAgICB0aGlzLmJlYXQgPSB0aGlzLmJhcnMgPSAwO1xuICAgICAgdGhpcy5vbkJlYXQgPSB0aGlzLm9uQmFyID0gZmFsc2U7XG4gICAgICByZXR1cm4gdGhpcy5vbjY0ID0gdGhpcy5vbjMyID0gdGhpcy5vbjE2ID0gdGhpcy5vbjggPSB0aGlzLm9uNCA9IHRoaXMub24yID0gZmFsc2U7XG4gICAgfTtcblxuICAgIFRlbXBvLnByb3RvdHlwZS51cGRhdGUgPSBmdW5jdGlvbihkdCkge1xuICAgICAgdmFyIGZvcmNlT25HcmlkO1xuICAgICAgZm9yY2VPbkdyaWQgPSB0aGlzLnRpbWUgLSB0aGlzLnByZXZFdmVudCA+PSB0aGlzLmdyaWRJbnRlcnZhbDtcbiAgICAgIHRoaXMuc2V0VGltZSh0aGlzLnRpbWUgKyBkdCwgZm9yY2VPbkdyaWQpO1xuICAgICAgcmV0dXJuIHRoaXMuYmVhdDtcbiAgICB9O1xuXG4gICAgVGVtcG8ucHJvdG90eXBlLnNldFRpbWUgPSBmdW5jdGlvbih0aW1lLCBmb3JjZU9uR3JpZCkge1xuICAgICAgdmFyIGdyaWRVbml0cywgciwgdTtcbiAgICAgIHRoaXMudGltZSA9IHRpbWU7XG4gICAgICBpZiAodGhpcy50aW1lICUgdGhpcy5ncmlkSW50ZXJ2YWwgPT09IDAgfHwgZm9yY2VPbkdyaWQpIHtcbiAgICAgICAgdGhpcy5wcmV2RXZlbnQgPSB0aW1lO1xuICAgICAgICBncmlkVW5pdHMgPSBNYXRoLmZsb29yKHRoaXMudGltZSAvIHRoaXMuZ3JpZEludGVydmFsKTtcbiAgICAgICAgdSA9IGdyaWRVbml0cztcbiAgICAgICAgciA9IHRoaXMucmVzb2x1dGlvbjtcbiAgICAgICAgdGhpcy5iZWF0cyA9IE1hdGguZmxvb3IodGhpcy50aW1lIC8gdGhpcy5iZWF0SW50ZXJ2YWwpO1xuICAgICAgICB0aGlzLmJhcnMgPSBNYXRoLmZsb29yKHRoaXMuYmVhdHMgLyB0aGlzLnNpZ0Rlbm9tKTtcbiAgICAgICAgdGhpcy5iZWF0ID0gdGhpcy5iZWF0cyAlIHRoaXMuc2lnTnVtO1xuICAgICAgICB0aGlzLm9uQmVhdCA9IHUgJSAociAvIHRoaXMuc2lnTnVtKSA9PT0gMDtcbiAgICAgICAgdGhpcy5vbkJhciA9ICh1ICUgdGhpcy5yZXNvbHV0aW9uKSA9PT0gMDtcbiAgICAgICAgdGhpcy5vbjY0ID0gdSAlIChyIC8gNjQpID09PSAwO1xuICAgICAgICB0aGlzLm9uMzIgPSB1ICUgKHIgLyAzMikgPT09IDA7XG4gICAgICAgIHRoaXMub24xNiA9IHUgJSAociAvIDE2KSA9PT0gMDtcbiAgICAgICAgdGhpcy5vbjggPSB1ICUgKHIgLyA4KSA9PT0gMDtcbiAgICAgICAgdGhpcy5vbjQgPSB1ICUgKHIgLyA0KSA9PT0gMDtcbiAgICAgICAgdGhpcy5vbjIgPSB1ICUgKHIgLyAyKSA9PT0gMDtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHRoaXMub25CZWF0ID0gdGhpcy5vbkJhciA9IGZhbHNlO1xuICAgICAgICB0aGlzLm9uNjQgPSB0aGlzLm9uMzIgPSB0aGlzLm9uMTYgPSB0aGlzLm9uOCA9IHRoaXMub240ID0gdGhpcy5vbjIgPSBmYWxzZTtcbiAgICAgIH1cbiAgICAgIHJldHVybiB0aGlzLmJlYXQ7XG4gICAgfTtcblxuICAgIHJldHVybiBUZW1wbztcblxuICB9KSgpO1xuXG4gIC8qXG4gICAgVGltZTogUmVwcmVzZW50cyBhIHNpbmdsZSBtb21lbnQgaW4gdGltZVxuICAgIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuICAqL1xuXG5cbiAgVGltZSA9IChmdW5jdGlvbihfc3VwZXIpIHtcbiAgICBfX2V4dGVuZHMoVGltZSwgX3N1cGVyKTtcblxuICAgIFRpbWUucHJvdG90eXBlLnZhbHVlID0gMDtcblxuICAgIGZ1bmN0aW9uIFRpbWUoYXJnLCBmcHMsIHRlbXBvKSB7XG4gICAgICB0aGlzLnNldChhcmcsIGZwcywgdGVtcG8pO1xuICAgIH1cblxuICAgIFRpbWUucHJvdG90eXBlLnNldCA9IGZ1bmN0aW9uKGFyZywgZnBzLCB0ZW1wbykge1xuICAgICAgaWYgKGFyZyA9PSBudWxsKSB7XG4gICAgICAgIGFyZyA9IDA7XG4gICAgICB9XG4gICAgICByZXR1cm4gdGhpcy52YWx1ZSA9IChmdW5jdGlvbigpIHtcbiAgICAgICAgc3dpdGNoICh0eXBlb2YgYXJnKSB7XG4gICAgICAgICAgY2FzZSAnbnVtYmVyJzpcbiAgICAgICAgICAgIHJldHVybiBhcmc7XG4gICAgICAgICAgY2FzZSAnc3RyaW5nJzpcbiAgICAgICAgICAgIHJldHVybiB0aGlzW1wiZXZhbFwiXShhcmcsIGZwcywgdGVtcG8pO1xuICAgICAgICAgIGRlZmF1bHQ6XG4gICAgICAgICAgICByZXR1cm4gYXJnLnZhbHVlO1xuICAgICAgICB9XG4gICAgICB9KS5jYWxsKHRoaXMpO1xuICAgIH07XG5cbiAgICBUaW1lLnByb3RvdHlwZS5hZGQgPSBmdW5jdGlvbih0aW1lKSB7XG4gICAgICByZXR1cm4gdGhpcy52YWx1ZSArPSB0eXBlb2YgdGltZSA9PT0gJ251bWJlcicgPyB0aW1lIDogdGltZS52YWx1ZTtcbiAgICB9O1xuXG4gICAgVGltZS5wcm90b3R5cGUuYWRkXyA9IGZ1bmN0aW9uKHRpbWUpIHtcbiAgICAgIHJldHVybiBuZXcgVGltZSh0aGlzLnZhbHVlICsgKHR5cGVvZiB0aW1lID09PSAnbnVtYmVyJyA/IHRpbWUgOiB0aW1lLnZhbHVlKSk7XG4gICAgfTtcblxuICAgIFRpbWUucHJvdG90eXBlLnN1YiA9IGZ1bmN0aW9uKHRpbWUpIHtcbiAgICAgIHJldHVybiB0aGlzLnRpbWUgLT0gdHlwZW9mIHRpbWUgPT09ICdudW1iZXInID8gdGltZSA6IHRpbWUudmFsdWU7XG4gICAgfTtcblxuICAgIFRpbWUucHJvdG90eXBlLnN1Yl8gPSBmdW5jdGlvbih0aW1lKSB7XG4gICAgICByZXR1cm4gbmV3IFRpbWUodGhpcy52YWx1ZSAtICh0eXBlb2YgdGltZSA9PT0gJ251bWJlcicgPyB0aW1lIDogdGltZS52YWx1ZSkpO1xuICAgIH07XG5cbiAgICBUaW1lLnByb3RvdHlwZS5zY2FsZSA9IGZ1bmN0aW9uKGZhY3Rvcikge1xuICAgICAgcmV0dXJuIHRoaXMudmFsdWUgKj0gZmFjdG9yO1xuICAgIH07XG5cbiAgICBUaW1lLnByb3RvdHlwZS5zY2FsZV8gPSBmdW5jdGlvbihmYWN0b3IpIHtcbiAgICAgIHJldHVybiBuZXcgVGltZSh0aGlzLnZhbHVlICogZmFjdG9yKTtcbiAgICB9O1xuXG4gICAgVGltZS5wcm90b3R5cGUuY2xvbmUgPSBmdW5jdGlvbigpIHtcbiAgICAgIHJldHVybiBuZXcgVGltZSh0aGlzLnZhbHVlKTtcbiAgICB9O1xuXG4gICAgVGltZS5wcm90b3R5cGUuZXF1YWxzID0gZnVuY3Rpb24odGltZSkge1xuICAgICAgcmV0dXJuIHRoaXMudmFsdWUgPT09ICh0eXBlb2YgdGltZSA9PT0gJ251bWJlcicgPyB0aW1lIDogdGltZS52YWx1ZSk7XG4gICAgfTtcblxuICAgIFRpbWUucHJvdG90eXBlLnRvU3RyaW5nID0gZnVuY3Rpb24oKSB7XG4gICAgICByZXR1cm4gXCJcIiArIHRoaXMudmFsdWUgKyBcIm1zXCI7XG4gICAgfTtcblxuICAgIFRpbWUucHJvdG90eXBlLm5vcm1hbGl6ZWRUbyA9IGZ1bmN0aW9uKHNwYW4pIHtcbiAgICAgIHJldHVybiBtYXRoLmZpdCh0aGlzLnZhbHVlLCBzcGFuLmZyb20udmFsdWUsIHNwYW4udG8udmFsdWUsIDAsIDEpO1xuICAgIH07XG5cbiAgICBUaW1lLnNldCgncycsIGZ1bmN0aW9uKHNlY29uZHMpIHtcbiAgICAgIHJldHVybiB0aGlzLnZhbHVlID0gc2Vjb25kcyAqIDEwMDA7XG4gICAgfSk7XG5cbiAgICBUaW1lLmdldCgncycsIGZ1bmN0aW9uKCkge1xuICAgICAgcmV0dXJuIHRoaXMudmFsdWUgLyAxMDAwO1xuICAgIH0pO1xuXG4gICAgVGltZS5wcm90b3R5cGUudG9GcmFtZSA9IGZ1bmN0aW9uKGZwcykge1xuICAgICAgaWYgKGZwcyA9PSBudWxsKSB7XG4gICAgICAgIGZwcyA9IDYwO1xuICAgICAgfVxuICAgICAgcmV0dXJuIE1hdGguZmxvb3IodGhpcy52YWx1ZSAvICgxMDAwIC8gZnBzKSk7XG4gICAgfTtcblxuICAgIFRpbWUucHJvdG90eXBlW1wiZXZhbFwiXSA9IGZ1bmN0aW9uKHN0cmluZywgZnBzLCB0ZW1wbykge1xuICAgICAgdmFyIGludGVydmFsLCByZSwgdW5pdCwgdW5pdHMsIF9pLCBfbGVuO1xuICAgICAgaWYgKHRlbXBvID09IG51bGwpIHtcbiAgICAgICAgdGVtcG8gPSBudWxsO1xuICAgICAgfVxuICAgICAgdW5pdHMgPSBbXG4gICAgICAgIHtcbiAgICAgICAgICBzeW1ib2w6ICdtcycsXG4gICAgICAgICAgZmFjdG9yOiAxXG4gICAgICAgIH0sIHtcbiAgICAgICAgICBzeW1ib2w6ICdzJyxcbiAgICAgICAgICBmYWN0b3I6IDEwMDBcbiAgICAgICAgfSwge1xuICAgICAgICAgIHN5bWJvbDogJ20nLFxuICAgICAgICAgIGZhY3RvcjogNjAwMDBcbiAgICAgICAgfSwge1xuICAgICAgICAgIHN5bWJvbDogJ2YnLFxuICAgICAgICAgIGZhY3RvcjogMTAwMCAvIGZwc1xuICAgICAgICB9XG4gICAgICBdO1xuICAgICAgaWYgKHRlbXBvICE9IG51bGwpIHtcbiAgICAgICAgaW50ZXJ2YWwgPSB0ZW1wby5ncmlkSW50ZXJ2YWw7XG4gICAgICAgIHVuaXRzLnB1c2goe1xuICAgICAgICAgIHN5bWJvbDogJ2knLFxuICAgICAgICAgIGZhY3RvcjogaW50ZXJ2YWxcbiAgICAgICAgfSk7XG4gICAgICAgIHVuaXRzLnB1c2goe1xuICAgICAgICAgIHN5bWJvbDogJ24nLFxuICAgICAgICAgIGZhY3RvcjogaW50ZXJ2YWwgKiB0ZW1wby5yZXNvbHV0aW9uXG4gICAgICAgIH0pO1xuICAgICAgfVxuICAgICAgZm9yIChfaSA9IDAsIF9sZW4gPSB1bml0cy5sZW5ndGg7IF9pIDwgX2xlbjsgX2krKykge1xuICAgICAgICB1bml0ID0gdW5pdHNbX2ldO1xuICAgICAgICByZSA9IG5ldyBSZWdFeHAoXCJcXFxcZCsoPz1cIiArIHVuaXQuc3ltYm9sICsgXCIpXCIsIFwiZ1wiKTtcbiAgICAgICAgc3RyaW5nID0gc3RyaW5nLnJlcGxhY2UocmUsIGZ1bmN0aW9uKHZhbHVlKSB7XG4gICAgICAgICAgcmV0dXJuIHZhbHVlICogdW5pdC5mYWN0b3I7XG4gICAgICAgIH0pO1xuICAgICAgICByZSA9IG5ldyBSZWdFeHAodW5pdC5zeW1ib2wsIFwiZ1wiKTtcbiAgICAgICAgc3RyaW5nID0gc3RyaW5nLnJlcGxhY2UocmUsICcnKTtcbiAgICAgIH1cbiAgICAgIHJldHVybiBldmFsKHN0cmluZyk7XG4gICAgfTtcblxuICAgIFRpbWUucyA9IGZ1bmN0aW9uKHNlY29uZHMpIHtcbiAgICAgIHJldHVybiBuZXcgVGltZShzZWNvbmRzICogMTAwMCk7XG4gICAgfTtcblxuICAgIFRpbWUubXMgPSBmdW5jdGlvbihtaWxsaXNlY29uZHMpIHtcbiAgICAgIHJldHVybiBuZXcgVGltZShtaWxsaXNlY29uZHMpO1xuICAgIH07XG5cbiAgICBUaW1lLmYgPSBmdW5jdGlvbihmcmFtZSwgZnBzKSB7XG4gICAgICByZXR1cm4gbmV3IFRpbWUoZnJhbWUgLyAoMTAwMCAvIGZwcykpO1xuICAgIH07XG5cbiAgICBUaW1lLmkgPSBmdW5jdGlvbihpbnRlcnZhbHMsIHRlbXBvKSB7XG4gICAgICByZXR1cm4gbmV3IFRpbWUoaW50ZXJ2YWxzICogdGVtcG8uZ3JpZEludGVydmFsKTtcbiAgICB9O1xuXG4gICAgVGltZS5zdHIgPSBmdW5jdGlvbihzdHJpbmcsIGZwcywgdGVtcG8pIHtcbiAgICAgIHJldHVybiBuZXcgVGltZShzdHJpbmcsIGZwcywgdGVtcG8pO1xuICAgIH07XG5cbiAgICByZXR1cm4gVGltZTtcblxuICB9KSh1dGlsLkVYT2JqZWN0KTtcblxuICAvKlxuICAgIFRpbWVzcGFuOiBSZXByZXNlbnRzIGEgZHVyYXRpb24gb2YgdGltZSBiZXR3ZWVuIHR3byBtb21lbnRzXG4gICAgLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4gICovXG5cblxuICBUaW1lc3BhbiA9IChmdW5jdGlvbihfc3VwZXIpIHtcbiAgICBfX2V4dGVuZHMoVGltZXNwYW4sIF9zdXBlcik7XG5cbiAgICBmdW5jdGlvbiBUaW1lc3BhbigpIHtcbiAgICAgIHN3aXRjaCAoYXJndW1lbnRzLmxlbmd0aCkge1xuICAgICAgICBjYXNlIDA6XG4gICAgICAgICAgdGhpcy5mcm9tID0gbmV3IFRpbWUoMCk7XG4gICAgICAgICAgdGhpcy50byA9IG5ldyBUaW1lKDApO1xuICAgICAgICAgIGJyZWFrO1xuICAgICAgICBjYXNlIDE6XG4gICAgICAgICAgdGhpcy5mcm9tID0gbmV3IFRpbWUoMCk7XG4gICAgICAgICAgdGhpcy50byA9IG5ldyBUaW1lKGFyZ3VtZW50c1swXSk7XG4gICAgICAgICAgYnJlYWs7XG4gICAgICAgIGNhc2UgMjpcbiAgICAgICAgICB0aGlzLmZyb20gPSBuZXcgVGltZShhcmd1bWVudHNbMF0pO1xuICAgICAgICAgIHRoaXMudG8gPSBuZXcgVGltZShhcmd1bWVudHNbMV0pO1xuICAgICAgfVxuICAgIH1cblxuICAgIFRpbWVzcGFuLnByb3RvdHlwZS5zZWdtZW50QnlJbnRlcnZhbCA9IGZ1bmN0aW9uKGludGVydmFsLCBzbmFwRW5kKSB7XG4gICAgICB2YXIgY3VycmVudCwgaGFsZkludGVydmFsLCBsYXN0LCBzZWdtZW50cztcbiAgICAgIGlmIChzbmFwRW5kID09IG51bGwpIHtcbiAgICAgICAgc25hcEVuZCA9IGZhbHNlO1xuICAgICAgfVxuICAgICAgaW50ZXJ2YWwgPSB0eXBlb2YgaW50ZXJ2YWwgPT09ICdudW1iZXInID8gbmV3IFRpbWUoaW50ZXJ2YWwpIDogaW50ZXJ2YWw7XG4gICAgICBzZWdtZW50cyA9IFtdO1xuICAgICAgY3VycmVudCA9IG5ldyBUaW1lc3Bhbih0aGlzLmZyb20sIHRoaXMuZnJvbS5hZGRfKGludGVydmFsKSk7XG4gICAgICBzZWdtZW50cy5wdXNoKGN1cnJlbnQuY2xvbmUoKSk7XG4gICAgICB3aGlsZSAoY3VycmVudC50by52YWx1ZSA8IHRoaXMudG8udmFsdWUpIHtcbiAgICAgICAgY3VycmVudC5mcm9tLmFkZChpbnRlcnZhbCk7XG4gICAgICAgIGN1cnJlbnQudG8uYWRkKGludGVydmFsKTtcbiAgICAgICAgc2VnbWVudHMucHVzaChjdXJyZW50LmNsb25lKCkpO1xuICAgICAgfVxuICAgICAgaWYgKHNuYXBFbmQgJiYgc2VnbWVudHMubGVuZ3RoID4gMCkge1xuICAgICAgICBsYXN0ID0gc2VnbWVudHNbc2VnbWVudHMubGVuZ3RoIC0gMV07XG4gICAgICAgIGhhbGZJbnRlcnZhbCA9IGludGVydmFsLnNjYWxlXygwLjUpO1xuICAgICAgICBpZiAodGhpcy50by52YWx1ZSAtIGxhc3QudG8udmFsdWUgPiBoYWxmSW50ZXJ2YWwpIHtcbiAgICAgICAgICBzZWdtZW50cy5wdXNoKG5ldyBUaW1lc3BhbihsYXN0LmNsb25lKCksIHRoaXMudG8uY2xvbmUoKSkpO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIGxhc3QudG8udmFsdWUgPSB0aGlzLnRvLnZhbHVlO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgICByZXR1cm4gc2VnbWVudHM7XG4gICAgfTtcblxuICAgIFRpbWVzcGFuLnByb3RvdHlwZS5vdmVybGFwcyA9IGZ1bmN0aW9uKG90aGVyKSB7XG4gICAgICB2YXIgZnJvbSwgZnJvbTIsIHRvLCB0bzI7XG4gICAgICBmcm9tID0gdGhpcy5mcm9tLnZhbHVlO1xuICAgICAgdG8gPSB0aGlzLnRvLnZhbHVlO1xuICAgICAgZnJvbTIgPSBvdGhlci5mcm9tLnZhbHVlO1xuICAgICAgdG8yID0gb3RoZXIudG8udmFsdWU7XG4gICAgICByZXR1cm4gKHRvMiA+PSBmcm9tICYmIHRvMiA8PSB0bykgfHwgKGZyb20yID49IGZyb20gJiYgZnJvbTIgPD0gdG8pIHx8IChmcm9tMiA8PSBmcm9tICYmIHRvMiA+PSB0byk7XG4gICAgfTtcblxuICAgIFRpbWVzcGFuLnByb3RvdHlwZS5jbG9uZSA9IGZ1bmN0aW9uKCkge1xuICAgICAgcmV0dXJuIG5ldyBUaW1lc3Bhbih0aGlzLmZyb20sIHRoaXMudG8pO1xuICAgIH07XG5cbiAgICBUaW1lc3Bhbi5wcm90b3R5cGUudG9TdHJpbmcgPSBmdW5jdGlvbigpIHtcbiAgICAgIHJldHVybiBcIlRpbWVzcGFuKFwiICsgdGhpcy5mcm9tICsgXCIgLSBcIiArIHRoaXMudG8gKyBcIilcIjtcbiAgICB9O1xuXG4gICAgVGltZXNwYW4uc2V0KCdsZW5ndGgnLCBmdW5jdGlvbihsZW5ndGgpIHtcbiAgICAgIHJldHVybiB0aGlzLnRvLnZhbHVlID0gdGhpcy5mcm9tLnZhbHVlICsgbGVuZ3RoO1xuICAgIH0pO1xuXG4gICAgVGltZXNwYW4uZ2V0KCdsZW5ndGgnLCBmdW5jdGlvbigpIHtcbiAgICAgIHJldHVybiBuZXcgVGltZSh0aGlzLnRvLnZhbHVlIC0gdGhpcy5mcm9tLnZhbHVlKTtcbiAgICB9KTtcblxuICAgIFRpbWVzcGFuLmdldCgncycsIGZ1bmN0aW9uKCkge1xuICAgICAgcmV0dXJuIHRoaXMubGVuZ3RoLnM7XG4gICAgfSk7XG5cbiAgICBUaW1lc3Bhbi5zID0gZnVuY3Rpb24oc2Vjb25kcykge1xuICAgICAgcmV0dXJuIG5ldyBUaW1lc3BhbihzZWNvbmRzICogMTAwMCk7XG4gICAgfTtcblxuICAgIFRpbWVzcGFuLnN0ciA9IGZ1bmN0aW9uKHN0cmluZywgZnBzLCB0ZW1wbykge1xuICAgICAgdmFyIGZyb20sIHRpbWVzLCB0bztcbiAgICAgIHRpbWVzID0gc3RyaW5nLnNwbGl0KCcuLicpO1xuICAgICAgaWYgKHRpbWVzLmxlbmd0aCAhPT0gMikge1xuICAgICAgICB0aHJvdyBcIkludmFsaWQgYXJndW1lbnQ6IFwiICsgc3RyaW5nO1xuICAgICAgfVxuICAgICAgZnJvbSA9IFRpbWUuc3RyKHRpbWVzWzBdLCBmcHMsIHRlbXBvKTtcbiAgICAgIHRvID0gVGltZS5zdHIodGltZXNbMV0sIGZwcywgdGVtcG8pO1xuICAgICAgcmV0dXJuIG5ldyBUaW1lc3Bhbihmcm9tLCB0byk7XG4gICAgfTtcblxuICAgIHJldHVybiBUaW1lc3BhbjtcblxuICB9KSh1dGlsLkVYT2JqZWN0KTtcblxuICBtb2R1bGUuZXhwb3J0cyA9IHtcbiAgICBUaW1lcjogVGltZXIsXG4gICAgVGVtcG86IFRlbXBvLFxuICAgIFRpbWU6IFRpbWUsXG4gICAgVGltZXNwYW46IFRpbWVzcGFuXG4gIH07XG5cbn0pLmNhbGwodGhpcyk7XG4iLCIoZnVuY3Rpb24gKEJ1ZmZlcil7XG4vLyBHZW5lcmF0ZWQgYnkgQ29mZmVlU2NyaXB0IDEuNi4zXG4oZnVuY3Rpb24oKSB7XG4gIHZhciBFWE9iamVjdCwgTWl4aW4sIGNsb25lLCBleHRlbmQsIHJlbW92ZUVsZW1lbnQsIHNodWZmbGUsIHV0aWwsXG4gICAgX19pbmRleE9mID0gW10uaW5kZXhPZiB8fCBmdW5jdGlvbihpdGVtKSB7IGZvciAodmFyIGkgPSAwLCBsID0gdGhpcy5sZW5ndGg7IGkgPCBsOyBpKyspIHsgaWYgKGkgaW4gdGhpcyAmJiB0aGlzW2ldID09PSBpdGVtKSByZXR1cm4gaTsgfSByZXR1cm4gLTE7IH07XG5cbiAgdXRpbCA9IHJlcXVpcmUoXCJ1dGlsXCIpO1xuXG4gIC8qXG4gICAgSmF2YVNjcmlwdCBMYW5ndWFnZSBVdGlsaXRpZXNcbiAgICAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiAgKi9cblxuXG4gIGV4dGVuZCA9IGZ1bmN0aW9uKG9iaiwgc291cmNlKSB7XG4gICAgdmFyIGksIGlsLCBrZXlzLCBwcm9wLCBzYWZlSGFzT3duUHJvcGVydHk7XG4gICAgaWYgKE9iamVjdC5rZXlzKSB7XG4gICAgICBrZXlzID0gT2JqZWN0LmtleXMoc291cmNlKTtcbiAgICAgIGkgPSAwO1xuICAgICAgaWwgPSBrZXlzLmxlbmd0aDtcbiAgICAgIHdoaWxlIChpIDwgaWwpIHtcbiAgICAgICAgcHJvcCA9IGtleXNbaV07XG4gICAgICAgIE9iamVjdC5kZWZpbmVQcm9wZXJ0eShvYmosIHByb3AsIE9iamVjdC5nZXRPd25Qcm9wZXJ0eURlc2NyaXB0b3Ioc291cmNlLCBwcm9wKSk7XG4gICAgICAgIGkrKztcbiAgICAgIH1cbiAgICB9IGVsc2Uge1xuICAgICAgc2FmZUhhc093blByb3BlcnR5ID0ge30uaGFzT3duUHJvcGVydHk7XG4gICAgICBmb3IgKHByb3AgaW4gc291cmNlKSB7XG4gICAgICAgIGlmIChzYWZlSGFzT3duUHJvcGVydHkuY2FsbChzb3VyY2UsIHByb3ApKSB7XG4gICAgICAgICAgb2JqW3Byb3BdID0gc291cmNlW3Byb3BdO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuICAgIHJldHVybiBvYmo7XG4gIH07XG5cbiAgLypcbiAgXG4gICAgU3dhcHBhYmxlIE1peGlucyBpbiBDb2ZmZWVTY3JpcHRcbiAgXG4gIFxuICAgIE1hbnkgdGhhbmtzIHRvIEhhc2htYWwsIHdobyB3cm90ZSB0aGlzIHRvIHN0YXJ0LlxuICAgIGh0dHBzOi8vZ2lzdC5naXRodWIuY29tLzgwMzgxNi9hY2VlZDhmYzU3MTg4YzNhMTljZTJlY2NkYjI1YWNiNjRmMmJlOTRlXG4gIFxuICAgIFVzYWdlXG4gICAgLS0tLS1cbiAgXG4gICAgY2xhc3MgRGVycCBleHRlbmRzIE1peGluXG4gICAgICBzZXR1cDogLT5cbiAgICAgICAgQGdvb2dseSA9IFwiZXllc1wiXG4gIFxuICAgIGRlcnA6IC0+XG4gICAgICBhbGVydCBcIkhlcnAgZGVycCEgV2hhdCdzIHdpdGggeW91ciAjeyBAZ29vZ2x5IH0/XCJcbiAgXG4gICAgY2xhc3MgSGVycFxuICAgICAgY29uc3RydWN0b3I6IC0+XG4gICAgICAgIERlcnA6OmF1Z21lbnQgdGhpc1xuICBcbiAgICBoZXJwID0gbmV3IEhlcnBcbiAgICBoZXJwLmRlcnAoKVxuICBcbiAgICBNaXhpblxuICAgIC0tLS0tXG4gIFxuICAgIENsYXNzZXMgaW5oZXJpdGluZyBgTWl4aW5gIHdpbGwgYmVjb21lIHJlbW92YWJsZSBtaXhpbnMsIGVuYWJsaW5nIHlvdSB0b1xuICAgIHN3YXAgdGhlbSBhcm91bmQuXG4gIFxuICAgIExpbWl0YXRpb25zXG4gICAgLS0tLS0tLS0tLS1cbiAgXG4gICAgKiBXaGVuIGEgY2xhc3MgaXMgYXVnbWVudGVkLCBhbGwgaW5zdGFuY2VzIG9mIHRoYXQgY2xhc3MgYXJlIGF1Z21lbnRlZCB0b28sXG4gICAgICBhbmQgd2hlbiBhIG1peGluIGlzIGVqZWN0ZWQgZnJvbSBhIGNsYXNzLCBhbGwgaW5zdGFuY2VzIGxvc2UgdGhhdCBtaXhpbiB0b28uXG4gICAgKiBZb3UgY2FuJ3QgZWplY3QgYSBtaXhpbiBmcm9tIGFuIG9iamVjdCBpZiB0aGF0IG1peGluIHdhcyBhZGRlZCB0byB0aGUgb2JqZWN0J3MgY2xhc3MuIEVqZWN0IHRoZSBtaXhpbiBmcm9tIHRoZSBjbGFzcyBpbnN0ZWFkLlxuICAqL1xuXG5cbiAgTWl4aW4gPSAoZnVuY3Rpb24oKSB7XG4gICAgZnVuY3Rpb24gTWl4aW4oKSB7fVxuXG4gICAgTWl4aW4ucHJvdG90eXBlLmF1Z21lbnQgPSBmdW5jdGlvbih0KSB7XG4gICAgICB2YXIgbSwgbjtcbiAgICAgIGZvciAobiBpbiB0aGlzKSB7XG4gICAgICAgIG0gPSB0aGlzW25dO1xuICAgICAgICBpZiAoIShuID09PSAnYXVnbWVudCcgfHwgKHRoaXNbbl0ucHJvdG90eXBlID09IG51bGwpKSkge1xuICAgICAgICAgIHRbbl0gPSBtO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgICByZXR1cm4gdC5zZXR1cCgpO1xuICAgIH07XG5cbiAgICBNaXhpbi5wcm90b3R5cGUuZWplY3QgPSBmdW5jdGlvbihtaXhpbikge1xuICAgICAgdmFyIG0sIG4sIG8sIHAsIF9yZXN1bHRzO1xuICAgICAgX3Jlc3VsdHMgPSBbXTtcbiAgICAgIGZvciAobiBpbiB0aGlzKSB7XG4gICAgICAgIG0gPSB0aGlzW25dO1xuICAgICAgICBfcmVzdWx0cy5wdXNoKF9faW5kZXhPZi5jYWxsKChmdW5jdGlvbigpIHtcbiAgICAgICAgICB2YXIgX3JlZiwgX3Jlc3VsdHMxO1xuICAgICAgICAgIF9yZWYgPSBtaXhpbi5wcm90b3R5cGU7XG4gICAgICAgICAgX3Jlc3VsdHMxID0gW107XG4gICAgICAgICAgZm9yIChvIGluIF9yZWYpIHtcbiAgICAgICAgICAgIHAgPSBfcmVmW29dO1xuICAgICAgICAgICAgX3Jlc3VsdHMxLnB1c2gocCk7XG4gICAgICAgICAgfVxuICAgICAgICAgIHJldHVybiBfcmVzdWx0czE7XG4gICAgICAgIH0pKCksIG0pID49IDAgPyBkZWxldGUgdGhpc1tuXSA6IHZvaWQgMCk7XG4gICAgICB9XG4gICAgICByZXR1cm4gX3Jlc3VsdHM7XG4gICAgfTtcblxuICAgIE1peGluLnByb3RvdHlwZS5zZXR1cCA9IGZ1bmN0aW9uKCkge307XG5cbiAgICByZXR1cm4gTWl4aW47XG5cbiAgfSkoKTtcblxuICAvKlxuICBcbiAgICBDbG9uZXMgKGNvcGllcykgYW4gT2JqZWN0IHVzaW5nIGRlZXAgY29weWluZy5cbiAgXG4gIFxuICAgIFRoaXMgZnVuY3Rpb24gc3VwcG9ydHMgY2lyY3VsYXIgcmVmZXJlbmNlcyBieSBkZWZhdWx0LCBidXQgaWYgeW91IGFyZSBjZXJ0YWluXG4gICAgdGhlcmUgYXJlIG5vIGNpcmN1bGFyIHJlZmVyZW5jZXMgaW4geW91ciBvYmplY3QsIHlvdSBjYW4gc2F2ZSBzb21lIENQVSB0aW1lXG4gICAgYnkgY2FsbGluZyBjbG9uZShvYmosIGZhbHNlKS5cbiAgXG4gICAgQ2F1dGlvbjogaWYgYGNpcmN1bGFyYCBpcyBmYWxzZSBhbmQgYHBhcmVudGAgY29udGFpbnMgY2lyY3VsYXIgcmVmZXJlbmNlcyxcbiAgICB5b3VyIHByb2dyYW0gbWF5IGVudGVyIGFuIGluZmluaXRlIGxvb3AgYW5kIGNyYXNoLlxuICBcbiAgICBAcGFyYW0gYHBhcmVudGAgLSB0aGUgb2JqZWN0IHRvIGJlIGNsb25lZFxuICAgIEBwYXJhbSBgY2lyY3VsYXJgIC0gc2V0IHRvIHRydWUgaWYgdGhlIG9iamVjdCB0byBiZSBjbG9uZWQgbWF5IGNvbnRhaW5cbiAgICBjaXJjdWxhciByZWZlcmVuY2VzLiAob3B0aW9uYWwgLSB0cnVlIGJ5IGRlZmF1bHQpXG4gICovXG5cblxuICBjbG9uZSA9IGZ1bmN0aW9uKHBhcmVudCwgY2lyY3VsYXIpIHtcbiAgICB2YXIgYywgY2hpbGQsIGNpcmN1bGFyUGFyZW50LCBjaXJjdWxhclJlcGxhY2UsIGNpcmN1bGFyUmVzb2x2ZWQsIGNsb25lZCwgaSwgdXNlQnVmZmVyLCBfY2xvbmU7XG4gICAgaWYgKHR5cGVvZiBjaXJjdWxhciA9PT0gXCJ1bmRlZmluZWRcIikge1xuICAgICAgY2lyY3VsYXIgPSB0cnVlO1xuICAgIH1cbiAgICB1c2VCdWZmZXIgPSB0eXBlb2YgQnVmZmVyICE9PSBcInVuZGVmaW5lZFwiO1xuICAgIGkgPSB2b2lkIDA7XG4gICAgaWYgKGNpcmN1bGFyKSB7XG4gICAgICBfY2xvbmUgPSBmdW5jdGlvbihwYXJlbnQsIGNvbnRleHQsIGNoaWxkLCBjSW5kZXgpIHtcbiAgICAgICAgaSA9IHZvaWQgMDtcbiAgICAgICAgaWYgKHR5cGVvZiBwYXJlbnQgPT09IFwib2JqZWN0XCIpIHtcbiAgICAgICAgICBpZiAocGFyZW50ID09IG51bGwpIHtcbiAgICAgICAgICAgIHJldHVybiBwYXJlbnQ7XG4gICAgICAgICAgfVxuICAgICAgICAgIGZvciAoaSBpbiBjaXJjdWxhclBhcmVudCkge1xuICAgICAgICAgICAgaWYgKGNpcmN1bGFyUGFyZW50W2ldID09PSBwYXJlbnQpIHtcbiAgICAgICAgICAgICAgY2lyY3VsYXJSZXBsYWNlLnB1c2goe1xuICAgICAgICAgICAgICAgIHJlc29sdmVUbzogaSxcbiAgICAgICAgICAgICAgICBjaGlsZDogY2hpbGQsXG4gICAgICAgICAgICAgICAgaTogY0luZGV4XG4gICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICByZXR1cm4gbnVsbDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9XG4gICAgICAgICAgY2lyY3VsYXJQYXJlbnRbY29udGV4dF0gPSBwYXJlbnQ7XG4gICAgICAgICAgaWYgKHV0aWwuaXNBcnJheShwYXJlbnQpKSB7XG4gICAgICAgICAgICBjaGlsZCA9IFtdO1xuICAgICAgICAgICAgZm9yIChpIGluIHBhcmVudCkge1xuICAgICAgICAgICAgICBjaGlsZFtpXSA9IF9jbG9uZShwYXJlbnRbaV0sIGNvbnRleHQgKyBcIltcIiArIGkgKyBcIl1cIiwgY2hpbGQsIGkpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH0gZWxzZSBpZiAodXRpbC5pc0RhdGUocGFyZW50KSkge1xuICAgICAgICAgICAgY2hpbGQgPSBuZXcgRGF0ZShwYXJlbnQuZ2V0VGltZSgpKTtcbiAgICAgICAgICB9IGVsc2UgaWYgKHV0aWwuaXNSZWdFeHAocGFyZW50KSkge1xuICAgICAgICAgICAgY2hpbGQgPSBuZXcgUmVnRXhwKHBhcmVudC5zb3VyY2UpO1xuICAgICAgICAgIH0gZWxzZSBpZiAodXNlQnVmZmVyICYmIEJ1ZmZlci5pc0J1ZmZlcihwYXJlbnQpKSB7XG4gICAgICAgICAgICBjaGlsZCA9IG5ldyBCdWZmZXIocGFyZW50Lmxlbmd0aCk7XG4gICAgICAgICAgICBwYXJlbnQuY29weShjaGlsZCk7XG4gICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIGNoaWxkID0ge307XG4gICAgICAgICAgICBjaGlsZC5fX3Byb3RvX18gPSBwYXJlbnQuX19wcm90b19fO1xuICAgICAgICAgICAgZm9yIChpIGluIHBhcmVudCkge1xuICAgICAgICAgICAgICBjaGlsZFtpXSA9IF9jbG9uZShwYXJlbnRbaV0sIGNvbnRleHQgKyBcIltcIiArIGkgKyBcIl1cIiwgY2hpbGQsIGkpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH1cbiAgICAgICAgICBjaXJjdWxhclJlc29sdmVkW2NvbnRleHRdID0gY2hpbGQ7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgY2hpbGQgPSBwYXJlbnQ7XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIGNoaWxkO1xuICAgICAgfTtcbiAgICAgIGNpcmN1bGFyUGFyZW50ID0ge307XG4gICAgICBjaXJjdWxhclJlc29sdmVkID0ge307XG4gICAgICBjaXJjdWxhclJlcGxhY2UgPSBbXTtcbiAgICAgIGNsb25lZCA9IF9jbG9uZShwYXJlbnQsIFwiKlwiKTtcbiAgICAgIGZvciAoaSBpbiBjaXJjdWxhclJlcGxhY2UpIHtcbiAgICAgICAgYyA9IGNpcmN1bGFyUmVwbGFjZVtpXTtcbiAgICAgICAgaWYgKGMgJiYgYy5jaGlsZCAmJiBjLmkgaW4gYy5jaGlsZCkge1xuICAgICAgICAgIGMuY2hpbGRbYy5pXSA9IGNpcmN1bGFyUmVzb2x2ZWRbYy5yZXNvbHZlVG9dO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgICByZXR1cm4gY2xvbmVkO1xuICAgIH0gZWxzZSB7XG4gICAgICBjaGlsZCA9IHZvaWQgMDtcbiAgICAgIGlmICh0eXBlb2YgcGFyZW50ID09PSBcIm9iamVjdFwiKSB7XG4gICAgICAgIGlmIChwYXJlbnQgPT0gbnVsbCkge1xuICAgICAgICAgIHJldHVybiBwYXJlbnQ7XG4gICAgICAgIH1cbiAgICAgICAgaWYgKHBhcmVudC5jb25zdHJ1Y3Rvci5uYW1lID09PSBcIkFycmF5XCIpIHtcbiAgICAgICAgICBjaGlsZCA9IFtdO1xuICAgICAgICAgIGZvciAoaSBpbiBwYXJlbnQpIHtcbiAgICAgICAgICAgIGNoaWxkW2ldID0gY2xvbmUocGFyZW50W2ldLCBjaXJjdWxhcik7XG4gICAgICAgICAgfVxuICAgICAgICB9IGVsc2UgaWYgKHV0aWwuaXNEYXRlKHBhcmVudCkpIHtcbiAgICAgICAgICBjaGlsZCA9IG5ldyBEYXRlKHBhcmVudC5nZXRUaW1lKCkpO1xuICAgICAgICB9IGVsc2UgaWYgKCF1dGlsLmlzUmVnRXhwKHBhcmVudCkpIHtcbiAgICAgICAgICBjaGlsZCA9IHt9O1xuICAgICAgICAgIGNoaWxkLl9fcHJvdG9fXyA9IHBhcmVudC5fX3Byb3RvX187XG4gICAgICAgICAgZm9yIChpIGluIHBhcmVudCkge1xuICAgICAgICAgICAgY2hpbGRbaV0gPSBjbG9uZShwYXJlbnRbaV0sIGNpcmN1bGFyKTtcbiAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIGNoaWxkID0gcGFyZW50O1xuICAgICAgfVxuICAgICAgcmV0dXJuIGNoaWxkO1xuICAgIH1cbiAgfTtcblxuICBjbG9uZS5jbG9uZVByb3RvdHlwZSA9IGZ1bmN0aW9uKHBhcmVudCkge1xuICAgIHZhciBjdG9yO1xuICAgIGlmIChwYXJlbnQgPT09IG51bGwpIHtcbiAgICAgIHJldHVybiBudWxsO1xuICAgIH1cbiAgICBjdG9yID0gZnVuY3Rpb24oKSB7fTtcbiAgICBjdG9yLnByb3RvdHlwZSA9IHBhcmVudDtcbiAgICByZXR1cm4gbmV3IGN0b3IoKTtcbiAgfTtcblxuICAvKlxuICBcbiAgSmF2YVNjcmlwdCBnZXR0ZXIgYW5kIHNldHRlciBzdXBwb3J0IGZvciBDb2ZmZWVTY3JpcHQgY2xhc3Nlc1xuICBSZWY6IGh0dHBzOi8vZ2l0aHViLmNvbS9qYXNoa2VuYXMvY29mZmVlLXNjcmlwdC9pc3N1ZXMvMTAzOVxuICBcbiAgTm90ZTpcbiAgQ2xhc3NlcyB1c2luZyB0aGlzIHdvbnQgd29yayB1bmRlciBJRTYgKyBJRTdcbiAgXG4gIFVzYWdlOlxuICBjbGFzcyBWZWN0b3IzRCBleHRlbmRzIEVYT2JqZWN0XG4gICAgY29uc3RydWN0b3I6IChAeCwgQHksIEB6KSAtPlxuICBcbiAgICBAZ2V0ICd4JywgLT4gQFswXVxuICAgIEBnZXQgJ3knLCAtPiBAWzFdXG4gICAgQGdldCAneicsIC0+IEBbMl1cbiAgXG4gICAgQHNldCAneCcsICh4KSAtPiBAWzBdID0geFxuICAgIEBzZXQgJ3knLCAoeSkgLT4gQFsxXSA9IHlcbiAgICBAc2V0ICd6JywgKHopIC0+IEBbMl0gPSB6XG4gICovXG5cblxuICBFWE9iamVjdCA9IChmdW5jdGlvbigpIHtcbiAgICBmdW5jdGlvbiBFWE9iamVjdCgpIHt9XG5cbiAgICBFWE9iamVjdC5nZXQgPSBmdW5jdGlvbihwcm9wZXJ0eU5hbWUsIGZ1bmMpIHtcbiAgICAgIHJldHVybiBPYmplY3QuZGVmaW5lUHJvcGVydHkodGhpcy5wcm90b3R5cGUsIHByb3BlcnR5TmFtZSwge1xuICAgICAgICBjb25maWd1cmFibGU6IHRydWUsXG4gICAgICAgIGVudW1lcmFibGU6IHRydWUsXG4gICAgICAgIGdldDogZnVuY1xuICAgICAgfSk7XG4gICAgfTtcblxuICAgIEVYT2JqZWN0LnNldCA9IGZ1bmN0aW9uKHByb3BlcnR5TmFtZSwgZnVuYykge1xuICAgICAgcmV0dXJuIE9iamVjdC5kZWZpbmVQcm9wZXJ0eSh0aGlzLnByb3RvdHlwZSwgcHJvcGVydHlOYW1lLCB7XG4gICAgICAgIGNvbmZpZ3VyYWJsZTogdHJ1ZSxcbiAgICAgICAgZW51bWVyYWJsZTogdHJ1ZSxcbiAgICAgICAgc2V0OiBmdW5jXG4gICAgICB9KTtcbiAgICB9O1xuXG4gICAgcmV0dXJuIEVYT2JqZWN0O1xuXG4gIH0pKCk7XG5cbiAgLypcbiAgICBBcnJheSBVdGlsaXRpZXNcbiAgICAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbiAgKi9cblxuXG4gIHJlbW92ZUVsZW1lbnQgPSBmdW5jdGlvbihlbGVtZW50LCBsaXN0KSB7XG4gICAgdmFyIGluZGV4O1xuICAgIGluZGV4ID0gbGlzdC5pbmRleE9mKGVsZW1lbnQpO1xuICAgIGlmIChpbmRleCAhPT0gLTEpIHtcbiAgICAgIGxpc3Quc3BsaWNlKGluZGV4LCAxKTtcbiAgICB9XG4gICAgcmV0dXJuIGxpc3Q7XG4gIH07XG5cbiAgc2h1ZmZsZSA9IGZ1bmN0aW9uKG9iamVjdCwgcm5nKSB7XG4gICAgdmFyIGksIGosIHg7XG4gICAgaWYgKHJuZyA9PSBudWxsKSB7XG4gICAgICBybmcgPSBNYXRoO1xuICAgIH1cbiAgICBpID0gb2JqZWN0Lmxlbmd0aDtcbiAgICB3aGlsZSAoaSkge1xuICAgICAgaiA9IHBhcnNlSW50KHJuZy5yYW5kb20oKSAqIGkpO1xuICAgICAgeCA9IG9iamVjdFstLWldO1xuICAgICAgb2JqZWN0W2ldID0gb2JqZWN0W2pdO1xuICAgICAgb2JqZWN0W2pdID0geDtcbiAgICB9XG4gICAgcmV0dXJuIG9iamVjdDtcbiAgfTtcblxuICBtb2R1bGUuZXhwb3J0cyA9IHtcbiAgICBleHRlbmQ6IGV4dGVuZCxcbiAgICBjbG9uZTogY2xvbmUsXG4gICAgRVhPYmplY3Q6IEVYT2JqZWN0LFxuICAgIE1peGluOiBNaXhpbixcbiAgICByZW1vdmVFbGVtZW50OiByZW1vdmVFbGVtZW50LFxuICAgIHNodWZmbGU6IHNodWZmbGVcbiAgfTtcblxufSkuY2FsbCh0aGlzKTtcblxufSkuY2FsbCh0aGlzLHJlcXVpcmUoXCJidWZmZXJcIikuQnVmZmVyKSIsIi8vIHRoaXMgcHJvZ3JhbSBpcyBhIEphdmFTY3JpcHQgdmVyc2lvbiBvZiBNZXJzZW5uZSBUd2lzdGVyLCB3aXRoIGNvbmNlYWxtZW50IGFuZCBlbmNhcHN1bGF0aW9uIGluIGNsYXNzLFxyXG4vLyBhbiBhbG1vc3Qgc3RyYWlnaHQgY29udmVyc2lvbiBmcm9tIHRoZSBvcmlnaW5hbCBwcm9ncmFtLCBtdDE5OTM3YXIuYyxcclxuLy8gdHJhbnNsYXRlZCBieSB5LiBva2FkYSBvbiBKdWx5IDE3LCAyMDA2LlxyXG4vLyBhbmQgbW9kaWZpZWQgYSBsaXR0bGUgYXQganVseSAyMCwgMjAwNiwgYnV0IHRoZXJlIGFyZSBub3QgYW55IHN1YnN0YW50aWFsIGRpZmZlcmVuY2VzLlxyXG4vLyBpbiB0aGlzIHByb2dyYW0sIHByb2NlZHVyZSBkZXNjcmlwdGlvbnMgYW5kIGNvbW1lbnRzIG9mIG9yaWdpbmFsIHNvdXJjZSBjb2RlIHdlcmUgbm90IHJlbW92ZWQuXHJcbi8vIGxpbmVzIGNvbW1lbnRlZCB3aXRoIC8vYy8vIHdlcmUgb3JpZ2luYWxseSBkZXNjcmlwdGlvbnMgb2YgYyBwcm9jZWR1cmUuIGFuZCBhIGZldyBmb2xsb3dpbmcgbGluZXMgYXJlIGFwcHJvcHJpYXRlIEphdmFTY3JpcHQgZGVzY3JpcHRpb25zLlxyXG4vLyBsaW5lcyBjb21tZW50ZWQgd2l0aCAvKiBhbmQgKi8gYXJlIG9yaWdpbmFsIGNvbW1lbnRzLlxyXG4vLyBsaW5lcyBjb21tZW50ZWQgd2l0aCAvLyBhcmUgYWRkaXRpb25hbCBjb21tZW50cyBpbiB0aGlzIEphdmFTY3JpcHQgdmVyc2lvbi5cclxuLy8gYmVmb3JlIHVzaW5nIHRoaXMgdmVyc2lvbiwgY3JlYXRlIGF0IGxlYXN0IG9uZSBpbnN0YW5jZSBvZiBNZXJzZW5uZVR3aXN0ZXIxOTkzNyBjbGFzcywgYW5kIGluaXRpYWxpemUgdGhlIGVhY2ggc3RhdGUsIGdpdmVuIGJlbG93IGluIGMgY29tbWVudHMsIG9mIGFsbCB0aGUgaW5zdGFuY2VzLlxyXG4vKlxyXG4gICBBIEMtcHJvZ3JhbSBmb3IgTVQxOTkzNywgd2l0aCBpbml0aWFsaXphdGlvbiBpbXByb3ZlZCAyMDAyLzEvMjYuXHJcbiAgIENvZGVkIGJ5IFRha3VqaSBOaXNoaW11cmEgYW5kIE1ha290byBNYXRzdW1vdG8uXHJcblxyXG4gICBCZWZvcmUgdXNpbmcsIGluaXRpYWxpemUgdGhlIHN0YXRlIGJ5IHVzaW5nIGluaXRfZ2VucmFuZChzZWVkKVxyXG4gICBvciBpbml0X2J5X2FycmF5KGluaXRfa2V5LCBrZXlfbGVuZ3RoKS5cclxuXHJcbiAgIENvcHlyaWdodCAoQykgMTk5NyAtIDIwMDIsIE1ha290byBNYXRzdW1vdG8gYW5kIFRha3VqaSBOaXNoaW11cmEsXHJcbiAgIEFsbCByaWdodHMgcmVzZXJ2ZWQuXHJcblxyXG4gICBSZWRpc3RyaWJ1dGlvbiBhbmQgdXNlIGluIHNvdXJjZSBhbmQgYmluYXJ5IGZvcm1zLCB3aXRoIG9yIHdpdGhvdXRcclxuICAgbW9kaWZpY2F0aW9uLCBhcmUgcGVybWl0dGVkIHByb3ZpZGVkIHRoYXQgdGhlIGZvbGxvd2luZyBjb25kaXRpb25zXHJcbiAgIGFyZSBtZXQ6XHJcblxyXG4gICAgIDEuIFJlZGlzdHJpYnV0aW9ucyBvZiBzb3VyY2UgY29kZSBtdXN0IHJldGFpbiB0aGUgYWJvdmUgY29weXJpZ2h0XHJcbiAgICAgICAgbm90aWNlLCB0aGlzIGxpc3Qgb2YgY29uZGl0aW9ucyBhbmQgdGhlIGZvbGxvd2luZyBkaXNjbGFpbWVyLlxyXG5cclxuICAgICAyLiBSZWRpc3RyaWJ1dGlvbnMgaW4gYmluYXJ5IGZvcm0gbXVzdCByZXByb2R1Y2UgdGhlIGFib3ZlIGNvcHlyaWdodFxyXG4gICAgICAgIG5vdGljZSwgdGhpcyBsaXN0IG9mIGNvbmRpdGlvbnMgYW5kIHRoZSBmb2xsb3dpbmcgZGlzY2xhaW1lciBpbiB0aGVcclxuICAgICAgICBkb2N1bWVudGF0aW9uIGFuZC9vciBvdGhlciBtYXRlcmlhbHMgcHJvdmlkZWQgd2l0aCB0aGUgZGlzdHJpYnV0aW9uLlxyXG5cclxuICAgICAzLiBUaGUgbmFtZXMgb2YgaXRzIGNvbnRyaWJ1dG9ycyBtYXkgbm90IGJlIHVzZWQgdG8gZW5kb3JzZSBvciBwcm9tb3RlXHJcbiAgICAgICAgcHJvZHVjdHMgZGVyaXZlZCBmcm9tIHRoaXMgc29mdHdhcmUgd2l0aG91dCBzcGVjaWZpYyBwcmlvciB3cml0dGVuXHJcbiAgICAgICAgcGVybWlzc2lvbi5cclxuXHJcbiAgIFRISVMgU09GVFdBUkUgSVMgUFJPVklERUQgQlkgVEhFIENPUFlSSUdIVCBIT0xERVJTIEFORCBDT05UUklCVVRPUlNcclxuICAgXCJBUyBJU1wiIEFORCBBTlkgRVhQUkVTUyBPUiBJTVBMSUVEIFdBUlJBTlRJRVMsIElOQ0xVRElORywgQlVUIE5PVFxyXG4gICBMSU1JVEVEIFRPLCBUSEUgSU1QTElFRCBXQVJSQU5USUVTIE9GIE1FUkNIQU5UQUJJTElUWSBBTkQgRklUTkVTUyBGT1JcclxuICAgQSBQQVJUSUNVTEFSIFBVUlBPU0UgQVJFIERJU0NMQUlNRUQuICBJTiBOTyBFVkVOVCBTSEFMTCBUSEUgQ09QWVJJR0hUIE9XTkVSIE9SXHJcbiAgIENPTlRSSUJVVE9SUyBCRSBMSUFCTEUgRk9SIEFOWSBESVJFQ1QsIElORElSRUNULCBJTkNJREVOVEFMLCBTUEVDSUFMLFxyXG4gICBFWEVNUExBUlksIE9SIENPTlNFUVVFTlRJQUwgREFNQUdFUyAoSU5DTFVESU5HLCBCVVQgTk9UIExJTUlURUQgVE8sXHJcbiAgIFBST0NVUkVNRU5UIE9GIFNVQlNUSVRVVEUgR09PRFMgT1IgU0VSVklDRVM7IExPU1MgT0YgVVNFLCBEQVRBLCBPUlxyXG4gICBQUk9GSVRTOyBPUiBCVVNJTkVTUyBJTlRFUlJVUFRJT04pIEhPV0VWRVIgQ0FVU0VEIEFORCBPTiBBTlkgVEhFT1JZIE9GXHJcbiAgIExJQUJJTElUWSwgV0hFVEhFUiBJTiBDT05UUkFDVCwgU1RSSUNUIExJQUJJTElUWSwgT1IgVE9SVCAoSU5DTFVESU5HXHJcbiAgIE5FR0xJR0VOQ0UgT1IgT1RIRVJXSVNFKSBBUklTSU5HIElOIEFOWSBXQVkgT1VUIE9GIFRIRSBVU0UgT0YgVEhJU1xyXG4gICBTT0ZUV0FSRSwgRVZFTiBJRiBBRFZJU0VEIE9GIFRIRSBQT1NTSUJJTElUWSBPRiBTVUNIIERBTUFHRS5cclxuXHJcblxyXG4gICBBbnkgZmVlZGJhY2sgaXMgdmVyeSB3ZWxjb21lLlxyXG4gICBodHRwOi8vd3d3Lm1hdGguc2NpLmhpcm9zaGltYS11LmFjLmpwL35tLW1hdC9NVC9lbXQuaHRtbFxyXG4gICBlbWFpbDogbS1tYXQgQCBtYXRoLnNjaS5oaXJvc2hpbWEtdS5hYy5qcCAocmVtb3ZlIHNwYWNlKVxyXG4qL1xyXG5cclxuZnVuY3Rpb24gTWVyc2VubmVUd2lzdGVyMTk5MzcoKVxyXG57XHJcblx0LyogY29uc3RhbnRzIHNob3VsZCBiZSBzY29wZWQgaW5zaWRlIHRoZSBjbGFzcyAqL1xyXG5cdHZhciBOLCBNLCBNQVRSSVhfQSwgVVBQRVJfTUFTSywgTE9XRVJfTUFTSztcclxuXHQvKiBQZXJpb2QgcGFyYW1ldGVycyAqL1xyXG5cdC8vYy8vI2RlZmluZSBOIDYyNFxyXG5cdC8vYy8vI2RlZmluZSBNIDM5N1xyXG5cdC8vYy8vI2RlZmluZSBNQVRSSVhfQSAweDk5MDhiMGRmVUwgICAvKiBjb25zdGFudCB2ZWN0b3IgYSAqL1xyXG5cdC8vYy8vI2RlZmluZSBVUFBFUl9NQVNLIDB4ODAwMDAwMDBVTCAvKiBtb3N0IHNpZ25pZmljYW50IHctciBiaXRzICovXHJcblx0Ly9jLy8jZGVmaW5lIExPV0VSX01BU0sgMHg3ZmZmZmZmZlVMIC8qIGxlYXN0IHNpZ25pZmljYW50IHIgYml0cyAqL1xyXG5cdE4gPSA2MjQ7XHJcblx0TSA9IDM5NztcclxuXHRNQVRSSVhfQSA9IDB4OTkwOGIwZGY7ICAgLyogY29uc3RhbnQgdmVjdG9yIGEgKi9cclxuXHRVUFBFUl9NQVNLID0gMHg4MDAwMDAwMDsgLyogbW9zdCBzaWduaWZpY2FudCB3LXIgYml0cyAqL1xyXG5cdExPV0VSX01BU0sgPSAweDdmZmZmZmZmOyAvKiBsZWFzdCBzaWduaWZpY2FudCByIGJpdHMgKi9cclxuXHQvL2MvL3N0YXRpYyB1bnNpZ25lZCBsb25nIG10W05dOyAvKiB0aGUgYXJyYXkgZm9yIHRoZSBzdGF0ZSB2ZWN0b3IgICovXHJcblx0Ly9jLy9zdGF0aWMgaW50IG10aT1OKzE7IC8qIG10aT09TisxIG1lYW5zIG10W05dIGlzIG5vdCBpbml0aWFsaXplZCAqL1xyXG5cdHZhciBtdCA9IG5ldyBBcnJheShOKTsgICAvKiB0aGUgYXJyYXkgZm9yIHRoZSBzdGF0ZSB2ZWN0b3IgICovXHJcblx0dmFyIG10aSA9IE4rMTsgICAgICAgICAgIC8qIG10aT09TisxIG1lYW5zIG10W05dIGlzIG5vdCBpbml0aWFsaXplZCAqL1xyXG5cclxuXHRmdW5jdGlvbiB1bnNpZ25lZDMyIChuMSkgLy8gcmV0dXJucyBhIDMyLWJpdHMgdW5zaWdlZCBpbnRlZ2VyIGZyb20gYW4gb3BlcmFuZCB0byB3aGljaCBhcHBsaWVkIGEgYml0IG9wZXJhdG9yLlxyXG5cdHtcclxuXHRcdHJldHVybiBuMSA8IDAgPyAobjEgXiBVUFBFUl9NQVNLKSArIFVQUEVSX01BU0sgOiBuMTtcclxuXHR9XHJcblxyXG5cdGZ1bmN0aW9uIHN1YnRyYWN0aW9uMzIgKG4xLCBuMikgLy8gZW11bGF0ZXMgbG93ZXJmbG93IG9mIGEgYyAzMi1iaXRzIHVuc2lnZWQgaW50ZWdlciB2YXJpYWJsZSwgaW5zdGVhZCBvZiB0aGUgb3BlcmF0b3IgLS4gdGhlc2UgYm90aCBhcmd1bWVudHMgbXVzdCBiZSBub24tbmVnYXRpdmUgaW50ZWdlcnMgZXhwcmVzc2libGUgdXNpbmcgdW5zaWduZWQgMzIgYml0cy5cclxuXHR7XHJcblx0XHRyZXR1cm4gbjEgPCBuMiA/IHVuc2lnbmVkMzIoKDB4MTAwMDAwMDAwIC0gKG4yIC0gbjEpKSAmIDB4ZmZmZmZmZmYpIDogbjEgLSBuMjtcclxuXHR9XHJcblxyXG5cdGZ1bmN0aW9uIGFkZGl0aW9uMzIgKG4xLCBuMikgLy8gZW11bGF0ZXMgb3ZlcmZsb3cgb2YgYSBjIDMyLWJpdHMgdW5zaWdlZCBpbnRlZ2VyIHZhcmlhYmxlLCBpbnN0ZWFkIG9mIHRoZSBvcGVyYXRvciArLiB0aGVzZSBib3RoIGFyZ3VtZW50cyBtdXN0IGJlIG5vbi1uZWdhdGl2ZSBpbnRlZ2VycyBleHByZXNzaWJsZSB1c2luZyB1bnNpZ25lZCAzMiBiaXRzLlxyXG5cdHtcclxuXHRcdHJldHVybiB1bnNpZ25lZDMyKChuMSArIG4yKSAmIDB4ZmZmZmZmZmYpXHJcblx0fVxyXG5cclxuXHRmdW5jdGlvbiBtdWx0aXBsaWNhdGlvbjMyIChuMSwgbjIpIC8vIGVtdWxhdGVzIG92ZXJmbG93IG9mIGEgYyAzMi1iaXRzIHVuc2lnZWQgaW50ZWdlciB2YXJpYWJsZSwgaW5zdGVhZCBvZiB0aGUgb3BlcmF0b3IgKi4gdGhlc2UgYm90aCBhcmd1bWVudHMgbXVzdCBiZSBub24tbmVnYXRpdmUgaW50ZWdlcnMgZXhwcmVzc2libGUgdXNpbmcgdW5zaWduZWQgMzIgYml0cy5cclxuXHR7XHJcblx0XHR2YXIgc3VtID0gMDtcclxuXHRcdGZvciAodmFyIGkgPSAwOyBpIDwgMzI7ICsraSl7XHJcblx0XHRcdGlmICgobjEgPj4+IGkpICYgMHgxKXtcclxuXHRcdFx0XHRzdW0gPSBhZGRpdGlvbjMyKHN1bSwgdW5zaWduZWQzMihuMiA8PCBpKSk7XHJcblx0XHRcdH1cclxuXHRcdH1cclxuXHRcdHJldHVybiBzdW07XHJcblx0fVxyXG5cclxuXHQvKiBpbml0aWFsaXplcyBtdFtOXSB3aXRoIGEgc2VlZCAqL1xyXG5cdC8vYy8vdm9pZCBpbml0X2dlbnJhbmQodW5zaWduZWQgbG9uZyBzKVxyXG5cdHRoaXMuaW5pdF9nZW5yYW5kID0gZnVuY3Rpb24gKHMpXHJcblx0e1xyXG5cdFx0Ly9jLy9tdFswXT0gcyAmIDB4ZmZmZmZmZmY7XHJcblx0XHRtdFswXT0gdW5zaWduZWQzMihzICYgMHhmZmZmZmZmZik7XHJcblx0XHRmb3IgKG10aT0xOyBtdGk8TjsgbXRpKyspIHtcclxuXHRcdFx0bXRbbXRpXSA9IFxyXG5cdFx0XHQvL2MvLygxODEyNDMzMjUzICogKG10W210aS0xXSBeIChtdFttdGktMV0gPj4gMzApKSArIG10aSk7XHJcblx0XHRcdGFkZGl0aW9uMzIobXVsdGlwbGljYXRpb24zMigxODEyNDMzMjUzLCB1bnNpZ25lZDMyKG10W210aS0xXSBeIChtdFttdGktMV0gPj4+IDMwKSkpLCBtdGkpO1xyXG5cdFx0XHQvKiBTZWUgS251dGggVEFPQ1AgVm9sMi4gM3JkIEVkLiBQLjEwNiBmb3IgbXVsdGlwbGllci4gKi9cclxuXHRcdFx0LyogSW4gdGhlIHByZXZpb3VzIHZlcnNpb25zLCBNU0JzIG9mIHRoZSBzZWVkIGFmZmVjdCAgICovXHJcblx0XHRcdC8qIG9ubHkgTVNCcyBvZiB0aGUgYXJyYXkgbXRbXS4gICAgICAgICAgICAgICAgICAgICAgICAqL1xyXG5cdFx0XHQvKiAyMDAyLzAxLzA5IG1vZGlmaWVkIGJ5IE1ha290byBNYXRzdW1vdG8gICAgICAgICAgICAgKi9cclxuXHRcdFx0Ly9jLy9tdFttdGldICY9IDB4ZmZmZmZmZmY7XHJcblx0XHRcdG10W210aV0gPSB1bnNpZ25lZDMyKG10W210aV0gJiAweGZmZmZmZmZmKTtcclxuXHRcdFx0LyogZm9yID4zMiBiaXQgbWFjaGluZXMgKi9cclxuXHRcdH1cclxuXHR9XHJcblxyXG5cdC8qIGluaXRpYWxpemUgYnkgYW4gYXJyYXkgd2l0aCBhcnJheS1sZW5ndGggKi9cclxuXHQvKiBpbml0X2tleSBpcyB0aGUgYXJyYXkgZm9yIGluaXRpYWxpemluZyBrZXlzICovXHJcblx0Lyoga2V5X2xlbmd0aCBpcyBpdHMgbGVuZ3RoICovXHJcblx0Lyogc2xpZ2h0IGNoYW5nZSBmb3IgQysrLCAyMDA0LzIvMjYgKi9cclxuXHQvL2MvL3ZvaWQgaW5pdF9ieV9hcnJheSh1bnNpZ25lZCBsb25nIGluaXRfa2V5W10sIGludCBrZXlfbGVuZ3RoKVxyXG5cdHRoaXMuaW5pdF9ieV9hcnJheSA9IGZ1bmN0aW9uIChpbml0X2tleSwga2V5X2xlbmd0aClcclxuXHR7XHJcblx0XHQvL2MvL2ludCBpLCBqLCBrO1xyXG5cdFx0dmFyIGksIGosIGs7XHJcblx0XHQvL2MvL2luaXRfZ2VucmFuZCgxOTY1MDIxOCk7XHJcblx0XHR0aGlzLmluaXRfZ2VucmFuZCgxOTY1MDIxOCk7XHJcblx0XHRpPTE7IGo9MDtcclxuXHRcdGsgPSAoTj5rZXlfbGVuZ3RoID8gTiA6IGtleV9sZW5ndGgpO1xyXG5cdFx0Zm9yICg7IGs7IGstLSkge1xyXG5cdFx0XHQvL2MvL210W2ldID0gKG10W2ldIF4gKChtdFtpLTFdIF4gKG10W2ktMV0gPj4gMzApKSAqIDE2NjQ1MjUpKVxyXG5cdFx0XHQvL2MvL1x0KyBpbml0X2tleVtqXSArIGo7IC8qIG5vbiBsaW5lYXIgKi9cclxuXHRcdFx0bXRbaV0gPSBhZGRpdGlvbjMyKGFkZGl0aW9uMzIodW5zaWduZWQzMihtdFtpXSBeIG11bHRpcGxpY2F0aW9uMzIodW5zaWduZWQzMihtdFtpLTFdIF4gKG10W2ktMV0gPj4+IDMwKSksIDE2NjQ1MjUpKSwgaW5pdF9rZXlbal0pLCBqKTtcclxuXHRcdFx0bXRbaV0gPSBcclxuXHRcdFx0Ly9jLy9tdFtpXSAmPSAweGZmZmZmZmZmOyAvKiBmb3IgV09SRFNJWkUgPiAzMiBtYWNoaW5lcyAqL1xyXG5cdFx0XHR1bnNpZ25lZDMyKG10W2ldICYgMHhmZmZmZmZmZik7XHJcblx0XHRcdGkrKzsgaisrO1xyXG5cdFx0XHRpZiAoaT49TikgeyBtdFswXSA9IG10W04tMV07IGk9MTsgfVxyXG5cdFx0XHRpZiAoaj49a2V5X2xlbmd0aCkgaj0wO1xyXG5cdFx0fVxyXG5cdFx0Zm9yIChrPU4tMTsgazsgay0tKSB7XHJcblx0XHRcdC8vYy8vbXRbaV0gPSAobXRbaV0gXiAoKG10W2ktMV0gXiAobXRbaS0xXSA+PiAzMCkpICogMTU2NjA4Mzk0MSkpXHJcblx0XHRcdC8vYy8vLSBpOyAvKiBub24gbGluZWFyICovXHJcblx0XHRcdG10W2ldID0gc3VidHJhY3Rpb24zMih1bnNpZ25lZDMyKChkYmc9bXRbaV0pIF4gbXVsdGlwbGljYXRpb24zMih1bnNpZ25lZDMyKG10W2ktMV0gXiAobXRbaS0xXSA+Pj4gMzApKSwgMTU2NjA4Mzk0MSkpLCBpKTtcclxuXHRcdFx0Ly9jLy9tdFtpXSAmPSAweGZmZmZmZmZmOyAvKiBmb3IgV09SRFNJWkUgPiAzMiBtYWNoaW5lcyAqL1xyXG5cdFx0XHRtdFtpXSA9IHVuc2lnbmVkMzIobXRbaV0gJiAweGZmZmZmZmZmKTtcclxuXHRcdFx0aSsrO1xyXG5cdFx0XHRpZiAoaT49TikgeyBtdFswXSA9IG10W04tMV07IGk9MTsgfVxyXG5cdFx0fVxyXG5cdFx0bXRbMF0gPSAweDgwMDAwMDAwOyAvKiBNU0IgaXMgMTsgYXNzdXJpbmcgbm9uLXplcm8gaW5pdGlhbCBhcnJheSAqL1xyXG5cdH1cclxuXHJcbiAgICAvKiBtb3ZlZCBvdXRzaWRlIG9mIGdlbnJhbmRfaW50MzIoKSBieSBqd2F0dGUgMjAxMC0xMS0xNzsgZ2VuZXJhdGUgbGVzcyBnYXJiYWdlICovXHJcbiAgICB2YXIgbWFnMDEgPSBbMHgwLCBNQVRSSVhfQV07XHJcblxyXG5cdC8qIGdlbmVyYXRlcyBhIHJhbmRvbSBudW1iZXIgb24gWzAsMHhmZmZmZmZmZl0taW50ZXJ2YWwgKi9cclxuXHQvL2MvL3Vuc2lnbmVkIGxvbmcgZ2VucmFuZF9pbnQzMih2b2lkKVxyXG5cdHRoaXMuZ2VucmFuZF9pbnQzMiA9IGZ1bmN0aW9uICgpXHJcblx0e1xyXG5cdFx0Ly9jLy91bnNpZ25lZCBsb25nIHk7XHJcblx0XHQvL2MvL3N0YXRpYyB1bnNpZ25lZCBsb25nIG1hZzAxWzJdPXsweDBVTCwgTUFUUklYX0F9O1xyXG5cdFx0dmFyIHk7XHJcblx0XHQvKiBtYWcwMVt4XSA9IHggKiBNQVRSSVhfQSAgZm9yIHg9MCwxICovXHJcblxyXG5cdFx0aWYgKG10aSA+PSBOKSB7IC8qIGdlbmVyYXRlIE4gd29yZHMgYXQgb25lIHRpbWUgKi9cclxuXHRcdFx0Ly9jLy9pbnQga2s7XHJcblx0XHRcdHZhciBraztcclxuXHJcblx0XHRcdGlmIChtdGkgPT0gTisxKSAgIC8qIGlmIGluaXRfZ2VucmFuZCgpIGhhcyBub3QgYmVlbiBjYWxsZWQsICovXHJcblx0XHRcdFx0Ly9jLy9pbml0X2dlbnJhbmQoNTQ4OSk7IC8qIGEgZGVmYXVsdCBpbml0aWFsIHNlZWQgaXMgdXNlZCAqL1xyXG5cdFx0XHRcdHRoaXMuaW5pdF9nZW5yYW5kKDU0ODkpOyAvKiBhIGRlZmF1bHQgaW5pdGlhbCBzZWVkIGlzIHVzZWQgKi9cclxuXHJcblx0XHRcdGZvciAoa2s9MDtrazxOLU07a2srKykge1xyXG5cdFx0XHRcdC8vYy8veSA9IChtdFtra10mVVBQRVJfTUFTSyl8KG10W2trKzFdJkxPV0VSX01BU0spO1xyXG5cdFx0XHRcdC8vYy8vbXRba2tdID0gbXRba2srTV0gXiAoeSA+PiAxKSBeIG1hZzAxW3kgJiAweDFdO1xyXG5cdFx0XHRcdHkgPSB1bnNpZ25lZDMyKChtdFtra10mVVBQRVJfTUFTSyl8KG10W2trKzFdJkxPV0VSX01BU0spKTtcclxuXHRcdFx0XHRtdFtra10gPSB1bnNpZ25lZDMyKG10W2trK01dIF4gKHkgPj4+IDEpIF4gbWFnMDFbeSAmIDB4MV0pO1xyXG5cdFx0XHR9XHJcblx0XHRcdGZvciAoO2trPE4tMTtraysrKSB7XHJcblx0XHRcdFx0Ly9jLy95ID0gKG10W2trXSZVUFBFUl9NQVNLKXwobXRba2srMV0mTE9XRVJfTUFTSyk7XHJcblx0XHRcdFx0Ly9jLy9tdFtra10gPSBtdFtraysoTS1OKV0gXiAoeSA+PiAxKSBeIG1hZzAxW3kgJiAweDFdO1xyXG5cdFx0XHRcdHkgPSB1bnNpZ25lZDMyKChtdFtra10mVVBQRVJfTUFTSyl8KG10W2trKzFdJkxPV0VSX01BU0spKTtcclxuXHRcdFx0XHRtdFtra10gPSB1bnNpZ25lZDMyKG10W2trKyhNLU4pXSBeICh5ID4+PiAxKSBeIG1hZzAxW3kgJiAweDFdKTtcclxuXHRcdFx0fVxyXG5cdFx0XHQvL2MvL3kgPSAobXRbTi0xXSZVUFBFUl9NQVNLKXwobXRbMF0mTE9XRVJfTUFTSyk7XHJcblx0XHRcdC8vYy8vbXRbTi0xXSA9IG10W00tMV0gXiAoeSA+PiAxKSBeIG1hZzAxW3kgJiAweDFdO1xyXG5cdFx0XHR5ID0gdW5zaWduZWQzMigobXRbTi0xXSZVUFBFUl9NQVNLKXwobXRbMF0mTE9XRVJfTUFTSykpO1xyXG5cdFx0XHRtdFtOLTFdID0gdW5zaWduZWQzMihtdFtNLTFdIF4gKHkgPj4+IDEpIF4gbWFnMDFbeSAmIDB4MV0pO1xyXG5cdFx0XHRtdGkgPSAwO1xyXG5cdFx0fVxyXG5cclxuXHRcdHkgPSBtdFttdGkrK107XHJcblxyXG5cdFx0LyogVGVtcGVyaW5nICovXHJcblx0XHQvL2MvL3kgXj0gKHkgPj4gMTEpO1xyXG5cdFx0Ly9jLy95IF49ICh5IDw8IDcpICYgMHg5ZDJjNTY4MDtcclxuXHRcdC8vYy8veSBePSAoeSA8PCAxNSkgJiAweGVmYzYwMDAwO1xyXG5cdFx0Ly9jLy95IF49ICh5ID4+IDE4KTtcclxuXHRcdHkgPSB1bnNpZ25lZDMyKHkgXiAoeSA+Pj4gMTEpKTtcclxuXHRcdHkgPSB1bnNpZ25lZDMyKHkgXiAoKHkgPDwgNykgJiAweDlkMmM1NjgwKSk7XHJcblx0XHR5ID0gdW5zaWduZWQzMih5IF4gKCh5IDw8IDE1KSAmIDB4ZWZjNjAwMDApKTtcclxuXHRcdHkgPSB1bnNpZ25lZDMyKHkgXiAoeSA+Pj4gMTgpKTtcclxuXHJcblx0XHRyZXR1cm4geTtcclxuXHR9XHJcblxyXG5cdC8qIGdlbmVyYXRlcyBhIHJhbmRvbSBudW1iZXIgb24gWzAsMHg3ZmZmZmZmZl0taW50ZXJ2YWwgKi9cclxuXHQvL2MvL2xvbmcgZ2VucmFuZF9pbnQzMSh2b2lkKVxyXG5cdHRoaXMuZ2VucmFuZF9pbnQzMSA9IGZ1bmN0aW9uICgpXHJcblx0e1xyXG5cdFx0Ly9jLy9yZXR1cm4gKGdlbnJhbmRfaW50MzIoKT4+MSk7XHJcblx0XHRyZXR1cm4gKHRoaXMuZ2VucmFuZF9pbnQzMigpPj4+MSk7XHJcblx0fVxyXG5cclxuXHQvKiBnZW5lcmF0ZXMgYSByYW5kb20gbnVtYmVyIG9uIFswLDFdLXJlYWwtaW50ZXJ2YWwgKi9cclxuXHQvL2MvL2RvdWJsZSBnZW5yYW5kX3JlYWwxKHZvaWQpXHJcblx0dGhpcy5nZW5yYW5kX3JlYWwxID0gZnVuY3Rpb24gKClcclxuXHR7XHJcblx0XHQvL2MvL3JldHVybiBnZW5yYW5kX2ludDMyKCkqKDEuMC80Mjk0OTY3Mjk1LjApO1xyXG5cdFx0cmV0dXJuIHRoaXMuZ2VucmFuZF9pbnQzMigpKigxLjAvNDI5NDk2NzI5NS4wKTtcclxuXHRcdC8qIGRpdmlkZWQgYnkgMl4zMi0xICovXHJcblx0fVxyXG5cclxuXHQvKiBnZW5lcmF0ZXMgYSByYW5kb20gbnVtYmVyIG9uIFswLDEpLXJlYWwtaW50ZXJ2YWwgKi9cclxuXHQvL2MvL2RvdWJsZSBnZW5yYW5kX3JlYWwyKHZvaWQpXHJcblx0dGhpcy5nZW5yYW5kX3JlYWwyID0gZnVuY3Rpb24gKClcclxuXHR7XHJcblx0XHQvL2MvL3JldHVybiBnZW5yYW5kX2ludDMyKCkqKDEuMC80Mjk0OTY3Mjk2LjApO1xyXG5cdFx0cmV0dXJuIHRoaXMuZ2VucmFuZF9pbnQzMigpKigxLjAvNDI5NDk2NzI5Ni4wKTtcclxuXHRcdC8qIGRpdmlkZWQgYnkgMl4zMiAqL1xyXG5cdH1cclxuXHJcblx0LyogZ2VuZXJhdGVzIGEgcmFuZG9tIG51bWJlciBvbiAoMCwxKS1yZWFsLWludGVydmFsICovXHJcblx0Ly9jLy9kb3VibGUgZ2VucmFuZF9yZWFsMyh2b2lkKVxyXG5cdHRoaXMuZ2VucmFuZF9yZWFsMyA9IGZ1bmN0aW9uICgpXHJcblx0e1xyXG5cdFx0Ly9jLy9yZXR1cm4gKChnZW5yYW5kX2ludDMyKCkpICsgMC41KSooMS4wLzQyOTQ5NjcyOTYuMCk7XHJcblx0XHRyZXR1cm4gKCh0aGlzLmdlbnJhbmRfaW50MzIoKSkgKyAwLjUpKigxLjAvNDI5NDk2NzI5Ni4wKTtcclxuXHRcdC8qIGRpdmlkZWQgYnkgMl4zMiAqL1xyXG5cdH1cclxuXHJcblx0LyogZ2VuZXJhdGVzIGEgcmFuZG9tIG51bWJlciBvbiBbMCwxKSB3aXRoIDUzLWJpdCByZXNvbHV0aW9uKi9cclxuXHQvL2MvL2RvdWJsZSBnZW5yYW5kX3JlczUzKHZvaWQpXHJcblx0dGhpcy5nZW5yYW5kX3JlczUzID0gZnVuY3Rpb24gKClcclxuXHR7XHJcblx0XHQvL2MvL3Vuc2lnbmVkIGxvbmcgYT1nZW5yYW5kX2ludDMyKCk+PjUsIGI9Z2VucmFuZF9pbnQzMigpPj42O1xyXG5cdFx0dmFyIGE9dGhpcy5nZW5yYW5kX2ludDMyKCk+Pj41LCBiPXRoaXMuZ2VucmFuZF9pbnQzMigpPj4+NjtcclxuXHRcdHJldHVybihhKjY3MTA4ODY0LjArYikqKDEuMC85MDA3MTk5MjU0NzQwOTkyLjApO1xyXG5cdH1cclxuXHQvKiBUaGVzZSByZWFsIHZlcnNpb25zIGFyZSBkdWUgdG8gSXNha3UgV2FkYSwgMjAwMi8wMS8wOSBhZGRlZCAqL1xyXG59XHJcblxyXG4vLyAgRXhwb3J0czogUHVibGljIEFQSVxyXG5cclxuLy8gIEV4cG9ydCB0aGUgdHdpc3RlciBjbGFzc1xyXG5leHBvcnRzLk1lcnNlbm5lVHdpc3RlcjE5OTM3ID0gTWVyc2VubmVUd2lzdGVyMTk5Mzc7XHJcblxyXG4vLyAgRXhwb3J0IGEgc2ltcGxpZmllZCBmdW5jdGlvbiB0byBnZW5lcmF0ZSByYW5kb20gbnVtYmVyc1xyXG52YXIgZ2VuID0gbmV3IE1lcnNlbm5lVHdpc3RlcjE5OTM3O1xyXG5nZW4uaW5pdF9nZW5yYW5kKChuZXcgRGF0ZSkuZ2V0VGltZSgpICUgMTAwMDAwMDAwMCk7XHJcbmV4cG9ydHMucmFuZCA9IGZ1bmN0aW9uKE4pIHtcclxuICAgIGlmICghTilcclxuICAgICAgICB7XHJcbiAgICAgICAgTiA9IDMyNzY4O1xyXG4gICAgICAgIH1cclxuICAgIHJldHVybiBNYXRoLmZsb29yKGdlbi5nZW5yYW5kX3JlYWwyKCkgKiBOKTtcclxufVxyXG5leHBvcnRzLnNlZWQgPSBmdW5jdGlvbihTKSB7XHJcbiAgICBpZiAodHlwZW9mKFMpICE9ICdudW1iZXInKVxyXG4gICAgICAgIHtcclxuICAgICAgICB0aHJvdyBuZXcgRXJyb3IoXCJzZWVkKFMpIG11c3QgdGFrZSBudW1lcmljIGFyZ3VtZW50OyBpcyBcIiArIHR5cGVvZihTKSk7XHJcbiAgICAgICAgfVxyXG4gICAgZ2VuLmluaXRfZ2VucmFuZChTKTtcclxufVxyXG5leHBvcnRzLnNlZWRfYXJyYXkgPSBmdW5jdGlvbihBKSB7XHJcbiAgICBpZiAodHlwZW9mKEEpICE9ICdvYmplY3QnKVxyXG4gICAgICAgIHtcclxuICAgICAgICB0aHJvdyBuZXcgRXJyb3IoXCJzZWVkX2FycmF5KEEpIG11c3QgdGFrZSBhcnJheSBvZiBudW1iZXJzOyBpcyBcIiArIHR5cGVvZihBKSk7XHJcbiAgICAgICAgfVxyXG4gICAgZ2VuLmluaXRfYnlfYXJyYXkoQSk7XHJcbn1cclxuXHJcblxyXG4iLCIvKlxuICogQSBzcGVlZC1pbXByb3ZlZCBwZXJsaW4gYW5kIHNpbXBsZXggbm9pc2UgYWxnb3JpdGhtcyBmb3IgMkQuXG4gKlxuICogQmFzZWQgb24gZXhhbXBsZSBjb2RlIGJ5IFN0ZWZhbiBHdXN0YXZzb24gKHN0ZWd1QGl0bi5saXUuc2UpLlxuICogT3B0aW1pc2F0aW9ucyBieSBQZXRlciBFYXN0bWFuIChwZWFzdG1hbkBkcml6emxlLnN0YW5mb3JkLmVkdSkuXG4gKiBCZXR0ZXIgcmFuayBvcmRlcmluZyBtZXRob2QgYnkgU3RlZmFuIEd1c3RhdnNvbiBpbiAyMDEyLlxuICogQ29udmVydGVkIHRvIEphdmFzY3JpcHQgYnkgSm9zZXBoIEdlbnRsZS5cbiAqXG4gKiBWZXJzaW9uIDIwMTItMDMtMDlcbiAqXG4gKiBUaGlzIGNvZGUgd2FzIHBsYWNlZCBpbiB0aGUgcHVibGljIGRvbWFpbiBieSBpdHMgb3JpZ2luYWwgYXV0aG9yLFxuICogU3RlZmFuIEd1c3RhdnNvbi4gWW91IG1heSB1c2UgaXQgYXMgeW91IHNlZSBmaXQsIGJ1dFxuICogYXR0cmlidXRpb24gaXMgYXBwcmVjaWF0ZWQuXG4gKlxuICovXG5cbihmdW5jdGlvbihnbG9iYWwpe1xuXG4gIC8vIFBhc3NpbmcgaW4gc2VlZCB3aWxsIHNlZWQgdGhpcyBOb2lzZSBpbnN0YW5jZVxuICBmdW5jdGlvbiBOb2lzZShzZWVkKSB7XG4gICAgZnVuY3Rpb24gR3JhZCh4LCB5LCB6KSB7XG4gICAgICB0aGlzLnggPSB4OyB0aGlzLnkgPSB5OyB0aGlzLnogPSB6O1xuICAgIH1cblxuICAgIEdyYWQucHJvdG90eXBlLmRvdDIgPSBmdW5jdGlvbih4LCB5KSB7XG4gICAgICByZXR1cm4gdGhpcy54KnggKyB0aGlzLnkqeTtcbiAgICB9O1xuXG4gICAgR3JhZC5wcm90b3R5cGUuZG90MyA9IGZ1bmN0aW9uKHgsIHksIHopIHtcbiAgICAgIHJldHVybiB0aGlzLngqeCArIHRoaXMueSp5ICsgdGhpcy56Kno7XG4gICAgfTtcblxuICAgIHRoaXMuZ3JhZDMgPSBbbmV3IEdyYWQoMSwxLDApLG5ldyBHcmFkKC0xLDEsMCksbmV3IEdyYWQoMSwtMSwwKSxuZXcgR3JhZCgtMSwtMSwwKSxcbiAgICAgICAgICAgICAgICAgbmV3IEdyYWQoMSwwLDEpLG5ldyBHcmFkKC0xLDAsMSksbmV3IEdyYWQoMSwwLC0xKSxuZXcgR3JhZCgtMSwwLC0xKSxcbiAgICAgICAgICAgICAgICAgbmV3IEdyYWQoMCwxLDEpLG5ldyBHcmFkKDAsLTEsMSksbmV3IEdyYWQoMCwxLC0xKSxuZXcgR3JhZCgwLC0xLC0xKV07XG5cbiAgICB0aGlzLnAgPSBbMTUxLDE2MCwxMzcsOTEsOTAsMTUsXG4gICAgMTMxLDEzLDIwMSw5NSw5Niw1MywxOTQsMjMzLDcsMjI1LDE0MCwzNiwxMDMsMzAsNjksMTQyLDgsOTksMzcsMjQwLDIxLDEwLDIzLFxuICAgIDE5MCwgNiwxNDgsMjQ3LDEyMCwyMzQsNzUsMCwyNiwxOTcsNjIsOTQsMjUyLDIxOSwyMDMsMTE3LDM1LDExLDMyLDU3LDE3NywzMyxcbiAgICA4OCwyMzcsMTQ5LDU2LDg3LDE3NCwyMCwxMjUsMTM2LDE3MSwxNjgsIDY4LDE3NSw3NCwxNjUsNzEsMTM0LDEzOSw0OCwyNywxNjYsXG4gICAgNzcsMTQ2LDE1OCwyMzEsODMsMTExLDIyOSwxMjIsNjAsMjExLDEzMywyMzAsMjIwLDEwNSw5Miw0MSw1NSw0NiwyNDUsNDAsMjQ0LFxuICAgIDEwMiwxNDMsNTQsIDY1LDI1LDYzLDE2MSwgMSwyMTYsODAsNzMsMjA5LDc2LDEzMiwxODcsMjA4LCA4OSwxOCwxNjksMjAwLDE5NixcbiAgICAxMzUsMTMwLDExNiwxODgsMTU5LDg2LDE2NCwxMDAsMTA5LDE5OCwxNzMsMTg2LCAzLDY0LDUyLDIxNywyMjYsMjUwLDEyNCwxMjMsXG4gICAgNSwyMDIsMzgsMTQ3LDExOCwxMjYsMjU1LDgyLDg1LDIxMiwyMDcsMjA2LDU5LDIyNyw0NywxNiw1OCwxNywxODIsMTg5LDI4LDQyLFxuICAgIDIyMywxODMsMTcwLDIxMywxMTksMjQ4LDE1MiwgMiw0NCwxNTQsMTYzLCA3MCwyMjEsMTUzLDEwMSwxNTUsMTY3LCA0MywxNzIsOSxcbiAgICAxMjksMjIsMzksMjUzLCAxOSw5OCwxMDgsMTEwLDc5LDExMywyMjQsMjMyLDE3OCwxODUsIDExMiwxMDQsMjE4LDI0Niw5NywyMjgsXG4gICAgMjUxLDM0LDI0MiwxOTMsMjM4LDIxMCwxNDQsMTIsMTkxLDE3OSwxNjIsMjQxLCA4MSw1MSwxNDUsMjM1LDI0OSwxNCwyMzksMTA3LFxuICAgIDQ5LDE5MiwyMTQsIDMxLDE4MSwxOTksMTA2LDE1NywxODQsIDg0LDIwNCwxNzYsMTE1LDEyMSw1MCw0NSwxMjcsIDQsMTUwLDI1NCxcbiAgICAxMzgsMjM2LDIwNSw5MywyMjIsMTE0LDY3LDI5LDI0LDcyLDI0MywxNDEsMTI4LDE5NSw3OCw2NiwyMTUsNjEsMTU2LDE4MF07XG4gICAgLy8gVG8gcmVtb3ZlIHRoZSBuZWVkIGZvciBpbmRleCB3cmFwcGluZywgZG91YmxlIHRoZSBwZXJtdXRhdGlvbiB0YWJsZSBsZW5ndGhcbiAgICB0aGlzLnBlcm0gPSBuZXcgQXJyYXkoNTEyKTtcbiAgICB0aGlzLmdyYWRQID0gbmV3IEFycmF5KDUxMik7XG5cbiAgICB0aGlzLnNlZWQoc2VlZCB8fCAwKTtcbiAgfVxuXG4gIC8vIFRoaXMgaXNuJ3QgYSB2ZXJ5IGdvb2Qgc2VlZGluZyBmdW5jdGlvbiwgYnV0IGl0IHdvcmtzIG9rLiBJdCBzdXBwb3J0cyAyXjE2XG4gIC8vIGRpZmZlcmVudCBzZWVkIHZhbHVlcy4gV3JpdGUgc29tZXRoaW5nIGJldHRlciBpZiB5b3UgbmVlZCBtb3JlIHNlZWRzLlxuICBOb2lzZS5wcm90b3R5cGUuc2VlZCA9IGZ1bmN0aW9uKHNlZWQpIHtcbiAgICBpZihzZWVkID4gMCAmJiBzZWVkIDwgMSkge1xuICAgICAgLy8gU2NhbGUgdGhlIHNlZWQgb3V0XG4gICAgICBzZWVkICo9IDY1NTM2O1xuICAgIH1cblxuICAgIHNlZWQgPSBNYXRoLmZsb29yKHNlZWQpO1xuICAgIGlmKHNlZWQgPCAyNTYpIHtcbiAgICAgIHNlZWQgfD0gc2VlZCA8PCA4O1xuICAgIH1cblxuICAgIHZhciBwID0gdGhpcy5wO1xuICAgIGZvcih2YXIgaSA9IDA7IGkgPCAyNTY7IGkrKykge1xuICAgICAgdmFyIHY7XG4gICAgICBpZiAoaSAmIDEpIHtcbiAgICAgICAgdiA9IHBbaV0gXiAoc2VlZCAmIDI1NSk7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICB2ID0gcFtpXSBeICgoc2VlZD4+OCkgJiAyNTUpO1xuICAgICAgfVxuXG4gICAgICB2YXIgcGVybSA9IHRoaXMucGVybTtcbiAgICAgIHZhciBncmFkUCA9IHRoaXMuZ3JhZFA7XG4gICAgICBwZXJtW2ldID0gcGVybVtpICsgMjU2XSA9IHY7XG4gICAgICBncmFkUFtpXSA9IGdyYWRQW2kgKyAyNTZdID0gdGhpcy5ncmFkM1t2ICUgMTJdO1xuICAgIH1cbiAgfTtcblxuICAvKlxuICBmb3IodmFyIGk9MDsgaTwyNTY7IGkrKykge1xuICAgIHBlcm1baV0gPSBwZXJtW2kgKyAyNTZdID0gcFtpXTtcbiAgICBncmFkUFtpXSA9IGdyYWRQW2kgKyAyNTZdID0gZ3JhZDNbcGVybVtpXSAlIDEyXTtcbiAgfSovXG5cbiAgLy8gU2tld2luZyBhbmQgdW5za2V3aW5nIGZhY3RvcnMgZm9yIDIsIDMsIGFuZCA0IGRpbWVuc2lvbnNcbiAgdmFyIEYyID0gMC41KihNYXRoLnNxcnQoMyktMSk7XG4gIHZhciBHMiA9ICgzLU1hdGguc3FydCgzKSkvNjtcblxuICB2YXIgRjMgPSAxLzM7XG4gIHZhciBHMyA9IDEvNjtcblxuICAvLyAyRCBzaW1wbGV4IG5vaXNlXG4gIE5vaXNlLnByb3RvdHlwZS5zaW1wbGV4MiA9IGZ1bmN0aW9uKHhpbiwgeWluKSB7XG4gICAgdmFyIG4wLCBuMSwgbjI7IC8vIE5vaXNlIGNvbnRyaWJ1dGlvbnMgZnJvbSB0aGUgdGhyZWUgY29ybmVyc1xuICAgIC8vIFNrZXcgdGhlIGlucHV0IHNwYWNlIHRvIGRldGVybWluZSB3aGljaCBzaW1wbGV4IGNlbGwgd2UncmUgaW5cbiAgICB2YXIgcyA9ICh4aW4reWluKSpGMjsgLy8gSGFpcnkgZmFjdG9yIGZvciAyRFxuICAgIHZhciBpID0gTWF0aC5mbG9vcih4aW4rcyk7XG4gICAgdmFyIGogPSBNYXRoLmZsb29yKHlpbitzKTtcbiAgICB2YXIgdCA9IChpK2opKkcyO1xuICAgIHZhciB4MCA9IHhpbi1pK3Q7IC8vIFRoZSB4LHkgZGlzdGFuY2VzIGZyb20gdGhlIGNlbGwgb3JpZ2luLCB1bnNrZXdlZC5cbiAgICB2YXIgeTAgPSB5aW4tait0O1xuICAgIC8vIEZvciB0aGUgMkQgY2FzZSwgdGhlIHNpbXBsZXggc2hhcGUgaXMgYW4gZXF1aWxhdGVyYWwgdHJpYW5nbGUuXG4gICAgLy8gRGV0ZXJtaW5lIHdoaWNoIHNpbXBsZXggd2UgYXJlIGluLlxuICAgIHZhciBpMSwgajE7IC8vIE9mZnNldHMgZm9yIHNlY29uZCAobWlkZGxlKSBjb3JuZXIgb2Ygc2ltcGxleCBpbiAoaSxqKSBjb29yZHNcbiAgICBpZih4MD55MCkgeyAvLyBsb3dlciB0cmlhbmdsZSwgWFkgb3JkZXI6ICgwLDApLT4oMSwwKS0+KDEsMSlcbiAgICAgIGkxPTE7IGoxPTA7XG4gICAgfSBlbHNlIHsgICAgLy8gdXBwZXIgdHJpYW5nbGUsIFlYIG9yZGVyOiAoMCwwKS0+KDAsMSktPigxLDEpXG4gICAgICBpMT0wOyBqMT0xO1xuICAgIH1cbiAgICAvLyBBIHN0ZXAgb2YgKDEsMCkgaW4gKGksaikgbWVhbnMgYSBzdGVwIG9mICgxLWMsLWMpIGluICh4LHkpLCBhbmRcbiAgICAvLyBhIHN0ZXAgb2YgKDAsMSkgaW4gKGksaikgbWVhbnMgYSBzdGVwIG9mICgtYywxLWMpIGluICh4LHkpLCB3aGVyZVxuICAgIC8vIGMgPSAoMy1zcXJ0KDMpKS82XG4gICAgdmFyIHgxID0geDAgLSBpMSArIEcyOyAvLyBPZmZzZXRzIGZvciBtaWRkbGUgY29ybmVyIGluICh4LHkpIHVuc2tld2VkIGNvb3Jkc1xuICAgIHZhciB5MSA9IHkwIC0gajEgKyBHMjtcbiAgICB2YXIgeDIgPSB4MCAtIDEgKyAyICogRzI7IC8vIE9mZnNldHMgZm9yIGxhc3QgY29ybmVyIGluICh4LHkpIHVuc2tld2VkIGNvb3Jkc1xuICAgIHZhciB5MiA9IHkwIC0gMSArIDIgKiBHMjtcbiAgICAvLyBXb3JrIG91dCB0aGUgaGFzaGVkIGdyYWRpZW50IGluZGljZXMgb2YgdGhlIHRocmVlIHNpbXBsZXggY29ybmVyc1xuICAgIGkgJj0gMjU1O1xuICAgIGogJj0gMjU1O1xuXG4gICAgdmFyIHBlcm0gPSB0aGlzLnBlcm07XG4gICAgdmFyIGdyYWRQID0gdGhpcy5ncmFkUDtcbiAgICB2YXIgZ2kwID0gZ3JhZFBbaStwZXJtW2pdXTtcbiAgICB2YXIgZ2kxID0gZ3JhZFBbaStpMStwZXJtW2orajFdXTtcbiAgICB2YXIgZ2kyID0gZ3JhZFBbaSsxK3Blcm1baisxXV07XG4gICAgLy8gQ2FsY3VsYXRlIHRoZSBjb250cmlidXRpb24gZnJvbSB0aGUgdGhyZWUgY29ybmVyc1xuICAgIHZhciB0MCA9IDAuNSAtIHgwKngwLXkwKnkwO1xuICAgIGlmKHQwPDApIHtcbiAgICAgIG4wID0gMDtcbiAgICB9IGVsc2Uge1xuICAgICAgdDAgKj0gdDA7XG4gICAgICBuMCA9IHQwICogdDAgKiBnaTAuZG90Mih4MCwgeTApOyAgLy8gKHgseSkgb2YgZ3JhZDMgdXNlZCBmb3IgMkQgZ3JhZGllbnRcbiAgICB9XG4gICAgdmFyIHQxID0gMC41IC0geDEqeDEteTEqeTE7XG4gICAgaWYodDE8MCkge1xuICAgICAgbjEgPSAwO1xuICAgIH0gZWxzZSB7XG4gICAgICB0MSAqPSB0MTtcbiAgICAgIG4xID0gdDEgKiB0MSAqIGdpMS5kb3QyKHgxLCB5MSk7XG4gICAgfVxuICAgIHZhciB0MiA9IDAuNSAtIHgyKngyLXkyKnkyO1xuICAgIGlmKHQyPDApIHtcbiAgICAgIG4yID0gMDtcbiAgICB9IGVsc2Uge1xuICAgICAgdDIgKj0gdDI7XG4gICAgICBuMiA9IHQyICogdDIgKiBnaTIuZG90Mih4MiwgeTIpO1xuICAgIH1cbiAgICAvLyBBZGQgY29udHJpYnV0aW9ucyBmcm9tIGVhY2ggY29ybmVyIHRvIGdldCB0aGUgZmluYWwgbm9pc2UgdmFsdWUuXG4gICAgLy8gVGhlIHJlc3VsdCBpcyBzY2FsZWQgdG8gcmV0dXJuIHZhbHVlcyBpbiB0aGUgaW50ZXJ2YWwgWy0xLDFdLlxuICAgIHJldHVybiA3MCAqIChuMCArIG4xICsgbjIpO1xuICB9O1xuXG4gIC8vIDNEIHNpbXBsZXggbm9pc2VcbiAgTm9pc2UucHJvdG90eXBlLnNpbXBsZXgzID0gZnVuY3Rpb24oeGluLCB5aW4sIHppbikge1xuICAgIHZhciBuMCwgbjEsIG4yLCBuMzsgLy8gTm9pc2UgY29udHJpYnV0aW9ucyBmcm9tIHRoZSBmb3VyIGNvcm5lcnNcblxuICAgIC8vIFNrZXcgdGhlIGlucHV0IHNwYWNlIHRvIGRldGVybWluZSB3aGljaCBzaW1wbGV4IGNlbGwgd2UncmUgaW5cbiAgICB2YXIgcyA9ICh4aW4reWluK3ppbikqRjM7IC8vIEhhaXJ5IGZhY3RvciBmb3IgMkRcbiAgICB2YXIgaSA9IE1hdGguZmxvb3IoeGluK3MpO1xuICAgIHZhciBqID0gTWF0aC5mbG9vcih5aW4rcyk7XG4gICAgdmFyIGsgPSBNYXRoLmZsb29yKHppbitzKTtcblxuICAgIHZhciB0ID0gKGkraitrKSpHMztcbiAgICB2YXIgeDAgPSB4aW4taSt0OyAvLyBUaGUgeCx5IGRpc3RhbmNlcyBmcm9tIHRoZSBjZWxsIG9yaWdpbiwgdW5za2V3ZWQuXG4gICAgdmFyIHkwID0geWluLWordDtcbiAgICB2YXIgejAgPSB6aW4tayt0O1xuXG4gICAgLy8gRm9yIHRoZSAzRCBjYXNlLCB0aGUgc2ltcGxleCBzaGFwZSBpcyBhIHNsaWdodGx5IGlycmVndWxhciB0ZXRyYWhlZHJvbi5cbiAgICAvLyBEZXRlcm1pbmUgd2hpY2ggc2ltcGxleCB3ZSBhcmUgaW4uXG4gICAgdmFyIGkxLCBqMSwgazE7IC8vIE9mZnNldHMgZm9yIHNlY29uZCBjb3JuZXIgb2Ygc2ltcGxleCBpbiAoaSxqLGspIGNvb3Jkc1xuICAgIHZhciBpMiwgajIsIGsyOyAvLyBPZmZzZXRzIGZvciB0aGlyZCBjb3JuZXIgb2Ygc2ltcGxleCBpbiAoaSxqLGspIGNvb3Jkc1xuICAgIGlmKHgwID49IHkwKSB7XG4gICAgICBpZih5MCA+PSB6MCkgICAgICB7IGkxPTE7IGoxPTA7IGsxPTA7IGkyPTE7IGoyPTE7IGsyPTA7IH1cbiAgICAgIGVsc2UgaWYoeDAgPj0gejApIHsgaTE9MTsgajE9MDsgazE9MDsgaTI9MTsgajI9MDsgazI9MTsgfVxuICAgICAgZWxzZSAgICAgICAgICAgICAgeyBpMT0wOyBqMT0wOyBrMT0xOyBpMj0xOyBqMj0wOyBrMj0xOyB9XG4gICAgfSBlbHNlIHtcbiAgICAgIGlmKHkwIDwgejApICAgICAgeyBpMT0wOyBqMT0wOyBrMT0xOyBpMj0wOyBqMj0xOyBrMj0xOyB9XG4gICAgICBlbHNlIGlmKHgwIDwgejApIHsgaTE9MDsgajE9MTsgazE9MDsgaTI9MDsgajI9MTsgazI9MTsgfVxuICAgICAgZWxzZSAgICAgICAgICAgICB7IGkxPTA7IGoxPTE7IGsxPTA7IGkyPTE7IGoyPTE7IGsyPTA7IH1cbiAgICB9XG4gICAgLy8gQSBzdGVwIG9mICgxLDAsMCkgaW4gKGksaixrKSBtZWFucyBhIHN0ZXAgb2YgKDEtYywtYywtYykgaW4gKHgseSx6KSxcbiAgICAvLyBhIHN0ZXAgb2YgKDAsMSwwKSBpbiAoaSxqLGspIG1lYW5zIGEgc3RlcCBvZiAoLWMsMS1jLC1jKSBpbiAoeCx5LHopLCBhbmRcbiAgICAvLyBhIHN0ZXAgb2YgKDAsMCwxKSBpbiAoaSxqLGspIG1lYW5zIGEgc3RlcCBvZiAoLWMsLWMsMS1jKSBpbiAoeCx5LHopLCB3aGVyZVxuICAgIC8vIGMgPSAxLzYuXG4gICAgdmFyIHgxID0geDAgLSBpMSArIEczOyAvLyBPZmZzZXRzIGZvciBzZWNvbmQgY29ybmVyXG4gICAgdmFyIHkxID0geTAgLSBqMSArIEczO1xuICAgIHZhciB6MSA9IHowIC0gazEgKyBHMztcblxuICAgIHZhciB4MiA9IHgwIC0gaTIgKyAyICogRzM7IC8vIE9mZnNldHMgZm9yIHRoaXJkIGNvcm5lclxuICAgIHZhciB5MiA9IHkwIC0gajIgKyAyICogRzM7XG4gICAgdmFyIHoyID0gejAgLSBrMiArIDIgKiBHMztcblxuICAgIHZhciB4MyA9IHgwIC0gMSArIDMgKiBHMzsgLy8gT2Zmc2V0cyBmb3IgZm91cnRoIGNvcm5lclxuICAgIHZhciB5MyA9IHkwIC0gMSArIDMgKiBHMztcbiAgICB2YXIgejMgPSB6MCAtIDEgKyAzICogRzM7XG5cbiAgICAvLyBXb3JrIG91dCB0aGUgaGFzaGVkIGdyYWRpZW50IGluZGljZXMgb2YgdGhlIGZvdXIgc2ltcGxleCBjb3JuZXJzXG4gICAgaSAmPSAyNTU7XG4gICAgaiAmPSAyNTU7XG4gICAgayAmPSAyNTU7XG5cbiAgICB2YXIgcGVybSA9IHRoaXMucGVybTtcbiAgICB2YXIgZ3JhZFAgPSB0aGlzLmdyYWRQO1xuICAgIHZhciBnaTAgPSBncmFkUFtpKyAgIHBlcm1baisgICBwZXJtW2sgICBdXV07XG4gICAgdmFyIGdpMSA9IGdyYWRQW2kraTErcGVybVtqK2oxK3Blcm1baytrMV1dXTtcbiAgICB2YXIgZ2kyID0gZ3JhZFBbaStpMitwZXJtW2orajIrcGVybVtrK2syXV1dO1xuICAgIHZhciBnaTMgPSBncmFkUFtpKyAxK3Blcm1baisgMStwZXJtW2srIDFdXV07XG5cbiAgICAvLyBDYWxjdWxhdGUgdGhlIGNvbnRyaWJ1dGlvbiBmcm9tIHRoZSBmb3VyIGNvcm5lcnNcbiAgICB2YXIgdDAgPSAwLjUgLSB4MCp4MC15MCp5MC16MCp6MDtcbiAgICBpZih0MDwwKSB7XG4gICAgICBuMCA9IDA7XG4gICAgfSBlbHNlIHtcbiAgICAgIHQwICo9IHQwO1xuICAgICAgbjAgPSB0MCAqIHQwICogZ2kwLmRvdDMoeDAsIHkwLCB6MCk7ICAvLyAoeCx5KSBvZiBncmFkMyB1c2VkIGZvciAyRCBncmFkaWVudFxuICAgIH1cbiAgICB2YXIgdDEgPSAwLjUgLSB4MSp4MS15MSp5MS16MSp6MTtcbiAgICBpZih0MTwwKSB7XG4gICAgICBuMSA9IDA7XG4gICAgfSBlbHNlIHtcbiAgICAgIHQxICo9IHQxO1xuICAgICAgbjEgPSB0MSAqIHQxICogZ2kxLmRvdDMoeDEsIHkxLCB6MSk7XG4gICAgfVxuICAgIHZhciB0MiA9IDAuNSAtIHgyKngyLXkyKnkyLXoyKnoyO1xuICAgIGlmKHQyPDApIHtcbiAgICAgIG4yID0gMDtcbiAgICB9IGVsc2Uge1xuICAgICAgdDIgKj0gdDI7XG4gICAgICBuMiA9IHQyICogdDIgKiBnaTIuZG90Myh4MiwgeTIsIHoyKTtcbiAgICB9XG4gICAgdmFyIHQzID0gMC41IC0geDMqeDMteTMqeTMtejMqejM7XG4gICAgaWYodDM8MCkge1xuICAgICAgbjMgPSAwO1xuICAgIH0gZWxzZSB7XG4gICAgICB0MyAqPSB0MztcbiAgICAgIG4zID0gdDMgKiB0MyAqIGdpMy5kb3QzKHgzLCB5MywgejMpO1xuICAgIH1cbiAgICAvLyBBZGQgY29udHJpYnV0aW9ucyBmcm9tIGVhY2ggY29ybmVyIHRvIGdldCB0aGUgZmluYWwgbm9pc2UgdmFsdWUuXG4gICAgLy8gVGhlIHJlc3VsdCBpcyBzY2FsZWQgdG8gcmV0dXJuIHZhbHVlcyBpbiB0aGUgaW50ZXJ2YWwgWy0xLDFdLlxuICAgIHJldHVybiAzMiAqIChuMCArIG4xICsgbjIgKyBuMyk7XG5cbiAgfTtcblxuICAvLyAjIyMjIyBQZXJsaW4gbm9pc2Ugc3R1ZmZcblxuICBmdW5jdGlvbiBmYWRlKHQpIHtcbiAgICByZXR1cm4gdCp0KnQqKHQqKHQqNi0xNSkrMTApO1xuICB9XG5cbiAgZnVuY3Rpb24gbGVycChhLCBiLCB0KSB7XG4gICAgcmV0dXJuICgxLXQpKmEgKyB0KmI7XG4gIH1cblxuICAvLyAyRCBQZXJsaW4gTm9pc2VcbiAgTm9pc2UucHJvdG90eXBlLnBlcmxpbjIgPSBmdW5jdGlvbih4LCB5KSB7XG4gICAgLy8gRmluZCB1bml0IGdyaWQgY2VsbCBjb250YWluaW5nIHBvaW50XG4gICAgdmFyIFggPSBNYXRoLmZsb29yKHgpLCBZID0gTWF0aC5mbG9vcih5KTtcbiAgICAvLyBHZXQgcmVsYXRpdmUgeHkgY29vcmRpbmF0ZXMgb2YgcG9pbnQgd2l0aGluIHRoYXQgY2VsbFxuICAgIHggPSB4IC0gWDsgeSA9IHkgLSBZO1xuICAgIC8vIFdyYXAgdGhlIGludGVnZXIgY2VsbHMgYXQgMjU1IChzbWFsbGVyIGludGVnZXIgcGVyaW9kIGNhbiBiZSBpbnRyb2R1Y2VkIGhlcmUpXG4gICAgWCA9IFggJiAyNTU7IFkgPSBZICYgMjU1O1xuXG4gICAgLy8gQ2FsY3VsYXRlIG5vaXNlIGNvbnRyaWJ1dGlvbnMgZnJvbSBlYWNoIG9mIHRoZSBmb3VyIGNvcm5lcnNcbiAgICB2YXIgcGVybSA9IHRoaXMucGVybTtcbiAgICB2YXIgZ3JhZFAgPSB0aGlzLmdyYWRQO1xuICAgIHZhciBuMDAgPSBncmFkUFtYK3Blcm1bWV1dLmRvdDIoeCwgeSk7XG4gICAgdmFyIG4wMSA9IGdyYWRQW1grcGVybVtZKzFdXS5kb3QyKHgsIHktMSk7XG4gICAgdmFyIG4xMCA9IGdyYWRQW1grMStwZXJtW1ldXS5kb3QyKHgtMSwgeSk7XG4gICAgdmFyIG4xMSA9IGdyYWRQW1grMStwZXJtW1krMV1dLmRvdDIoeC0xLCB5LTEpO1xuXG4gICAgLy8gQ29tcHV0ZSB0aGUgZmFkZSBjdXJ2ZSB2YWx1ZSBmb3IgeFxuICAgIHZhciB1ID0gZmFkZSh4KTtcblxuICAgIC8vIEludGVycG9sYXRlIHRoZSBmb3VyIHJlc3VsdHNcbiAgICByZXR1cm4gbGVycChcbiAgICAgICAgbGVycChuMDAsIG4xMCwgdSksXG4gICAgICAgIGxlcnAobjAxLCBuMTEsIHUpLFxuICAgICAgIGZhZGUoeSkpO1xuICB9O1xuXG4gIC8vIDNEIFBlcmxpbiBOb2lzZVxuICBOb2lzZS5wcm90b3R5cGUucGVybGluMyA9IGZ1bmN0aW9uKHgsIHksIHopIHtcbiAgICAvLyBGaW5kIHVuaXQgZ3JpZCBjZWxsIGNvbnRhaW5pbmcgcG9pbnRcbiAgICB2YXIgWCA9IE1hdGguZmxvb3IoeCksIFkgPSBNYXRoLmZsb29yKHkpLCBaID0gTWF0aC5mbG9vcih6KTtcbiAgICAvLyBHZXQgcmVsYXRpdmUgeHl6IGNvb3JkaW5hdGVzIG9mIHBvaW50IHdpdGhpbiB0aGF0IGNlbGxcbiAgICB4ID0geCAtIFg7IHkgPSB5IC0gWTsgeiA9IHogLSBaO1xuICAgIC8vIFdyYXAgdGhlIGludGVnZXIgY2VsbHMgYXQgMjU1IChzbWFsbGVyIGludGVnZXIgcGVyaW9kIGNhbiBiZSBpbnRyb2R1Y2VkIGhlcmUpXG4gICAgWCA9IFggJiAyNTU7IFkgPSBZICYgMjU1OyBaID0gWiAmIDI1NTtcblxuICAgIC8vIENhbGN1bGF0ZSBub2lzZSBjb250cmlidXRpb25zIGZyb20gZWFjaCBvZiB0aGUgZWlnaHQgY29ybmVyc1xuICAgIHZhciBwZXJtID0gdGhpcy5wZXJtO1xuICAgIHZhciBncmFkUCA9IHRoaXMuZ3JhZFA7XG4gICAgdmFyIG4wMDAgPSBncmFkUFtYKyAgcGVybVtZKyAgcGVybVtaICBdXV0uZG90Myh4LCAgIHksICAgICB6KTtcbiAgICB2YXIgbjAwMSA9IGdyYWRQW1grICBwZXJtW1krICBwZXJtW1orMV1dXS5kb3QzKHgsICAgeSwgICB6LTEpO1xuICAgIHZhciBuMDEwID0gZ3JhZFBbWCsgIHBlcm1bWSsxK3Blcm1bWiAgXV1dLmRvdDMoeCwgICB5LTEsICAgeik7XG4gICAgdmFyIG4wMTEgPSBncmFkUFtYKyAgcGVybVtZKzErcGVybVtaKzFdXV0uZG90Myh4LCAgIHktMSwgei0xKTtcbiAgICB2YXIgbjEwMCA9IGdyYWRQW1grMStwZXJtW1krICBwZXJtW1ogIF1dXS5kb3QzKHgtMSwgICB5LCAgIHopO1xuICAgIHZhciBuMTAxID0gZ3JhZFBbWCsxK3Blcm1bWSsgIHBlcm1bWisxXV1dLmRvdDMoeC0xLCAgIHksIHotMSk7XG4gICAgdmFyIG4xMTAgPSBncmFkUFtYKzErcGVybVtZKzErcGVybVtaICBdXV0uZG90Myh4LTEsIHktMSwgICB6KTtcbiAgICB2YXIgbjExMSA9IGdyYWRQW1grMStwZXJtW1krMStwZXJtW1orMV1dXS5kb3QzKHgtMSwgeS0xLCB6LTEpO1xuXG4gICAgLy8gQ29tcHV0ZSB0aGUgZmFkZSBjdXJ2ZSB2YWx1ZSBmb3IgeCwgeSwgelxuICAgIHZhciB1ID0gZmFkZSh4KTtcbiAgICB2YXIgdiA9IGZhZGUoeSk7XG4gICAgdmFyIHcgPSBmYWRlKHopO1xuXG4gICAgLy8gSW50ZXJwb2xhdGVcbiAgICByZXR1cm4gbGVycChcbiAgICAgICAgbGVycChcbiAgICAgICAgICBsZXJwKG4wMDAsIG4xMDAsIHUpLFxuICAgICAgICAgIGxlcnAobjAwMSwgbjEwMSwgdSksIHcpLFxuICAgICAgICBsZXJwKFxuICAgICAgICAgIGxlcnAobjAxMCwgbjExMCwgdSksXG4gICAgICAgICAgbGVycChuMDExLCBuMTExLCB1KSwgdyksXG4gICAgICAgdik7XG4gIH07XG5cbiAgZ2xvYmFsLk5vaXNlID0gTm9pc2U7XG5cbn0pKHR5cGVvZiBtb2R1bGUgPT09IFwidW5kZWZpbmVkXCIgPyB0aGlzIDogbW9kdWxlLmV4cG9ydHMpO1xuIiwiUmVuZGVyZXIgPSByZXF1aXJlICcuLi9nZngvUmVuZGVyZXIuY29mZmVlJ1xuXG5mayA9IHJlcXVpcmUgJ2ZpZWxka2l0J1xuVmVjMiA9IGZrLm1hdGguVmVjMlxuXG5ub2lzZWpzID0gcmVxdWlyZSAnbm9pc2VqcydcblxuR2FtZU9mTGlmZSA9IHJlcXVpcmUgJy4uL2dhbWUvR2FtZU9mTGlmZS5jb2ZmZWUnXG5cbmNsYXNzIEFwcFxuXHRjb25zdHJ1Y3RvcjogKCBAY29udGFpbmVyICkgLT5cblx0XHRAcmVuZGVyZXIgPSBuZXcgUmVuZGVyZXIgQGNvbnRhaW5lclxuXHRcdEB0aW1lID0gMFxuXHRcdEBvVGltZSA9IERhdGUubm93KClcblxuXHRcdHdpbmRvdy5hZGRFdmVudExpc3RlbmVyICdyZXNpemUnLCAoIGUgKSA9PlxuXHRcdFx0QHJlbmRlcmVyLnJlc2l6ZSgpXG5cdFx0LCBmYWxzZVxuXG5cdFx0QG5vaXNlID0gbmV3IG5vaXNlanMuTm9pc2UgTWF0aC5yYW5kb20oKVxuXG5cdFx0QHNldHRpbmdzID0gXG5cdFx0XHRyZXM6IEByZW5kZXJlci5ncmlkLnJlc1xuXHRcdFx0c3BlZWQ6IDFcblx0XHRcdG5vaXNlOiBcIkdhbWUgT2YgTGlmZVwiXG5cblx0XHRjaGFuZ2VSZXMgPSA9PlxuXHRcdFx0Z3JpZCA9IEByZW5kZXJlci5ncmlkXG5cdFx0XHRncmlkLnJlcyA9IEBzZXR0aW5ncy5yZXNcblxuXHRcdFx0Z3JpZC5zZXRTaXplIGdyaWQuc2l6ZS54LCBncmlkLnNpemUueVxuXHRcdFx0Z3JpZC5jcmVhdGVDZWxscygpXG5cdFx0XHRncmlkLmRyYXdCdWZmZXIgXCIjOTk5XCIsIDEuNVxuXG5cdFx0XHRAZ2FtZSA9IG5ldyBHYW1lT2ZMaWZlIEByZW5kZXJlci5ncmlkXG5cblx0XHRAZ3VpID0gbmV3IGRhdC5HVUkoKVxuXHRcdEBndWkuYWRkKCBAc2V0dGluZ3MsIFwicmVzXCIsIDgsIDEyOCApLnN0ZXAoMSkub25DaGFuZ2UoIGNoYW5nZVJlcyApXG5cdFx0QGd1aS5hZGQoIEBzZXR0aW5ncywgXCJzcGVlZFwiLCAwLjEsIDEgKVxuXHRcdEBndWkuYWRkKCBAc2V0dGluZ3MsIFwibm9pc2VcIiwgWyBcInNpbXBsZXhcIiwgXCJwZXJsaW5cIiwgXCJHYW1lIE9mIExpZmVcIiBdIClcblxuXHRcdEBnYW1lID0gbmV3IEdhbWVPZkxpZmUgQHJlbmRlcmVyLmdyaWRcblxuXHR1cGRhdGU6ICgpIC0+XG5cdFx0dCA9IERhdGUubm93KClcblx0XHR0aW1lID0gKCB0IC0gQG9UaW1lICkgKiAuMDAxXG5cdFx0ZHQgPSB0aW1lIC0gQHRpbWVcblx0XHRAdGltZSA9IHRpbWVcblxuXHRcdHJlcyA9IEByZW5kZXJlci5ncmlkLnJlc1xuXG5cdFx0aWYgQHNldHRpbmdzLm5vaXNlID09IFwiR2FtZSBPZiBMaWZlXCJcblx0XHRcdEBnYW1lLnVwZGF0ZSgpXG5cdFx0XHRyZXR1cm5cblxuXHRcdGZvciB4IGluIFswLi4ucmVzXVxuXHRcdFx0Zm9yIHkgaW4gWzAuLi5yZXNdXG5cdFx0XHRcdGlmIEBzZXR0aW5ncy5ub2lzZSA9PSBcInNpbXBsZXhcIlxuXHRcdFx0XHRcdHZhbHVlID0gQG5vaXNlLnNpbXBsZXgzIHgsIHksIHQgKiAuMDAxICogQHNldHRpbmdzLnNwZWVkXG5cdFx0XHRcdGVsc2Vcblx0XHRcdFx0XHR2YWx1ZSA9IEBub2lzZS5wZXJsaW4zIHgsIHksIHQgKiAuMDAxICogQHNldHRpbmdzLnNwZWVkXG5cdFx0XHRcdEByZW5kZXJlci5ncmlkLm1hcmtDZWxsIHgsIHksIHZhbHVlID4gMFxuXHRcblx0cmVuZGVyOiAoKSAtPlxuXHRcdEByZW5kZXJlci5yZW5kZXIoKVxuXG5tb2R1bGUuZXhwb3J0cyA9IEFwcDsiLCJmayA9IHJlcXVpcmUgJ2ZpZWxka2l0J1xuVmVjMiA9IGZrLm1hdGguVmVjMlxuXG5SZWN0MiA9IHJlcXVpcmUgJy4vUmVjdDIuY29mZmVlJ1xuXG4jIDEwMHgxMDAgR3JpZCBjbGFzcyBmb3IgY2hhcmFjdGVyIGdlbmVyYXRvclxuY2xhc3MgR3JpZFxuXHRjb25zdHJ1Y3RvcjogKCBAcmVzLCBAc2l6ZSwgY3JlYXRlQnVmZmVyPWZhbHNlICkgLT5cblx0XHRAY3JlYXRlQ2VsbHMoKVxuXHRcdEBzdGVwID0gbmV3IFZlYzIgQHNpemUueCAvIEByZXMsIEBzaXplLnkgLyBAcmVzXG5cdFx0QGJ1ZmZlciA9IG51bGxcblxuXHRcdGlmIGNyZWF0ZUJ1ZmZlclxuXHRcdFx0QGJ1ZmZlciA9IGVsICdjYW52YXMnXG5cdFx0XHRAYmN0eCA9IEBidWZmZXIuZ2V0Q29udGV4dCAnMmQnXG5cdFx0XHRAYnVmZmVyLndpZHRoID0gQHNpemUueFxuXHRcdFx0QGJ1ZmZlci5oZWlnaHQgPSBAc2l6ZS55XG5cblx0c2V0U2l6ZTogKCB3LCBoICkgLT5cblx0XHRAc2l6ZS5zZXQyIHcsIGhcblx0XHRAc3RlcCA9IG5ldyBWZWMyIEBzaXplLnggLyBAcmVzLCBAc2l6ZS55IC8gQHJlc1xuXHRcdGlmIEBidWZmZXIgIT0gbnVsbFxuXHRcdFx0QGJ1ZmZlci53aWR0aCA9IEBzaXplLnhcblx0XHRcdEBidWZmZXIuaGVpZ2h0ID0gQHNpemUueVxuXG5cdGNyZWF0ZUNlbGxzOiAoKSAtPlxuXHRcdGlmIEBjZWxsc1xuXHRcdFx0QGNlbGxzID0gbnVsbFxuXG5cdFx0QGNlbGxzID0gZm9yIHggaW4gWzEuLkByZXNdXG5cdFx0XHRmb3IgeSBpbiBbMS4uQHJlc11cblx0XHRcdFx0ZmFsc2VcblxuXHRkcmF3QnVmZmVyOiAoIGNvbD1cIiM2NjZcIiwgbGluZVdpZHRoPS41ICkgLT5cblx0XHRpZiBAYnVmZmVyID09IG51bGxcblx0XHRcdHJldHVyblxuXG5cdFx0QGJjdHguY2xlYXJSZWN0IDAsIDAsIEBidWZmZXIud2lkdGgsIEBidWZmZXIuaGVpZ2h0XG5cdFx0QGJjdHguc3Ryb2tlU3R5bGUgPSBjb2xcblx0XHRAYmN0eC5saW5lV2lkdGggPSBsaW5lV2lkdGhcblx0XHRAYmN0eC5iZWdpblBhdGgoKVxuXHRcdEBkcmF3UGF0aHMgQGJjdHhcblx0XHRAYmN0eC5zdHJva2UoKVxuXG5cdGRyYXdQYXRoczogKCBjdHgsIG9mZnNldD17IHg6IDAsIHk6IDAgfSwgc2NhbGU9MSApIC0+XG5cdFx0eDEgPSBvZmZzZXQueFxuXHRcdHgyID0gb2Zmc2V0LnggKyBAc2l6ZS54ICogc2NhbGVcblx0XHR5MSA9IG9mZnNldC55XG5cdFx0eTIgPSBvZmZzZXQueSArIEBzaXplLnkgKiBzY2FsZVxuXG5cdFx0Zm9yIGkgaW4gWzAuLkByZXNdXG5cdFx0XHRjdHgubW92ZVRvIHgxLCB5MSArIEBzdGVwLnkgKiBpICogc2NhbGVcblx0XHRcdGN0eC5saW5lVG8geDIsIHkxICsgQHN0ZXAueSAqIGkgKiBzY2FsZVxuXHRcdFx0Y3R4Lm1vdmVUbyB4MSArIEBzdGVwLnggKiBpICogc2NhbGUsIHkxXG5cdFx0XHRjdHgubGluZVRvIHgxICsgQHN0ZXAueCAqIGkgKiBzY2FsZSwgeTJcblxuXHQjIHJlY2l2ZXMgYSBjb29yZCBpbiAxMDB4MTAwIFstNTAuLjUwXSBhbmQgcmV0dXJucyBpbnRlcnNlY3Rpb24gY2VsbCByZWN0IGlmIGFueSwgb3IgbnVsbFxuXHRnZXRJbnRlcnNlY3Rpb25DZWxsOiAoIHg9MCwgeT0wLCBtYXJrPWZhbHNlICkgLT5cblx0XG5cdFx0aWYgeCA8IDAgb3IgeCA+IEBzaXplLnhcblx0XHRcdHJldHVybiBudWxsXG5cdFx0aWYgeSA8IDAgb3IgeSA+IEBzaXplLnlcblx0XHRcdHJldHVybiBudWxsXG5cblx0XHRpID0gTWF0aC5mbG9vciAoIHggLyBAc2l6ZS54ICkgKiBAcmVzXG5cdFx0aiA9IE1hdGguZmxvb3IgKCB5IC8gQHNpemUueSApICogQHJlc1xuXG5cdFx0aWYgbWFya1xuXHRcdFx0QGNlbGxzW2pdW2ldID0gdHJ1ZVxuXG5cdFx0cmV0dXJuIEBnZXRDZWxsUmVjdCBqLCBpXG5cblx0Z2V0Q2VsbFJlY3Q6ICggaT0wLCBqPTAgKSAtPlxuXHRcdG5ldyBSZWN0MiBAc3RlcC54ICogaiwgQHN0ZXAueSAqIGksIEBzdGVwLngsIEBzdGVwLnlcblxuXHRnZXROZXdEZW5zaXR5R3JpZDogKCBmYWN0b3I9MiApIC0+XG5cdFx0cmV0dXJuIG5ldyBHcmlkIE1hdGguZmxvb3IoIEByZXMgKiBmYWN0b3IgKSwgQHNpemUuY2xvbmUoKVxuXG5cdGNsb25lOiAoKSAtPlxuXHRcdGcgPSBAZ2V0TmV3RGVuc2l0eUdyaWQgMVxuXG5cdFx0Zm9yIHggaW4gWzAuLi5AcmVzXVxuXHRcdFx0Zm9yIHkgaW4gWzAuLi5AcmVzXVxuXHRcdFx0XHRnLmNlbGxzW3hdW3ldID0gQGNlbGxzW3hdW3ldXG5cblx0XHRyZXR1cm4gZ1xuXG5cdGNsZWFyQ2VsbHM6ICgpIC0+XG5cdFx0Zm9yIHggaW4gWzAuLi5AcmVzXVxuXHRcdFx0Zm9yIHkgaW4gWzAuLi5AcmVzXVxuXHRcdFx0XHRAY2VsbHNbeF1beV0gPSBmYWxzZVxuXG5cdG1hcmtDZWxsIDogKCBpLCBqLCB2YWw9dHJ1ZSApIC0+XG5cdFx0QGNlbGxzW2ldW2pdID0gdmFsXG5cblx0bWFya1JhbmRvbUNlbGxzOiAoIHNlZWQ9RGF0ZS5ub3coKSwgdGhyZXNob2xkPS41ICkgLT5cblx0XHRSYW5kb20gPSBuZXcgZmsubWF0aC5SYW5kb20gc2VlZFxuXHRcdGZvciB4IGluIFswLi4uQHJlc11cblx0XHRcdGZvciB5IGluIFswLi4uQHJlc11cblx0XHRcdFx0QGNlbGxzW3hdW3ldID0gUmFuZG9tLnJhbmRvbSgpIDwgdGhyZXNob2xkXG5cblx0ZHJhd01hcmtlZENlbGxQYXRoczogKCBjdHggKSAtPlxuXHRcdGZvciBpIGluIFswLi4uQHJlc11cblx0XHRcdGZvciBqIGluIFswLi4uQHJlc11cblx0XHRcdFx0aWYgQGNlbGxzW2ldW2pdXG5cdFx0XHRcdFx0ciA9IEBnZXRDZWxsUmVjdCBqLCBpXG5cdFx0XHRcdFx0Y3R4LnJlY3Qgci54LCByLnksIHIud2lkdGgsIHIuaGVpZ2h0XG5cblx0Z2V0TmVpZ2hib3VyczogKCBpLCBqICkgLT5cblx0XHRuID0gWyBudWxsLCBudWxsLCBudWxsLCBudWxsLCBudWxsLCBudWxsLCBudWxsLCBudWxsIF1cblx0XHRcblx0XHRpZiBpID4gMFxuXHRcdFx0blswXSA9IEBjZWxsc1tpLTFdW2pdXG5cdFx0XHRpZiBqIDwgQHJlcy0xXG5cdFx0XHRcdG5bMV0gPSBAY2VsbHNbaS0xXVtqKzFdXG5cblx0XHRpZiBqIDwgQHJlcy0xXG5cdFx0XHRuWzJdID0gQGNlbGxzW2ldW2orMV1cblx0XHRcdGlmIGkgPCBAcmVzLTFcblx0XHRcdFx0blszXSA9IEBjZWxsc1tpKzFdW2orMV1cblxuXHRcdGlmIGkgPCBAcmVzLTFcblx0XHRcdG5bNF0gPSBAY2VsbHNbaSsxXVtqXSBcblx0XHRcdGlmIGogPiAwXG5cdFx0XHRcdG5bNV0gPSBAY2VsbHNbaSsxXVtqLTFdIFxuXG5cdFx0aWYgaiA+IDBcblx0XHRcdG5bNl0gPSBAY2VsbHNbaV1bai0xXSBcblx0XHRcdGlmIGkgPiAwXG5cdFx0XHRcdG5bN10gPSBAY2VsbHNbaS0xXVtqLTFdXG5cblx0XHRyZXR1cm4gblxuXG5cdGdldERpZmZlcmVudENlbGxDb3VudDogKCBnICkgLT5cblx0XHRpZiBnLnJlcyAhPSBAcmVzXG5cdFx0XHRyZXR1cm4gLTFcblxuXHRcdGQgPSAwXG5cdFx0Zm9yIGkgaW4gWzAuLi5AcmVzXVxuXHRcdFx0Zm9yIGogaW4gWzAuLi5AcmVzXVxuXHRcdFx0XHRpZiBAY2VsbHNbaV1bal0gIT0gZy5jZWxsc1tpXVtqXVxuXHRcdFx0XHRcdGQrK1xuXG5cdFx0cmV0dXJuIGRcblxubW9kdWxlLmV4cG9ydHMgPSBHcmlkO1xuXG4iLCIjIHJlY3QgY2xhc3Mgd2l0aCB4LHksdyxoIHNwZWNcblxuY2xhc3MgUmVjdDJcblx0Y29uc3RydWN0b3I6ICggQHg9MCwgQHk9MCwgQHdpZHRoPTAsIEBoZWlnaHQ9MCApIC0+XG5cblx0aGl0VGVzdDogKCByICkgLT5cblx0XHRyZXR1cm4gKCByLnggKyByLndpZHRoID49IEB4KSBhbmQgKHIueCA8PSBAeCArIEB3aWR0aCkgYW5kICggci55ICsgci5oZWlnaHQgPj0gQHkpIGFuZCAoci55IDw9IEB5ICsgQGhlaWdodClcblxubW9kdWxlLmV4cG9ydHMgPSBSZWN0MjsiLCIjIENvbndheSdzIGdhbWUgb2YgTGlmZVxuIyByZXF1aXJlcyBHcmlkIGNsYXNzXG5cbmNsYXNzIEdhbWVPZkxpZmVcblx0Y29uc3RydWN0b3I6ICggQGdyaWQgKSAtPlxuXHRcdEB1cGRhdGVTdGVwID0gOFxuXHRcdEBzdGVwID0gMFxuXHRcdEBidWZmZXIgPSBbIDAsIDEsIDIsIDAgXVxuXHRcdEBzdGFydFRpbWUgPSBEYXRlLm5vdygpICogLjAwMVxuXHR1cGRhdGU6ICgpIC0+XG5cdFx0QHN0ZXAgPSAoIEBzdGVwICsgMSApICUgKCBAdXBkYXRlU3RlcCApXG5cblx0XHRpZiBAc3RlcCA+IDBcblx0XHRcdHJldHVyblxuXG5cdFx0IyBjcmVhdGUgYSBjb3B5XG5cdFx0ZyA9IEBncmlkLmNsb25lKClcblxuXHRcdGZvciBpIGluIFswLi4uZy5yZXNdXG5cdFx0XHRmb3IgaiBpbiBbMC4uLmcucmVzXVxuXHRcdFx0XHRuID0gZy5nZXROZWlnaGJvdXJzIGksIGpcblx0XHRcdFx0YWxpdmUgPSAwXG5cdFx0XHRcdGZvciBrIGluIFswLi43XVxuXHRcdFx0XHRcdGlmIG5ba10gPT0gdHJ1ZVxuXHRcdFx0XHRcdFx0YWxpdmUrK1xuXHRcdFx0XHRcblx0XHRcdFx0aWYgZy5jZWxsc1tpXVtqXVxuXHRcdFx0XHRcdCMgaXMgY3VycmVudGx5IGFsaXZlXG5cdFx0XHRcdFx0aWYgYWxpdmUgPCAyIG9yIGFsaXZlID4gM1xuXHRcdFx0XHRcdFx0IyBtdXN0IGRpZVxuXHRcdFx0XHRcdFx0QGdyaWQuY2VsbHNbaV1bal0gPSBmYWxzZVxuXG5cdFx0XHRcdGVsc2Vcblx0XHRcdFx0XHQjIGlzIGN1cnJlbnRseSBkZWFkXG5cdFx0XHRcdFx0aWYgYWxpdmUgPT0gM1xuXHRcdFx0XHRcdFx0IyBuZXcgY2VsbCBpcyBib3JuXG5cdFx0XHRcdFx0XHRAZ3JpZC5jZWxsc1tpXVtqXSA9IHRydWVcblxuXHRcdGQgPSBAZ3JpZC5nZXREaWZmZXJlbnRDZWxsQ291bnQoIGcgKVxuXHRcdEBidWZmZXJbMF0gPSBAYnVmZmVyWzFdXG5cdFx0QGJ1ZmZlclsxXSA9IEBidWZmZXJbMl1cblx0XHRAYnVmZmVyWzJdID0gZFxuXG5cdFx0aWYgQGJ1ZmZlclswXSA9PSBAYnVmZmVyWzFdIG9yIEBidWZmZXJbMF0gPT0gQGJ1ZmZlclsyXVxuXHRcdFx0QGJ1ZmZlclszXSsrXG5cdFx0ZWxzZVxuXHRcdFx0QGJ1ZmZlclszXSA9IDBcblxuXHRcdHQgPSBEYXRlLm5vdygpICogLjAwMVxuXHRcdFxuXHRcdGlmIGQgPCAyIG9yIEBidWZmZXJbM10gPiA1IG9yIHQgLSBAc3RhcnRUaW1lID4gMTZcblx0XHRcdEByZXN0YXJ0KClcblxuXHRyZXN0YXJ0OiAoKSAtPlxuXHRcdEBzdGFydFRpbWUgPSBEYXRlLm5vdygpICogLjAwMVxuXHRcdEBncmlkLm1hcmtSYW5kb21DZWxscygpXG5cblx0cmVuZGVyOiAoIGN0eCwgZGVidWc9ZmFsc2UgKSAtPlxuXHRcdGlmICFkZWJ1Z1xuXHRcdFx0cmV0dXJuXG5cblx0XHRjdHguZmlsbFN0eWxlID0gXCJyZ2JhKCAyNTUsIDI1NSwgMjU1LCAuMiApXCJcblx0XHRjdHguYmVnaW5QYXRoKClcblx0XHRAZ3JpZC5kcmF3TWFya2VkQ2VsbFBhdGhzIGN0eFxuXHRcdGN0eC5maWxsKClcblxubW9kdWxlLmV4cG9ydHMgPSBHYW1lT2ZMaWZlOyIsIkdyaWQgPSByZXF1aXJlICcuLi9jb3JlL0dyaWQuY29mZmVlJ1xuXG5mayA9IHJlcXVpcmUgJ2ZpZWxka2l0J1xuVmVjMiA9IGZrLm1hdGguVmVjMlxuXG5jbGFzcyBSZW5kZXJlclxuXHRjb25zdHJ1Y3RvcjogKCBjb250YWluZXIgKSAtPlxuXHRcdEB3aWR0aCA9IE1hdGgubWF4IHdpbmRvdy5pbm5lcldpZHRoLCA1MTJcblx0XHRcblx0XHRAY2FudmFzID0gZWwgJ2NhbnZhcydcblx0XHRAY2FudmFzLndpZHRoID0gQHdpZHRoXG5cdFx0QGNhbnZhcy5oZWlnaHQgPSBAd2lkdGhcblxuXHRcdEBjdHggPSBAY2FudmFzLmdldENvbnRleHQgJzJkJ1xuXG5cdFx0Y29udGFpbmVyLmFwcGVuZENoaWxkIEBjYW52YXNcblxuXHRcdEBncmlkID0gbmV3IEdyaWQgMTYsIG5ldyBWZWMyKCBAd2lkdGgsIEB3aWR0aCApLCB0cnVlXG5cblx0XHRAZ3JpZC5kcmF3QnVmZmVyIFwiIzk5OVwiLCAxLjVcblxuXHRyZW5kZXI6IC0+XG5cdFx0QGN0eC5jbGVhclJlY3QgMCwgMCwgQHdpZHRoLCBAd2lkdGhcblx0XHRcblx0XHQjIGRyYXcgYmlnIGdyaWRcblx0XHRAY3R4LmRyYXdJbWFnZSBAZ3JpZC5idWZmZXIsIDAsIDBcblxuXHRcdCMgZHJhdyBhY3RpdmUgY2VsbHNcblx0XHRAY3R4LmZpbGxTdHlsZSA9IFwiI0FBQVwiXG5cdFx0QGN0eC5iZWdpblBhdGgoKVxuXHRcdEBncmlkLmRyYXdNYXJrZWRDZWxsUGF0aHMgQGN0eFxuXHRcdEBjdHguZmlsbCgpXG5cblx0cmVzaXplOiAoKSAtPlxuXHRcdEB3aWR0aCA9IE1hdGgubWF4IHdpbmRvdy5pbm5lcldpZHRoLCA1MTJcblx0XHQjQGhlaWdodCA9IE1hdGgubWF4IHdpbmRvdy5pbm5lckhlaWdodCwgNTQwXG5cdFx0QGNhbnZhcy53aWR0aCA9IEB3aWR0aFxuXHRcdEBjYW52YXMuaGVpZ2h0ID0gQHdpZHRoXG5cdFx0QGdyaWQuc2V0U2l6ZSBAd2lkdGgsIEB3aWR0aFxuXHRcdEBncmlkLmRyYXdCdWZmZXIgXCIjOTk5XCIsIDEuNVxuXG5tb2R1bGUuZXhwb3J0cyA9IFJlbmRlcmVyOyIsIkFwcCA9IHJlcXVpcmUgJy4vYXBwcy9BcHAuY29mZmVlJ1xuI1RXRUVOID0gcmVxdWlyZSAndHdlZW4uanMnXG5cbiNuYXZpZ2F0b3IuZ2V0VXNlck1lZGlhICA9IG5hdmlnYXRvci5nZXRVc2VyTWVkaWEgfHwgbmF2aWdhdG9yLndlYmtpdEdldFVzZXJNZWRpYSB8fCBuYXZpZ2F0b3IubW96R2V0VXNlck1lZGlhIHx8IG5hdmlnYXRvci5tc0dldFVzZXJNZWRpYVxuXG5NYWluID0gbmV3IC0+XG5cdGluaXRpYWxpemVkID0gZmFsc2Vcblx0YXBwID0gbnVsbFxuXG5cdGluaXQgPSAoKSAtPlxuXHRcdGlmICFpbml0aWFsaXplZFxuXHRcdFx0aW5pdGlhbGl6ZWQgPSB0cnVlXG5cdFx0XHRcblx0XHRcdGFwcCA9IG5ldyBBcHAgJCggJ2NvbnRhaW5lcicgKVxuXHRcdFx0YW5pbWF0ZSgpXG5cdFxuXHRhbmltYXRlID0gKCkgLT5cblx0XHRyZXF1ZXN0QW5pbWF0aW9uRnJhbWUgYW5pbWF0ZVxuXHRcdCNUV0VFTi51cGRhdGUoKVxuXHRcdGFwcC51cGRhdGUoKVxuXHRcdGFwcC5yZW5kZXIoKVxuXHRcdFxuXG5cdCMgLS0tIEhFTFBFUlMgLS0tLS0tLS0tXG5cblx0aXNNb2JpbGUgPSAoKSAtPlxuXHRcdGlmKCBuYXZpZ2F0b3IudXNlckFnZW50Lm1hdGNoKC9BbmRyb2lkL2kpIG9yIG5hdmlnYXRvci51c2VyQWdlbnQubWF0Y2goL3dlYk9TL2kpIG9yIG5hdmlnYXRvci51c2VyQWdlbnQubWF0Y2goL2lQaG9uZS9pKSBvciBuYXZpZ2F0b3IudXNlckFnZW50Lm1hdGNoKC9pUGFkL2kpIG9yIG5hdmlnYXRvci51c2VyQWdlbnQubWF0Y2goL2lQb2QvaSkgb3IgbmF2aWdhdG9yLnVzZXJBZ2VudC5tYXRjaCgvQmxhY2tCZXJyeS9pKSBvciBuYXZpZ2F0b3IudXNlckFnZW50Lm1hdGNoKC9XaW5kb3dzIFBob25lL2kpIClcblx0XHRcdHJldHVybiB0cnVlXG5cblx0XHRlbHNlXG5cdFx0XHRyZXR1cm4gZmFsc2VcblxuXHRjc3MzRCA9ICgpIC0+XG5cdFx0ZSA9IGVsICdwJyBcblx0XHRoYXMzZCA9IGZhbHNlXG5cdFx0dHJhbnNmb3JtcyA9IHtcblx0XHRcdCd3ZWJraXRUcmFuc2Zvcm0nOictd2Via2l0LXRyYW5zZm9ybScsXG5cdFx0XHQnT1RyYW5zZm9ybSc6Jy1vLXRyYW5zZm9ybScsXG5cdFx0XHQnbXNUcmFuc2Zvcm0nOictbXMtdHJhbnNmb3JtJyxcblx0XHRcdCdNb3pUcmFuc2Zvcm0nOictbW96LXRyYW5zZm9ybScsXG5cdFx0XHQndHJhbnNmb3JtJzondHJhbnNmb3JtJ1xuXHRcdH1cblxuXHRcdCMgQWRkIGl0IHRvIHRoZSBib2R5IHRvIGdldCB0aGUgY29tcHV0ZWQgc3R5bGUuXG5cdFx0ZG9jdW1lbnQuYm9keS5pbnNlcnRCZWZvcmUgZSwgbnVsbFxuXG5cdFx0Zm9yIHQsdiBvZiB0cmFuc2Zvcm1zXG5cdFx0ICAgIGlmICggZS5zdHlsZVt0XSAhPSB1bmRlZmluZWQgKVxuXHRcdCAgICAgICAgZS5zdHlsZVt0XSA9IFwidHJhbnNsYXRlM2QoMXB4LDFweCwxcHgpXCJcblx0XHQgICAgICAgIGhhczNkID0gd2luZG93LmdldENvbXB1dGVkU3R5bGUoIGUgKS5nZXRQcm9wZXJ0eVZhbHVlKCB0cmFuc2Zvcm1zW3RdIClcblxuXHRcdGRvY3VtZW50LmJvZHkucmVtb3ZlQ2hpbGQgZVxuXG5cdFx0cmV0dXJuICggaGFzM2QgIT0gdW5kZWZpbmVkICYmIGhhczNkLmxlbmd0aCA+IDAgJiYgaGFzM2QgIT0gXCJub25lXCIgKVxuXG5cdHNldFRyYW5zZm9ybSA9ICggZSwgdCwgcCA9IFsgJ3dlYmtpdCcsICdNb3onLCAnbXMnIF0gKSAtPlxuXHRcdGZvciBrLHYgb2YgcFxuXHRcdFx0ZS5zdHlsZVsgcFtrXSArICdUcmFuc2Zvcm0nIF0gPSB0XG5cdFx0ZS5zdHlsZS50cmFuc2Zvcm0gPSB0XG5cblx0cmV0dXJuIHtcblx0XHRpbml0OiBpbml0XG5cdFx0Z2V0Q29udGVudHM6ICgpIC0+XG5cdFx0XHRyZXR1cm4gY29udGVudHNcblx0XHRpc01vYmlsZTogaXNNb2JpbGVcblx0XHRjc3MzRDogY3NzM0Rcblx0XHRzZXRUcmFuc2Zvcm06IHNldFRyYW5zZm9ybVxuXHR9XG5cbiMgZ2xvYmFsc1xuZ2xvYmFscyA9XG5cblx0JCA6ICggaWQgKSAtPlxuXHRcdHJldHVybiBkb2N1bWVudC5nZXRFbGVtZW50QnlJZCBpZFxuXG5cdGVsIDogKCB0eXBlLCBjbHMgKSAtPlxuXHRcdGUgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50IHR5cGVcblx0XHRpZiAoIGNscyApIFxuXHRcdFx0ZS5jbGFzc05hbWUgPSBjbHM7XG5cdFx0cmV0dXJuIGVcblxuXHRNYWluOiBNYWluXG5cbmZvciBrLHYgb2YgZ2xvYmFsc1xuXHR3aW5kb3dba10gPSB2XG5cbiNsYXVuY2hcbmlmIHdpbmRvdy5hZGRFdmVudExpc3RlbmVyXG5cdHdpbmRvdy5hZGRFdmVudExpc3RlbmVyICdsb2FkJywgKCBlICkgLT5cblx0XHRNYWluLmluaXQoKVxuXHQsIGZhbHNlXG5lbHNlIGlmIHdpbmRvdy5hdHRhY2hFdmVudFxuXHR3aW5kb3cuYXR0YWNoRXZlbnQgJ2xvYWQnLCAoIGUgKSAtPlxuXHRcdE1haW4uaW5pdCgpXG5cdCwgZmFsc2UiXX0=
