# Resonate Grid
Grid + Noise

### Requeriments

* node + NPM

### Instructions of use

1) Install

`make install`

2) To compile type:

`make`

3) To launch server type:

`make server`

4) Run by openning [http://localhost:8989](http://localhost:8989) in your browser.