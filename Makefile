# TOOLS
BIN = node_modules/.bin
BROWSERIFY = ${BIN}/browserify
#SASS = ${BIN}/node-sass
COFFEE = ${BIN}/coffee
SERVER = ${BIN}/http-server
COMPILER = utils/compiler/compile.py

# PATHS
JS=public/assets/js

# CONFIG
SERVER_PORT = 8989
FLAGS="--debug"

MAIN_IN = src/main.coffee
MAIN_OUT = ${JS}/main.js
MAIN_COMPRESSED = deploy/js/main.js

default: build

clean:
	rm ${MAIN_OUT}
	rm -R deploy

install:
	npm install

build:
	${BROWSERIFY} -t coffeeify ${MAIN_IN} ${FLAGS} > ${MAIN_OUT}
	
server:
	${SERVER} -p ${SERVER_PORT} public
